import React, {useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text

} from 'react-native';
import {
  SafeAreaView,
  Header,
  PostListItem,
  Button
} from '@components';
import _ from "lodash";
import { Images, useTheme, } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import {  modelsGetRequest } from '../../api/models';
import {  subCategoriesGetRequest } from '../../api/subCategories';
import Icon from "react-native-vector-icons/Entypo";
import { useSelector, useDispatch } from 'react-redux';
export default function SubCategories({ navigation,route }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const [Orders] = useState([
    {
      image: Images.event4,
      name: 'MEN',
      description: 'kurta',
      price: '$399',
      route: 'Hotel',
    },
    {
      image: Images.event8,
      name: 'WOMEN',
      description: 'kurta',
      price: '$399',
      route: 'Tour',
    },
    {
      image: Images.event9,
      name: 'KIDS',
      description: 'kurta',
      price: '$399',
      route: 'OverViewCar',
    },
    {
      image: Images.event10,
      name: 'JACKETS',
      description: 'kurta',
      price: '$399',
      route: 'FlightSearch',
    },

  ]);

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const subCategoriesData = useSelector(state => state.subCategoriesReducer.subCategoriesData);
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  // const loginModelData = useSelector(state => state.modelsReducer.modelsData);
  const [loading, setLoading] = useState(false);
  const {categoriesObject}=route.params;
  useEffect(() => {
    dispatch(subCategoriesGetRequest(categoriesObject.id));
  }, [dispatch])

  console.log("subCategoriesData", subCategoriesData);



const orderView = async (orderObject) => {
  setLoading(true);
  // await dispatch(collectionsPostRequest(cName, profileImage,labelId));
  navigation.navigate('PreviewBooking', { orderObject })
  setLoading(false);
};
  const renderIconService = () => {
    return (
      <FlatList
        vertical
        data={subCategoriesData}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              style={styles.itemService}
              activeOpacity={0.9}
              // onPress={() => {
              //   navigation.navigate('Post');
              // }}
              >
              <View>
                <PostListItem
                  title={item.subCategoryName}
                  //description="New stylish jacket"
                  style={{ marginTop: 20, width: '100%' }}
                  //image={item?.products[0].url}
                 // image={item?.model_products ? `https://staging-backend.labl.store` + item?.model_products[0]?.productImages[0]?.url : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsQ-YHX2i3RvTDDmpfnde4qyb2P8up7Wi3Ww&usqp=CAU'}
                  // onPress={() => {
                  //   navigation.navigate('PreviewBooking');
                  // }}
                 // onPress={() => orderView(item)}
                />
              </View>
              <View style={{alignItems:'flex-end',bottom:110}}>
              <Icon name="chevron-thin-right" size={15}  />
              </View>

              {/* <View style={[styles.line, { backgroundColor: colors.border }]} /> */}
            </TouchableOpacity>
          )
        }}
      />
    );
  };



  return (
    <View style={{ flex: 1, top: 10 }}>
      <Header
        title={categoriesObject.name}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

        // renderRight={() => {
        //   return <Icon name="shopping-cart" size={24} color={colors.primary} />;
        // }}
        // onPressRight={() => {
        //   navigation.navigate('Cart');
        // }}
      />

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        <ScrollView
          onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
          scrollEventThrottle={8}>
          <View style={{ paddingHorizontal: 15 }}>

            {renderIconService()}
          </View>






        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

