import React, { useState, useRef, useCallback, useEffect } from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import _ from 'lodash';
import { BaseStyle, Images, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Image, Text, TextInput, ProfileAuthor1 } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import * as Utils from '@utils';
// import { GiftedChat } from 'react-native-gifted-chat'
import { useDispatch, useSelector } from 'react-redux';
// import { commentsGetRequest, commentsGetRequestForFeed, commentsPostRequest } from '../../api/comments';
import { BottomSheet, Button, ListItem } from 'react-native-elements';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import { commentDeleteRequest, commentGetRequestByModelUser, commentGetRequestByUser, commentPostRequest } from '../../api/comment';


export default function CommentsModel({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const windowHeight = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const dispatch = useDispatch();
  const { object } = route.params;
  console.log(object, 'CommentDetails');
  // useEffect(() => {
  //   dispatch(commentsGetRequestForFeed(feedId));

  // }, [dispatch])

  const [commentData, setCommentData] = useState({
    isVisible: false,
    id: ""
  });
  const [input, setInput] = useState("");
  const [Loading, setLoading] = useState();

  const UserData = useSelector(state => state.accessTokenReducer.userData)
  console.log('UserData', UserData);
  const userId = UserData?.user?.id;

  useEffect(() => {
    dispatch(commentGetRequestByModelUser(object?.id));
  }, [])

  const commentDataFromUser = useSelector(state => state.commentReducer.commentDataByUser)
  console.log("commentDataFromUser", commentDataFromUser);
  // const { feedDataObject } = route.params

  // console.log("feedDataObject", feedDataObject);

  // const feedComments = feedDataObject?.comments
  // const feedId = feedDataObject?.id

  const userToken = useSelector(state => state.accessTokenReducer.accessToken)

  // const [commentId, setCommentId] = useState("")
  // useEffect(() => {
  //   const commented = _.find(commentDataFromUser, function (o) { return o.commentFeed?.id === feedDataObject.id; });
  //   if (commented) {
  //     setCommentId(commented.id);
  //     setInput(true);
  //   }
  // }, []);

  // useEffect(() => {
  //   const commented = _.find(commentDataFromUser, function (o) { return o.commentFeed?.id === feedDataObject.id; });
  //   if (commented) {
  //     setCommentId(commented.id);
  //     setInput(true);
  //   }
  // }, [commentDataFromUser]);
  // const saveComment = async () => {
  //   console.log("commentPressed");
  //   if (!input) {
  //     alert('please enter Comment')
  //     await dispatch(commentsGetRequest())
  //     await dispatch(commentsGetRequestForFeed(feedId));
  //   }
  //   else {
  //     let objectValue = {};
  //     objectValue.commentFeed = feedId;
  //     objectValue.commentedBy = userId;
  //     objectValue.message = input;
  //     objectValue.postType = "feed";
  //     setLoading(true);
  //     // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
  //     await dispatch(commentsPostRequest(objectValue));

  //     await dispatch(commentsGetRequestForFeed(feedId))
  //   }

  // };
  
    const addComment = () => {
      setLoading1(true)
      setInput("");
      const comment = {};
      comment.message = input,
        comment.model = UserData?.user?.model?.id,
        comment.product = object?.id,
        dispatch(commentPostRequest(comment));
      setLoading1(false)
    }
  
    const deleteComment = (id) => {
      dispatch(commentDeleteRequest(id));
      setCommentData({ isVisible: false })
    }



  // const list = [
  //   {
  //     title: 'Delete',
  //     containerStyle: { backgroundColor: '#000', alignSelf: 'center' },
  //     titleStyle: { color: '#fff', alignSelf: 'center', fontFamily: 'Montserrat-Bold', fontSize: 20 },
  //     onPress: () => deleteComment(commentData?.id),
  //   },
  //   {
  //     title: 'Edit',
  //     containerStyle: { backgroundColor: '#000', alignSelf: 'center' },
  //     titleStyle: { color: '#fff', alignSelf: 'center', fontFamily: 'Montserrat-Bold', fontSize: 20 },
  //   },
  //   {
  //     title: 'Cancel',
  //     containerStyle: { backgroundColor: '#000', alignSelf: 'center' },
  //     titleStyle: { color: 'white', alignSelf: 'center', fontFamily: 'Montserrat-Bold', fontSize: 20 },
  //     onPress: () => setCommentData({ isVisible: false }),
  //   },
  // ];
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  return (
    <SafeAreaView style={BaseStyle.safeAreaView}
      edges={['right', 'left', 'bottom']}>
      <ScrollView showsVerticalScrollIndicator={true}>
        <View style={{ flex: 1 }}>
          <Header
            // title={t('My Orders')} 
            style={{ height: 80, backgroundColor: '#000' }}
            renderLeft={() => {
              return (
                <View style={{ flex: 1, width: 300, justifyContent: 'space-between', flexDirection: 'row' }}>
                  {/* <View style={{ flexDirection: "row", width: 300, justifyContent: "space-evenly", }}> */}
                  <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                    <Icon
                      name="arrow-left"
                      size={20}
                      color={'#000'}
                      enableRTL={true}
                    />
                  </View>
                  <Text style={{ width: 220, alignSelf: 'center', fontFamily: 'Montserrat-Bold', fontSize: 20, color: '#fff', right: 40 }}>Comments</Text>
                  {/* </View> */}

                </View>
              );
            }}
            onPressLeft={() => {
              navigation.goBack();
            }}
          />
          <View>
            <FlatList
              data={commentDataFromUser}
              style={{ margin: 20 }}
              vertical={true}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <ProfileAuthor1
                  image={item?.model?.users?.image ?  item?.model?.users?.image : "https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png"}
                  name={item?.model?.users?.username}
                  description={item?.message}
                  time={item?.createdAt}
                  style={{ top: 0 }}
                  onPress={() => setCommentData({ isVisible: true, id: item?.id })}
                />
              )}
            />
          </View>

          {/* <BottomSheet modalProps={{}} isVisible={commentData?.isVisible}>
            {list.map((l, i) => (
              <ListItem
                key={i}
                containerStyle={l.containerStyle}
                onPress={l.onPress}
              >
                <ListItem.Content>
                  <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            ))}
          </BottomSheet> */}
          <View>
          </View>
        </View>
      </ScrollView>
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around',alignItems:"flex-end",marginVertical:20}}>
        <TextInput
          onChangeText={text => setInput(text)}
          style={{ width: 300, height: 52, elevation: 5, borderRadius: 30 }}
          // onSubmitEditing={() => sendMessage()}
          placeholder={t('Add a new comment')}
          value={input}
        />
        <TouchableOpacity onPress={() => addComment()} style={{ borderRadius: 15, height: 54, width: 58, justifyContent: 'center', backgroundColor: '#000' }}>
          <Icon1 name="message-plus" size={28} color="#DDDDDD" style={{ left: 15 }} />
        </TouchableOpacity>
      </View>
      {loading1 &&
        <View style={{ width: width, height: windowHeight, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </SafeAreaView>
  )
}
