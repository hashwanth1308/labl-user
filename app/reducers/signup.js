import {
    SIGNUP_LOADING,
    SIGNUP_SUCCESS,
    SIGNUP_FAILURE,
    VERIFYOTP_SUCCESS,
    VERIFYOTP_FAILURE,
    SIGNUP_DATA,
    
  } from '../actions/actionTypes';
  
  let initialState = {
    signupLoading: false,
    signupSuccess: false,
    signupFailure: false,
    verifyOtpSuccess: false,
    verifyOtpFailure: true,
    signupMessageData : null
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case SIGNUP_DATA:
        return {
          ...state,
          signupMessageData: action.payload,
        };
      case SIGNUP_LOADING:
        return {
          ...state,
          signupLoading: action.payload,
        };
      case SIGNUP_SUCCESS:  
        return {
          ...state,
          signupSuccess: action.payload,
        };
        case VERIFYOTP_SUCCESS:  
        return {
          ...state,
          verifyOtpSuccess: action.payload,
        };
        case VERIFYOTP_FAILURE:
          return {
            ...state,
            verifyOtpFailure : action.payload,
          };
      case SIGNUP_FAILURE:
        return {
          ...state,
          signupFailure: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default reducer;
  