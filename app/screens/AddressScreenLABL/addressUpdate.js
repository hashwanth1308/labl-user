import React, { useEffect, useState } from 'react';
import {
    View,
    Switch,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    Dimensions
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Text, Button } from '@components';
import { useTranslation } from 'react-i18next';
import { TextInput } from 'react-native-paper';
import CheckBox from '@react-native-community/checkbox';
import styles from './styles';
import {
    addressCountRequest,
    addressGetRequest,
    addressFindOneRequest,
    addressPostRequest,
    addressDeleteRequest,
    addressUpdateRequest,
} from '../../api/address';

export default function UpdateAddressScreenLABL({ navigation, route }) {
    const { colors } = useTheme();
    const { t } = useTranslation();
    const height = Dimensions.get('window').height;
    const width = Dimensions.get('window').width;
    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });
    const {updateData} = route.params;
    console.log(updateData, 'updateData');
    const [toggleCheckBox, setToggleCheckBox] = useState(updateData?.typeOfAddress);
    const themeStorage = useSelector(state => state.application.theme);

    const addressData = useSelector(state => state.addressReducer.addressData);
    const userData = useSelector(state => state.accessTokenReducer.userData);
    const dispatch = useDispatch();
    const [card, setCard] = useState('');
    const [valid, setValid] = useState('');
    const [mobile, setMobile] = useState(updateData?.mobileNumber);
    const [name, setName] = useState(updateData?.fullName);
    const [pincode, setPincode] = useState(updateData?.pincode);
    const [state, setState] = useState(updateData?.state);
    const [address, setAddress] = useState(updateData?.houseNo);
    const [locality, setLocality] = useState(updateData?.areaColony);
    const [city, setCity] = useState(updateData?.city);
    const [country, setCountry] = useState(updateData?.country);
    const [loading, setLoading] = useState(false);
    const [primary, setPrimary] = useState(true);
    const [success] = useState({
        card: true,
        valid: true,
        digit: true,
        name: true,
    });

    const onSubmit = () => {
        const data = {};
        data.mobileNumber = mobile;
        data.fullName = name;
        data.pincode = pincode;
        data.state = state;
        data.houseNo = address;
        data.areaColony = locality;
        data.city = city;
        data.country = country;
        data.typeOfAddress = toggleCheckBox;
        data.user = userData?.user?.id;
        dispatch(addressUpdateRequest(updateData?.id, data));
        console.log(data, 'data');
        setMobile('');
        setName('');
        setPincode('');
        setState('');
        setAddress('');
        setLocality('');
        setCity('');
        setCountry('');
        setToggleCheckBox('');
    };
    // useEffect(() => {
    //     dispatch(addressCountRequest());

    // }, [dispatch])
    // useEffect(() => {
    //   dispatch(addressGetRequest());

    // }, [dispatch])

    // const onFindOneAddress = () => {
    //   setLoading(true);
    //   dispatch(addressFindOneRequest());
    //   // setTimeout(() => {
    //   //   navigation.goBack();
    //   // }, 1000);
    // };

    // const onAddAddress = () => {
    //   setLoading(true);
    //   dispatch(addressPostRequest());
    //   // setTimeout(() => {
    //   //   navigation.goBack();
    //   // }, 1000);
    // };

    // const onDeleteAddress = () => {
    //   setLoading(true);
    //   dispatch(addressDeleteRequest());
    //   // setTimeout(() => {
    //   //   navigation.goBack();
    //   // }, 1000);
    // };

    // const onUpdateAddress = () => {
    //   setLoading(true);
    //   dispatch(addressUpdateRequest());
    //   // setTimeout(() => {
    //   //   navigation.goBack();
    //   // }, 1000);
    // };
    const [loading1, setLoading1] = useState(true);
    useEffect(() => {
      setTimeout(() => (
        setLoading1((prev)=>(!prev))
      ), 1000)
    }, []);
    return (
        <View style={{ flex: 1 }}>
            <Header
                // title={t('Shopping Bag')}
                style={{ height: 80, backgroundColor: '#000' }}
                renderLeft={() => {
                    return (
                        <View
                            style={{
                                flexDirection: 'row',
                                width: 200,
                                justifyContent: 'space-between',
                                alignSelf: 'flex-start',
                                top: -10,
                            }}>
                            <View
                                style={{
                                    height: 30,
                                    width: 30,
                                    borderRadius: 30,
                                    backgroundColor: '#fff',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                <Icon
                                    name="arrow-left"
                                    size={20}
                                    color={colors.primary}
                                    enableRTL={true}
                                />
                            </View>
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    fontFamily: 'Montserrat-Bold',
                                    fontSize: 20,
                                    color: '#fff',
                                    right: 60,
                                }}>
                                Address
                            </Text>
                            {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
                        </View>
                    );
                }}
                onPressLeft={() => {
                    navigation.goBack();
                }}
            />
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>
                    <ScrollView contentContainerStyle={{ padding: 20 }}>
                        <View style={{ padding: 10 }}>
                            <TextInput
                                label="Name"
                                value={name}
                                style={{ backgroundColor: 'transparent' }}
                                underlineColor="#000"
                                activeUnderlineColor="#000"
                                onChangeText={text => setName(text)}
                            />
                            <TextInput
                                label="Mobile"
                                value={mobile}
                                style={{ backgroundColor: 'transparent', marginVertical: 10 }}
                                underlineColor="#000"
                                activeUnderlineColor="#000"
                                onChangeText={text => setMobile(text)}
                            />
                            <View
                                style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <TextInput
                                    label="Pincode"
                                    value={pincode}
                                    style={{
                                        backgroundColor: 'transparent',
                                        width: 150,
                                        marginVertical: 10,
                                    }}
                                    underlineColor="#000"
                                    activeUnderlineColor="#000"
                                    onChangeText={text => setPincode(text)}
                                />
                                <TextInput
                                    label="State"
                                    value={state}
                                    style={{
                                        backgroundColor: 'transparent',
                                        width: 150,
                                        marginVertical: 10,
                                    }}
                                    underlineColor="#000"
                                    activeUnderlineColor="#000"
                                    onChangeText={text => setState(text)}
                                />
                            </View>
                            <TextInput
                                label="Address (House no, Building, Street, Area)"
                                value={address}
                                style={{ backgroundColor: 'transparent', marginVertical: 10 }}
                                underlineColor="#000"
                                activeUnderlineColor="#000"
                                onChangeText={text => setAddress(text)}
                            />
                            <TextInput
                                label="Locality/Town"
                                value={locality}
                                style={{ backgroundColor: 'transparent', marginVertical: 10 }}
                                underlineColor="#000"
                                activeUnderlineColor="#000"
                                onChangeText={text => setLocality(text)}
                            />
                            <TextInput
                                label="City/District"
                                value={city}
                                style={{ backgroundColor: 'transparent', marginVertical: 10 }}
                                underlineColor="#000"
                                activeUnderlineColor="#000"
                                onChangeText={text => setCity(text)}
                            />
                            <TextInput
                                label="Country"
                                value={country}
                                style={{ backgroundColor: 'transparent', marginVertical: 10 }}
                                underlineColor="#000"
                                activeUnderlineColor="#000"
                                onChangeText={text => setCountry(text)}
                            />
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    marginVertical: 10,
                                }}>
                                <CheckBox
                                    value={toggleCheckBox}
                                    size={40}
                                    onValueChange={newValue => setToggleCheckBox(newValue)}
                                />
                                <Text
                                    style={{
                                        fontFamily: 'Montserrat-Bold',
                                        fontSize: 15,
                                        color: '#000',
                                    }}>
                                    Make this as my default address
                                </Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    height: 120,
                                    width: '100%',
                                    backgroundColor: '#fff',
                                }}>
                                <TouchableOpacity
                                    style={{
                                        backgroundColor: '#D9D9D9',
                                        height: 50,
                                        width: 150,
                                        borderRadius: 5,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Text
                                        style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 20 }}>
                                        Cancel
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => onSubmit()}
                                    style={{
                                        backgroundColor: '#000',
                                        height: 50,
                                        width: 150,
                                        borderRadius: 5,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Text
                                        style={{
                                            fontFamily: 'Montserrat-SemiBold',
                                            fontSize: 20,
                                            color: '#fff',
                                        }}>
                                        Save
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
            {loading1 &&
        <View style={{ width: width, height:height,backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor:"#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
        </View>
    );
}
