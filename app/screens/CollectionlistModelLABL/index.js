import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import {
  Image,
  Icon,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  EventCard,
  Header,
  CartCard,
  TextInput
} from '@components';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import Icon1 from 'react-native-vector-icons/Fontisto';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { productsGetRequestUsingCollectionId } from '../../api/products';
import { subCategoriesFindOneRequest, subCategoriesGetRequest } from '../../api/subCategories';
import { useSelector, useDispatch } from 'react-redux';
export default function CollectionlistModelLABL({ navigation, route }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const [imageData] = useState([]);
  const [adults, setAdults] = useState(2);
  const { productObject } = route.params;
  console.log("collection", productObject);
  const id = productObject?.id

  useEffect(() => {
    dispatch(subCategoriesFindOneRequest(id))
  }, [dispatch])
  useEffect(() => {
    dispatch(subCategoriesGetRequest())
  }, [dispatch])


  const subCategories = useSelector(state => state.subCategoriesReducer.subCategoriesOneData);
  console.log("subCategories", subCategories);

  const subCate = useSelector(state => state.subCategoriesReducer.subCategoriesData);
  console.log("subCate", subCate);



  const [Collection] = useState([
    {
      image: Images.event4,
      name: 'MEN',
      description: 'kurta',
      price: '$399',
      route: 'Hotel',
    },
    {
      image: Images.event8,
      name: 'WOMEN',
      description: 'kurta',
      price: '$399',
      route: 'Tour',
    },
    {
      image: Images.event9,
      name: 'KIDS',
      description: 'kurta',
      price: '$399',
      route: 'OverViewCar',
    },
    {
      image: Images.event10,
      name: 'JACKETS',
      description: 'kurta',
      price: '$399',
      route: 'FlightSearch',
    },


  ]);

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const [loading, setLoading] = useState(false);
  const deltaY = new Animated.Value(0);
  // useEffect(() => {
  //   dispatch(productsGetRequestUsingCollectionId(item.id));

  // }, [dispatch])
  const productsDataFromSelectedCollection = useSelector(state => state.productsReducer.productsDataFromSelectedCollection);
  console.log("productsDataFromSelectedCollection", productsDataFromSelectedCollection);
  const selectedProducts = async (object) => {
    setLoading(true);
    // await dispatch(collectionsPostRequest(cName, profileImage,labelId));
    navigation.navigate('PostDetailModelLABL', { object })
    setLoading(false);
  };
  const renderIconService = () => {
    return (
      <FlatList
        numColumns={2}
        data={productObject?.products}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              style={{ margin: 8, borderColor: "#CDCDCD", height: 300, width: 180, borderWidth: 1 }}
              activeOpacity={0.9}
              onPress={() => selectedProducts(item)}
            // onPress={() => {
            //   navigation.navigate('HotelDetail');
            // }}
            >
              <View
                style={[styles.iconContent]}>

                <Image
                  source={{
                    uri:
                      item?.productImages ? item?.productImages[0]?.url : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsQ-YHX2i3RvTDDmpfnde4qyb2P8up7Wi3Ww&usqp=CAU'
                  }}
                  //source={item?.productImages ? `https://staging-backend.labl.store` + item?.productImages[0]?.url : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsQ-YHX2i3RvTDDmpfnde4qyb2P8up7Wi3Ww&usqp=CAU'} 
                  style={{ height: 180, width: 180, borderRadius: 0 }}
                  onPress={() => selectedProducts(item)} />

              </View>
              <View style={styles.content}>
                <View style={styles.left}>
                  <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, width: 150 }}>
                    {item?.productName}
                  </Text>
                  <Text
                    style={{ fontFamily: "Montserrat-Medium", fontSize: 18, width: 150, marginVertical: 10 }}>
                    ₹ {item?.productPrice}
                  </Text>

                  {/* <Text
                    note
                    numberOfLines={1}
                    footnote
                    grayColor
                  >
                    {item.productPrice}
                  </Text> */}
                </View>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center",padding:10 }}>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#000" }}>4.6</Text>
                <Icon1 name="star" size={12} color="#000" style={{ marginHorizontal: 5 }} />
                <View style={{ borderWidth: 0.6, borderColor: "#000", height: 20, marginHorizontal: 10 }}></View>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#000" }}>82</Text>
              </View>
            </TouchableOpacity>
          )
        }}
      />
    );
  };
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev)=>(!prev))
    ), 1000)
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('My Orders')} 
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flex: 1, width: 300, justifyContent: "space-between", flexDirection: "row" }}>
              {/* <View style={{ flexDirection: "row", width: 300, justifyContent: "space-evenly", }}> */}
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center", alignSelf: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
              <Text style={{ width: 220, alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", right: 40 }}>{productObject?.subCategoryName}</Text>
              {/* </View> */}

            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        <ScrollView
          onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
          scrollEventThrottle={8}>
          <View style={{}}>

            {renderIconService()}
          </View>
        </ScrollView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height:height,backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor:"#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}

