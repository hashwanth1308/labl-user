import {
  MODELEARNINGS_POST_CHECK_SUCCESS,
  MODELEARNINGS_SET_COUNT,
  MODELEARNINGS_COUNT_CHECK_SUCCESS,
  MODELEARNINGS_DELETE_CHECK_SUCCESS,
  MODELEARNINGS_FINDONE_CHECK_SUCCESS,
  MODELEARNINGS_SET_ONE_DATA,
  MODELEARNINGS_GET_CHECK_SUCCESS,
  MODELEARNINGS_SET_DATA,
  MODELEARNINGS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function modelEarningsPostCheckSuccess(bool = false) {
  return {
    type: MODELEARNINGS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelEarningsSetCount(object) {
  return {
    type: MODELEARNINGS_SET_COUNT,
    payload: object,
  };
}
export function modelEarningsCountCheckSuccess(bool = false) {
  return {
    type: MODELEARNINGS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelEarningsDeleteCheckSuccess(bool = false) {
  return {
    type: MODELEARNINGS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelEarningsFindOneCheckSuccess(bool = false) {
  return {
    type: MODELEARNINGS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelEarningsSetOneData(object) {
  return {
    type: MODELEARNINGS_SET_ONE_DATA,
    payload: object,
  };
}
export function modelEarningsGetCheckSuccess(bool = false) {
  return {
    type: MODELEARNINGS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelEarningsSetData(object) {
  return {
    type: MODELEARNINGS_SET_DATA,
    payload: object,
  };
}

export function modelEarningsUpdateCheckSuccess(bool = false) {
  return {
    type: MODELEARNINGS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

