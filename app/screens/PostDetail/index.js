import React, { useEffect, useRef, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Dimensions,
  ToastAndroid,
  Vibration,
  BackHandler,
  RefreshControl
} from 'react-native';
import { BaseColor, Images, useTheme } from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  ProfileAuthor,
  ProfileGroup,
  Card,
  PostListItem,
  Image,
} from '@components';
import * as Utils from '@utils';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import Icon1 from 'react-native-vector-icons/Fontisto';
import { useDispatch, useSelector } from 'react-redux';
import Swiper from 'react-native-swiper';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon3 from 'react-native-vector-icons/Ionicons';
import { userCartsPostRequest } from '../../api/userCarts';
import { modelsFindOneRequest } from '../../api/models';
import {
  bookmarksDeleteRequest,
  bookmarksGetRequestByUser,
  bookmarksPostRequest,
} from '../../api/bookmarks';
import _ from 'lodash';
import Modal from 'react-native-modal';
import { productVarientGetRequest, productsGetRequestUsingCollectionId, productFindOneRequest } from '../../api/products';
import { addressGetRequest } from '../../api/address';
import Snackbar from 'react-native-snackbar';
import { userCartsGetRequest } from '../../api/userCarts';
import { productRatingGetRequest } from '../../api';
import AddAddressScreenLABL from '../AddressScreenLABL/address';
import axios from 'axios';

export default function PostDetail({ navigation, route }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { colors } = useTheme();
  const { object,name } = route.params;
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const [refreshing, setRefreshing] = useState(false);
  const [dummy, setDummy] = useState(false);

  console.log(object, name , 'postDetails');
  const deltaY = new Animated.Value(0);
  const heightHeader = Utils.heightHeader();
  const heightImageBanner = Utils.scaleWithPixel(300);
  const marginTopBanner = heightImageBanner - heightHeader - 30;

  const userData = useSelector(state => state.accessTokenReducer.userData);
  console.log(userData, 'userData');


  const scrollToTop = (item) => {
    setLoading1(true);
    setRefreshing(true)
    setTimeout(() => {
      if (viewRef.current) {
        viewRef.current.scrollTo({
          animated: true,
          y: height - 1000,
          x: 0,
        });
      }
      navigation.navigate('PostDetail', { object: item,name:name })
      setLoading1(false);
      setRefreshing(false);
      setDummy(!dummy)
    }, 1000);
  }

  useEffect(() => {
    setChecked(null);
    setChecked1(null);

  }, [loading1, refreshing, dummy]);
  useEffect(() => {
    const obj = {};
    obj.user = userData?.user?.id;
    dispatch(addressGetRequest(obj));

  }, []);
  useEffect(() => {
    dispatch(productFindOneRequest(object?.id));
  }, [dispatch,dummy]);
  const product = useSelector(state => state.productsReducer.lablPromotedData);
  console.log("product", product);

  useEffect(() => {
    const obj = {};
    obj.modelProduct = object?.id;
    dispatch(productRatingGetRequest(obj));

  }, [dummy]);
  const rating = useSelector(state => state.productRatingReducer.productRatingData);
  console.log("rating", rating);

  const totalRating = rating?.map((item) => item?.rating);
  console.log("totalRating", totalRating);
  const totalSum =
    totalRating?.length > 0 &&
    totalRating?.reduce((partialSum, a) => Number(partialSum) + Number(a), 0) / totalRating?.length;

  useEffect(() => {
    const obj = {};
    obj.user = userData?.user?.id;
    dispatch(userCartsGetRequest(obj));

  }, [loading1, dummy]);
  const userCartData = useSelector(
    state => state.userCartsReducer.userCartsData,
  );
  console.log(userCartData, 'userCartData');

  useEffect(() => {
    const cartitem = name === "lablExclusive" || name === "lablFranchise" || name === "lablPromoted"  ? _.find(userCartData, function (o) {
      return o?.product?.id === object?.id;
    }) : _.find(userCartData, function (o) {
      return o?.modelProduct?.id === object?.id;
    });
    console.log(cartitem, 'cartitem');
    if (cartitem) {
      // setProductId(whishlistitems.id);
      setAddedCart(true);
    }
  }, []);

  useEffect(() => {
    const cartitem = _.find(userCartData, function (o) {
      return o?.modelProduct?.id === object?.id;
    });
    if (cartitem) {
      // setProductId(whishlistitems.id);
      setAddedCart(true);
    }
  }, [userCartData, refreshing, dummy]);

  const viewRef = useRef();
  const _scrollViewBottom = height;
  const [sel, setSel] = useState(false);
  const scrollToBottom = () => {
    setTimeout(() => {
      if (viewRef.current) {
        viewRef.current.scrollTo({
          animated: true,
          y: height + 100,
          x: 0,
        });
      }
    }, 500);
  }


  const UserAddress = useSelector(state => state.addressReducer.addressData);
  console.log("address", UserAddress);
  const productsData = useSelector(
    state => state.modelProductsReducer.modelProductsData,
  );
  useEffect(() => {
    dispatch(bookmarksGetRequestByUser(userData?.user?.id));
  }, []);
  useEffect(() => {
    dispatch(modelsFindOneRequest(object?.model?.id));
  }, []);
  const models = useSelector(state => state.modelsReducer.modelsOneData);
  console.log('models', models);
  const WhistList = useSelector(
    state => state.bookMarksReducer.bookmarksDataByUser,
  );
  console.log(WhistList, 'whishlist');
  console.log(productsData, 'productsData');
  
    const [otherProducts, setOtherProducts] = useState([])
    console.log("others", otherProducts);

  useEffect(async() => {
    if(object?.isLablExclusive || object?.isLablFranchise || object?.isLablPromoted)
    await axios.get('https://staging-backend.labl.store/allproductfilter',{
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userData.jwt}`,
      }
    }).then((res) => {
      if(name  === "lablExclusive"){
        const other = res?.data[1]?.lablExclusive?.filter((item) => item?.product?.id !== object?.id)
        setOtherProducts(other)
      }
      else if(name  === "lablPromoted"){
        const other = res?.data[3]?.lablPromoted?.filter((item) => item?.product?.id !== object?.id)
        setOtherProducts(other)
      }
      else if(name  === "lablFranchise"){
        const other = res?.data[2]?.lablFranchise?.filter((item) => item?.product?.id !== object?.id)
        setOtherProducts(other)
      }
    })
    .catch((err) => console.log(err))
    else{
      setOtherProducts(productsData?.filter((item) => item?.id !== object?.id))
    }
  }, [dispatch]);

  const [modalVisible, setModalVisible] = useState(false);
  const toggleModal = () => {
    setModalVisible(!modalVisible);
  };

  useEffect(() => {
    loadUserData();
  }, [refreshing]);

  const loadUserData = () => {
    // setRefreshing(true)
    if (object?.isLablExclusive || object?.isLablFranchise || object?.isLablPromoted) {
      dispatch(productVarientGetRequest(object?.id));
    } else {
      dispatch(productVarientGetRequest(object?.product?.id));
    }
  };
  useEffect(() => {
    if (object?.isLablExclusive || object?.isLablFranchise || object?.isLablPromoted) {
      dispatch(productVarientGetRequest(object?.id));
    } else {
      dispatch(productVarientGetRequest(object?.product?.id));
    }
  }, []);

  const productVarient = useSelector(state => state.productsReducer.productVarientData);
  console.log("productVarient", productVarient);

  const sizes = ['XS','S', 'M', 'L', 'XL', 'XXL', 'XXS', 'XXXL', 'XXXS', 'Free Size'];

  const sortByReference = (a, b) => {
    const aIndex = sizes.findIndex(reference => reference === a.type);
    const bIndex = sizes.findIndex(reference => reference === b.type);
    return aIndex - bIndex;
  };

  const [varientSelected, setVarientSelected] = useState(null);
  console.log("varientSelected", varientSelected);
  const uniqueIds = [];


  const uniqueProduct = [...new Set(productVarient.map(item => item.size))];
  console.log("uniqueProduct", uniqueProduct);

  const uniquePr = uniqueProduct.filter(element => {
    const isDuplicate = uniqueIds.includes(element?.type);

    if (!isDuplicate) {
      uniqueIds.push(element?.type);

      return true;
    }

    return false;
  });
  const unique = uniquePr?.sort(sortByReference);
  console.log("unique", unique);

  const productColor = useSelector(state => state.productsReducer.productsDataFromSelectedCollection);
  console.log("productColor", productColor);

  const [productId, setProductId] = useState('');
  const [productmarked, setProductmarked] = useState(false);

  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) {
      return o.product?.id === object?.id;
    });
    console.log(whishlistitems, 'whishlistitems');
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true);
    }
  }, []);

  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) {
      return o.product?.id === object?.id;
    });
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true);
    }
  }, [WhistList]);
  const addWhishlist = async () => {
    if (productmarked) {
      setProductmarked(false);
      await dispatch(bookmarksDeleteRequest(productId));
    } else {
      setProductmarked(true);
      const objectValue = {};
      if(name === "lablExclusive"){
        objectValue.lablExclusive = object?.lablExclusive
      }else if(name === "lablFranchise"){
        objectValue.lablFranchise = object?.lablFranchise
      }else if(name === "lablPromoted"){
        objectValue.lablPromoted = object?.lablPromoted
      }else{
        objectValue.modelProduct = object?.model_product?.id;
      }
      objectValue.user = userData?.user?.id;
      objectValue.product = object?.id;
      // objectValue.modelProduct = object?.id;
      console.log('bookmarkpost', objectValue);
      // setLoading(true);

      await dispatch(bookmarksPostRequest(objectValue));
    }
  };
  const [selectAddress, setSelectedAddress] = useState(null);
  console.log("selectAddress", selectAddress);
  const [addedCart, setAddedCart] = useState(false);
  const address =
    userData?.user?.address &&
    userData?.user?.address.filter(item => item?.typeOfAddress);
  console.log(address, 'address');
  const handleAddToCart = () => {
    if (varientSelected === null) {
      alert("Please select Size and Color")
    } else {
      setLoading1(true)
      const cartData = {};
      if(name === "lablExclusive"){
        cartData.lablExclusive = object?.lablExclusive
      }else if(name === "lablFranchise"){
        cartData.lablFranchise = object?.lablFranchise
      }else if(name === "lablPromoted"){
        cartData.lablPromoted = object?.lablPromoted
      }else{
        cartData.modelProduct = object?.model_product?.id;
      }
      cartData.user = userData?.user?.id;
      cartData.productVarient = varientSelected?.id;
      cartData.product = object?.id;
      // cartData.quantity = value;
      dispatch(userCartsPostRequest(cartData));
      console.log(cartData, 'cartData');
      setAddedCart(true);
      Snackbar.show({
        text: 'Product is added your Cart',
        duration: Snackbar.LENGTH_SHORT,
      });
      Vibration.vibrate(400, false);
      setLoading1(false)
    }
  };
  const [checked, setChecked] = useState(null);
  const [checked1, setChecked1] = useState(null);

  const onSelectSizes = select => {
    if (object?.isLablExclusive || object?.isLablFranchise || object?.isLablPromoted) {
      setLoading1(true);
      const obj = {};
      obj.product = product?.id;
      obj.size = select?.id;
      dispatch(productsGetRequestUsingCollectionId(obj))
      setChecked(select?.id);
      setLoading1(false);
    } else {
      setLoading1(true);
      const obj = {};
      obj.product = object?.product?.id;
      obj.size = select?.id;
      dispatch(productsGetRequestUsingCollectionId(obj))
      setChecked(select?.id);
      setLoading1(false);
    }
  };

  const handleChange = (item) => {
    setChecked1(item?.color?.id);
    setVarientSelected(item)
  }


  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('Shopping Bag')}
        style={{ height: 80, backgroundColor: '#000' }}
        renderLeft={() => {
          return (
            <View
              style={{
                flexDirection: 'row',
                width: 200,
                justifyContent: 'space-between',
                alignSelf: 'flex-start',
                top: -10,
              }}>
              <View
                style={{
                  height: 30,
                  width: 30,
                  borderRadius: 30,
                  backgroundColor: '#fff',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>

                <Text style={{ alignSelf: "center", fontFamily: "Roboto-Bold", fontSize: 20, color: "#fff", width: 200, marginHorizontal: 20 }}>{product?.productBrandName}</Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        renderRight={() => {
          return (
            <Image
              source={require('../../assets/images/Vector.png')}
              style={{ height: 17, width: 20 }}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
        onPressRight={() => {
          navigation.navigate('Wishlist');
        }}
      />
      <SafeAreaView style={{ flex: 1 }} edges={['right', 'left', 'bottom']}>
        <ScrollView
          ref={viewRef}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={loadUserData} />
          }
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: { y: deltaY },
              },
            },
          ])}
          scrollEventThrottle={8}>
          {object?.isLablExclusive || object?.isLablFranchise || object?.isLablPromoted ?
            <Profile
              image={product?.label?.user?.image}
              name={product?.label?.labelName}
              // description="5 hours ago | 100k views"
              // textRight="Jun 2018"
              style={{
                marginTop: 20,
              }}
            /> :
            <Profile
              image={models?.users?.image}
              name={models?.users?.username}
              // description="5 hours ago | 100k views"
              // textRight="Jun 2018"
              style={{
                marginTop: 20,
              }}
            />}
          <View
            style={{
              paddingHorizontal: 20,
              marginBottom: 20,
              // marginTop: marginTopBanner,
            }}>
            {object?.isLablExclusive || object?.isLablFranchise || object?.isLablPromoted ?
              <Swiper
                autoplay={true}
                height={height / 2}
                showsPagination={false}
                removeClippedSubviews={false}>
                {object?.productImages?.length > 0 && object?.productImages?.map((item, index) => {
                  console.log(item, "items")
                  return (
                    <TouchableOpacity onPress={() => navigation.navigate("PreviewImage", { obj: object?.productImages })} style={styles.slide} key={item.key}>
                      <Image
                        source={{ uri: item?.url }}
                        style={styles.imgBanner}
                      />
                    </TouchableOpacity>
                  );
                })}
              </Swiper> :
              <Swiper
                autoplay={true}
                height={height / 2}
                showsPagination={false}
                removeClippedSubviews={false}>
                {object?.imagesOfProductByModel?.length > 0 && object?.imagesOfProductByModel?.map((item, index) => {
                  console.log(item, "items")
                  return (
                    <TouchableOpacity onPress={() => navigation.navigate("PreviewImage", { obj: object?.imagesOfProductByModel })} style={styles.slide} key={item.key}>
                      <Image
                        source={{ uri: item?.url }}
                        style={styles.imgBanner}
                      />
                    </TouchableOpacity>
                  );
                })}
              </Swiper>}
            <View
              style={{
                flex: 1,
                height: 70,
                backgroundColor: '#909090',
                opacity: 1,
                padding: 15,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <TouchableOpacity onPress={() => scrollToBottom()}
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon1
                  name="shopping-bag"
                  size={17}
                  color="#fff"
                  style={{ marginVertical: 5 }}
                />
                <Text
                  style={{
                    fontFamily: 'Roboto-SemiBold',
                    fontSize: 16,
                    color: '#fff',
                  }}>
                  Similar Products
                </Text>
              </TouchableOpacity>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text
                  style={{
                    fontFamily: 'Roboto-SemiBold',
                    fontSize: 16,
                    color: '#fff',
                  }}>
                  {Number(totalSum)}
                </Text>
                <Icon1
                  name="star"
                  size={12}
                  color="#fff"
                  style={{ marginHorizontal: 5 }}
                />
                <View
                  style={{
                    borderWidth: 0.6,
                    borderColor: '#fff',
                    height: 20,
                    marginHorizontal: 10,
                  }}></View>
                <Text
                  style={{
                    fontFamily: 'Roboto-SemiBold',
                    fontSize: 16,
                    color: '#fff',
                  }}>
                  {Number(totalRating?.length)}
                </Text>
              </View>
            </View>
            {/* <Animated.Image
              source={{ uri: object?.product?.productImages ? "https://staging-backend.labl.store" + object?.product?.productImages[0]?.url : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png' }}
              style={[
                styles.imgBanner,
                // {
                //   height: deltaY.interpolate({
                //     inputRange: [
                //       0,
                //       Utils.scaleWithPixel(195),
                //       Utils.scaleWithPixel(195),
                //     ],
                //     outputRange: [heightImageBanner, heightHeader, heightHeader],
                //   }),
                // },
              ]}
            /> */}
            {/* <View style={{flex:1, position: "absolute"}}>
              <Text style={{ color: "#000"}} numberOfLines={1}>
                {object.name}
              </Text>
            </View> */}
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: "center",
                marginHorizontal: 20,
                justifyContent: 'space-around',
                height: 120,
                width: '100%',
                backgroundColor: '#fff',
              }}>
              <TouchableOpacity
                onPress={() => addWhishlist()}
                style={{
                  flexDirection: 'row',
                  borderColor: '#D9D9D9',
                  borderWidth: 1,
                  height: 50,
                  width: 150,
                  borderRadius: 10,
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                }}>
                {productmarked ? (
                  <>
                    <Icon1 name="heart" size={20} color="black" />
                    <Text
                      style={{
                        fontFamily: 'Roboto-SemiBold',
                        fontSize: 20,
                        color: 'black',
                      }}>
                      Wishlist
                    </Text>
                  </>
                ) : (
                  <>
                    <Icon1 name="heart" size={20} color="#CDCDCD" />
                    <Text
                      style={{
                        fontFamily: 'Roboto-SemiBold',
                        fontSize: 20,
                        color: '#CDCDCD',
                      }}>
                      Wishlist
                    </Text>
                  </>
                )}
              </TouchableOpacity>
              {addedCart ?
                <TouchableOpacity
                  onPress={() => navigation.navigate("Cart")}
                  style={{
                    flexDirection: 'row',
                    backgroundColor: '#000',
                    height: 50,
                    width: 150,
                    borderRadius: 10,
                    // marginHorizontal:20,
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Icon1 name="shopping-bag" size={17} color="#fff" />
                  <Text
                    style={{
                      fontFamily: 'Roboto-SemiBold',
                      fontSize: 18,
                      color: '#fff',
                    }}>
                    View Cart
                  </Text>
                </TouchableOpacity> :
                <TouchableOpacity
                  onPress={() => handleAddToCart()}
                  style={{
                    flexDirection: 'row',
                    backgroundColor: '#000',
                    height: 50,
                    width: 150,
                    padding: 10,
                    // marginHorizontal:20,
                    borderRadius: 10,
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Icon1 name="shopping-bag" size={17} color="#fff" />
                  <Text
                    style={{
                      fontFamily: 'Roboto-SemiBold',
                      fontSize: 18,
                      marginHorizontal: "5%",
                      color: '#fff',
                    }}>
                    Add to Bag
                  </Text>
                </TouchableOpacity>}
            </View>
            <Text
              style={{
                fontFamily: 'Roboto-Medium',
                fontSize: 16,
              }}>
              {product?.productName}
            </Text>
            <Text
              style={{
                fontFamily: 'Roboto-SemiBold',
                fontSize: 16,
                marginVertical: 10,
              }}>
             Price : ₹ {object?.isLablExclusive || object?.isLablFranchise || object?.isLablPromoted ? object?.productPrice : object?.priceOfProductWithModelCommission}
            </Text>
            {/* <View style={{ flexDirection: "row", marginTop: 10 }}>
              <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 20, }}>
                Select Quantity :{' '}
              </Text>
              <View style={styles.contentPicker}>
                
                <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => onChange('up')}>
                  <Icon name="plus-circle" size={24} color={colors.primary} />
                </TouchableOpacity>
                <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20 }}>{value}</Text>
                <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => onChange('down')}>
                  <Icon name="minus-circle" size={24} color={BaseColor.grayColor} />
                </TouchableOpacity>
              </View>
            </View> */}
            <View style={{ marginVertical: 5 }}>
              <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 16 }}>
                Available Sizes
              </Text>
              {unique != "" ?
                // <Component unique={unique} object={object} productColor={productColor} onChange={setVarientSelected} />
                <View>
                  <FlatList
                    horizontal
                    data={product?.productvarients}
                    keyExtractor={(item, index) => item.id}
                    renderItem={({ item, index }) => (
                      (checked === item?.size?.id ?
                        <TouchableOpacity style={{ margin: 5, backgroundColor: "#000", borderRadius: 20, width: 55, height: 35, justifyContent: "center", alignItems: "center" }}>
                          <Text style={{ fontFamily: "Roboto-Bold", fontSize: 12, color: "#fff", textAlign: "center" }}>{item?.size?.type}</Text>
                        </TouchableOpacity> :
                        <TouchableOpacity onPress={() => onSelectSizes(item?.size)} style={{ margin: 5, borderWidth: 1, borderColor: "#000", borderRadius: 20, width: 55, height: 35, justifyContent: "center", alignItems: "center" }}>
                          <Text style={{ fontFamily: "Roboto-Bold", fontSize: 12, }}>{item?.size?.type}</Text>
                        </TouchableOpacity>)
                    )}
                  />
                  {checked ?
                    <View style={{ flexDirection: "column", marginTop: 15 }}>
                      <Text style={{ fontFamily: "Roboto-Bold", fontSize: 16 }}>Available Colors</Text>
                      {/* <ColorComponent color={productColor} onChange={onChange} /> */}
                      {/* <View>
                        <FlatList
                          horizontal
                          data={product?.productvarients}
                          keyExtractor={(item, index) => item.id}
                          renderItem={({ item, index }) => (
                            (checked1 === item?.id ?
                              <TouchableOpacity style={{ margin: 5, elevation: 5, backgroundColor: `${item?.color?.color?.toLowerCase()}`, borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
                                <Icon name="check" color={item?.color?.color === "White" ? "#000" : "#fff"} size={20} />
                              </TouchableOpacity> :
                              <TouchableOpacity onPress={() => handleChange(item)} style={{ margin: 5, elevation: 5, backgroundColor: `${item?.color?.color?.toLowerCase()}`, borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
                              </TouchableOpacity>)
                          )}

                        />
                      </View> */}
                      {checked1 === productColor[0]?.color?.id ?
                        <TouchableOpacity style={{ margin: 5, elevation: 5, backgroundColor: `${productColor[0]?.color?.color?.toLowerCase()}`, borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
                          <Icon name="check" color={productColor[0]?.color?.color === "White" ? "#000" : "#fff"} size={20} />
                        </TouchableOpacity> :
                        <TouchableOpacity onPress={() => handleChange(productColor[0])} style={{ margin: 5, elevation: 5, backgroundColor: `${productColor[0]?.color?.color?.toLowerCase()}`, borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
                        </TouchableOpacity>}
                    </View> : null}
                </View>
                :
                <View style={{ marginVertical: 15 }}>
                  <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 16, color: "#000" }}>No Sizes available!!!</Text>
                </View>}
            </View>
            {/* <View style={{ height: 70, width: "100%", backgroundColor: "#000", borderRadius: 10, justifyContent: "space-around", alignItems: "center", flexDirection: "row" }}>
              <View style={{ height: 50, width: 50, borderRadius: 10, backgroundColor: "#fff", justifyContent: "center" }}>
                <Icon2 name="message-processing" size={35} style={{ alignSelf: "center" }} />
              </View>
              <View style={{ flexDirection: "column" }}>
                <Text style={{ color: "#fff", fontFamily: "Roboto-Bold", fontSize: 23, marginVertical: 0 }}>Chat with vendor</Text>
                <Text style={{ color: "#fff", fontFamily: "Roboto-SemiBold", fontSize: 20, marginVertical: 5 }}>Tap to chat</Text>
              </View>
              <TouchableOpacity style={{ height: 40, width: 40, backgroundColor: "#909090", justifyContent: "center", borderRadius: 20 }}>
                <Icon2 name="chevron-right" size={40} style={{ color: "#fff", alignSelf: "center" }} />
              </TouchableOpacity>
            </View> */}
            {/* <View style={{ marginVertical: 10 }}>
              <Text
                style={{
                  fontFamily: 'Roboto-Bold',
                  fontSize: 16,
                  marginVertical: 10,
                }}>
                Delivery Details
              </Text>
              <View
                style={{
                  borderWidth: 1,
                  height: 60,
                  width: '100%',
                  borderRadius: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  padding: 10,
                  alignItems: 'center',
                }}>
                <Text style={{ fontFamily: 'Roboto-SemiBold', fontSize: 14 }}>
                  {selectAddress?.pincode
                    ? selectAddress?.pincode
                    : 'please add address'}
                </Text>
                <TouchableOpacity onPress={toggleModal}>
                  <Text
                    style={{ fontFamily: 'Roboto-SemiBold', fontSize: 14 }}>
                    Change
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <Modal
                  isVisible={modalVisible}
                  onBackdropPress={() => setModalVisible(false)}
                  onSwipeComplete={() => setModalVisible(false)}
                  swipeDirection={['down']}
                  style={styles.bottomModal}>
                  <View style={styles.contain}>
                    <View style={{
                      paddingTop: 20,
                      paddingLeft: 30,
                      justifyContent: 'center',
                      flexDirection: "row",
                    }}>
                      <View style={{ padding: 0 }}>
                        <Text style={{ fontFamily: "Roboto-Bold", fontSize: 22 }}>Choose your Delivery Address</Text>
                      </View>
                    </View>
                    <AddressComponent address={UserAddress} onChange={setSelectedAddress} modal={setModalVisible} />
                  </View>
                </Modal>
              </View>
              {UserAddress.length > 0 ?
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 20,
                  }}>
                  <Icon1
                    name="shopping-bag"
                    size={15}
                    color="#000"
                    style={{ marginHorizontal: 10 }}
                  />
                  <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>
                    Expected Delivery by 12 Jun
                  </Text>
                </View> : null}
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 20,
                }}>
                <Icon2
                  name="cash"
                  size={18}
                  color="#000"
                  style={{ marginHorizontal: 8 }}
                />
                <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>
                  Cash on Delivery Available
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 20,
                }}>
                <Icon3
                  name="swap-horizontal"
                  size={15}
                  color="#000"
                  style={{ marginHorizontal: 10 }}
                />
                <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>
                  Easy 30 days return & exchange available
                </Text>
              </View>
            </View> */}
            <View>
              <Text
                style={{
                  fontFamily: 'Roboto-Bold',
                  fontSize: 16,
                  marginVertical: 10,
                }}>
                Product Details
              </Text>
              <Text
                style={{
                  fontFamily: 'Roboto-Regular',
                  fontSize: 14,
                  marginVertical: 0,
                }}>
                {object?.isLablExclusive || object?.isLablFranchise || object?.isLablPromoted ? object?.productDescription : object?.product?.productDescription}
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontFamily: 'Roboto-Bold',
                  fontSize: 16,
                  marginVertical: 10,
                }}>
                Manufacturer Details
              </Text>
              <Text
                style={{
                  fontFamily: 'Roboto-Regular',
                  fontSize: 14,
                  marginVertical: 0,
                }}>
                {product?.manufacturerDetails}
              </Text>
            </View>
            <View style={{ marginVertical: 10 }}>
              <Text
                style={{
                  fontFamily: 'Roboto-Bold',
                  fontSize: 16,
                  marginVertical: 10,
                }}>
                Easy 30 days returns and exchanges
              </Text>
              <Text
                style={{
                  fontFamily: 'Roboto-Regular',
                  fontSize: 14,
                  marginVertical: 0,
                }}>
                Choose to return or exchange for a different size (If available)
                within 30 days.
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontFamily: 'Roboto-Bold',
                  fontSize: 20,
                  marginVertical: 10,
                }}>
                You may also like
              </Text>
              {name === "lablExclusive" || name === "lablFranchise" || name === "lablPromoted" ?
              <FlatList
                horizontal
                data={otherProducts}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <TouchableOpacity onPress={() => scrollToTop(item?.product)} style={{ margin: 10 }}>
                    <Image
                      source={{
                        uri:
                          item?.product?.productImages[0]?.url
                            ?
                            item?.product?.productImages[0]?.url
                            : '',
                      }}
                      style={{ height: 105, width: 105, borderRadius: 0 }}
                    />
                    <Text style={{width:110,height:25,marginVertical:5, fontFamily: 'Roboto-Bold', fontSize: 12 }}>
                      {item?.product?.productName}
                    </Text>
                    <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 16 }}>
                      ₹ {item?.product?.productPrice}
                    </Text>
                  </TouchableOpacity>
                )}
              />:
              <FlatList
                horizontal
                data={otherProducts}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <TouchableOpacity onPress={() => scrollToTop(item)} style={{ margin: 10 }}>
                    <Image
                      source={{
                        uri:
                          item?.imagesOfProductByModel[0]?.url
                            ?
                            item?.imagesOfProductByModel[0]?.url
                            : '',
                      }}
                      style={{ height: 105, width: 105, borderRadius: 0 }}
                    />
                    <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 16 }}>
                      {item?.product?.productBrandName}
                    </Text>
                    <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 16 }}>
                      ₹ {item?.priceOfProductWithModelCommission}
                    </Text>
                  </TouchableOpacity>
                )}
              />}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
      {
        loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View >
  );
}

function Profile(props) {
  const { image, name } = props;
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'flex-start',
        margin: 20,
      }}>
      <Image
        source={{
          uri: image
            ? image
            : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png',
        }}
        style={{ height: 45, width: 45, borderRadius: 30 }}
      />
      <Text
        style={{
          fontFamily: 'Roboto-Bold',
          fontSize: 18,
          alignSelf: 'center',
          color: '#000',
          marginHorizontal: 20,
        }}>
        {name}
      </Text>
    </View>
  );
}


function AddressComponent(props) {
  const { address, onChange, modal } = props;
  const [checked, setChecked] = React.useState(null);
  const [modalAddressVisible, setModalAddressVisible] = useState(false);
  const handleClick = (item) => {
    setChecked(item?.id);
    onChange(item);
    modal(false);
  }
  return (
    <ScrollView horizontal>
      <View style={{ flexDirection: "row", marginVertical: 30 }}>
        {address?.map((item) => (
          <TouchableOpacity onPress={() => handleClick(item)} style={{ height: 150, borderWidth: 1, borderColor: "#000", width: 200, marginHorizontal: 10, padding: 10 }}>
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
              <Text style={{ fontFamily: "Roboto-Bold", fontSize: 18, marginVertical: 10, }}>{item?.fullName}</Text>
              {checked === item?.id ?
                <View style={{ alignSelf: "center" }}>
                  <Icon2
                    name="check-circle"
                    size={20}
                  />
                </View> : null}
            </View>
            <Text style={{ fontFamily: "Roboto-Medium", fontSize: 15 }}>{item?.houseNo},{'\n'}{item?.areaColony},{'\n'}{item?.city},{'\n'}{item?.state},{item?.pincode}</Text>
          </TouchableOpacity>
        ))}
        <TouchableOpacity style={{ alignSelf: "center", justifyContent: "center", alignItems: "center", borderWidth: 1, borderColor: "#000", flex: 1, marginHorizontal: 10, padding: 20 }} onPress={() => setModalAddressVisible(true)}>
          <Text style={{ fontFamily: "Roboto-Bold", fontSize: 14 }}>+ Add Address </Text>
        </TouchableOpacity>
      </View>
      <View>
        <Modal
          isVisible={modalAddressVisible}
          onBackdropPress={() => setModalAddressVisible(false)}
          onSwipeComplete={() => setModalAddressVisible(false)}
          swipeDirection={['down']}
          style={styles.bottomModal}>
          <View style={styles.Address}>
            <AddAddressScreenLABL AddState="ProductAdd" setModalAddressVisible={setModalAddressVisible} />
          </View>
        </Modal>
      </View>
    </ScrollView>
  )
}