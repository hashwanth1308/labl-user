import{ 
    
  withdrawalAccountDetailsPostCheckSuccess,
  withdrawalAccountDetailsSetCount,
  withdrawalAccountDetailsCountCheckSuccess,
  withdrawalAccountDetailsDeleteCheckSuccess,
  withdrawalAccountDetailsFindOneCheckSuccess,
  withdrawalAccountDetailsSetOneData,
  withdrawalAccountDetailsGetCheckSuccess,
  withdrawalAccountDetailsSetData,
  withdrawalAccountDetailsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function withdrawalAccountDetailsPostRequest(element) {
      return dispatch => {
        dispatch(withdrawalAccountDetailsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `withdrawal-account-details`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(withdrawalAccountDetailsSetData(a));
            dispatch(withdrawalAccountDetailsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(withdrawalAccountDetailsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function withdrawalAccountDetailsCountRequest(element) {
      return dispatch => {
          dispatch(withdrawalAccountDetailsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `withdrawal-account-details/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(withdrawalAccountDetailsSetCount(data));
              dispatch(withdrawalAccountDetailsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(withdrawalAccountDetailsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function withdrawalAccountDetailsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(withdrawalAccountDetailsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `withdrawal-account-details/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(withdrawalAccountDetailsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(withdrawalAccountDetailsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function withdrawalAccountDetailsFindOneRequest(id) {
      return dispatch => {
        dispatch(withdrawalAccountDetailsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `withdrawal-account-details/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(withdrawalAccountDetailsOneData(data));
            dispatch(withdrawalAccountDetailsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(withdrawalAccountDetailsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function withdrawalAccountDetailsGetRequest(element) {
  return dispatch => {
    dispatch(withdrawalAccountDetailsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `withdrawal-account-details`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(withdrawalAccountDetailsSetOneData(data));
        dispatch(withdrawalAccountDetailsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(withdrawalAccountDetailsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function withdrawalAccountDetailsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(withdrawalAccountDetailsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `withdrawal-account-details/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(withdrawalAccountDetailsSetData(data));
              dispatch(withdrawalAccountDetailsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(withdrawalAccountDetailsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 