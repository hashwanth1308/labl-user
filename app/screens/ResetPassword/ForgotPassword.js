import React, { useState, useEffect } from 'react';
import { View, KeyboardAvoidingView, Platform, ActivityIndicator, Dimensions } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, TextInput, Button, Text } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { fpPostRequest, usersUpdateRequest } from '../../api/users';
import axios from 'axios';

export default function ForgotPassword({ navigation }) {
    const { colors } = useTheme();
    const { t } = useTranslation();
    const height = Dimensions.get('window').height;
    const width = Dimensions.get('window').width;
    const offsetKeyboard = Platform.select({
        ios: 0,
        android: 20,
    });
    const dispatch = useDispatch();
    const [mobile, setMobile] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('')
    const [loading, setLoading] = useState(false);
    const [success, setSuccess] = useState({ confirmPassword: true });
    const check = useSelector(state => state.usersReducer.fpPostCheckSuccess);
    console.log(check);


    /**
     * call when action reset pass
     */
    const onReset = async () => {
        if (mobile == '') {
            alert('PLease Enter your Registered Mobile Number')
        } else {
            setLoading1(true);
            const obj = {};
            obj.mobile = mobile;
            await axios.post('https://staging-backend.labl.store/users/', obj,
                {
                    headers: {
                        'Authorization':"Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYWM2NDg5ZjBhM2NkMGU2ZTcwNTJlMyIsImlhdCI6MTY3MTYxNjQ4MywiZXhwIjoxNjc0MjA4NDgzfQ.7-HthpNT8_knhGPhN_IUxfa7LXRFcBgPdP9omece8uY",
                        'content-type': 'application/json'
                    }
                }).then(({ data }) =>
                    console.log("res", data)
                ).catch((err) => {
                    // alert(err.response.data.message[0].messages[0].message)
                    console.log(err);
                });
            // await dispatch(fpPostRequest(mobile));
            //   navigation.navigate('ResetPassword')
            setLoading(false);
        }
    };
    if (check) {
        navigation.navigate('ResetPassword')
    }
    const [loading1, setLoading1] = useState(true);
    useEffect(() => {
        setTimeout(() => (
            setLoading1((prev) => (!prev))
        ), 1000)
    }, []);
    return (
        <View style={{ flex: 1 }}>
            <Header
                title={t('reset_password')}
                renderLeft={() => {
                    return (
                        <Icon
                            name="arrow-left"
                            size={20}
                            color={"#000"}
                            enableRTL={true}
                        />
                    );
                }}
                onPressLeft={() => {
                    navigation.goBack();
                }}
            />
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'android' ? 'height' : 'padding'}
                    keyboardVerticalOffset={offsetKeyboard}
                    style={{ flex: 1 }}>
                    <View
                        style={{
                            flex: 1,
                            padding: 20,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 25, color: "#000", fontFamily: "Montserrat-Bold", marginBottom: 30, position: "relative" }}>Forgot Password</Text>
                        {/* <TextInput
              onChangeText={text => setPassword(text)}
              onFocus={() => {
                setSuccess({
                  ...success,
                  password: true,
                });
              }}
              placeholder={t('input_password')}
              success={success.password}
              value={password}
            /> */}
                        <TextInput
                            onChangeText={text => setMobile(text)}
                            style={{ marginTop: 20, width: 322, height: 52, elevation: 5, borderRadius: 30, marginVertical: 20 }}
                            onFocus={() => {
                                setSuccess({
                                    ...success,
                                    mobile: true,
                                });
                            }}
                            placeholder={t('Mobile Number')}
                            success={success.mobile}
                            value={mobile}
                        />
                        <Button
                            style={{ marginTop: 20, width: 150 }}
                            //   full
                            onPress={() => {
                                onReset();
                            }}
                            loading={loading}>
                            {t('Next')}
                        </Button>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
            {loading1 &&
                <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
                    <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
                        <View>
                            <ActivityIndicator size={"large"} color="#000" />
                        </View>
                        <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
                    </View>
                </View>
            }
        </View>
    );
}
