import React, {useState} from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from 'react-native';
import {BaseStyle, BaseColor, Images, useTheme} from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Icon,
  ProfileDescription,
  ProfilePerformance,
  Tag,
  Text,
  Card,
  TourDay,
  TourItem,
  Button,
  PackageItem,
  RateDetail,
  CommentItem,
} from '@components';
import {TabView, TabBar} from 'react-native-tab-view';
import styles from './styles';
import {UserData, ReviewData, TourData, PackageData} from '@data';
import {useTranslation} from 'react-i18next';

export default function TourDetail({navigation}) {
  const {colors} = useTheme();
  const {t} = useTranslation();

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    //  {key: 'information', title: t('information')},
    {key: 'tour', title: t('Date')},
    {key: 'information', title: t('Product')},
    {key: 'package', title: t('Price')},
    {key: 'review', title: t('Earnings')},
  ]);
  const [userData] = useState(UserData[0]);

  // When tab is activated, set what's index value
  const handleIndexChange = index => setIndex(index);

  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, {backgroundColor: colors.primary}]}
      style={[styles.tabbar, {backgroundColor: colors.background}]}
      tabStyle={styles.tab}
      inactiveColor={BaseColor.grayColor}
      activeColor={colors.text}
      renderLabel={({route, focused, color}) => (
        <View style={{flex: 1, width: 130, alignItems: 'center'}}>
          <Text headline semibold={focused} style={{color}}>
            {route.title}
          </Text>
        </View>
      )}
    />
  );

  // Render correct screen container when tab is activated
  const renderScene = ({route, jumpTo}) => {
    switch (route.key) {
      case 'information':
        return <InformationTab jumpTo={jumpTo} navigation={navigation} />;
      case 'tour':
        return <TourTab jumpTo={jumpTo} navigation={navigation} />;
      case 'package':
        return <PackageTab jumpTo={jumpTo} navigation={navigation} />;
      case 'review':
        return <ReviewTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };

  return (
    <View style={{flex: 1}}>
      <Header
        title={t('Sale Details')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <ProfileDescription
          image={userData.image}
          name={userData.name}
          subName={userData.major}
          description={userData.address}
          style={{marginTop: -60, paddingHorizontal: 20}}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 20,
          }}>
          {/* <Tag primary style={{width: 80}}>
            + {t('follow')}
          </Tag> */}
          <View style={{flex: 1, paddingLeft: 10, paddingVertical: 5}}>
            <ProfilePerformance data={userData.performance} type="small" />
          </View>
        </View>
        <View style={{flex: 1}}>
          <TabView
            lazy
            navigationState={{index, routes}}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            onIndexChange={handleIndexChange}
          />
         
        </View>
      </SafeAreaView>
    </View>
  );
}

/**
 * @description Show when tab Information activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function InformationTab({navigation}) {
  const [tours] = useState(TourData);
  const [dayTour] = useState([
    
  ]);
  const [information] = useState([
    {title: 'Order1', detail: ''},
    {title: 'Location', detail: 'Hyderabad'},
    {title: 'Expected Delivery', detail: '2 Days'},
    {title: 'Departure', detail: '08:00'},
    {title: 'Order2', detail: ''},
    {title: 'Location', detail: 'Secunderabad'},
    {title: 'Expected Delivery', detail: '4 Days'},
    {title: 'Departure', detail: '06:00'},
    
    ,
  ]);
  
  const {colors} = useTheme();
  return (
    <ScrollView>
      
      <View style={{paddingHorizontal: 20}}>
        {information.map((item, index) => {
          return (
            
            <View
              style={[
                styles.lineInformation,
                {borderBottomColor: colors.border},
              ]}
              key={'information' + index}>
              <Text body2 grayColor>
                {item.title}
              </Text>
              <Text body2 semibold accentColor>
                {item.detail}
              </Text>
            </View>
          );
        })}
        
        
        
      </View>
      <View>
        
        <FlatList
          contentContainerStyle={{paddingLeft: 5, paddingRight: 20,}}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={dayTour}
          keyExtractor={(item, index) => item.id}
          renderItem={({item}) => (
            <TourDay
              image={item.image}
              day={item.day}
              title={item.title}
              description={item.description}
              style={{marginLeft: 15}}
              onPress={() => {}}
            />
          )}
        />
      </View>
      
      
      <View>
        
        
        <FlatList
          // contentContainerStyle={{paddingLeft: 5, paddingRight: 20}}
          // horizontal={true}
          // showsHorizontalScrollIndicator={false}
          // data={tours}
          // keyExtractor={(item, index) => item.id}
          renderItem={({item, index}) => (
            <TourItem
              grid
              style={[styles.tourItem, {marginLeft: 15}]}
              onPress={() => {
                navigation.navigate('TourDetail');
              }}
              // image={item.image}
              // name={item.name}
              // location={item.location}
              // travelTime={item.location}
              // startTime={item.startTime}
              // price={item.price}
              rate={item.rate}
              rateCount={item.rateCount}
              numReviews={item.numReviews}
              author={item.author}
              services={item.services}
            />
          )}
        />
      </View>
    </ScrollView>
  );
}

/**
 * @description Show when tab Tour activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function TourTab({navigation}) {
  return (
    <ScrollView>
   

    </ScrollView>
  );
}

/**
 * @description Show when tab Package activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function PackageTab({navigation}) {
  const [packageItem] = useState(PackageData[0]);
  const [packageItem2] = useState(PackageData[2]);

  return (
    <ScrollView>
      
      
    </ScrollView>
  );
}

/**
 * @description Show when tab Review activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function ReviewTab({navigation}) {
  const [refreshing] = useState(false);
  // const [rateDetail] = useState({
  //   // point: 4.7,
  //   // maxPoint: 5,
  //   // totalRating: 25,
  //   data: ['80%', '10%', '10%', '0%', '0%'],
  // });
  const [reviewList] = useState(ReviewData);
  const {colors} = useTheme();

  return (
    <FlatList
      contentContainerStyle={{padding: 20}}
      refreshControl={
        <RefreshControl
          colors={[colors.primary]}
          tintColor={colors.primary}
          refreshing={refreshing}
          onRefresh={() => {}}
        />
      }
      data={reviewList}
      keyExtractor={(item, index) => item.id}
      ListHeaderComponent={() => (
        <RateDetail
          // point={rateDetail.point}
          // maxPoint={rateDetail.maxPoint}
          // totalRating={rateDetail.totalRating}
          // data={rateDetail.data}
        />
      )}
      renderItem={({item}) => (
        <CommentItem
          style={{marginTop: 10}}
          image={item.source}
          name={item.name}
          rate={item.rate}
          date={item.date}
          title={item.title}
          comment={item.comment}
        />
      )}
    />
  );
}
