import{ 
    
  labelsPostCheckSuccess,
  labelsSetCount,
  labelsCountCheckSuccess,
  labelsDeleteCheckSuccess,
  labelsFindOneCheckSuccess,
  labelsSetOneData,
  labelsGetCheckSuccess,
  labelsSetData,
  labelsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function labelsPostRequest(element) {
      return dispatch => {
        dispatch(labelsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `labels`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(labelsSetData(a));
            dispatch(labelsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(labelsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function labelsCountRequest(element) {
      return dispatch => {
          dispatch(labelsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `labels/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(labelsSetCount(data));
              dispatch(labelsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(labelsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function labelsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(labelsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `labels/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(labelsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(labelsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function labelsFindOneRequest(id) {
    console.log(id)
      return dispatch => {
        dispatch(labelsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `labels/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            console.log(response);
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            console.log(data);
            dispatch(labelsSetData(data));
            dispatch(labelsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(labelsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function labelsGetRequest(element) {
  return dispatch => {
    dispatch(labelsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `labels`,
        params:element,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
        
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(labelsSetData(data));
        dispatch(labelsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(labelsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function labelsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(labelsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `labels/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(labelsSetData(data));
              dispatch(labelsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(labelsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 