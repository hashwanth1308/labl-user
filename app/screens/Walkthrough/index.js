import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { AuthActions } from '@actions';
import { View, TouchableOpacity, ScrollView } from 'react-native';
import { SafeAreaView, Text, Button, Image } from '@components';
import styles from './styles';
import Swiper from 'react-native-swiper';
import { BaseColor, BaseStyle, Images, useTheme } from '@config';
import * as Utils from '@utils';
import { useTranslation } from 'react-i18next';

export default function Walkthrough({ navigation }) {
  const [loading, setLoading] = useState(false);
  const [scrollEnabled, setScrollEnabled] = useState(true);
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  /**
   * @description Simple authentication without call any APIs
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   */
  const authentication = () => {
    setLoading(true);
    dispatch(AuthActions.authentication(true, response => { }));
  };
  return (
    <SafeAreaView
      style={BaseStyle.safeAreaView}
      edges={['right', 'left', 'bottom']}>
      <ScrollView
        contentContainerStyle={styles.contain}
        scrollEnabled={scrollEnabled}
        onContentSizeChange={(contentWidth, contentHeight) =>
          setScrollEnabled(Utils.scrollEnabled(contentWidth, contentHeight))
        }>
        <Image source={Images.blackLogo} style={{ height: Utils.scaleWithPixel(140), width: Utils.scaleWithPixel(240), alignSelf: "center", marginTop: Utils.scaleWithPixel(190) }} />
        <View style={{ width: '100%' }}>
          <TouchableOpacity onPress={() => navigation.navigate("Walkthrough1")} style={{ backgroundColor: "#fff", width: 154, height: 36, top: 200, borderRadius: 50, alignItems: "center", justifyContent: "center", alignSelf: "center" }}>
            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 19, color: "#000" }}>
              Let's Explore
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
