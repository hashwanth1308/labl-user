import {
  MODELORDERS_POST_CHECK_SUCCESS,
  MODELORDERS_SET_COUNT,
  MODELORDERS_COUNT_CHECK_SUCCESS,
  MODELORDERS_DELETE_CHECK_SUCCESS,
  MODELORDERS_FINDONE_CHECK_SUCCESS,
  MODELORDERS_SET_ONE_DATA,
  MODELORDERS_GET_CHECK_SUCCESS,
  MODELORDERITEMS_GET_CHECK_SUCCESS,
  MODELORDERS_SET_DATA,
  MODELORDERITEMS_SET_DATA,
  MODELORDERS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  modelOrdersPostCheckSuccess: false,
  modelOrdersCount: 0,
  modelOrdersCountCheckSuccess: false,
  modelOrdersDeleteCheckSuccess: false,
  modelOrdersFindOneCheckSuccess: false,
  modelOrdersOneData: [],
  modelOrdersGetCheckSuccess: false,
  modelOrderItemsGetCheckSuccess: false,
  modelOrdersData: [],
  modelOrderItemsData: [],
  modelOrdersUpdateCheckSuccess: false,


};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MODELORDERS_POST_CHECK_SUCCESS:
      return {
        ...state,
        modelOrdersPostCheckSuccess: action.payload,
      };
    case MODELORDERS_SET_COUNT:
      return {
        ...state,
        modelOrdersCount: action.payload,
      };
    case MODELORDERS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        modelOrdersCountCheckSuccess: action.payload,
      };
    case MODELORDERS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        modelOrdersDeleteCheckSuccess: action.payload,
      };
    case MODELORDERS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        modelOrdersFindOneCheckSuccess: action.payload,
      };
    case MODELORDERS_SET_ONE_DATA:
      return {
        ...state,
        modelOrdersOneData: action.payload,
      };
    case MODELORDERS_GET_CHECK_SUCCESS:
      return {
        ...state,
        modelOrdersGetCheckSuccess: action.payload,
      };
    case MODELORDERITEMS_GET_CHECK_SUCCESS:
      return {
        ...state,
        modelOrderItemsGetCheckSuccess: action.payload,
      };
    case MODELORDERS_SET_DATA:
      return {
        ...state,
        modelOrdersData: action.payload,
      };
    case MODELORDERITEMS_SET_DATA:
      return {
        ...state,
        modelOrderItemsData: action.payload,
      };
    case MODELORDERS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        modelOrdersUpdateCheckSuccess: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
