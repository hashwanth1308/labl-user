import{ 
    
  productRatingPostCheckSuccess,
  productRatingSetCount,
  productRatingCountCheckSuccess,
  productRatingDeleteCheckSuccess,
  productRatingFindOneCheckSuccess,
  productRatingSetOneData,
  productRatingGetCheckSuccess,
  productRatingSetData,
  productRatingUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function productRatingPostRequest(obj) {
      return dispatch => {
        dispatch(productRatingPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `product-ratings`,
            data:obj,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(productRatingSetData(data));
            dispatch(productRatingPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log("err",error);
            console.log(error,axiosLogin)
            dispatch(productRatingPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function productRatingCountRequest(element) {
      return dispatch => {
          dispatch(productRatingCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `product-ratings/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(productRatingSetCount(data));
              dispatch(productRatingCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(productRatingCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function productRatingDeleteRequest(id) {
      
      return dispatch => {
          dispatch(productRatingDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `product-ratings/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(productRatingDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(productRatingDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function productRatingFindOneRequest(id) {
      return dispatch => {
        dispatch(productRatingFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `product-ratings/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(productRatingSetOneData(data));
            dispatch(productRatingFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(productRatingFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function productRatingGetRequest(obj) {
  return dispatch => {
    dispatch(productRatingGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `product-ratings`,
        params:obj,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(productRatingSetData(data));
        dispatch(productRatingGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(productRatingGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function productRatingUpdateRequest(id) {
     
      return dispatch => {
          dispatch(productRatingUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `product-ratings/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(productRatingSetData(data));
              dispatch(productRatingUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(productRatingUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 