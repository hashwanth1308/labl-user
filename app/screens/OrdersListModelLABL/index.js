import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import {
  SafeAreaView,
  Header,
  PostListItem,
  Button,
  Icon,
  OrderCardModel,
  StarRating,
  Image
} from '@components';
import { RadioButton } from 'react-native-paper';
import SegmentedControlTab from 'react-native-segmented-control-tab'
import SearchBar from 'react-native-dynamic-search-bar';
import _ from 'lodash';
import { Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import Modal from 'react-native-modal';
import { useTranslation } from 'react-i18next';
import { ordersGetRequest } from '../../api/orders';
import Icon3 from 'react-native-vector-icons/Ionicons';
import Icon4 from 'react-native-vector-icons/Feather';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSelector, useDispatch } from 'react-redux';
import { TabView, TabBar } from 'react-native-tab-view';
import { modelOrdersGetRequest } from '../../api/modelOrders';

export default function OrdersListModelLABL({ navigation }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const windowHeight = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;


  const [customStyleIndex, setCustomStyleIndex] = useState(0);

  const handleCustomIndexSelect = (index) => {
    setCustomStyleIndex(index);
  }

  const handleIndexChange = index => setIndex(index);
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: 'grid', title: 'My Orders', icon: 'view-grid-outline' },
    { key: 'list', title: 'User Orders', icon: 'format-list-bulleted' },
  ]);
  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, { backgroundColor: colors.primary }]}
      style={[styles.tabbar, { backgroundColor: colors.background }]}
      tabStyle={styles.tab}
      inactiveColor={BaseColor.grayColor}
      activeColor={colors.text}
      renderLabel={({ route, focused, color }) => (
        <View style={{ flex: 1, alignItems: 'center', width: 150 }}>
          <Text
            headline
            semibold={focused}
            style={{
              color,
              fontFamily: 'Montserrat-Bold',
              fontSize: 18,
              margin: 10,
            }}>
            {route.title}
          </Text>
        </View>
      )}
    />
  );

  // Render correct screen container when tab is activated
  const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'grid':
        return <BookingTab jumpTo={jumpTo} navigation={navigation} />;
      case 'list':
        return <SettingTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };

  const [modalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!modalVisible);
  };

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const modelOrdersData = useSelector(
    state => state.modelOrdersReducer?.modelOrdersData,
  );
  const userData = useSelector(state => state.accessTokenReducer.userData);
  // const loginModelData = useSelector(state => state.modelsReducer.modelsData);
  console.log(userData, 'userData');
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const obj = {};
    obj.model = userData?.user?.model?.id;
    dispatch(modelOrdersGetRequest(obj));
  }, [dispatch]);

  useEffect(() => {
    const obj = {};
    obj.model = userData?.user?.model?.id;
    dispatch(ordersGetRequest(obj));
  }, [dispatch])
  const ordersData = useSelector(state => state.ordersReducer.ordersData);
  console.log("userordersData", ordersData);

  const filteredData = (value, status) => {
    if (status === "All") {
      setChecked(value);
      const obj = {};
      obj.model = userData?.user?.model?.id;
      dispatch(modelOrdersGetRequest(obj));
      toggleModal();
    } else {
      setLoading1(true);
      setChecked(value);
      const obj = {};
      obj.status = status;
      obj.model = userData?.user?.model?.id;
      dispatch(modelOrdersGetRequest(obj));
      toggleModal();
      setLoading1(false);
    }
  }
  // const modelorderitems = ordersData.length > 0 && ordersData?.map((items) => {
  //   return items?.modelorderitems
  // })
  // console.log('ordersData', modelorderitems);
  // const orderView = async orderObject => {
  //   setLoading(true);
  //   // await dispatch(collectionsPostRequest(cName, profileImage,labelId));
  //   navigation.navigate('PreviewBooking', { orderObject });
  //   setLoading(false);
  // };
  const [checked, setChecked] = React.useState('first');
  const [checked1, setChecked1] = React.useState('first');
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  return (
    <View style={{ flex: 1 }}>
      {/* <View style={{ flex: 1, backgroundColor: "#000", height: 140, position: "absolute", width: "100%", marginBottom: 10 }}> */}
      <Header
        // title={t('My Orders')}
        style={{ height: 70, backgroundColor: '#000' }}
        renderLeft={() => {
          return (
            <View style={{ flex: 1, justifyContent: 'space-around' }}>
              <View
                style={{
                  flexDirection: 'row',
                  width: 200,
                  justifyContent: 'space-between',
                  alignSelf: 'flex-start',
                }}>
                <View
                  style={{
                    height: 30,
                    width: 30,
                    borderRadius: 30,
                    backgroundColor: '#fff',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Icon
                    name="arrow-left"
                    size={20}
                    color={colors.primary}
                    enableRTL={true}
                  />
                </View>
                <Text
                  style={{
                    alignSelf: 'center',
                    fontFamily: 'Montserrat-Bold',
                    fontSize: 20,
                    color: '#fff',
                    right: 40,
                  }}>
                  Orders List
                </Text>
              </View>
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

      // renderRight={() => {
      //   return <Icon name="shopping-cart" size={24} color={colors.primary} />;
      // }}
      // onPressRight={() => {
      //   navigation.navigate('Cart');
      // }}
      />

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: '#000',
          height: 60,
          padding: 20,
        }}>
        <SearchBar
          placeholder="Search all orders"
          onPress={() => alert('onPress')}
          onChangeText={text => console.log(text)}
          style={{ alignSelf: 'center', width: 250 }}
        />
        <TouchableOpacity
          onPress={toggleModal}
          style={{
            alignSelf: 'center',
            flexDirection: 'row',
            height: 38,
            width: 100,
            backgroundColor: '#fff',
            borderRadius: 10,
            justifyContent: 'space-evenly',
            alignItems: 'center',
            marginHorizontal: 10,
          }}>
          <Text
            style={{
              fontFamily: 'Montserrrat-Medium',
              fontSize: 20,
              color: '#000',
            }}>
            Filter
          </Text>
          <Icon1 name="sliders" size={18} color="#000" />
        </TouchableOpacity>
      </View>
      {/* </View> */}
      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        <View>
          <Modal
            isVisible={modalVisible}
            onBackdropPress={() => setModalVisible(false)}
            onSwipeComplete={() => setModalVisible(false)}
            swipeDirection={['down']}
            style={styles.bottomModal}>
            <View style={styles.contain}>
              <TouchableOpacity
                onPress={toggleModal}
                style={{
                  height: 50,
                  width: '100%',
                  backgroundColor: '#fff',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  padding: 10,
                }}>
                <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 18 }}>
                  Filter Orders
                </Text>
                <Icon2 name="close" size={28} />
              </TouchableOpacity>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                  marginVertical: 5,
                  height: 250,
                  width: '100%',
                  backgroundColor: '#fff',
                }}>
                <Text
                  style={{
                    top: Utils.scaleWithPixel(15),
                    fontSize: 20,
                    fontFamily: 'Montserrat-Bold',
                    marginBottom: 30,
                    marginHorizontal: 20,
                  }}>
                  Status
                </Text>
                <View style={{ marginHorizontal: 20 }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <RadioButton
                      value="first"
                      color="#000"
                      status={checked === 'first' ? 'checked' : 'unchecked'}
                      onPress={() => filteredData('first', 'All')}
                    />
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 18,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      All
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <RadioButton
                      value="second"
                      color="#000"
                      status={checked === 'second' ? 'checked' : 'unchecked'}
                      onPress={() => filteredData('second', 'Shipped')}
                    />
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 18,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      Shipped
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <RadioButton
                      value="third"
                      color="#000"
                      status={checked === 'third' ? 'checked' : 'unchecked'}
                      onPress={() => filteredData('third', 'Delivered')}
                    />
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 18,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      Delivered
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <RadioButton
                      value="fourth"
                      color="#000"
                      status={checked === 'fourth' ? 'checked' : 'unchecked'}
                      onPress={() => filteredData('fourth', 'Cancelled')}
                    />
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 18,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      Cancelled
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <RadioButton
                      value="fifth"
                      color="#000"
                      status={checked === 'fifth' ? 'checked' : 'unchecked'}
                      onPress={() => filteredData('fifth', 'Returned')}
                    />
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 18,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      Returned
                    </Text>
                  </View>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                  height: 210,
                  width: '100%',
                  backgroundColor: '#fff',
                  marginBottom: 5,
                }}>
                <Text
                  style={{
                    top: Utils.scaleWithPixel(15),
                    fontSize: 20,
                    fontFamily: 'Montserrat-Bold',
                    marginBottom: 30,
                    marginHorizontal: 20,
                  }}>
                  Time
                </Text>
                <View style={{ marginHorizontal: 20 }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <RadioButton
                      value="first"
                      color="#000"
                      status={checked1 === 'first' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked1('first')}
                    />
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 18,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      Anytime
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <RadioButton
                      value="second"
                      color="#000"
                      status={checked1 === 'second' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked1('second')}
                    />
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 18,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      last 30 days
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <RadioButton
                      value="third"
                      color="#000"
                      status={checked1 === 'third' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked1('third')}
                    />
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 18,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      Last 6 months
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <RadioButton
                      value="fourth"
                      color="#000"
                      status={checked1 === 'fourth' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked1('fourth')}
                    />
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 18,
                        alignSelf: 'center',
                        marginLeft: 20,
                      }}>
                      Last year
                    </Text>
                  </View>
                </View>
              </View>

            </View>
          </Modal>
        </View>
        <ScrollView
          onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
          scrollEventThrottle={8}>
          {/* <TabView
            // lazy
            navigationState={{ index, routes }}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            onIndexChange={handleIndexChange}
            style={{ height: windowHeight * 5, marginTop: 0 }}
          /> */}
          <SegmentedControlTab
            values={['My Orders', 'User Orders']}
            selectedIndex={customStyleIndex}
            onTabPress={(index) => handleCustomIndexSelect(index)}
            borderRadius={0}
            tabsContainerStyle={{ height: 50 }}
            tabStyle={{ backgroundColor: 'transparent', borderWidth: 0, borderColor: 'transparent' }}
            activeTabStyle={{ borderBottomColor: "#000", borderBottomWidth: 1, backgroundColor: "transparent" }}
            tabTextStyle={{ color: '#CDCDCD', fontWeight: 'bold', fontSize: 20 }}
            activeTabTextStyle={{ color: '#000' }}
          />
          {customStyleIndex === 0
            && <ScrollView>
              <View
                style={{
                  marginVertical: 30,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <FlatList
                  data={modelOrdersData}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item }) => {
                    return (
                      <OrderCardModel
                        orderItem={item}
                        image={item?.product?.productImages[0]?.url}
                        name={item?.product?.productName}
                        productName={item?.product?.productBrandName}
                        size={item?.productVarient?.size?.type}
                        quantity={item?.quantity}
                        color={item?.productVarient?.color?.color}
                        status={item.status}
                        orderId={item?.modelOrder?.modelOrderId}
                        time={item?.createdAt}
                      />
                    );
                  }}
                />
              </View>
            </ScrollView>
          }
          {customStyleIndex === 1
            && <ScrollView>
              <View
                style={{
                  marginVertical: 30,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <FlatList
                  data={ordersData}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item }) => {
                    return (
                      <OrderComp
                        orderItem={item}
                        image={item?.modelProduct?.product?.productImages[0]?.url}
                        name={item?.modelProduct?.product?.productName}
                        productName={item?.modelProduct?.product?.productBrandName}
                        size={item?.productVarient?.size?.type}
                        quantity={item?.quantity}
                        color={item?.productVarient?.color?.color}
                        status={item.status}
                        orderId={item?.order?.orderId}
                        time={item?.createdAt}
                      />
                    );
                  }}
                />
              </View>
            </ScrollView>
          }
        </ScrollView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: windowHeight, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}

function OrderComp(props) {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const [rate, setRate] = useState(0);
  const { style, name, size, color, orderId, time, image, quantity, onPress, status, productName, orderItem } = props;
  console.log("orderItem", orderItem);
  return (
    <View style={[styles.content, { borderRadius: 20, borderColor: colors.border }, style]}>
      <View style={{ flexDirection: "row", height: 100, width: 347, backgroundColor: "#000", borderTopLeftRadius: 20, borderTopRightRadius: 20, alignItems: "center", justifyContent: "flex-start" }}>
        {orderItem?.status == "Delivered" ?
          <View style={{ marginLeft: 20, height: 40, width: 40, borderRadius: 30, backgroundColor: "#909090", justifyContent: "center", alignItems: "center" }}>
            <Icon3 name="swap-horizontal" size={25} color="#fff" />
          </View> : <View style={{ marginLeft: 20, height: 40, width: 40, borderRadius: 30, backgroundColor: "#909090", justifyContent: "center", alignItems: "center" }}>
            <Icon4 name="package" size={25} color="#fff" />
          </View>}
        <View style={{ marginLeft: 20, alignSelf: "center" }}>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22, color: "#fff", marginVertical: 5 }}>{orderId}</Text>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20, color: "#fff" }}>{(new Date(time).toLocaleString())}</Text>
          </View>
        </View>
      </View>
      <TouchableOpacity
        // style={[styles.content, { borderColor: colors.border }, style]}
        onPress={onPress}
        activeOpacity={0.9}>
        <View
          style={{
            padding: 10,
            flexDirection: 'row',
          }}>
          <Image source={{uri : image}} style={styles.imageBanner} />
          <View style={{ flex: 1, alignItems: 'flex-start', margin: 20 }}>
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, width: 150,height:32 }}>
              {name}
            </Text>
            <Text blackColor style={{ fontFamily: "Montserrat-Medium", fontSize: 17, marginVertical: 5 }}>
              {productName}
            </Text>
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 14, marginBottom: 5 }}>
              Quantity : {quantity}
            </Text>
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 14 }}>
              Size : {size}
            </Text>
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 14, marginVertical: 10, width: 200 }}>
              Color : {color}
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: "row", alignSelf: "center", marginBottom: 20 }}>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000" }}>Status :- {' '}</Text>
          {status === "Cancelled" ?
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "red" }}>{status}</Text> :
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: status === "Order Not Placed" ? "orange" : "green" }}>{status}</Text>}
        </View>
        {/* {status !== "Delivered" ?
          <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
            <TouchableOpacity onPress={() => orderShipped("Cancelled")} style={{ borderRadius: 5, borderWidth: 1.5, height: 40, width: 140, borderColor: "#000", justifyContent: "center", alignItems: "center" }} >
              <Text style={{ color: "#000", fontFamily: "Montserrat-SemiBold", fontSize: 22 }}>Cancel</Text>
            </TouchableOpacity>
          </View> :
          <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
            <TouchableOpacity style={{ borderRadius: 5, borderWidth: 1.5, height: 40, width: 140, borderColor: "#000", justifyContent: "center", alignItems: "center" }} >
              <Text style={{ color: "#000", fontFamily: "Montserrat-SemiBold", fontSize: 22 }}>Exchange</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => orderShipped("Returned")} style={{ borderRadius: 5, borderWidth: 1.5, height: 40, width: 140, borderColor: "#000", justifyContent: "center", alignItems: "center" }} >
              <Text style={{ color: "#000", fontFamily: "Montserrat-SemiBold", fontSize: 22 }}>Return</Text>
            </TouchableOpacity>
          </View>} */}
        {/* <View style={{ width: "100%", borderWidth: 0.5, borderColor: "#000", marginBottom: 10 }}></View>
        <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
          <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20, color: "#000", alignSelf: "flex-start" }}>Rate Product : </Text>
          <View style={{ width: 130 }}>
            <StarRating starSize={20}
              maxStars={5}
              rating={rate}
              selectedStar={rating => {
                setRate(rating);
              }}
              fullStarColor={BaseColor.yellowColor}
              containerStyle={{ paddingBottom: 25 }} />
          </View>
        </View> */}
      </TouchableOpacity>
    </View>
  );
}