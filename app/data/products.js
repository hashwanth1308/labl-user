import {Images} from '@config';
import {UserData} from './user';
const ProductsData = [
  {
    id: '1',
    // image: Images.profile9,
    image: Images.product,
    title1: '200 ',
    title2: 'Camera',
  },
  {
    id: '2',
    image: Images.product1,
    title1: '500',
    title2: 'PC & Laptops',
  },
  {
    id: '3',
    image: Images.product2,
    title1: '550',
    title2: 'Beauty Products',
  },
  {
    id: '4',
    image: Images.product3,
    title1: '200',
    title2: 'Hair Products',
  },
  {
    id: '5',
    image: Images.product,
    title1: '1000',
    title2: 'Mobiles',
  },
];

export {ProductsData};