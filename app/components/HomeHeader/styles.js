import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  contain: {height: 45, flexDirection: 'row',width:"100%"},
  contentLeft: {
    flex: 1,
    marginTop:25,
    justifyContent: 'center',
    paddingHorizontal: 0,
    width: "100%",
  },
  contentCenter: {
    flex: 2,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  contentRight: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingLeft: 10,
    paddingRight: 0,
    height: '100%',
  },
  contentRightSecond: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingLeft: 10,
    paddingTop: 5,
    paddingRight: 10,
    height: '100%',
  },
  right: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    right:20,
    top:10
  },
});
