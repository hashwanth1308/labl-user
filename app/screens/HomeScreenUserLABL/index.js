import React, { useEffect, useMemo, useRef, useState } from 'react';
import {
  View,
  Animated,
  RefreshControl,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  useWindowDimensions,
} from 'react-native';
// import { TouchableOpacity } from 'react-native-gesture-handler';
import SegmentedControlTab from 'react-native-segmented-control-tab'
import {
  HotelItem,
  SafeAreaView,
  HomeHeader,
  Text,
  Image,
  Button,
  ListThumbCircle,
  PostItem,
  ProfileAuthor,
} from '@components';
import Geolocation from '@react-native-community/geolocation';
import Modal from 'react-native-modal';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Avatar } from 'react-native-elements';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
// import { modelProductsGetRequest } from '../../api/modelProducts';
import { categoriesGetRequest } from '../../api/categories';
import SearchBar from 'react-native-dynamic-search-bar';
import { UserData } from '../../data/user';
import { HotelData } from '../../data/hotel';
import { TabView, TabBar } from 'react-native-tab-view';
import { userFollowingGetRequest } from '../../api/following';
import { statusGetRequest } from '../../api/status';
import { StoryContainer } from 'react-native-stories-view';
import { logout } from '../../actions';
import { lablExclusiveGetRequest, lablPromotedGetRequest } from '../../api/products';
import Geocoder from 'react-native-geocoding';
import axios from 'axios';

export default function HomeScreenUserLABL({ navigation }) {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [users] = useState(UserData);
  const width = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const userData = useSelector(state => state.accessTokenReducer.userData);
  console.log(userData, 'userdata');
  const [refreshing] = useState(false);
  const [search, setSearch] = useState('');
  const onChangeSort = () => { };
  Geocoder.init('AIzaSyA8rJ9jrXHQHgbGqcTq00XemqeIhXVDC0s');


  Geolocation.getCurrentPosition(
    position => {
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;
      console.log(latitude, longitude);
      Geocoder.from(latitude, longitude)
        .then(json => {
          const addressComponent = json.results[0].address_components;
          const formattedAddress = json.results[0].formatted_address;
          // console.log("address",addressComponent, formattedAddress);
        })
        .catch(error => console.log(error));
    },
    error => {
      console.log(error);
    },
    { enableHighAccuracy: true, timeout: 20000 }
  );
  const [banners, setBanners] = useState([]);
  useEffect(async () => {
    await axios.get("https://staging-backend.labl.store/banners?type=Mobile", {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${userData?.jwt}`
      }
    }).then((res) => {
      setBanners(res?.data)
    }).catch((err) => console.log(err))
  }, [])

  const obj = {};
  obj.user = userData?.user?.id;

  // useEffect(() => {
  //   dispatch(userFollowingGetRequest(obj));
  // }, [dispatch]);
  // useEffect(() => {
  //   dispatch(statusGetRequest());
  // }, [dispatch]);

  // const statuses = useSelector(state => state.statusReducer.statusOneData);
  // // console.log('statuses', statuses);
  // const imgs = [];
  // statuses.forEach(item => imgs.push({ it: item?.image }));
  // console.log('imags', imgs);

  const [customStyleIndex, setCustomStyleIndex] = useState(0);

  const handleCustomIndexSelect = (index) => {
    setCustomStyleIndex(index);
  }

  // console.log('follow', userFollowings);

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: 'grid', title: t('Grid'), icon: 'view-grid-outline' },
    { key: 'list', title: t('List'), icon: 'format-list-bulleted' },
  ]);
const [AllProducts,setAllProducts] = useState([]);
console.log("AllProducts",AllProducts);

  const dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch(modelProductsGetRequest());
  // }, [dispatch]);
  // useEffect(() => {
  //   const obj = {};
  //   obj._limit = -1;
  //   dispatch(lablExclusiveGetRequest(obj));
  // }, [dispatch]);
  useEffect(async() => {
    await axios.get('https://staging-backend.labl.store/allproductfilter',{
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userData.jwt}`,
      }
    }).then((res) => setAllProducts(res?.data))
    .catch((err) => console.log(err))
  }, [dispatch]);

  useEffect(async() => {
   await axios.get('https://staging-backend.labl.store/lablexlusivefilter',{
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userData.jwt}`,
      }
    }).then((res) => console.log("res---->",res?.data))
    .catch((err) => console.log(err))
  }, [dispatch]);
  useEffect(() => {
    dispatch(categoriesGetRequest());
  }, [dispatch]);
  // console.log('modelproductsData', productsData);

  const [showButton, setShowButton] = useState(false);
  const flatListRef = useRef(null);

  // const handleScroll = (event) => {
  //   const offsetY = event.nativeEvent.contentOffset.y;
  //   const visibleHeight = event.nativeEvent.layoutMeasurement.height;
  //   const contentHeight = event.nativeEvent.contentSize.height;
  //   const isScrolledToEnd = offsetY + visibleHeight >= contentHeight - 20;
  //   setShowButton(isScrolledToEnd);
  // };
  const handleScroll = (event) => {
    const offsetY = event.nativeEvent.contentOffset.y;
    setShowButton(offsetY > 0);
  };

  const handlePress = () => {
    flatListRef.current.scrollTo({ x: 0, y: 0, animated: true });
  };
  const [modeView, setModeView] = useState('grid');

  const scrollAnim = new Animated.Value(0);
  const offsetAnim = new Animated.Value(0);
  const clampedScroll = Animated.diffClamp(
    Animated.add(
      scrollAnim.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
        extrapolateLeft: 'clamp',
      }),
      offsetAnim,
    ),
    0,
    40,
  );

  const [modalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    console.log('openn---');
    setModalVisible(!modalVisible);
  };
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => setLoading1(prev => !prev), 1000);
  }, []);
  const layout = useWindowDimensions();
  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        {/* {renderModal()} */}
        <ScrollView ref={flatListRef} onScroll={handleScroll}>
          <View
            style={{
              flex: 1,
              backgroundColor: '#000',
              height: 120,
              // position: 'absolute',
              width: '100%',
              marginBottom: -5,
            }}>
            <HomeHeader
              title={userData?.user?.username}
              // subTitle="24 Dec 2018, 2 Nights, 1 Room"
              renderLeft={() => {
                return (
                  // <TouchableOpacity onPress={() => {
                  //   navigation.navigate('Profile');
                  // }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Avatar
                      rounded
                      style={{ height: 32, width: 32 }}
                      onPress={() => {
                        navigation.navigate('Profile');
                      }}
                      source={{
                        uri:
                          userData?.user?.image ? userData?.user?.image : "https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png"
                      }}
                    />
                    <Text numberOfLines={1}
                      style={{
                        width: 200,
                        marginHorizontal: 10,
                        alignSelf: 'center',
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 20,
                        color: '#fff',
                      }}>
                      {userData?.user?.username}
                    </Text>
                  </View>
                );
              }}
              // renderRight={() => {
              //   return (
              //     <View>
              //       <TouchableOpacity
              //         onPress={() => onLogOut()}
              //         style={{
              //           flexDirection: 'row',
              //           backgroundColor: '#fff',
              //           // flex:1,
              //           height:30,
              //           padding:5,
              //           borderRadius: 30,
              //           marginBottom: -10,
              //           justifyContent: 'center',
              //         }}>
              //         <Text
              //           style={{
              //             fontFamily: 'Montserrat-SemiBold',
              //             fontSize: 16,
              //             color: '#000',
              //             alignSelf: 'center',
              //           }}>
              //           User
              //         </Text>
              //         <Icon
              //           name="chevron-down"
              //           size={15}
              //           color="#000"
              //           style={{ alignSelf: 'center' }}
              //         />
              //       </TouchableOpacity>
              //     </View>
              //   );
              // }}
              renderRight={() => {
                return <Icon name="bell" color="#CDCDCD" size={24} />;
              }}
              // onPressLeft={() => {
              //   navigation.goBack();
              // }}
              onPressRight={() => {
                navigation.navigate('Notification');
              }}
            />
            {/* <SearchBar
              placeholder="Search here"
              onPress={() => navigation.navigate("SearchHistory")}
              // onChangeText={text => console.log(text)}
              style={{ alignSelf: 'center', top: 35 }}
            /> */}
            <TouchableOpacity onPress={() => navigation.navigate("SearchHistory")} style={{ padding: 10 }}>
              <Image source={require('../../assets/images/search.png')} style={{ width: 360, height: 35, borderRadius: 20, alignSelf: "center", marginVertical: 20 }} />
            </TouchableOpacity>
          </View>

          {/* <View
            style={{
              height: 125,
              width: '100%',
              elevation: 5,
              backgroundColor: '#fff',
              top: 150,
              marginBottom: -170,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  height: 70,
                  width: 70,
                  borderWidth: 1,
                  borderColor: '#CDCDCD',
                  borderRadius: 30,
                  alignItems: 'center',
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}>
                <Image
                  source={{
                    uri: 'https://smallimg.pngkey.com/png/small/52-522921_kathrine-vangen-profile-pic-empty-png.png',
                  }}
                  style={{ height: 70, width: 70, borderRadius: 30 }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'Montserrat-Medium',
                  fontSize: 18,
                  color: '#000',
                  alignSelf: 'center',
                }}>
                {UserData?.user?.username}
              </Text>
            </View>
            <FlatList
              horizontal
              data={userFollowings}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => {
                return (
                  <TouchableOpacity
                    onPress={() => toggleModal()}
                    style={[styles.contain, { borderBottomColor: colors.border }]}
                    activeOpacity={0.9}>
                    {item?.model?.statuses?.length > 0 ? (
                      <View
                        style={{
                          height: 70,
                          width: 70,
                          borderRadius: 38,
                          borderWidth: 1,
                          borderColor: 'green',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Image
                          source={{
                            uri:
                              `https://staging-backend.labl.store` +
                                item?.model?.users?.image?.url
                                ? `https://staging-backend.labl.store` +
                                item?.model?.users?.image?.url
                                : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png',
                          }}
                          style={styles.thumb}
                        />
                      </View>
                    ) : (
                      <View
                        style={{
                          height: 68,
                          width: 68,
                          borderRadius: 34,
                          borderWidth: 1,
                          borderColor: '#CDCDCD',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Image
                          source={{
                            uri:
                              `https://staging-backend.labl.store` +
                                item?.model?.users?.image?.url
                                ? `https://staging-backend.labl.store` +
                                item?.model?.users?.image?.url
                                : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png',
                          }}
                          style={styles.thumb}
                        />
                      </View>
                    )}
                    <Text
                      numberOfLines={1}
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 14,
                        textAlign: 'center',
                      }}>
                      {item?.model?.users?.username}
                    </Text>
                    <View>
                      <Modal
                        isVisible={modalVisible}
                        onSwipeComplete={() => setModalVisible(false)}
                        onBackdropPress={() => setModalVisible(false)}
                        swipeDirection={['down']}
                        anim
                        style={styles.bottomModal}>
                        <View style={styles.contain1}>
                          <Swiper
                            dotStyle={{
                              backgroundColor: BaseColor.dividerColor,
                              // top:-100
                            }}
                            autoplay={true}
                            autoplayTimeout={3}
                            showsPagination={true}
                            // showsButtons={true}
                            scrollEnabled={true}
                            // showsHorizontalScrollIndicator={true}
                            activeDotColor={colors.primary}
                            paginationStyle={{ marginBottom: 0 }}
                            removeClippedSubviews={false}>
                            {statuses?.map((item, index) => {
                              // if (item?.model?.id.includes(object?.model?.id)) {
                              return (
                                <View style={styles.slide}>
                                  <Image
                                    source={{
                                      uri:
                                        `https://staging-backend.labl.store` +
                                        item?.image?.url,
                                    }}
                                    style={{ flex: 0.8, width: '100%' }}
                                  />
                                </View>
                              );
                            })}
                          </Swiper>
                        </View>
                      </Modal>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
          </View> */}
          <Swiper
            // dotStyle={{
            //   backgroundColor: BaseColor.dividerColor,
            //   // top:-100
            // }}
            height={140}
            autoplay={true}
            showsPagination={false}
            // activeDotColor={colors.primary}
            // paginationStyle={styles.contentPage}
            removeClippedSubviews={false}>
            {banners?.map((item, index) => {
              return (
                <View style={styles.slide} key={item.key}>
                  <Image source={{ uri: item?.image }} style={styles.img} />
                </View>
              );
            })}
          </Swiper>

          <View style={{ margin: 5, marginVertical: 10 }}>
            {/* <SegmentedControlTab
              values={['GRID', 'LIST']}
              selectedIndex={customStyleIndex}
              onTabPress={(index) => handleCustomIndexSelect(index)}
              borderRadius={0}
              tabsContainerStyle={{ height: 50, backgroundColor: '#000' }}
              tabStyle={{ backgroundColor: '#CDCDCD', borderWidth: 0, borderColor: 'transparent' }}
              activeTabStyle={{ backgroundColor: '#000', marginTop: 2 }}
              tabTextStyle={{ color: '#000', fontWeight: 'bold', fontSize: 20 }}
              activeTabTextStyle={{ color: '#fff' }}
            /> */}
            {/* {customStyleIndex === 0
              && <FlatList
                contentContainerStyle={{
                  // paddingLeft: 5,
                  // paddingRight: 20,
                  paddingTop: 10,
                }}
                numColumns={3}
                data={productsData}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  // console.log("https://staging-backend.labl.store" + item?.product?.productImages[0]?.url, "imagesss")
                  // <HotelItem
                  // // block
                  //   image={
                  //     item?.imagesOfProductByModel[0]?.url
                  //   }
                  //   name={item?.name}
                  //   location={item.location}
                  //   price={item.price}
                  //   available={item.available}
                  //   rate={item.rate}
                  //   rateStatus={item.rateStatus}
                  //   numReviews={item.numReviews}
                  //   services={item.services}
                  //   // style={{ marginLeft: 15, marginBottom: 20 }}
                  //   onPress={() => navigation.navigate('PostDetail', { object: item })}
                  // />
                  <TouchableOpacity onPress={() => navigation.navigate('PostDetail', { object: item })}>
                    <Image source={{uri :  item?.imagesOfProductByModel[0]?.url}} style={{height:130,width:width/3}} />
                  </TouchableOpacity>
                )}
              />}
            {customStyleIndex === 1
              && <FlatList
                contentContainerStyle={{
                  flex: 1,
                  // paddingLeft: 5,
                  // paddingRight: 20,
                  paddingTop: 20,
                }}
                // numColumns={3}
                vertical
                data={productsData}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <View
                    style={{
                      // height: '100%',
                      width: '100%',
                      borderColor: '#CDCDCD',
                      borderRadius: 10,
                      borderWidth: 1,
                      marginBottom: 30,
                    }}>
                    <PostItem
                      image={
                        item?.imagesOfProductByModel[0]?.url
                      }
                      title={item?.product?.productBrandName}
                      description={item?.product?.productName}
                      likes={item?.likes?.length}
                      commentsLength={item?.comments}
                      style={{ margin: 10 }}
                      onPress={() => navigation.navigate('PostDetail', { object: item })}
                      object={item}
                      commentNavigate={() =>
                        navigation.navigate('Comments', { object: item })
                      }>
                      <ProfileAuthor
                        image={
                          item?.model?.users?.image ? item?.model?.users?.image : ""
                        }
                        name={item?.model?.users?.username}
                        textRight={item.name}
                        description={item?.createdAt}
                        object={item}
                        style={{ paddingHorizontal: 0 }}
                        onPress={() =>
                          navigation.navigate('Profile5', { object: item?.model })
                        }
                      />
                    </PostItem>
                  </View>
                )}
              />} */}
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", margin: 5 }}>
              {/* <View style={{ width: 10,borderRadius:10, backgroundColor:"#000",height:10,alignSelf:"center" }} /> */}
              <Text style={{ fontFamily: "Roboto-Bold", fontSize: 16, }}>LABL EXCLUSIVES</Text>
              <TouchableOpacity onPress={() => navigation.navigate('UserProductsScreenLABL',{productObject:{lablExclusive:true}})} style={{ height: 30, width: 100, alignSelf: "center",borderWidth:1,borderColor:"#CDCDCD", justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontFamily: "Roboto-Bold", fontSize: 14, color: "#000" }}>View more</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              contentContainerStyle={{
                // paddingLeft: 5,
                // paddingRight: 20,
                paddingTop: 0,
              }}
              numColumns={2}
              data={AllProducts[1]?.lablExclusive}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <TouchableOpacity onPress={() => navigation.navigate('PostDetail', { object: item?.product,name:"lablExclusive" })} style={{ marginBottom: 0, borderRightWidth: 1,backgroundColor:"#000", borderRightColor: "#CDCDCD", borderBottomWidth: 1 }}>
                  <Image source={{ uri: item?.product?.productImages[0]?.url }} style={{ height: 200, width: width / 2 }} />
                  <View style={{ margin: 5 }}>
                    <Text numberOfLines={0} style={{ fontSize: 14, fontFamily: "Roboto-Bold", width: 170, height: 15,color:"#fff" }}>{item?.product?.productName}</Text>
                    <Text style={{ fontSize: 12, fontFamily: "Roboto-Regular",color:"#fff" }}>{item?.product?.productBrandName}</Text>
                    <Text style={{ fontSize: 12, fontFamily: "Roboto-Bold",color:"#fff" }}>₹ {item?.product?.productPrice}</Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
          <View style={{ margin: 5 }}>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", margin: 5}}>
              <Text style={{ fontFamily: "Roboto-Bold", fontSize: 16 }}>LABL PROMOTED</Text>
              <TouchableOpacity  onPress={() => navigation.navigate('UserProductsScreenLABL',{productObject:{lablPromoted:true}})} style={{ height: 30, width: 100, alignSelf: "center",borderWidth:1,borderColor:"#CDCDCD", justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontFamily: "Roboto-Bold", fontSize: 14, color: "#000" }}>View more</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              contentContainerStyle={{
                // paddingLeft: 5,
                // paddingRight: 20,
                paddingTop: 0,
              }}
              numColumns={2}
              data={AllProducts[3]?.lablPromoted}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <TouchableOpacity onPress={() => navigation.navigate('PostDetail', { object: item?.product,name:"lablPromoted"  })} style={{ borderRightWidth: 1, borderRightWidth: 1,backgroundColor:"#CDCDCD", borderRightColor: "#000", borderBottomColor: "#000", borderBottomWidth: 1 }}>
                  <Image source={{ uri: item?.product?.productImages[0]?.url }} style={{ height: 200, width: width / 2 }} />
                  <View style={{ margin: 5 }}>
                    <Text style={{ fontSize: 14, fontFamily: "Roboto-Bold", width: 170, height: 15 }}>{item?.product?.productName}</Text>
                    <Text style={{ fontSize: 12, fontFamily: "Roboto-Regular" }}>{item?.product?.productBrandName}</Text>
                    <Text style={{ fontSize: 12, fontFamily: "Roboto-Bold" }}>₹ {item?.product?.productPrice}</Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
          <View style={{ margin: 5 }}>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", margin: 5}}>
              <Text style={{ fontFamily: "Roboto-Bold", fontSize: 16 }}>LABL FRANCHISE</Text>
              <TouchableOpacity  onPress={() => navigation.navigate('UserProductsScreenLABL',{productObject:{lablFranchise:true}})} style={{ height: 30, width: 100, alignSelf: "center",borderWidth:1,borderColor:"#CDCDCD", justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontFamily: "Roboto-Bold", fontSize: 14, color: "#000" }}>View more</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              contentContainerStyle={{
                // paddingLeft: 5,
                // paddingRight: 20,
                paddingTop: 0,
              }}
              numColumns={2}
              data={AllProducts[2]?.lablFranchise}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <TouchableOpacity onPress={() => navigation.navigate('PostDetail', { object: item?.product,name:"lablFranchise" })} style={{ borderRightWidth: 1, borderRightWidth: 1, backgroundColor:"#CDCDCD", borderRightColor: "#000", borderBottomColor: "#000", borderBottomWidth: 1 }}>
                  <Image source={{ uri: item?.product?.productImages[0]?.url }} style={{ height: 200, width: width / 2 }} />
                  <View style={{ margin: 5 }}>
                    <Text style={{ fontSize: 14, fontFamily: "Roboto-Bold", width: 170, height: 15 }}>{item?.product?.productName}</Text>
                    <Text style={{ fontSize: 12, fontFamily: "Roboto-Regular" }}>{item?.product?.productBrandName}</Text>
                    <Text style={{ fontSize: 12, fontFamily: "Roboto-Bold" }}>₹ {item?.product?.productPrice}</Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
          <View style={{ margin: 5 }}>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", margin: 5}}>
              <Text style={{ fontFamily: "Roboto-Bold", fontSize: 16 }}>MODEL PRODUCTS</Text>
              <TouchableOpacity  onPress={() => navigation.navigate('UserProductsScreenLABL',{productObject:{modelProduct:true}})} style={{ height: 30, width: 100, alignSelf: "center",borderWidth:1,borderColor:"#CDCDCD", justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontFamily: "Roboto-Bold", fontSize: 14, color: "#000" }}>View more</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              contentContainerStyle={{
                // paddingLeft: 5,
                // paddingRight: 20,
                paddingTop: 0,
              }}
              numColumns={2}
              data={AllProducts[0]?.modelProducts}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <TouchableOpacity onPress={() => navigation.navigate('PostDetail', { object: item?.product })} style={{ borderRightWidth: 1, borderRightWidth: 1, borderRightColor: "#CDCDCD", borderBottomColor: "#CDCDCD", borderBottomWidth: 1 }}>
                  <Image source={{ uri: item?.product?.productImages[0]?.url }} style={{ height: 200, width: width / 2 }} />
                  <View style={{ margin: 5 }}>
                    <Text style={{ fontSize: 12, fontFamily: "Roboto-Bold", width: 170, height: 15 }}>{item?.product?.productName}</Text>
                    <Text style={{ fontSize: 12, fontFamily: "Roboto-Regular" }}>{item?.product?.productBrandName}</Text>
                    <Text style={{ fontSize: 12, fontFamily: "Roboto-Bold" }}>₹ {item?.product?.productPrice}</Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </ScrollView>
        {showButton && (
          <TouchableOpacity onPress={handlePress} style={styles.buttonContainer}>
            <Icon name="arrow-up" size={16} color="#CDCDCD" />
            <Text style={{ fontSize: 16, fontFamily: "Roboto-Regular", color: "#CDCDCD", marginHorizontal: 10, alignSelf: "center" }}>Move to top</Text>
          </TouchableOpacity>
        )}
      </SafeAreaView>
      {loading1 && (
        <View
          style={{
            width: width,
            height: windowHeight,
            backgroundColor: 'rgba(0,0,0,0.8)',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
          }}>
          <View
            style={{
              width: '60%',
              paddingVertical: 20,
              backgroundColor: '#fff',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <View>
              <ActivityIndicator size={'large'} color="#000" />
            </View>
            <Text
              style={{
                marginLeft: 10,
                fontSize: 30,
                color: '#000',
                fontWeight: 'bold',
              }}>
              Loading..
            </Text>
          </View>
        </View>
      )}
    </View>
  );
}
