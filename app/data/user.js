import {Images} from '@config';

const UserData = [
  {
    id: '1',
    image: Images.profile9,
    name: 'AB-LABEL',
    email: 'lewis.victor@milford.tv',
    // address: 'Burgundy & Red Cotton Printed Tunic',
    // point: '9.5',
    id: '@lablvendor',
    about:
      'Andaz Tokyo Toranomon Hills is one of the newest luxury hotels in Tokyo. Located in one of the uprising areas of Tokyo',
    performance: [
      {value: '97.01%', title: 'Earnings'},
      {value: '999', title: 'Orders'},
      {value: '120k', title: 'Reviews'},
    ],
  },
  {
    id: '2',
    image: Images.profile3,
    name: 'Athena',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '9.5',
    id: '@steve.garrett',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
  {
    id: '3',
    image: Images.profile4,
    name: 'Paul',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '9.5',
    id: '@steve.garrett',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
  {
    id: '4',
    image: Images.profile5,
    name: 'Dung',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '9.5',
    id: '@steve.garrett',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
  {
    id: '5',
    image: Images.profile5,
    name: 'Rehman',
    major: 'Software Engineer',
    email: 'lewis.victor@milford.tv',
    address: '667 Wiegand Gardens Suite 330',
    point: '9.5',
    id: '@steve.garrett',
    about:
      'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit.',
    performance: [
      {value: '97.01%', title: 'Feedback'},
      {value: '999', title: 'Items'},
      {value: '120k', title: 'Followers'},
    ],
  },
];

export {UserData};
