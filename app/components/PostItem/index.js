import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Image, Icon } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { BaseColor, useTheme } from '@config';
import Icon1 from 'react-native-vector-icons/Ionicons';
import ProfileGroupSmall from '../ProfileGroupSmall';
// import { UserData } from '../../data/user';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import { bookmarksDeleteRequest, bookmarksGetRequestByUser, bookmarksPostRequest } from '../../api/bookmarks';
import { likeDeleteRequest, likeGetRequest, likeGetRequestByUser, likePostRequest } from '../../api/like';

export default function PostItem(props) {
  const { style, children, title, description, onPress, image, likes, commentsLength, commentNavigate, object } = props;
  const { colors } = useTheme();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(likeGetRequestByUser(userData?.user?.id));
  }, []);

  const LikedList = useSelector(
    state => state.likeReducer.likeDataByUser,
  );

  const [likecount, setLikeCount] = useState(object?.likes?.length);
  const [liked, setLiked] = useState(false);
  const [likedId, setLikedId] = useState('');
  const userData = useSelector(state => state.accessTokenReducer.userData);
  console.log(userData?.user?.id, "object");
  const userId = userData?.user?.id;
  const modelProduct = object?.id;
  useEffect(() => {
    const LikedListItems = _.find(LikedList, function (o) {
      return o.modelProduct?.id === object?.id;
    });
    console.log(LikedListItems, 'LikedList');
    if (LikedListItems) {
      setLikedId(LikedListItems.id);
      setLiked(true);
    }
  }, []);

  useEffect(() => {
    const LikedListItems = _.find(LikedList, function (o) {
      return o.modelProduct?.id === object?.id;
    });
    if (LikedListItems) {
      setLikedId(LikedListItems.id);
      setLiked(true);
    }
  }, [LikedList]);
  const addLike = async () => {
    if (liked) {
      setLiked(false);
      setLikeCount(likecount - 1);
      await dispatch(likeDeleteRequest(likedId));
      await dispatch(likeGetRequest());
    } else {
      setLiked(true);
      const objectValue = {};
      objectValue.user = userData?.user?.id;
      objectValue.modelProduct = object?.id;
      console.log('bookmarkpost', objectValue);
      // setLoading(true);

      await dispatch(likePostRequest(objectValue));
      await dispatch(likeGetRequestByUser(userId, modelProduct))
      setLikeCount(likecount + 1);
    }
  };

  useEffect(() => {
    dispatch(bookmarksGetRequestByUser(userData?.user?.id));
  }, []);

  const WhistList = useSelector(
    state => state.bookMarksReducer.bookmarksDataByUser,
  );

  const [productId, setProductId] = useState('');
  const [productmarked, setProductmarked] = useState(false);
  console.log(WhistList, "productmarked")
  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) {
      return o.modelProduct?.id === object?.id;
    });
    console.log(whishlistitems, 'whishlistitems');
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true);
    }
  }, []);

  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) {
      return o.modelProduct?.id === object?.id;
    });
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true);
    }
  }, [WhistList]);
  const addWhishlist = async () => {
    if (productmarked) {
      setProductmarked(false);
      await dispatch(bookmarksDeleteRequest(productId));
    } else {
      setProductmarked(true);
      const objectValue = {};
      objectValue.user = userData?.user?.id;
      objectValue.modelProduct = object?.id;
      console.log('bookmarkpost', objectValue);
      // setLoading(true);

      await dispatch(bookmarksPostRequest(objectValue));
    }
  };
  return (
    <View style={style}>
      {children}
      <TouchableOpacity onPress={onPress} activeOpacity={0.9}>
        <Image source={{ uri: image }} style={styles.imageBanner} />
      </TouchableOpacity>
      <View style={{ flexDirection: "row", marginVertical: 20, justifyContent: "space-around", marginLeft: 0 }}>
        <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => addLike()}>
          {liked ? <Icon2
            name="thumbs-up"
            solid
            size={24}
            color={BaseColor.blackColor}
            style={{ right: 20 }}
          /> : <Icon2
            name="thumbs-o-up"
            size={24}
            color={BaseColor.blackColor}
            style={{ right: 20 }}
          />}
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 22, right: 10 }}>{likecount}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flexDirection: "row" }} onPress={commentNavigate}>
          <Icon1
            name="chatbubble-ellipses"
            solid
            size={24}
            color={commentsLength?.length === 0 ? "#CDCDCD" : BaseColor.blackColor}
            style={{ right: 30 }}
          />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 22, right: 20, color: "#CDCDCD" }}>{commentsLength?.length}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => addWhishlist()}>
          {productmarked ? <Icon
            name="bookmark"
            solid
            size={24}
            color={BaseColor.blackColor}
            style={{ left: 20 }}
          /> : <Icon
            name="bookmark"
            size={24}
            color={BaseColor.blackColor}
            style={{ left: 20 }}
          />}
        </TouchableOpacity>
      </View>
      {/* <View style={[styles.content, { borderBottomColor: colors.border }]}>
        <Text headline semibold style={{ marginBottom: 6 }}>
          {title}
        </Text>
        <Text body2>{description}</Text>
      </View> */}
      {/* <View style={{ flexDirection: "row", marginLeft: 20 }}>
        {commentsLength?.length > 0 && <ProfileGroupSmall />}

       
        <TouchableOpacity style={{ height: 16, width: 75, backgroundColor: "#CDCDCD", borderRadius: 10, alignSelf: "center", left: 20 }} onPress={commentNavigate}>
          <Text style={{ fontFamily: "Montserrat-Regular", fontSize: 14, alignSelf: "center" }}>
            View more
          </Text>
        </TouchableOpacity>
      </View> */}
      {/* <View style={{ width: 370, borderWidth: 0.8, borderColor: "#CDCDCD", alignSelf: "center", marginVertical: 20 }}></View>
      <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
        <View style={{ flexDirection: "column", alignItems: "center" }}>
          <Icon2 name="shopping-bag" size={20} color="#000" />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000", top: 10 }}>Buy Combo</Text>
        </View>
        <View style={{ width: 1, height: 60, borderWidth: 0.8, borderColor: "#CDCDCD", alignSelf: "center", top: -10 }}></View>
        <View style={{ flexDirection: "column", alignItems: "center" }}>
          <Icon2 name="sunglasses-alt" size={20} color="#000" />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000", top: 10 }}>Glass</Text>
        </View>
        <View style={{ width: 1, height: 60, borderWidth: 0.8, borderColor: "#CDCDCD", alignSelf: "center", top: -10 }}></View>
        <View style={{ flexDirection: "column", alignItems: "center" }}>
          <Icon name="tshirt" size={20} color="#000" />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000", top: 10 }}>T-Shirt</Text>
        </View>
        <View style={{ width: 1, height: 60, borderWidth: 0.8, borderColor: "#CDCDCD", alignSelf: "center", top: -10 }}></View>
        <View style={{ flexDirection: "column", alignItems: "center" }}>
          <Icon1 name="ios-watch" size={20} color="#000" />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000", top: 10 }}>Watch</Text>
        </View>
      </View> */}
    </View>
  );
}

PostItem.propTypes = {
  image: PropTypes.node.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  title: PropTypes.string,
  likes: PropTypes.number,
  commentsLength: PropTypes.array,
  description: PropTypes.string,
  onPress: PropTypes.func,
};

PostItem.defaultProps = {
  image: '',
  title: '',
  likes: '',
  commentsLength: '',
  description: '',
  style: {},
  onPress: () => { },
};
