import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import {
  SafeAreaView,
  Header,
  PostListItem,
  Button,
  Icon,
  OrderCard
} from '@components';
import { RadioButton } from 'react-native-paper';
import SearchBar from 'react-native-dynamic-search-bar';
import _ from "lodash";
import { Images, useTheme, } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import Modal from 'react-native-modal';
import { useTranslation } from 'react-i18next';
import { modelsGetRequest } from '../../api/models';
import { ordersGetRequest } from '../../api/orders';
import Icon1 from "react-native-vector-icons/FontAwesome";
import Icon2 from "react-native-vector-icons/MaterialCommunityIcons";
import { useSelector, useDispatch } from 'react-redux';
export default function OrdersList({ navigation }) {
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;

  const rating = useSelector(state => state.productRatingReducer.productRatingPostCheckSuccess);

  const [modalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!modalVisible);
  };

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());

  console.log(UserData?.user?.id, "userData");
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const obj = {};
    obj.user = UserData?.user?.id
    dispatch(ordersGetRequest(obj));
  }, [])

  const ordersData = useSelector(state => state.ordersReducer.ordersData);
  console.log("ordersData", ordersData);
  // const modelorderitems = ordersData?.length > 0 && ordersData?.map((items) => {
  //   return items?.orderitems
  // })

  // const Orders = flatten(modelorderitems);
  // function flatten(arr) {
  //   return arr?.reduce(function (flat, toFlatten) {
  //     return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
  //   }, []);
  // }

  const orderView = async (orderObject) => {
    setLoading(true);
    // await dispatch(collectionsPostRequest(cName, profileImage,labelId));
    navigation.navigate('PreviewBooking', { orderObject })
    setLoading(false);
  };
  const [checked, setChecked] = React.useState('first');
  const [checked1, setChecked1] = React.useState('first');

  const filteredData = (value, status) => {
    if (status === "All") {
      setChecked(value);
      const obj = {};
      obj.user = UserData?.user?.id;
      dispatch(ordersGetRequest(obj));
      toggleModal();
    } else {
      setLoading1(true);
      setChecked(value);
      const obj = {};
      obj.status = status;
      obj.user = UserData?.user?.id;
      dispatch(ordersGetRequest(obj));
      toggleModal();
      setLoading1(false);
    }
  }

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  return (
    <View style={{ flex: 1 }}>

      {/* <View style={{ flex: 1, backgroundColor: "#000", height: 140, position: "absolute", width: "100%", marginBottom: 10 }}> */}
      <Header
        // title={t('My Orders')} 
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flex: 1, justifyContent: "space-around" }}>
              <View style={{ flexDirection: "row", width: 200, alignSelf: "flex-start" }}>
                <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                  <Icon
                    name="arrow-left"
                    size={20}
                    color={colors.primary}
                    enableRTL={true}
                  />
                </View>
                <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", marginHorizontal: 20 }}>My Orders</Text>
              </View>

            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

        renderRight={() => {
          return (<TouchableOpacity onPress={toggleModal} style={{ alignSelf: "center", flexDirection: "row", height: 38, width: 100, backgroundColor: "#fff", borderRadius: 10, justifyContent: "space-evenly", alignItems: "center", top: 10 }}>
            <Text style={{ fontFamily: "Montserrrat-Medium", fontSize: 20, color: "#000" }}>
              Filter
            </Text>
            <Icon1 name="sliders" size={18} color="#000" />
          </TouchableOpacity>)
          // <Icon name="shopping-cart" size={24} color={colors.primary} />;
        }}
      // onPressRight={() => {
      //   navigation.navigate('Cart');
      // }}
      />

      {/* <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", backgroundColor: "#000", height: 60, padding: 20 }} >
        <SearchBar
          placeholder="Search all orders"
          onPress={() => alert("onPress")}
          onChangeText={(text) => console.log(text)}
          style={{ alignSelf: "center", width: 250 }}
        />
        <TouchableOpacity onPress={toggleModal} style={{ alignSelf: "center", flexDirection: "row", height: 38, width: 100, backgroundColor: "#fff", borderRadius: 10, justifyContent: "space-evenly", alignItems: "center", marginHorizontal: 10 }}>
          <Text style={{ fontFamily: "Montserrrat-Medium", fontSize: 20, color: "#000" }}>
            Filter
          </Text>
          <Icon1 name="sliders" size={18} color="#000" />
        </TouchableOpacity>
      </View> */}
      {/* </View> */}
      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        <View>
          <Modal
            isVisible={modalVisible}
            onBackdropPress={() => setModalVisible(false)}
            onSwipeComplete={() => setModalVisible(false)}
            swipeDirection={['down']}
            style={styles.bottomModal}>
            <View style={styles.contain}>
              <TouchableOpacity onPress={toggleModal} style={{ height: 50, width: "100%", backgroundColor: "#fff", flexDirection: "row", justifyContent: "space-between", alignItems: "center", padding: 10 }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18 }}>Filter Orders</Text>
                <Icon2 name="close" size={28} />
              </TouchableOpacity>
              <View style={{ flexDirection: "column", alignItems: "flex-start", marginVertical: 5, height: 250, width: "100%", backgroundColor: "#fff" }}>
                <Text style={{ top: Utils.scaleWithPixel(15), fontSize: 20, fontFamily: "Montserrat-Bold", marginBottom: 30, marginHorizontal: 20 }}>Status</Text>
                <View style={{ marginHorizontal: 20 }}>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="first"
                      color='#000'
                      status={checked === 'first' ? 'checked' : 'unchecked'}
                      onPress={() => filteredData('first', 'All')}
                    />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", marginLeft: 20 }}>All</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="second"
                      color='#000'
                      status={checked === 'second' ? 'checked' : 'unchecked'}
                      onPress={() => filteredData('second', 'Shipped')}
                    />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", marginLeft: 20 }}>Shipped</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="third"
                      color='#000'
                      status={checked === 'third' ? 'checked' : 'unchecked'}
                      onPress={() => filteredData('third', 'Delivered')}
                    />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", marginLeft: 20 }}>Delivered</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="fourth"
                      color='#000'
                      status={checked === 'fourth' ? 'checked' : 'unchecked'}
                      onPress={() => filteredData('fourth', 'Cancelled')}
                    />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", marginLeft: 20 }}>Cancelled</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="fifth"
                      color='#000'
                      status={checked === 'fifth' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('fifth', 'Returned')}
                    />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", marginLeft: 20 }}>Returned</Text>
                  </View>
                </View>
              </View>
              <View style={{ flexDirection: "column", alignItems: "flex-start", height: 210, width: "100%", backgroundColor: "#fff", marginBottom: 5 }}>
                <Text style={{ top: Utils.scaleWithPixel(15), fontSize: 20, fontFamily: "Montserrat-Bold", marginBottom: 30, marginHorizontal: 20 }}>Time</Text>
                <View style={{ marginHorizontal: 20 }}>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="first"
                      color='#000'
                      status={checked1 === 'first' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked1('first')}
                    />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", marginLeft: 20 }}>Anytime</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="second"
                      color='#000'
                      status={checked1 === 'second' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked1('second')}
                    />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", marginLeft: 20 }}>last 30 days</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="third"
                      color='#000'
                      status={checked1 === 'third' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked1('third')}
                    />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", marginLeft: 20 }}>Last 6 months</Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="fourth"
                      color='#000'
                      status={checked1 === 'fourth' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked1('fourth')}
                    />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", marginLeft: 20 }}>Last year</Text>
                  </View>
                </View>
              </View>
              {/* <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-around", height: 120, width: "100%", backgroundColor: "#fff" }}>
                <TouchableOpacity style={{ backgroundColor: "#D9D9D9", height: 50, width: 150, borderRadius: 10, justifyContent: "center", alignItems: "center" }}>
                  <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20 }}>Clear filters</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ backgroundColor: "#000", height: 50, width: 150, borderRadius: 10, justifyContent: "center", alignItems: "center" }}>
                  <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#fff" }}>Apply</Text>
                </TouchableOpacity>
              </View> */}
            </View>
          </Modal>
        </View>
        {ordersData?.length > 0 ?
          <ScrollView
            onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
            scrollEventThrottle={8}>
            <View style={{ marginVertical: 10, alignItems: "center", justifyContent: "center", alignSelf: "center" }}>
              <FlatList
                data={ordersData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => {
                  return (
                    <OrderCard
                      orderItem={item}
                      orderId={item?.order?.orderId}
                      image={item?.modelProduct?.product?.productImages[0]?.url}
                      name={item?.modelProduct?.product?.productName}
                      productName={item?.modelProduct?.product?.productDescription}
                      quantity={item?.quantity}
                      size={item?.productVarient?.size?.type}
                      color={item?.productVarient?.color?.color}
                      status={item.status}
                      time={item?.updatedAt}
                    />
                  )
                }}
              />
            </View>
          </ScrollView>
          :
          <View style={{
            flex: 1,
            alignItems: 'center',
            flexDirection: 'column',
            top: 0,
            justifyContent: 'center',
          }}>
            <Text style={{ fontFamily: "Roboto-Bold", fontSize: 18, alignSelf: "center" }}>No Orders Yet 😔 !!!</Text>
          </View>}
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}

