import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { AuthActions } from '@actions';
import { View,SafeAreaView, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
import { Text, Button, Image } from '@components';
import styles from './styles';
import Swiper from 'react-native-swiper';
import { BaseColor, BaseStyle, Images, useTheme } from '@config';
import * as Utils from '@utils';
import Modal from 'react-native-modal';
import { useTranslation } from 'react-i18next';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';

export default function Walkthrough1({ navigation }) {
    const [loading, setLoading] = useState(false);
    const [scrollEnabled, setScrollEnabled] = useState(true);
    const [value, setValue] = useState(false);
    const [value1, setValue1] = useState(false);
    const { colors } = useTheme();
    const dispatch = useDispatch();
    const { t } = useTranslation();
    /**
     * @description Simple authentication without call any APIs
     * @author Passion UI <passionui.com>
     * @date 2019-08-03
     */
    const authentication = () => {
        setLoading(true);
        dispatch(AuthActions.authentication(true, response => { }));
    };
    const handleChange = () => {
        setValue(true);
        setValue1(false);
        navigation.navigate("SignIn");
    };
    const handleChange1 = () => {
        setValue1(true);
        setValue(false);
        navigation.navigate("SignInModelLABL");
    };
    return (
        <SafeAreaView
            style={BaseStyle.safeAreaView}
            edges={['right', 'left', 'bottom']}>
            <ImageBackground source={Images.bgImage} style={{ flex:1, justifyContent: "flex-end" }}>
                <Image source={Images.blackLogo} style={{width:190, height:112,alignSelf:"center",top:-130}} />
                <View style={{ backgroundColor: "#fff", height: 260, width: "100%", borderTopLeftRadius: 40, borderTopRightRadius: 40 }}>
                    <View style={styles.contentSwipeDown}>
                        <View style={styles.lineSwipeDown} />
                    </View>
                    <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, color: "#000", alignSelf: "center" }}>
                        Select an option
                    </Text>
                    <TouchableOpacity onPress={() => handleChange()} style={{ margin: 40, marginBottom: 20, flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={{ flexDirection: "column" }}>
                            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000", alignSelf: "flex-start", marginBottom: 10 }}>
                                USER
                            </Text>
                            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 16, color: "#000", alignSelf: "flex-start" }}>
                                Your online Aisle of Fashion & Life
                            </Text>
                        </View>
                        {value ? <Icon1 name="check-circle" size={35} style={{ alignSelf: "flex-end" }} /> : null}
                    </TouchableOpacity>
                    <View style={{ width: 340, color: "#CDCDCD", height: 1, borderWidth: 0.5, alignSelf: "center", marginBottom: -20 }}></View>
                    <TouchableOpacity onPress={() => handleChange1()} style={{ margin: 40, flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={{ flexDirection: "column" }}>
                            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000", alignSelf: "flex-start", marginBottom: 10 }}>
                                MODEL
                            </Text>
                            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 16, color: "#000", alignSelf: "flex-start" }}>
                                Your online Aisle of Fashion & Life
                            </Text>
                        </View>
                        {value1 ? <Icon1 name="check-circle" size={35} style={{ alignSelf: "flex-end" }} /> : null}
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </SafeAreaView>
    );
}
