import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import * as Utils from '@utils';
import { format, render, cancel, register } from 'timeago.js';

export default function ProfileAuthor1(props) {
  const {
    style,
    image,
    styleLeft,
    styleThumb,
    styleRight,
    onPress,
    name,
    time,
    description,
    textRight,
  } = props;
  // const newtime = new Date(time).toLocaleDateString();
  // console.log("newtime",newtime);
  return (
    <TouchableOpacity
      style={[styles.contain, style]}
      onPress={onPress}
      activeOpacity={0.9}>
      <View style={[styles.contentLeft, styleLeft]}>
        <Image source={{ uri: image ? image : 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png' }} style={[styles.thumb, styleThumb]} />
        <View>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, color: "#000", top: Utils.scaleWithPixel(0) }} numberOfLines={1}>
            {name} - <Text grayColor style={{ fontFamily: "Montserrat-Bold", fontSize: 16, top: Utils.scaleWithPixel(0) }} numberOfLines={1}>
              {format(new Date(time))}
            </Text>
          </Text>
          <Text semibold style={{ fontFamily: "Montserrat-Bold", fontSize: 18 }} >
            {description}
          </Text>
        </View>
        {/* <Text style={{fontsize:14,color:"#ffffff",fontFamily:"ProximaNova",left:Utils.scaleWithPixel(10),borderWidth:Utils.scaleWithPixel(1),borderColor:"#DDDDDD",borderRadius:Utils.scaleWithPixel(5),width:Utils.scaleWithPixel(50),padding:Utils.scaleWithPixel(2)}} >+ Follow</Text> */}
      </View>
      <View style={[styles.contentRight, styleRight]}>
        <Text caption2 grayColor numberOfLines={1}>
          {textRight}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

ProfileAuthor1.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  image: PropTypes.node.isRequired,
  name: PropTypes.string,
  time: PropTypes.string,
  description: PropTypes.string,
  textRight: PropTypes.string,
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

ProfileAuthor1.defaultProps = {
  image: '',
  name: '',
  time: '',
  description: '',
  textRight: '',
  styleLeft: {},
  styleThumb: {},
  styleRight: {},
  style: {},
  onPress: () => { },
};
