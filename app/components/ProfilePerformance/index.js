import React, {useState} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {Text} from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import {useNavigation} from '@react-navigation/native';

export default function ProfilePerformance(props) {
  const navigation = useNavigation();
  const renderValue = (type, value) => {
    switch (type) {
      case 'primary':
        return (
          <Text title3 semibold primaryColor>
            {value}
          </Text>
        );
      case 'small':
        return (
          <Text body1 semibold>
            {value}
          </Text>
        );
      default:
        return (
          <Text
            style={{
              fontFamily: 'Montserrat-Bold',
              fontSize: 22,
              marginVertical: 10,
            }}>
            {value}
          </Text>
        );
    }
  };

  const renderTitle = (type, value) => {
    switch (type) {
      case 'primary':
        return (
          <Text body2 grayColor>
            {value}
          </Text>
        );
      case 'small':
        return (
          <Text caption1 grayColor>
            {value}
          </Text>
        );
      default:
        return (
          <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 16}}>
            {value}
          </Text>
        );
    }
  };

  const {
    style,
    contentLeft,
    contentCenter,
    contentRight,
    data,
    type,
    flexDirection,
    profileData,
  } = props;
  console.log('profileData', profileData);
  const [loading, setLoading] = useState(false);

  const success = async () => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('FollowingScreen', {profileData});
    setLoading(false);
  };
  const success1 = async () => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('VendorCommonProfile', {profileData});
    setLoading(false);
  };

  switch (flexDirection) {
    case 'row':
      return (
        <View style={[styles.contain, style]}>
          {data.map((item, index) => {
            if (index == 0) {
              return (
                <TouchableOpacity
                  onPress={() => success()}
                  style={[styles.contentLeft]}
                  key={index}>
                  {renderValue(type, item.value)}
                  {renderTitle(type, item.title)}
                </TouchableOpacity>
              );
            } else if (index == data.length - 1) {
              return (
                <View style={[styles.contentRight]} key={index}>
                  {renderValue(type, item.value)}
                  {renderTitle(type, item.title)}
                </View>
              );
            } else {
              return (
                <TouchableOpacity
                  onPress={() => success1()}
                  style={[styles.contentCenter]}
                  key={index}>
                  {renderValue(type, item.value)}
                  {renderTitle(type, item.title)}
                </TouchableOpacity>
              );
            }
          })}
        </View>
      );
    case 'column':
      return (
        <View style={[{flex: 1}, style]}>
          {data.map((item, index) => (
            <View key={index}>
              {renderTitle(type, item.title)}
              {renderValue(type, item.value)}
            </View>
          ))}
        </View>
      );
  }
}

ProfilePerformance.propTypes = {
  flexDirection: PropTypes.string,
  type: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  data: PropTypes.array,
  contentLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  contentCenter: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  contentRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

ProfilePerformance.defaultProps = {
  flexDirection: 'row',
  type: 'medium',
  style: {},
  data: [],
  contentLeft: {},
  contentCenter: {},
  contentRight: {},
};
