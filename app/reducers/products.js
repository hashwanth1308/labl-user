import {
  PRODUCTS_GET_CHECK_SUCCESS,
  PRODUCTVARIENT_GET_CHECK_SUCCESS,
  PRODUCTS_POST_CHECK_SUCCESS,
  PRODUCTS_UPDATE_CHECK_SUCCESS,
  PRODUCTS_DELETE_CHECK_SUCCESS,
  PRODUCTS_SET_DATA,
  LABLPROMOTED_SET_DATA,
  LABLEXCLUSIVE_SET_DATA,
  PRODUCTSIZE_SET_DATA,
  PRODUCTVARIENT_SET_DATA,
  PRODUCTS_GET_COLLECTIONID_SET_DATA,
} from '../actions/actionTypes';

const initialState = {
  productsGetCheckSuccess: false,
  productVarientGetCheckSuccess: false,
  productsData: [],
  lablExclusiveData: [],
  lablPromotedData: [],
  productSizeData: [],
  productVarientData: [],
  productsPostCheckSuccess: false,
  productsUpdateCheckSuccess: false,
  productsDeleteCheckSuccess: false,
  productsDataFromSelectedCollection: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCTS_GET_CHECK_SUCCESS:
      return {
        ...state,
        productVarientGetCheckSuccess: action.payload,
      };
    case PRODUCTVARIENT_GET_CHECK_SUCCESS:
      return {
        ...state,
        productsGetCheckSuccess: action.payload,
      };
    case PRODUCTS_POST_CHECK_SUCCESS:
      return {
        ...state,
        productsPostCheckSuccess: action.payload,
      };
    case PRODUCTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        productsUpdateCheckSuccess: action.payload,
      };
    case PRODUCTS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        productsDeleteCheckSuccess: action.payload,
      };

    case PRODUCTS_SET_DATA:
      return {
        ...state,
        productsData: action.payload,
      };
    case LABLPROMOTED_SET_DATA:
      return {
        ...state,
        lablPromotedData: action.payload,
      };
    case LABLEXCLUSIVE_SET_DATA:
      return {
        ...state,
        lablExclusiveData: action.payload,
      };
    case PRODUCTSIZE_SET_DATA:
      return {
        ...state,
        productSizeData: action.payload,
      };
    case PRODUCTVARIENT_SET_DATA:
      return {
        ...state,
        productVarientData: action.payload,
      };
    case PRODUCTS_GET_COLLECTIONID_SET_DATA:
      return {
        ...state,
        productsDataFromSelectedCollection: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
