import {
  SUB_CATEGORIES_POST_CHECK_SUCCESS,
  SUB_CATEGORIES_SET_COUNT,
  SUB_CATEGORIES_COUNT_CHECK_SUCCESS,
  SUB_CATEGORIES_DELETE_CHECK_SUCCESS,
  SUB_CATEGORIES_FINDONE_CHECK_SUCCESS,
  SUB_CATEGORIES_SET_ONE_DATA,
  SUB_CATEGORIES_GET_CHECK_SUCCESS,
  SUB_CATEGORIES_SET_DATA,
  SUB_CATEGORIES_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function subCategoriesPostCheckSuccess(bool = false) {
  return {
    type: SUB_CATEGORIES_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function subCategoriesSetCount(object) {
  return {
    type: SUB_CATEGORIES_SET_COUNT,
    payload: object,
  };
}
export function subCategoriesCountCheckSuccess(bool = false) {
  return {
    type: SUB_CATEGORIES_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function subCategoriesDeleteCheckSuccess(bool = false) {
  return {
    type: SUB_CATEGORIES_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function subCategoriesFindOneCheckSuccess(bool = false) {
  return {
    type: SUB_CATEGORIES_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function subCategoriesSetOneData(object) {
  return {
    type: SUB_CATEGORIES_SET_ONE_DATA,
    payload: object,
  };
}
export function subCategoriesGetCheckSuccess(bool = false) {
  return {
    type: SUB_CATEGORIES_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function subCategoriesSetData(object) {
  return {
    type: SUB_CATEGORIES_SET_DATA,
    payload: object,
  };
}

export function subCategoriesUpdateCheckSuccess(bool = false) {
  return {
    type: SUB_CATEGORIES_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

