import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor, BaseStyle} from '@config';

export default StyleSheet.create({
  contentButtonBottom: {
    borderTopWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  blockView: {
    paddingVertical: 10,
    borderBottomWidth: 1,
  },
  example: {
    marginVertical: 10,
  },
  line: {
    height: 1,
    marginTop: 10,
    marginBottom: 15,
  },
});
