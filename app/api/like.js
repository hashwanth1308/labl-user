import {
    likeGetCheckSuccess,
    likeGetCheckSuccessByUser,
    likeSetData,
    likeSetDataByUser,
    likePostCheckSuccess,
    likeUpdateCheckSuccess,
    likeDeleteCheckSuccess,
    likeGetCheckSuccessForFeed,
    likeSetDataForFeed,
} from '../actions';
import axios from 'axios';
let a = [];

export function likeGetRequest(element, token) {
    console.log('like get request');
    return dispatch => {
        dispatch(likeGetCheckSuccess(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'get',
            url: `likes`,
            headers: {
                //  Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('get like from api', response);
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                dispatch(likeSetData(data));
                dispatch(likeGetCheckSuccess(true));
            })
            .catch(error => {
                console.log(error, axiosLogin, 'furnishings');
                dispatch(likeGetCheckSuccess(false));
            });
    };
}
export function likeGetRequestByUser(userId,modelProduct) {
    console.log(userId, "likeuserid");
    return dispatch => {
        dispatch(likeGetCheckSuccessByUser(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'get',
            url: `likes?user=${userId}&modelProduct=${modelProduct}`,
            headers: {
                //  Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('get like from api', response);
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                console.log(data, "response")
                // console.log(element,"api data",data.data[0])
                // const propertyValues = Object.values(data.data);
                // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                // a.push(obj)
                dispatch(likeSetDataByUser(data));
                dispatch(likeGetCheckSuccessByUser(true));
            })
            .catch(error => {
                console.log(error, axiosLogin, 'furnishings');
                dispatch(likeGetCheckSuccessByUser(false));
            });
    };
}
export function likeGetRequestByModelUser(userId,product) {
    console.log(userId, "likeuserid");
    return dispatch => {
        dispatch(likeGetCheckSuccessByUser(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'get',
            url: `likes?model=${userId}&product=${product}`,
            headers: {
                //  Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('get like from api', response);
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                console.log(data, "response")
                // console.log(element,"api data",data.data[0])
                // const propertyValues = Object.values(data.data);
                // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                // a.push(obj)
                dispatch(likeSetDataByUser(data));
                dispatch(likeGetCheckSuccessByUser(true));
            })
            .catch(error => {
                console.log(error, axiosLogin, 'furnishings');
                dispatch(likeGetCheckSuccessByUser(false));
            });
    };
}
export function likeGetRequestForFeed(userId) {
    const objectvalue = {};
    objectvalue.bookmarkedBy = userId;
    objectvalue.postType = 'feed';

    console.log('like get request', objectvalue);
    return dispatch => {
        dispatch(likeGetCheckSuccessForFeed(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'get',
            url: `likes`,
            params: objectvalue,
            headers: {
                //  Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('get like from api', response);
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log(data)
                // console.log(element,"api data",data.data[0])
                // const propertyValues = Object.values(data.data);
                // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                // a.push(obj)
                dispatch(likeSetDataForFeed(data));
                dispatch(likeGetCheckSuccessForFeed(true));
            })
            .catch(error => {
                console.log(error, axiosLogin, 'furnishings');
                dispatch(likeGetCheckSuccessForFeed(false));
            });
    };
}

export function likePostRequest(objectValue) {
    console.log('objectValue', objectValue);

    return dispatch => {
        // dispatch(likePostCheckSuccess(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'post',
            url: 'likes',
            data: objectValue,
            headers: {
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('response', response);
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // const propertyValues = Object.values(data.data);
                // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                // a.push(obj)
                dispatch(likeSetData(data));
                console.log(data, "successdata");
                // dispatch(likePostCheckSuccess(true));
            })
            .catch(error => {
                console.log(error, axiosLogin);
                // dispatch(likePostCheckSuccess(false));
            });
    };
}

export function likeUpdateRequest(element) {
    console.log(element.accessToken);
    const accessToken = element.accessToken;
    delete element.accessToken;
    return dispatch => {
        dispatch(likeSetData(element));
        dispatch(likeUpdateCheckSuccess(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'put',
            url: `likes`,
            headers: {
                Authorization: `Bearer ${accessToken}`,
                'Content-Type': 'application/json',
            },
            data: element,
        })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(likeSetData(a));
                dispatch(likeUpdateCheckSuccess(true));
            })
            .catch(error => {
                console.log(error, axiosLogin);
                dispatch(likeUpdateCheckSuccess(false));
            });
    };
}

export function likeDeleteRequest(bookmarkId) {
    return dispatch => {
        dispatch(likeDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'delete',
            url: `likes/${bookmarkId}`,
            headers: {
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                dispatch(likeSetData(data));
                dispatch(likeDeleteCheckSuccess(true));
            })
            .catch(error => {
                console.log(error, axiosLogin);
                dispatch(likeDeleteCheckSuccess(false));
            });
    };
}
