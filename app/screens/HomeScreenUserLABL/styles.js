import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils'

export default StyleSheet.create({
  navbar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  titleView: {
    paddingHorizontal: 20,
   flexDirection:"row",
   justifyContent:"space-between"
  },
  contentPage: {
    marginBottom: 0,
  },
  slide: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  img: {
    width: 392,
    height: 134,
    // borderRadius: Utils.scaleWithPixel(200) / 2,
  },
  img1: {
    flex:1,
    width: "100%",
    height: 500,
    // borderRadius: Utils.scaleWithPixel(200) / 2,
  },
  contain: {
    paddingHorizontal: 0,
    paddingVertical: 0,
  },
  tabbar: {
    height: 40,
    // borderTopLeftRadius:10
  },
  tab: {
    width: 200,
  },
  indicator: {
    height: 40,
    // borderTopLeftRadius:10,
    alignContent:"center"
  },
  contain: {
    flexDirection: 'column',
margin:15
    // paddingTop: 5,
    // paddingBottom: 5,
  },
  thumb: {width: 64, height: 64, borderRadius: 34},
  bottomModal: {
    justifyContent: 'center',
    // height:300,
    // backgroundColor:"#000",
    margin:0,
  },
  contain1: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    height:500,
    backgroundColor:"#CDCDCD",
    padding: 0,
    // flex: 1,
  },
  contentSwipeDown: {
    paddingTop: 20,
    height:100,
    alignItems: 'center',
  },
  lineSwipeDown: {
    width: 40,
    height: 3.5,
    borderRadius:20,
    backgroundColor: "#CDCDCD",
    bottom:10
  },
  buttonContainer: {
    position: 'absolute',
    flexDirection:"row",
    justifyContent:"space-between",
    alignItems:"flex-start", 
    backgroundColor:"black",
    borderWidth:1,
    borderColor:"#909090",
    padding:5,
    paddingHorizontal:10,
    borderRadius:10,
    // width:"50%",
    alignSelf:"center",
    bottom: 20,
    // right: 20,
  },
});
