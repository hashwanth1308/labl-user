import { StyleSheet } from 'react-native';
import * as Utils from '@utils';
import {BaseColor} from '@config';

export default StyleSheet.create({
  btnPromotion: {
    height: 30,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    top: 5,

  },
  btnPromotion1: {
    height: 30,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    top: 10
  },
  line: {
    height: 1,
    bottom: 10
  },
  bottomModal: {
    justifyContent: 'flex-end',
    // height:300,
    // backgroundColor:"#000",
    margin:0,
  },
  contain: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor:"#CDCDCD",
    padding: 0,
    // flex: 1,
  },
  contentSwipeDown: {
    paddingTop: 20,
    height:100,
    alignItems: 'center',
  },
  lineSwipeDown: {
    width: 40,
    height: 3.5,
    borderRadius:20,
    backgroundColor: "#CDCDCD",
    bottom:10
  },
});
