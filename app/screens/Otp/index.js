import React, { useState } from 'react';
import { View, KeyboardAvoidingView, Image, TouchableOpacity, Platform, ActivityIndicator } from 'react-native';
import { BaseStyle, useTheme, images, BaseColor } from '@config';
import { Header, SafeAreaView, Icon, Text, Button, TextInput } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { sendVerifyOtpRequest } from '../../api/signup';
import { Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import * as Utils from '@utils';
import LinearGradient from 'react-native-linear-gradient';
// import CheckBox from '@react-native-community/checkbox';
import CountDown from 'react-native-countdown-component';


export default function Otp({ navigation, phonenumber }) {
  const userdata = useSelector(state => state.signupReducer.signupMessageData);
  console.log("userdata", userdata);

  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const signupSuccessOtpRequest = useSelector(state => state.signupReducer.signupSuccessOtpRequest);
  const otpVerifySuccess = useSelector(state => state.signupReducer.verifyOtpSuccess);

  // console.log("otp screen",userdata)

  // const signupSuccess = useSelector(state => state.signupReducer.signupSuccess)
  const [otp, setOtp] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [referralId, setReferral] = useState('');
  const [phone, setPhone] = useState("");
  const [loading, setLoading] = useState(false);
  const [secure, setSecure] = useState(true);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [success, setSuccess] = useState({
    name: true,
    email: true,
    address: true,
    referralId: true,
    phone: true,
    password: true
  });
  /**
   * call when action signup
   *
   */
  const onVerifyOTP = async () => {
    if (otp === '') {
      // setSuccess({
      //   ...success,
      //   otp: otp != '' ? true : false,
      // });
      // alert('Enter the OTP');
    } else {
      setLoading(true);
      const mobile = userdata.user.mobile
      await dispatch(sendVerifyOtpRequest(mobile, otp));
      setLoading(false);
    }
  };
  if (otpVerifySuccess) {
    navigation.navigate('ProfileEdit', userdata)
  };

  const MobileNumber = userdata?.user?.mobile;

  // const arr = [2, 3, 4, 5, 6];

  // const replceWithAsterisk = (MobileNumber, indices) => {
  //   let res = '';
  //   res = indices.reduce((acc, val) => {
  //     acc[val] = '*';
  //     return acc;
  //   }, MobileNumber.split("")).join('');
  //   return res;
  // };
  // console.log(replceWithAsterisk(MobileNumber, arr));
  return (
    <SafeAreaView style={BaseStyle.safeAreaView} forceInset={{ top: 'always' }}>
      {/* <Header
        title={t('OTP')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.dgrblue}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}
      <ScrollView>
        {/* <KeyboardAvoidingView
        behavior={Platform.OS === 'android' ? 'height' : 'padding'}
        keyboardVerticalOffset={offsetKeyboard}
        style={{flex: 1}}> */}
        <View style={styles.contain}>
          <Image source={require("../../assets/images/logo.png")} style={styles.img} />
          <View style={{ alignItems: 'center', marginTop: 80, marginBottom: 40 }}>
            <Text bold style={[styles.Term]}>Enter the Code</Text>
            {/* <Text style={{ fontSize: 20, fontFamily: "Roboto", color: BaseColor.grayColor, textAlign: "center", }}>Enter the 4 digit OTP sent on {'\n'}{MobileNumber} to proceed</Text> */}
          </View>
          <TextInput
            style={{ marginTop: 20, width: 322, height: 52, borderRadius: 30, elevation: 5 }}
            onChangeText={(text) => setOtp(text)}
            placeholder={t('Enter OTP')}
            success={success.name}
            value={otp}
          />
          <View style={{ flexDirection: "row", alignContent: "center" }}>
            <Text body1 style={{ alignSelf: "center", fontFamily: "Montserrat-Medium", color: "#B5B5B5",marginTop:30 }}>
              Resend the code again?
            </Text>
          </View>
          <View style={{ marginTop: 50 }}>

            {/* {otp === '' ? (
              <Button
                disable
                style={{ marginTop: 20,width:160,height:40, alignItems: 'center',borderRadius:30 }}
                loading={loading}>
                {t('Submit')}
              </Button>
            ) : ( */}
              <Button
                style={{ marginTop: 20,width:160,height:40, alignItems: 'center',borderRadius:30 }}
                loading={loading}
                onPress={() => 
                  // onVerifyOTP()
                  navigation.navigate("HomeScreenUserLABL")
                }>
                {t('Submit')}
              </Button>
            {/* )} */}

            <View style={{ marginTop: 40 }}>
              <CountDown
                until={60}
                size={20}
                digitStyle={{ backgroundColor: 'transparent' }}
                digitTxtStyle={{ color: 'white' }}
                timeToShow={['M', 'S']}
                timeLabels={{ m: null, s: null }}
                showSeparator
                separatorStyle={{ color: 'white' }}
              />

            </View>
          </View>
          {/* </TouchableOpacity> */}
        </View>
        {/* </KeyboardAvoidingView> */}
      </ScrollView>
    </SafeAreaView>
  );
}
