import {
  SIZE_POST_CHECK_SUCCESS,
  SIZE_SET_COUNT,
  SIZE_COUNT_CHECK_SUCCESS,
  SIZE_DELETE_CHECK_SUCCESS,
  SIZE_GET_CHECK_SUCCESS,
  COLOR_GET_CHECK_SUCCESS,
  SIZE_SET_DATA,
  COLOR_SET_DATA,
  SIZE_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  sizePostCheckSuccess: false,
  sizeCount: 0,
  sizeCountCheckSuccess : false,
  sizeDeleteCheckSuccess: false,
  sizeGetCheckSuccess: false,
  colorGetCheckSuccess: false,
  sizeData: [],
  colorData: [],
  sizeUpdateCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SIZE_POST_CHECK_SUCCESS:
      return {
        ...state,
        sizePostCheckSuccess: action.payload,
      };
      case SIZE_SET_COUNT:
      return {
        ...state,
        sizeCount: action.payload,
      };
      case SIZE_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        sizeCountCheckSuccess: action.payload,
      };
      case SIZE_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        sizeDeleteCheckSuccess: action.payload,
      };
      case SIZE_GET_CHECK_SUCCESS:
      return {
        ...state,
        sizeGetCheckSuccess: action.payload,
          };
      case COLOR_GET_CHECK_SUCCESS:
      return {
        ...state,
        colorGetCheckSuccess: action.payload,
          };
      case SIZE_SET_DATA:
      return {
        ...state,
        sizeData: action.payload,
          };
      case COLOR_SET_DATA:
      return {
        ...state,
        colorData: action.payload,
          };
      case SIZE_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        sizeUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
