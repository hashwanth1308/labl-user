import {
  productsGetCheckSuccess,
  productVarientGetCheckSuccess,
  productsSetData,
  productSizeSetData,
  productVarientSetData,
  productsPostCheckSuccess,
  productsUpdateCheckSuccess,
  productsDeleteCheckSuccess,
  productsGetUsingCollectionIdSetData,
  lablExclusiveSetData,
  lablPromotedSetData
} from '../actions';
import axios from 'axios';
let a = [];

export function productsGetRequest() {
  return dispatch => {
    dispatch(productsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `products`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("produts response", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(productsSetData(data));
        dispatch(productsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(productsGetCheckSuccess(false));
      });
  };
};
export function lablExclusiveGetRequest(obj) {
  return dispatch => {
    dispatch(productsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `productBrands`,
      params:obj,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("produts response---->", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("exclusive---->",data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(lablExclusiveSetData(data));
        dispatch(productsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(productsGetCheckSuccess(false));
      });
  };
};
export function productFindOneRequest(id) {
  return dispatch => {
    dispatch(productsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `products/${id}`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        // console.log("produts response", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(lablPromotedSetData(data));
        dispatch(productsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(productsGetCheckSuccess(false));
      });
  };
};
export function productVarientGetRequest(id) {
  const obj={};
  obj.product = id;
  return dispatch => {
    dispatch(productVarientGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `productvarients`,
      params:obj,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("produts response", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(productVarientSetData(data));
        dispatch(productVarientGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(productVarientGetCheckSuccess(false));
      });
  };
};

export function productSizeGetRequest(element) {
  console.log("------====>");
  return dispatch => {
    // dispatch(productsPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `productsizes`,
      headers: {
        //  Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        console.log("response",response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("----Data--->",data);
        dispatch(productSizeSetData(data));
        // dispatch(productsPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        // dispatch(productsPostCheckSuccess(false));

      });
  };
}

export function productsUpdateRequest(element) {
  console.log(element.accessToken)
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(productsSetData(element));
    dispatch(productsUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `products`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(productsSetData(a));
        dispatch(productsUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(productsUpdateCheckSuccess(false));

      });
  };
}

export function productsDeleteRequest(element) {
  return dispatch => {
    dispatch(productsDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `products`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        const propertyValues = Object.values(data.data);
        const obj = { categoryID: element.id, categoryName: element.name, categoryImage: element.icon, subCategory: propertyValues }
        a.push(obj)
        dispatch(productsSetData(a));
        dispatch(productsDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(productsDeleteCheckSuccess(false));

      });
  };
}
// get products using collection id
export function productsGetRequestUsingCollectionId(obj) {
 console.log("obj",obj);
  return dispatch => {
    dispatch(productsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `productvarients`,
      params: obj,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("produts response", response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(productsGetUsingCollectionIdSetData(data));
        dispatch(productsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin, 'furnishings')
        dispatch(productsGetCheckSuccess(false));
      });
  };
};