import { StyleSheet } from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  imgBanner: {
    width: '100%',
    height: 450,
    resizeMode: 'cover'
    // position: 'absolute',
  },
  contentImageFollowing: {
    flexDirection: 'row',
    height: Utils.scaleWithPixel(160),
    marginTop: 10,
  },
  slide: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  img: {
    width: 392,
    height: 134,
    // borderRadius: Utils.scaleWithPixel(200) / 2,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    // height:300,
    // backgroundColor:"#000",
    margin: 0,
  },
  contain: {
    // alignItems: 'center',
    height: 250,
    backgroundColor: "#fff",
    padding: 0,
    // flex: 1,
  },
  Address: {
    // alignItems: 'center',
    height: 450,
    backgroundColor: "#fff",
    padding: 0,
    // flex: 1,
  },
  contentPicker: {
    // padding: 10,
    // borderRadius: 8,
    flex: 1,
    alignItems: 'center',
    flexDirection: "row"
  },
});
