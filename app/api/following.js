import {
  modelFollowingPostCheckSuccess,
  userFollowingPostCheckSuccess,
  modelFollowingSetCount,
  modelFollowingCountCheckSuccess,
  modelFollowingDeleteCheckSuccess,
  userFollowingDeleteCheckSuccess,
  modelFollowingGetCheckSuccess,
  userFollowingGetCheckSuccess,
  modelFollowingSetData,
  userFollowingSetData,
  modelFollowingUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

//create
export function modelFollowingPostRequest(objectValue) {
  return dispatch => {
    dispatch(modelFollowingPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `modelfollowings`,
      data: objectValue,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelFollowingSetData(data));
        dispatch(modelFollowingPostCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelFollowingPostCheckSuccess(false));
      });
  };
}
export function userFollowingPostRequest(objectValue) {
  return dispatch => {
    dispatch(userFollowingPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `userfollowings`,
      data: objectValue,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(userFollowingSetData(data));
        dispatch(userFollowingPostCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(userFollowingPostCheckSuccess(false));
      });
  };
}

//count
export function modelFollowingCountRequest(element) {
  return dispatch => {
    dispatch(modelFollowingCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `modelfollowings/count`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelFollowingSetCount(data));
        dispatch(modelFollowingCountCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelFollowingCountCheckSuccess(false));
      });
  };
}

//delete
export function modelFollowingDeleteRequest(id) {
  return dispatch => {
    dispatch(modelFollowingDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `modelfollowings/${id}`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelFollowingDeleteCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelFollowingDeleteCheckSuccess(false));
      });
  };
}
export function userFollowingDeleteRequest(id) {
  return dispatch => {
    dispatch(userFollowingDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `userfollowings/${id}`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(userFollowingDeleteCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(userFollowingDeleteCheckSuccess(false));
      });
  };
}

//find - get
export function modelFollowingGetRequest(obj) {
  return dispatch => {
    dispatch(modelFollowingGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `modelFollowings`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      params: obj,
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(modelFollowingSetData(data));
        dispatch(modelFollowingGetCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelFollowingGetCheckSuccess(false));
      });
  };
}
export function userFollowingGetRequest(obj) {
  return dispatch => {
    dispatch(userFollowingGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `userfollowings`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      params: obj,
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(userFollowingSetData(data));
        dispatch(userFollowingGetCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(userFollowingGetCheckSuccess(false));
      });
  };
}

//update
export function modelFollowingUpdateRequest(id) {
  return dispatch => {
    dispatch(modelFollowingUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `modelFollowings/${id}`,
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(modelFollowingSetData(data));
        dispatch(modelFollowingUpdateCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelFollowingUpdateCheckSuccess(false));
      });
  };
}
