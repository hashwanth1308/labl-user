import{ 
    
  vendorPayoutsPostCheckSuccess,
  vendorPayoutsSetCount,
  vendorPayoutsCountCheckSuccess,
  vendorPayoutsDeleteCheckSuccess,
  vendorPayoutsFindOneCheckSuccess,
  vendorPayoutsSetOneData,
  vendorPayoutsGetCheckSuccess,
  vendorPayoutsSetData,
  vendorPayoutsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function vendorPayoutsPostRequest(element) {
      return dispatch => {
        dispatch(vendorPayoutsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `vendor-payouts`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(vendorPayoutsSetData(a));
            dispatch(vendorPayoutsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(vendorPayoutsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function vendorPayoutsCountRequest(element) {
      return dispatch => {
          dispatch(vendorPayoutsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `vendor-payouts/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(vendorPayoutsSetCount(data));
              dispatch(vendorPayoutsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(vendorPayoutsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function vendorPayoutsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(vendorPayoutsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `vendor-payouts/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(vendorPayoutsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(vendorPayoutsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function vendorPayoutsFindOneRequest(id) {
      return dispatch => {
        dispatch(vendorPayoutsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `vendor-payouts/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(vendorPayoutsOneData(data));
            dispatch(vendorPayoutsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(vendorPayoutsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function vendorPayoutsGetRequest(element) {
  return dispatch => {
    dispatch(vendorPayoutsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `vendor-payouts`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(vendorPayoutsSetOneData(data));
        dispatch(vendorPayoutsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(vendorPayoutsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function vendorPayoutsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(vendorPayoutsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `vendor-payouts/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(vendorPayoutsSetData(data));
              dispatch(vendorPayoutsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(vendorPayoutsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 