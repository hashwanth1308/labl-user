import {
  COMBINATIONS_POST_CHECK_SUCCESS,
  COMBINATIONS_SET_COUNT,
  COMBINATIONS_COUNT_CHECK_SUCCESS,
  COMBINATIONS_DELETE_CHECK_SUCCESS,
  COMBINATIONS_FINDONE_CHECK_SUCCESS,
  COMBINATIONS_SET_ONE_DATA,
  COMBINATIONS_GET_CHECK_SUCCESS,
  COMBINATIONS_SET_DATA,
  COMBINATIONS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  combinationsPostCheckSuccess: false,
  combinationsCount: 0,
  combinationsCountCheckSuccess : false,
  combinationsDeleteCheckSuccess: false,
  combinationsFindOneCheckSuccess: false,
  combinationsOneData: [],
  combinationsGetCheckSuccess: false,
  combinationsData: [],
  combinationsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case COMBINATIONS_POST_CHECK_SUCCESS:
      return {
        ...state,
        combinationsPostCheckSuccess: action.payload,
      };
      case COMBINATIONS_SET_COUNT:
      return {
        ...state,
        combinationsCount: action.payload,
      };
      case COMBINATIONS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        combinationsCountCheckSuccess: action.payload,
      };
      case COMBINATIONS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        combinationsDeleteCheckSuccess: action.payload,
      };
      case COMBINATIONS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        combinationsFindOneCheckSuccess: action.payload,
          };
      case COMBINATIONS_SET_ONE_DATA:
      return {
        ...state,
        combinationsOneData: action.payload,
          };
      case COMBINATIONS_GET_CHECK_SUCCESS:
      return {
        ...state,
        combinationsGetCheckSuccess: action.payload,
          };
      case COMBINATIONS_SET_DATA:
      return {
        ...state,
        combinationsData: action.payload,
          };
      case COMBINATIONS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        combinationsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
