import {
  FAVDESIGNS_POST_CHECK_SUCCESS,
  FAVDESIGNS_SET_COUNT,
  FAVDESIGNS_COUNT_CHECK_SUCCESS,
  FAVDESIGNS_DELETE_CHECK_SUCCESS,
  FAVDESIGNS_FINDONE_CHECK_SUCCESS,
  FAVDESIGNS_SET_ONE_DATA,
  FAVDESIGNS_GET_CHECK_SUCCESS,
  FAVDESIGNS_SET_DATA,
  FAVDESIGNS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function favDesignsPostCheckSuccess(bool = false) {
  return {
    type: FAVDESIGNS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function favDesignsSetCount(object) {
  return {
    type: FAVDESIGNS_SET_COUNT,
    payload: object,
  };
}
export function favDesignsCountCheckSuccess(bool = false) {
  return {
    type: FAVDESIGNS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function favDesignsDeleteCheckSuccess(bool = false) {
  return {
    type: FAVDESIGNS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function favDesignsFindOneCheckSuccess(bool = false) {
  return {
    type: FAVDESIGNS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function favDesignsSetOneData(object) {
  return {
    type: FAVDESIGNS_SET_ONE_DATA,
    payload: object,
  };
}
export function favDesignsGetCheckSuccess(bool = false) {
  return {
    type: FAVDESIGNS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function favDesignsSetData(object) {
  return {
    type: FAVDESIGNS_SET_DATA,
    payload: object,
  };
}

export function favDesignsUpdateCheckSuccess(bool = false) {
  return {
    type: FAVDESIGNS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

