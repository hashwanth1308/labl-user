import{ 
    
  basicSettingsPostCheckSuccess,
  basicSettingsSetCount,
  basicSettingsCountCheckSuccess,
  basicSettingsDeleteCheckSuccess,
  basicSettingsFindOneCheckSuccess,
  basicSettingsSetOneData,
  basicSettingsGetCheckSuccess,
  basicSettingsSetData,
  basicSettingsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function basicSettingsPostRequest(element) {
      return dispatch => {
        dispatch(basicSettingsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `basic-settings`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(basicSettingsSetData(a));
            dispatch(basicSettingsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(basicSettingsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function basicSettingsCountRequest(element) {
      return dispatch => {
          dispatch(basicSettingsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `basic-settings/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(basicSettingsSetCount(data));
              dispatch(basicSettingsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(basicSettingsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function basicSettingsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(basicSettingsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `basic-settings/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(basicSettingsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(basicSettingsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function basicSettingsFindOneRequest(id) {
      return dispatch => {
        dispatch(basicSettingsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `basic-settings/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(basicSettingsOneData(data));
            dispatch(basicSettingsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(basicSettingsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function basicSettingsGetRequest(element) {
  return dispatch => {
    dispatch(basicSettingsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `basic-settings`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(basicSettingsSetOneData(data));
        dispatch(basicSettingsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(basicSettingsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function basicSettingsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(basicSettingsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `basic-settings/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(basicSettingsSetData(data));
              dispatch(basicSettingsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(basicSettingsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 