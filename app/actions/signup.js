import {SIGNUP_DATA,SIGNUP_FAILURE, SIGNUP_SUCCESS, SIGNUP_LOADING, VERIFYOTP_SUCCESS, VERIFYOTP_FAILURE} from './actionTypes';

export function signupData(object) {
  console.log("object",object)
  return {
    type: SIGNUP_DATA,
    payload: object,
  };
}

export function signupLoading(bool = false) {
  return {
    type: SIGNUP_LOADING,
    payload: bool,
  };
}

export function signupSuccess(bool = false) {
  return {
    type: SIGNUP_SUCCESS,
    payload: bool,
  };
}
export function verifyOtpSucess(bool = false) {
  return {
    type: VERIFYOTP_SUCCESS,
    payload: bool,
  };
}
 
export function signupFailure(bool = false) {
  return {
    type: SIGNUP_FAILURE,
    payload: bool,
  };
}

 
export function verifyOtpFailure(bool = false) {
  return {
    type: VERIFYOTP_FAILURE,
    payload: bool,
  };
}
