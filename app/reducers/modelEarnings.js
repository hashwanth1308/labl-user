import {
  MODELEARNINGS_POST_CHECK_SUCCESS,
  MODELEARNINGS_SET_COUNT,
  MODELEARNINGS_COUNT_CHECK_SUCCESS,
  MODELEARNINGS_DELETE_CHECK_SUCCESS,
  MODELEARNINGS_FINDONE_CHECK_SUCCESS,
  MODELEARNINGS_SET_ONE_DATA,
  MODELEARNINGS_GET_CHECK_SUCCESS,
  MODELEARNINGS_SET_DATA,
  MODELEARNINGS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  modelEarningsPostCheckSuccess: false,
  modelEarningsCount: 0,
  modelEarningsCountCheckSuccess : false,
  modelEarningsDeleteCheckSuccess: false,
  modelEarningsFindOneCheckSuccess: false,
  modelEarningsOneData: [],
  modelEarningsGetCheckSuccess: false,
  modelEarningsData: [],
  modelEarningsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MODELEARNINGS_POST_CHECK_SUCCESS:
      return {
        ...state,
        modelEarningsPostCheckSuccess: action.payload,
      };
      case MODELEARNINGS_SET_COUNT:
      return {
        ...state,
        modelEarningsCount: action.payload,
      };
      case MODELEARNINGS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        modelEarningsCountCheckSuccess: action.payload,
      };
      case MODELEARNINGS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        modelEarningsDeleteCheckSuccess: action.payload,
      };
      case MODELEARNINGS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        modelEarningsFindOneCheckSuccess: action.payload,
          };
      case MODELEARNINGS_SET_ONE_DATA:
      return {
        ...state,
        modelEarningsOneData: action.payload,
          };
      case MODELEARNINGS_GET_CHECK_SUCCESS:
      return {
        ...state,
        modelEarningsGetCheckSuccess: action.payload,
          };
      case MODELEARNINGS_SET_DATA:
      return {
        ...state,
        modelEarningsData: action.payload,
          };
      case MODELEARNINGS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        modelEarningsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
