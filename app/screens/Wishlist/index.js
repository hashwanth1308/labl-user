import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
  ActivityIndicator,
  Dimensions,
  Vibration,
} from 'react-native';
import {
  Image,
  Icon,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  EventCard,
  Header,
  CartCard,
  TextInput,
} from '@components';
import Modal from 'react-native-modal';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { Badge, VStack, Box, Center, NativeBaseProvider } from 'native-base';
import PropTypes from 'prop-types';
import Icon1 from 'react-native-vector-icons/AntDesign';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { PromotionData, TourData, HotelData, BestData } from '@data';
import { useTranslation } from 'react-i18next';
import Snackbar from 'react-native-snackbar';
import Feather from 'react-native-vector-icons/Feather'
import {
  bookmarksDeleteRequest,
  bookmarksDeleteRequestByUser,
  bookmarksGetRequestByUser,
} from '../../api/bookmarks';
import { useSelector, useDispatch } from 'react-redux';

import { cartsPostRequest } from '../../api/carts';
import { userCartsPostRequest } from '../../api/userCarts';
import { productsGetRequestUsingCollectionId, productVarientGetRequest } from '../../api';

export default function Wishlist({ navigation }) {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [imageData] = useState([]);
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const [adults, setAdults] = useState(2);
  const [product, setProduct] = useState(null);
  console.log(product, "product");
  const userData = useSelector(state => state.accessTokenReducer.userData);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(bookmarksGetRequestByUser(userData?.user?.id));
  }, [getBookmark,deleteBookmark]);
  const WhistList = useSelector(
    state => state.bookMarksReducer.bookmarksDataByUser,
  );
  console.log(WhistList, 'whishlist');

  const userCartData = useSelector(
    state => state.userCartsReducer.userCartsData,
  );
  console.log(userCartData.length, 'userCartData');

 

  const onBackdropPress = () => {
    setModalVisible(!modalVisible);
    setChecked(null);
    setChecked1(null)
  }

  function Example() {
    return (
      <Box alignItems="center">
        <VStack>
          <Badge // bg="red.400"
            bg="#fff"
            rounded="full"
            mb={-4}
            mr={-4}
            zIndex={1}
            variant="solid"
            alignSelf="flex-end"
            _text={{
              fontSize: 12,
              color: "#000"
            }}>
            {userCartData?.length}
          </Badge>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Cart');
            }}
            style={{
              height: 35,
              width: 35,
              borderRadius: 30,
              backgroundColor: '#2F2F2F',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../assets/images/bag-2.png')}
              style={{ height: 17, width: 17 }}
            />
          </TouchableOpacity>
        </VStack>
      </Box>
    );
  }

  const deleteBookmark = useSelector(state => state.bookMarksReducer.bookmarksDeleteCheckSuccess);
  const getBookmark = useSelector(state => state.bookMarksReducer.bookmarksGetCheckSuccessByUser);

  // const handleAddToCart = (item) => {
  //   setLoading1(true);
  //   const cartData = {};
  //   cartData.modelProduct = item?.modelProduct?.id;
  //   cartData.user = userData?.user?.id;
  //   dispatch(userCartsPostRequest(cartData));
  //   dispatch(bookmarksDeleteRequestByUser(item?.id));
  //   // navigation.navigate("Wishlist")
  //   setLoading1(false)
  // };

  const [modalVisible, setModalVisible] = useState(false);
  const toggleModal = (item) => {
    setModalVisible(!modalVisible);
    setProduct(item);
    dispatch(productVarientGetRequest(item?.modelProduct?.product?.id));
  };


  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const deltaY = new Animated.Value(0);

  let ImageUrl = imageData
    ? { imageData }
    : require('../../assets/images/avata-01.jpeg');

  const heightImageBanner = Utils.scaleWithPixel(80);
  const marginTopBanner = heightImageBanner - heightHeader;


  const [checked, setChecked] = useState(null);
  const [checked1, setChecked1] = useState(null);

  const productVarient = useSelector(state => state.productsReducer.productVarientData);
  console.log("productVarient", productVarient);


  const [varientSelected, setVarientSelected] = useState(null);
  // console.log("varientSelected", varientSelected);
  const uniqueIds = [];


  const uniqueProduct = [...new Set(productVarient.map(item => item.size))];
  console.log("uniqueProduct", uniqueProduct);

  const unique = uniqueProduct.filter(element => {
    const isDuplicate = uniqueIds.includes(element?.type);

    if (!isDuplicate) {
      uniqueIds.push(element?.type);

      return true;
    }

    return false;
  });


  const onSelectSizes = select => {
    console.log(select, "selectedbbbb");
    setLoading1(true);
    const obj = {};
    obj.product = product?.modelProduct?.product?.id;
    obj.size = select?.id;
    dispatch(productsGetRequestUsingCollectionId(obj))
    setChecked(select?.id);
    setLoading1(false);
  };

  const handleChange = (item) => {
    setChecked1(item?.id);
    setVarientSelected(item)
  }

  const productColor = useSelector(state => state.productsReducer.productsDataFromSelectedCollection);
  console.log(productColor, "productColor");


  const handleAddToCart = () => {
    setLoading1(true);
    if (varientSelected === null) {
      alert("Please select Size,Color")
    } else {
      const cartData = {};
      // cartData.address = selectAddress?.id;
      cartData.user = userData?.user?.id;
      cartData.modelProduct = product?.modelProduct?.id;
      cartData.productVarient = varientSelected;
      cartData.product = product?.modelProduct?.product?.id;
      // cartData.quantity = value;
      dispatch(userCartsPostRequest(cartData));
      dispatch(bookmarksDeleteRequestByUser(product?.id));
      setModalVisible(false)
      console.log(cartData, 'cartData');
      Snackbar.show({
        text: 'Product is added your Cart',
        duration: Snackbar.LENGTH_SHORT,
      });
      Vibration.vibrate(400, false);
    }
    setLoading1(false);
    setChecked(null);
    setChecked1(null)
  };

 

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 500)
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('WISHLIST')}
        style={{ height: 80, backgroundColor: '#000' }}
        renderLeft={() => {
          return (
            <View
              style={{
                flexDirection: 'row',
                width: 200,
                alignSelf: 'flex-start',
                top: -10,
              }}>
              <View
                style={{
                  height: 30,
                  width: 30,
                  borderRadius: 30,
                  backgroundColor: '#fff',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
              <Text
                style={{
                  alignSelf: 'center',
                  fontFamily: 'Roboto-Bold',
                  fontSize: 20,
                  color: '#fff',
                  // right: 60,
                  marginHorizontal: 20
                }}>
                Wishlist
              </Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      // renderRight={() => {
      //   return (
      //     <NativeBaseProvider>
      //       <Center flex={1} px="3">
      //         <Example />
      //       </Center>
      //     </NativeBaseProvider>
      //   );
      // }}
      // onPressRight={() => {
      //   navigation.navigate('Cart');
      // }}
      />

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        {WhistList.length > 0 ? (
          <ScrollView
            onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
            scrollEventThrottle={8}>
            <View style={{}}>
              <FlatList
                vertical
                // numColumns={2}
                data={WhistList}
                // style={{ margin: 10 }}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => {
                  return (
                   <Component object={item} />
                  );
                }}
              />
            </View>
          </ScrollView>
        ) : (
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              flexDirection: 'column',
              top: 0,
              justifyContent: 'center',
            }}>
            <Icon1
              name="heart"
              size={100}
              color="#CDCDCD"
              style={{ marginVertical: 20 }}
            />
            <Text
              style={{
                alignSelf: 'center',
                fontFamily: 'Roboto-Bold',
                fontSize: 20,
                color: '#000',
                marginVertical: 0,
              }}>
              Your wishlist is empty
            </Text>
            <Text
              style={{
                alignSelf: 'center',
                fontFamily: 'Roboto-Medium',
                fontSize: 14,
                color: '#000',
                textAlign: 'center',
                marginVertical: 20,
              }}>
              Add your wishes where you can make it true{'\n'} Review the
              anytime and mote to the bag
            </Text>
            <TouchableOpacity
              onPress={() => navigation.navigate("BottomTabNavigator")}
              style={{
                height: 40,
                width: 160,
                backgroundColor: '#000',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
                marginVertical: 20,
              }}>
              <Text
                style={{
                  fontFamily: 'Roboto-SemiBold',
                  fontSize: 18,
                  color: '#fff',
                }}>
                Shop now
              </Text>
            </TouchableOpacity>
          </View>
        )}

        <View>
          <Modal
            isVisible={modalVisible}
            onBackdropPress={() => onBackdropPress()}
            onSwipeComplete={() => setModalVisible(false)}
            swipeDirection={['down']}
            style={styles.bottomModal}>
            <View style={styles.contain}>
              <View style={{
                paddingTop: 20,
                paddingLeft: 30,
                justifyContent: 'center',
                flexDirection: "row",
              }}>
                {/* <Image source={image} style={{ height: 150, width: 120, borderRadius: 10 }} /> */}
                {/* <View style={{ padding: 0 }}>
                  <Text style={{ fontFamily: "Roboto-Bold", fontSize: 22 }}>Choose your pickup location</Text>
                </View> */}
              </View>
              <View style={{ padding: 20 }}>
                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 20 }}>
                  Available Size :{' '}
                </Text>
                {unique != "" ?
                  // <Component unique={unique} object={object} productColor={productColor} onChange={setVarientSelected} />
                  <View>
                    <FlatList
                      horizontal
                      data={unique}
                      keyExtractor={(item, index) => item.id}
                      renderItem={({ item, index }) => (
                        (checked === item?.id ?
                          <TouchableOpacity style={{ margin: 10, backgroundColor: "#000", borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20, color: "#fff" }}>{item?.type}</Text>
                          </TouchableOpacity> :
                          <TouchableOpacity onPress={() => onSelectSizes(item)} style={{ margin: 10, borderWidth: 1, borderColor: "#000", borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20 }}>{item?.type}</Text>
                          </TouchableOpacity>)
                      )}
                    />
                    {checked ?
                      <View style={{ flexDirection: "column", marginTop: 15 }}>
                        <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20 }}>Available Colors : </Text>
                        {/* <ColorComponent color={productColor} onChange={onChange} /> */}
                        <View>
                          <FlatList
                            horizontal
                            data={productColor}
                            keyExtractor={(item, index) => item.id}
                            renderItem={({ item, index }) => (
                              (checked1 === item?.id ?
                                <TouchableOpacity style={{ margin: 10, backgroundColor: "#000", borderRadius: 20, width: 75, height: 35, justifyContent: "center", alignItems: "center" }}>
                                  <Text style={{ fontFamily: "Roboto-Bold", fontSize: 18, color: "#fff" }}>{item?.color?.color}</Text>
                                </TouchableOpacity> :
                                <TouchableOpacity onPress={() => handleChange(item)} style={{ margin: 10, borderWidth: 1, borderColor: "#000", borderRadius: 20, width: 75, height: 35, justifyContent: "center", alignItems: "center" }}>
                                  <Text style={{ fontFamily: "Roboto-Bold", fontSize: 18 }}>{item?.color?.color}</Text>
                                </TouchableOpacity>)
                            )}

                          />
                        </View>
                        {/* <Text style={{ fontFamily: "Roboto-Medium", fontSize: 20 }}>{object?.productAvailableSizes}</Text> */}
                      </View> : null}
                  </View>
                  :
                  <View style={{ marginVertical: 15 }}>
                    <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 18, color: "#000" }}>No Sizes available!!!</Text>
                  </View>}

              </View>
              {checked1 && (
                <TouchableOpacity style={{ alignSelf: "center", height: 40, width: 100, backgroundColor: "#000", borderRadius: 10, justifyContent: "center", alignItems: "center" }} onPress={() => handleAddToCart()}>
                  <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20, color: "#fff" }}>Done</Text>
                </TouchableOpacity>
              )}
            </View>
          </Modal>
        </View>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }

    </View>
  );
}

function Component(props) {
  const { image, name, price, title, object } = props;
  const dispatch = useDispatch();
  console.log('obj', object);
  const userData = useSelector(state => state.accessTokenReducer.userData);
  const leftSwipe = (progress, dragX) => {
    const scale = dragX.interpolate({
      inputRange: [0, 100],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });
    return (
      <TouchableOpacity onPress={() => deleteFromWishlist()} style={{ alignSelf: "center" }} activeOpacity={0.6}>
        <View style={styles.deleteBox}>
          <Animated.Text style={{ transform: [{ scale: scale }] }}>
            <Feather name="trash-2" size={44} color="#000" />
          </Animated.Text>
        </View>
      </TouchableOpacity>
    );
  };

  const deleteFromWishlist = () => {
    dispatch(bookmarksDeleteRequestByUser(object?.id));
  }
  return (
    <GestureHandlerRootView>
      <Swipeable renderLeftActions={leftSwipe}>
        <View
          style={{
            width: "100%",
            borderWidth: 1,
            borderColor: "#CDCDCD",
            padding: 10,
            flexDirection: 'row',
            marginBottom: 20,
            justifyContent: 'space-evenly',
            margin: 10,
          }}>
          <Image
            source={{
              uri: object?.lablExclusive || object?.lablPromoted || object?.lablFranchise ? 
              object?.product?.productImages[0]?.url : object?.modelProduct?.product?.productImages[0]?.url
            }}
            style={{ height: 70, width: 70 }}
          />
          <View style={{ flexDirection: "column", justifyContent: "space-evenly", marginHorizontal: 20, alignItems: "center" }}>
            <Text numberOfLines={2}
              style={{
                fontFamily: 'Roboto-Bold',
                fontSize: 14,
                color: '#000',
                width: 100,
              }}>
              {object?.lablExclusive || object?.lablPromoted || object?.lablFranchise ? object?.product?.productName : object?.modelProduct?.product?.productName}
            </Text>
            <Text
              style={{ fontFamily: 'Roboto-Medium', fontSize: 16, color: '#000', alignSelf: "flex-start" }}>
              ₹ {object?.lablExclusive || object?.lablPromoted || object?.lablFranchise ? object?.product?.productPrice : object?.modelProduct?.product?.productPrice}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => toggleModal(object)}
            style={{
              height: 40,
              width: 130,
              alignSelf: "center",
              backgroundColor: '#000',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'Roboto-SemiBold',
                fontSize: 18,
                color: '#fff',
              }}>
              Move to Bag
            </Text>
          </TouchableOpacity>
        </View>
      </Swipeable>
    </GestureHandlerRootView>
  );
}