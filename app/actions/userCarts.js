import {
  USER_CARTS_POST_CHECK_SUCCESS,
  USER_CARTS_SET_COUNT,
  USER_CARTS_COUNT_CHECK_SUCCESS,
  USER_CARTS_DELETE_CHECK_SUCCESS,
  USER_CARTS_FINDONE_CHECK_SUCCESS,
  USER_CARTS_SET_ONE_DATA,
  USER_CARTS_GET_CHECK_SUCCESS,
  USER_CARTS_SET_DATA,
  USER_CARTS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function userCartsPostCheckSuccess(bool = false) {
  return {
    type:USER_CARTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function userCartsSetCount(object) {
  return {
    type: USER_CARTS_SET_COUNT,
    payload: object,
  };
}
export function userCartsCountCheckSuccess(bool = false) {
  return {
    type: USER_CARTS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function userCartsDeleteCheckSuccess(bool = false) {
  return {
    type: USER_CARTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function userCartsFindOneCheckSuccess(bool = false) {
  return {
    type: USER_CARTS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function userCartsSetOneData(object) {
  return {
    type: USER_CARTS_SET_ONE_DATA,
    payload: object,
  };
}
export function userCartsGetCheckSuccess(bool = false) {
  return {
    type: USER_CARTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function userCartsSetData(object) {
  return {
    type: USER_CARTS_SET_DATA,
    payload: object,
  };
}

export function userCartsUpdateCheckSuccess(bool = false) {
  return {
    type: USER_CARTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

