import {

  cartsPostCheckSuccess,
  cartsSetCount,
  cartsCountCheckSuccess,
  cartsDeleteCheckSuccess,
  cartsFindOneCheckSuccess,
  cartsSetOneData,
  cartsGetCheckSuccess,
  cartsSetData,
  cartsUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

//create
export function cartsPostRequest(element) {
  console.log(element);
  return dispatch => {
    dispatch(cartsPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `model-carts`,
      data: element,
      headers: {
        //  Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        console.log("cartpost", response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("cartpost", data)
        dispatch(cartsSetData(data));
        dispatch(cartsPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(cartsPostCheckSuccess(false));

      });
  };
}

//count
export function cartsCountRequest(element) {
  return dispatch => {
    dispatch(cartsCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `carts/count`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(cartsSetCount(data));
        dispatch(cartsCountCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(cartsCountCheckSuccess(false));

      });
  };
}

//delete
export function cartsDeleteRequest(id) {
  console.log("cart id for del", id);
  return dispatch => {
    dispatch(cartsDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `model-carts/${id}`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },

    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data)
        dispatch(cartsDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(cartsDeleteCheckSuccess(false));

      });
  };
}

//findOne
export function cartsFindOneRequest(id) {
  return dispatch => {
    dispatch(cartsFindOneCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `model-carts/${id}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(cartsSetOneData(data));
        dispatch(cartsFindOneCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin,)
        dispatch(cartsFindOneCheckSuccess(false));
      });
  };
};

//find - get
export function cartsGetRequest(obj) {
  return dispatch => {
    dispatch(cartsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `model-carts`,
      params:obj,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",

      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("cartdata", data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(cartsSetData(data));
        dispatch(cartsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(cartsGetCheckSuccess(false));
      });
  };
};

//update
export function cartsUpdateRequest(element,id) {

  return dispatch => {
    dispatch(cartsUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `model-carts/${id}`,
      data:element,
      headers: {
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(cartsSetOneData(data));
        dispatch(cartsUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(cartsUpdateCheckSuccess(false));

      });
  };
}



