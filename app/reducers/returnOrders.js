import {
  RETURNORDERS_POST_CHECK_SUCCESS,
  RETURNORDERS_SET_COUNT,
  RETURNORDERS_COUNT_CHECK_SUCCESS,
  RETURNORDERS_DELETE_CHECK_SUCCESS,
  RETURNORDERS_FINDONE_CHECK_SUCCESS,
  RETURNORDERS_SET_ONE_DATA,
  RETURNORDERS_GET_CHECK_SUCCESS,
  RETURNORDERS_SET_DATA,
  RETURNORDERS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  returnOrdersPostCheckSuccess: false,
  returnOrdersCount: 0,
  returnOrdersCountCheckSuccess : false,
  returnOrdersDeleteCheckSuccess: false,
  returnOrdersFindOneCheckSuccess: false,
  returnOrdersOneData: [],
  returnOrdersGetCheckSuccess: false,
  returnOrdersData: [],
  returnOrdersUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case RETURNORDERS_POST_CHECK_SUCCESS:
      return {
        ...state,
        returnOrdersPostCheckSuccess: action.payload,
      };
      case RETURNORDERS_SET_COUNT:
      return {
        ...state,
        returnOrdersCount: action.payload,
      };
      case RETURNORDERS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        returnOrdersCountCheckSuccess: action.payload,
      };
      case RETURNORDERS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        returnOrdersDeleteCheckSuccess: action.payload,
      };
      case RETURNORDERS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        returnOrdersFindOneCheckSuccess: action.payload,
          };
      case RETURNORDERS_SET_ONE_DATA:
      return {
        ...state,
        returnOrdersOneData: action.payload,
          };
      case RETURNORDERS_GET_CHECK_SUCCESS:
      return {
        ...state,
        returnOrdersGetCheckSuccess: action.payload,
          };
      case RETURNORDERS_SET_DATA:
      return {
        ...state,
        returnOrdersData: action.payload,
          };
      case RETURNORDERS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        returnOrdersUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
