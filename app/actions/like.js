import {
    LIKE_GET_CHECK_SUCCESS,
    LIKE_POST_CHECK_SUCCESS,
    LIKE_UPDATE_CHECK_SUCCESS,
    LIKE_DELETE_CHECK_SUCCESS,
    LIKE_SET_DATA,
    LIKE_SET_DATA_BY_USER,
    LIKE_GET_CHECK_SUCCESS_BY_USER,
    LIKE_GET_CHECK_SUCCESS_FOR_FEED,
    LIKE_SET_DATA_FOR_FEED
} from './actionTypes';



export function likeGetCheckSuccess(bool = false) {
    return {
        type: LIKE_GET_CHECK_SUCCESS,
        payload: bool,
    };
}
export function likeGetCheckSuccessByUser(bool = false) {
    return {
        type: LIKE_GET_CHECK_SUCCESS_BY_USER,
        payload: bool,
    };
}
export function likeGetCheckSuccessForFeed(bool = false) {
    return {
        type: LIKE_GET_CHECK_SUCCESS_FOR_FEED,
        payload: bool,
    };
}
export function likePostCheckSuccess(bool = false) {
    return {
        type: LIKE_POST_CHECK_SUCCESS,
        payload: bool,
    };
}

export function likeUpdateCheckSuccess(bool = false) {
    return {
        type: LIKE_UPDATE_CHECK_SUCCESS,
        payload: bool,
    };
}

export function likeDeleteCheckSuccess(bool = false) {
    return {
        type: LIKE_DELETE_CHECK_SUCCESS,
        payload: bool,
    };
}


export function likeSetData(object) {
    return {
        type: LIKE_SET_DATA,
        payload: object,
    };
}
export function likeSetDataByUser(object) {
    console.log(object, "object")
    return {
        type: LIKE_SET_DATA_BY_USER,
        payload: object,
    };
}
export function likeSetDataForFeed(object) {
    return {
        type: LIKE_SET_DATA_FOR_FEED,
        payload: object,
    };
}
