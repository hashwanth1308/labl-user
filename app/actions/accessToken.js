import {SET_ACCESS_TOKEN,SET_USER_DATA} from './actionTypes';

export function setAccessToken(object) {
  return {
    type: SET_ACCESS_TOKEN,
    payload: object,
  };
}
export function setUserData(object) {
  return {
    type: SET_USER_DATA,
    payload: object,
  };
}
