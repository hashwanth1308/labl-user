import {
  BOOKMARKS_GET_CHECK_SUCCESS,
  BOOKMARKS_POST_CHECK_SUCCESS,
  BOOKMARKS_UPDATE_CHECK_SUCCESS,
  BOOKMARKS_DELETE_CHECK_SUCCESS,
  BOOKMARKS_SET_DATA,
  BOOKMARKS_SET_DATA_BY_USER,
  BOOKMARKS_GET_CHECK_SUCCESS_BY_USER,
  BOOKMARKS_GET_CHECK_SUCCESS_FOR_FEED,
  BOOKMARKS_SET_DATA_FOR_FEED
} from './actionTypes';



export function bookmarksGetCheckSuccess(bool = false) {
  return {
    type: BOOKMARKS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function bookmarksGetCheckSuccessByUser(bool = false) {
  return {
    type: BOOKMARKS_GET_CHECK_SUCCESS_BY_USER,
    payload: bool,
  };
}
export function bookmarksGetCheckSuccessForFeed(bool = false) {
  return {
    type: BOOKMARKS_GET_CHECK_SUCCESS_FOR_FEED,
    payload: bool,
  };
}
export function bookmarksPostCheckSuccess(bool = false) {
  return {
    type: BOOKMARKS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function bookmarksUpdateCheckSuccess(bool = false) {
  return {
    type: BOOKMARKS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function bookmarksDeleteCheckSuccess(bool = false) {
  return {
    type: BOOKMARKS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function bookmarksSetData(object) {
  return {
    type: BOOKMARKS_SET_DATA,
    payload: object,
  };
}
export function bookmarksSetDataByUser(object) {
  console.log(object, "object")
  return {
    type: BOOKMARKS_SET_DATA_BY_USER,
    payload: object,
  };
}
export function bookmarksSetDataForFeed(object) {
  return {
    type: BOOKMARKS_SET_DATA_FOR_FEED,
    payload: object,
  };
}
