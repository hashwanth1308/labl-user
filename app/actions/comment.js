import {
    COMMENT_GET_CHECK_SUCCESS,
    COMMENT_POST_CHECK_SUCCESS,
    COMMENT_UPDATE_CHECK_SUCCESS,
    COMMENT_DELETE_CHECK_SUCCESS,
    COMMENT_SET_DATA,
    COMMENT_SET_DATA_BY_USER,
    COMMENT_GET_CHECK_SUCCESS_BY_USER,
    COMMENT_GET_CHECK_SUCCESS_FOR_FEED,
    COMMENT_SET_DATA_FOR_FEED
} from './actionTypes';



export function commentGetCheckSuccess(bool = false) {
    return {
        type: COMMENT_GET_CHECK_SUCCESS,
        payload: bool,
    };
}
export function commentGetCheckSuccessByUser(bool = false) {
    return {
        type: COMMENT_GET_CHECK_SUCCESS_BY_USER,
        payload: bool,
    };
}
export function commentGetCheckSuccessForFeed(bool = false) {
    return {
        type: COMMENT_GET_CHECK_SUCCESS_FOR_FEED,
        payload: bool,
    };
}
export function commentPostCheckSuccess(bool = false) {
    return {
        type: COMMENT_POST_CHECK_SUCCESS,
        payload: bool,
    };
}

export function commentUpdateCheckSuccess(bool = false) {
    return {
        type: COMMENT_UPDATE_CHECK_SUCCESS,
        payload: bool,
    };
}

export function commentDeleteCheckSuccess(bool = false) {
    return {
        type: COMMENT_DELETE_CHECK_SUCCESS,
        payload: bool,
    };
}


export function commentSetData(object) {
    return {
        type: COMMENT_SET_DATA,
        payload: object,
    };
}
export function commentSetDataByUser(object) {
    console.log(object, "object")
    return {
        type: COMMENT_SET_DATA_BY_USER,
        payload: object,
    };
}
export function commentSetDataForFeed(object) {
    return {
        type: COMMENT_SET_DATA_FOR_FEED,
        payload: object,
    };
}
