import { StyleSheet } from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  contentCartPromotion: {
    marginTop: 60,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  btnPromotion: {
    height: 35,
    borderRadius: 5,
    paddingHorizontal: 25,
    paddingVertical: 5,
  },
  btnPromotion1: {
    marginTop: 130,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    height: 40,
  },
  promotionItem: {
    width: Utils.scaleWithPixel(270),
    height: Utils.scaleWithPixel(200),
    paddingHorizontal: 25,
    marginLeft: 18,
    borderRadius: 10,
  },
  titleView: {
    paddingHorizontal: 25,
    bottom: 30,
    fontSize: 20,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    right: Utils.scaleWithPixel(23),
    top: 30,

  },
  content: {
    position: 'absolute',
    alignItems: 'flex-start',
    bottom: 50,
    padding: 10,

  },
});
