import {
  INVOICES_POST_CHECK_SUCCESS,
  INVOICES_SET_COUNT,
  INVOICES_COUNT_CHECK_SUCCESS,
  INVOICES_DELETE_CHECK_SUCCESS,
  INVOICES_FINDONE_CHECK_SUCCESS,
  INVOICES_SET_ONE_DATA,
  INVOICES_GET_CHECK_SUCCESS,
  INVOICES_SET_DATA,
  INVOICES_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  invoicesPostCheckSuccess: false,
  invoicesCount: 0,
  invoicesCountCheckSuccess : false,
  invoicesDeleteCheckSuccess: false,
  invoicesFindOneCheckSuccess: false,
  invoicesOneData: [],
  invoicesGetCheckSuccess: false,
  invoicesData: [],
  invoicesUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INVOICES_POST_CHECK_SUCCESS:
      return {
        ...state,
        invoicesPostCheckSuccess: action.payload,
      };
      case INVOICES_SET_COUNT:
      return {
        ...state,
        invoicesCount: action.payload,
      };
      case INVOICES_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        invoicesCountCheckSuccess: action.payload,
      };
      case INVOICES_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        invoicesDeleteCheckSuccess: action.payload,
      };
      case INVOICES_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        invoicesFindOneCheckSuccess: action.payload,
          };
      case INVOICES_SET_ONE_DATA:
      return {
        ...state,
        invoicesOneData: action.payload,
          };
      case INVOICES_GET_CHECK_SUCCESS:
      return {
        ...state,
        invoicesGetCheckSuccess: action.payload,
          };
      case INVOICES_SET_DATA:
      return {
        ...state,
        invoicesData: action.payload,
          };
      case INVOICES_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        invoicesUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
