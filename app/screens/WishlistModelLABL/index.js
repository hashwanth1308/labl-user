import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import {
  Image,
  Icon,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  EventCard,
  Header,
  CartCard,
  TextInput
} from '@components';
import {
  RoundedCheckbox,
  PureRoundedCheckbox,
} from "react-native-rounded-checkbox";
import { Badge, VStack, Box, Center, NativeBaseProvider } from 'native-base';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import Icon1 from 'react-native-vector-icons/AntDesign';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { PromotionData, TourData, HotelData, BestData } from '@data';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { sizeGetRequest } from '../../api/size';
import { cartsPostRequest } from '../../api/carts';
import { bookmarksDeleteRequest, bookmarksGetRequestByModelUser } from '../../api/bookmarks';

export default function WishlistModelLABL({ navigation }) {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [imageData] = useState([]);
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const [adults, setAdults] = useState(2);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const userData = useSelector(state => state.accessTokenReducer.userData);
  const dispatch = useDispatch();
  const cartsData = useSelector(state => state.cartsReducer.cartsData)
  console.log("cartsData", cartsData);
  useEffect(() => {
    dispatch(bookmarksGetRequestByModelUser(userData?.user?.model?.id));
  }, []);
  const WhistList = useSelector(
    state => state.bookMarksReducer.bookmarksDataByUser,
  );
  console.log(WhistList, "whishlist");
  // console.log("UserToken", userToken);
  // console.log(productsData[0], productsData);



  // const onFindOneCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };


  // const onAddCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  //   console.log(cartsData[0], cartsData);

  function Example() {
    return (
      <Box alignItems="center">
        <VStack>
          <Badge // bg="red.400"
            bg="#fff"
            rounded="full"
            mb={-4}
            mr={-4}
            zIndex={1}
            variant="solid"
            alignSelf="flex-end"
            _text={{
              fontSize: 12,
              color: "#000"
            }}>
            {cartsData?.length ? cartsData?.length : "0"}
          </Badge>
          <TouchableOpacity onPress={() => navigation.navigate("CartModelLABL")} style={{ height: 35, width: 35, borderRadius: 30, backgroundColor: "#2F2F2F", justifyContent: "center", alignItems: "center" }}>
            <Image source={require('../../assets/images/bag-2.png')} style={{ height: 17, width: 17 }} />
          </TouchableOpacity>
        </VStack>
      </Box>
    );
  }

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const deltaY = new Animated.Value(0);

  let ImageUrl = imageData ? { imageData } : require('../../assets/images/avata-01.jpeg')

  const heightImageBanner = Utils.scaleWithPixel(80);
  const marginTopBanner = heightImageBanner - heightHeader;

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('WISHLIST')}
        style={{ height: 70, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flexDirection: "row", width: 200, justifyContent: "space-between", alignSelf: "flex-start", top: -10 }}>
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
              <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", right: 30 }}>Model Wishlist</Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

        renderRight={() => {
          return <NativeBaseProvider>
            <Center flex={1} px="3">
              <Example />
            </Center>
          </NativeBaseProvider>
        }}
        renderRightSecond={() => {
          return <TouchableOpacity style={{ height: 35, width: 35, borderRadius: 30, backgroundColor: "#2F2F2F", justifyContent: "center", alignItems: "center" }}>
            <Image source={require('../../assets/images/trash.png')} style={{ height: 17, width: 17 }} />
          </TouchableOpacity>
        }}
      // onPressRight={() => {
      //   navigation.navigate('CartModelLABL');
      // }}
      />

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        {WhistList.length > 0 ?
          <ScrollView
            onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
            scrollEventThrottle={8}>
            <View style={{ paddingHorizontal: 20 }}>
              <FlatList
                vertical
                numColumns={2}
                data={WhistList}
                // style={{ margin: 10 }}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => {
                  return (
                    <Component image={item?.product?.productImages[0]?.url} name={item?.product?.productName} price={item?.product?.productPrice} object={item} />
                  )
                }}
              />
            </View>
          </ScrollView> :
          <View style={{ flex: 1, alignItems: "center", flexDirection: "column", top: 0, justifyContent: "center" }}>
            <Icon1 name="heart" size={100} color="#CDCDCD" style={{ marginVertical: 20 }} />
            <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000", marginVertical: 0 }}>Your wishlist is empty</Text>
            <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Medium", fontSize: 14, color: "#000", textAlign: "center", marginVertical: 20 }}>Add your wishes where you can make it true{'\n'} Review the anytime and mote to the bag</Text>
            <TouchableOpacity style={{ height: 40, width: 160, backgroundColor: "#000", justifyContent: "center", alignItems: "center", borderRadius: 10, marginVertical: 20 }}>
              <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, color: "#fff" }}>Shop now</Text>
            </TouchableOpacity>
          </View>
        }
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}

function Component(props) {
  const [modalVisible, setModalVisible] = useState(false);
  const { image, name, price, object } = props;
  const userData = useSelector(state => state.accessTokenReducer.userData);
  console.log("obj", object);
  const toggleModal = () => {
    console.log("hello world");
    setModalVisible(!modalVisible);
  };
  const handleAddToCart = () => {
    const cartData = {}
    cartData.model = userData?.user?.model?.id;
    cartData.product = object?.product?.id;
    dispatch(cartsPostRequest(cartData));
    dispatch(bookmarksDeleteRequest(object?.id));

  }
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(sizeGetRequest());

  }, [dispatch])
  const sizes = useSelector(state => state.sizeReducer.sizeData);
  console.log("size", sizes);

  const [selectedCat, setSelectedCat] = useState([]);
  function test(index) {
    /* 
      Use this approach while changing the state. 
      This will create the copy of original array, 
      and you can perform mutation there 
      and update state with it.
    */
    console.log("index", index);
    const size = [...sizes];
    size[index].checked = size[index].checked ? !size[index].checked : true
    var selectsizes = selectedCat;

    selectsizes.push({ _id: size[index]?.id, type: size[index]?.type })
    // setCategory(size);
    setSelectedCat(selectsizes);

    // selectsizes.push({ _id: size[index]?.id });
    console.log("sum", selectedCat);
  }

  return (
    <View style={{ height: 300, flexDirection: "column", marginBottom: -20, justifyContent: "space-evenly", margin: 10 }}>
      <Image source={image} style={{ height: 160, width: 160 }} />
      <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 15, color: "#000", width: 150 }}>{name}</Text>
      <Text style={{ fontFamily: "Montserrat-Regular", fontSize: 16, color: "#000" }}>₹ {price}</Text>
      <TouchableOpacity onPress={() => handleAddToCart()} style={{ height: 40, width: 160, backgroundColor: "#000", justifyContent: "center", alignItems: "center" }}>
        <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, color: "#fff" }}>Move to Bag</Text>
      </TouchableOpacity>
      <View>
        <Modal
          isVisible={modalVisible}
          onBackdropPress={() => setModalVisible(false)}
          onSwipeComplete={() => setModalVisible(false)}
          swipeDirection={['down']}
          style={styles.bottomModal}>
          <View style={styles.contain}>
            <View style={{
              // paddingTop: 20,
              // paddingLeft: 0,
              margin: 10,
              justifyContent: 'space-between',
              alignItems: "center",
              flexDirection: "row",
            }}>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22 }}>Size: {selectedCat[0]?.type}</Text>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22, marginVertical: 20 }}>Size chart</Text>
            </View>
            <View>
              <FlatList
                data={sizes}
                horizontal={true}
                style={{ alignSelf: "center" }}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <View style={{ flexDirection: "row", margin: 5 }}>
                    {/* <CheckBox
                      disabled={false}
                      value={item?.checked}
                      // value={toggleCheckBox}
                      onChange={() => test(index)}
                      // onValueChange={newValue => setToggleCheckBox(newValue)}
                      tintColor="red"
                      boxType="circle"
                      style={styles.checkbox}
                    /> */}
                    <RoundedCheckbox
                      text={item?.type}
                      active={item?.checked}
                      onPress={() => {
                        test(index)
                      }}
                    />
                    {/* <Text style={{ fontFamily: "Roboto", alignSelf: "center" }}></Text> */}
                  </View>
                )}
              />
            </View>
            <View style={{ padding: 20 }}>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>$ {object?.price}</Text>
              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20 }}>Seller : Visudh</Text>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  )
}
Component.propTypes = {
  image: PropTypes.node.isRequired,
  name: PropTypes.string,
  price: PropTypes.string,
  // title: PropTypes.string,
};

Component.defaultProps = {
  image: '',
  name: '',
  price: '',
  // title: '',
};
