import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AuthActions } from '@actions';
import { BlurView, VibrancyView } from '@react-native-community/blur';
import {
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
  Dimensions,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  Alert,
  BackHandler,

} from 'react-native';
import { BaseStyle, useTheme, Images } from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  Image,
  TextInput
} from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { loginRequest } from '../../api/login';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import * as Utils from '@utils';
import axios from 'axios';
// import { TextInput } from 'react-native-paper';

export default function SignIn({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  const [secure, setSecure] = useState(true);

  const accessToken = useSelector(
    state => state.accessTokenReducer.accessToken,
  );
  const userData = useSelector(state => state.accessTokenReducer.userData);
  // console.log(userData.user.mobile,"USER_DATA");
  const [mobile, setMobile] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState({ id: true, password: true });
  const login = useSelector(state => state.loginReducer.loginSuccess);

  /**
   * call when action login
   *
   */

  useEffect(() => {
    const backAction = () => {
      Alert.alert("Hold on!", "Are you sure you want to exit the App?", [
        {
          text: "Cancel",
          onPress: () => null,
          style: "cancel"
        },
        { text: "YES", onPress: () => BackHandler.exitApp() }
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  const findUser = async () => {
    if (mobile == '' || password == '') {
      // if (!id && !password) {
      setSuccess({
        ...success,
        mobile: false,
        password: false,
      });
    } else {
      setLoading1(true);
      const obj = {};
      obj.mobile = mobile;
      obj.isModel = false;
      await axios
        .get(`https://staging-backend.labl.store/users/`,
          {
            params: obj,
            headers: {
              Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYWM2NDg5ZjBhM2NkMGU2ZTcwNTJlMyIsImlhdCI6MTY3OTI5MjQyNiwiZXhwIjoxNjgxODg0NDI2fQ.XVDcipa0a01fI4VcWYAO7y6FcytFg59v6E5290iJse8`,
              "Content-Type": "application/json"
            }
          },
        )
        .then(async ({ data }) => {
          console.log(data, 'orderObject');
          if (data?.status === true) {
            await dispatch(loginRequest(mobile, password));
          }
        })
        .catch((err) => { console.log("err", err); });
      setLoading1(false);
    }
  };
  if (login && userData?.user?.isVendor === false && userData?.user?.isModel === false) {
    dispatch(
      AuthActions.authentication(true, response => {
        // setLoading(false);
        // console.log('console.log after signup', userData, response);
        navigation.navigate('HomeScreenUserLABL', accessToken);
      }),
    );
  }
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => setLoading1(prev => !prev), 1000);
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('sign_in')}
        renderLeft={() => {
          return (
            <View
              style={{
                height: 35,
                width: 35,
                borderRadius: 30,
                backgroundColor: 'white',
                elevation: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                name="arrow-left"
                size={20}
                color={'#000'}
                enableRTL={true}
              />
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          enabled
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
          <Image
            source={Images.logo}
            style={{
              height: Utils.scaleWithPixel(100),
              width: Utils.scaleWithPixel(150),
              alignSelf: 'center',
              top: Utils.scaleWithPixel(30),
              marginBottom: 80,
            }}
          />
          <ScrollView contentContainerStyle={{ marginVertical: 20 }}>
            <View style={styles.contain}>
              <Text
                style={{
                  fontSize: 25,
                  color: '#000',
                  fontFamily: 'Roboto-Bold',
                  marginBottom: 30,
                  position: 'relative',
                }}>
                Sign In
              </Text>
              <View style={{ marginBottom: 30 }}>
                <TextInput
                  onChangeText={text => setMobile(text)}
                  onFocus={() => {
                    setSuccess({
                      ...success,
                      mobile: true,
                    });
                  }}
                  placeholder={t('Enter Phone Number')}
                  // mode="outlined"
                  // activeUnderlineColor='#015093'
                  // outlineColor='#000'
                  // activeOutlineColor='#000'
                  success={success.mobile}
                  maxLength={10}
                  value={mobile}
                  keyboardType="numeric"
                  style={{ width: 322, height: 52, borderRadius: 30, elevation: 3 }}
                />
                <TextInput
                  style={{
                    marginTop: 20,
                    width: 322,
                    height: 52
                    , borderRadius: 30, elevation: 3
                  }}
                  onChangeText={text => setPassword(text)}
                  onFocus={() => {
                    setSuccess({
                      ...success,
                      password: true,
                    });
                  }}
                  placeholder={t('password')}
                  secureTextEntry={secure}
                  success={success.password}
                  value={password}>
                  <Icon1
                    name={secure ? 'eye-off-outline' : 'eye-outline'}
                    onPress={() => setSecure(!secure)}
                    solid
                    color="black"
                    size={24}
                    style={{ elevation: 5 }}
                  />
                </TextInput>
                <TouchableOpacity
                  style={{ alignSelf: 'flex-end', marginRight: 10, top: 25 }}
                  onPress={() => navigation.navigate('ForgotPassword')}>
                  <Text
                    body1
                    style={{
                      fontSize: 14,
                      color: '#B5B5B5',
                      fontFamily: 'Roboto-SemiBold',
                    }}>
                    {t('Forgot Password?')}
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={{
                  marginTop: 50,
                  width: 172,
                  height: 39,
                  borderRadius: 30,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "#000"
                }}
                // full
                loading={loading1}
                onPress={() => {
                  findUser();
                }}>
                <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20, color: "#fff" }}>{t('sign_in')}</Text>
              </TouchableOpacity>
              {loading ? (
                <ActivityIndicator
                  size={'large'}
                  color="#000"
                  style={{ flex: 1, alignSelf: 'center' }}
                />
              ) : null}
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  marginTop: 20,
                  marginBottom: 30,
                }}>
                <Text
                  style={{
                    color: '#000',
                    fontSize: 12,
                    marginBottom: 20,
                    fontFamily: 'Roboto-Medium',
                  }}>
                  or
                </Text>
                <TouchableOpacity
                  style={{ alignSelf: 'flex-end' }}
                  onPress={() => navigation.navigate('SignUpScreenUserLABL')}>
                  <Text
                    body1
                    style={{ color: '#000', fontFamily: 'Roboto-Medium' }}>
                    {t('Register')}
                  </Text>
                </TouchableOpacity>
              </View>
              {/* <View>
              <TouchableOpacity
                style={{ alignSelf: "flex-end", marginLeft: 20 }} onPress={() => navigation.navigate('SignUpScreenModelLABL')}>
                <Text body1 grayColor>
                  {t('Become LABL-Model')}
                </Text>
              </TouchableOpacity>

            </View> */}
            </View>
          </ScrollView>
          {/* </TouchableWithoutFeedback> */}
        </KeyboardAvoidingView>
      </SafeAreaView>
      {loading1 && (
        <View
          style={{
            width: width,
            height: height,
            backgroundColor: 'rgba(0,0,0,0.8)',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
          }}>
          <View
            style={{
              width: '60%',
              paddingVertical: 20,
              backgroundColor: '#fff',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <View>
              <ActivityIndicator size={'large'} color="#000" />
            </View>
            <Text
              style={{
                marginLeft: 10,
                fontSize: 30,
                color: '#000',
                fontWeight: 'bold',
              }}>
              Loading..
            </Text>
          </View>
        </View>
      )}
    </View>
  );
}
