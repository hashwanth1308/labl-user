import{ 
    
  categoriesPostCheckSuccess,
  categoriesSetCount,
  categoriesCountCheckSuccess,
  categoriesDeleteCheckSuccess,
  categoriesFindOneCheckSuccess,
  categoriesSetOneData,
  categoriesGetCheckSuccess,
  categoriesSetData,
  categoriesUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function categoriesPostRequest(element) {
      return dispatch => {
        dispatch(categoriesPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `categories`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(categoriesSetData(a));
            dispatch(categoriesPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(categoriesPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function categoriesCountRequest(element) {
      return dispatch => {
          dispatch(categoriesCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `categories/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(categoriesSetCount(data));
              dispatch(categoriesCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(categoriesCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function categoriesDeleteRequest(id) {
      
      return dispatch => {
          dispatch(categoriesDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `categories/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(categoriesDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(categoriesDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function categoriesFindOneRequest(id) {
      return dispatch => {
        dispatch(categoriesFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `categories/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(categoriesOneData(data));
            dispatch(categoriesFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(categoriesFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function categoriesGetRequest(element) {
  return dispatch => {
    dispatch(categoriesGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `categories`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
         
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(categoriesSetData(data));
        dispatch(categoriesGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(categoriesGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function categoriesUpdateRequest(id) {
     
      return dispatch => {
          dispatch(categoriesUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `categories/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(categoriesSetData(data));
              dispatch(categoriesUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(categoriesUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 