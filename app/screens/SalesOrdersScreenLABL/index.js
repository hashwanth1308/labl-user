import React, {useEffect, useState} from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {BaseStyle, useTheme} from '@config';
import {Header, SafeAreaView, Icon, Text, TextInput, Button} from '@components';
import {useTranslation} from 'react-i18next';
import styles from './styles';
import { salesOrdersCountRequest, salesOrdersGetRequest,salesOrdersFindOneRequest,
  salesOrdersPostRequest,salesOrdersDeleteRequest,
salesOrdersUpdateRequest} from '../../api/salesOrders';

export default function SalesOrdersScreenLABL({navigation}) {
  const {colors} = useTheme();
  const {t} = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const themeStorage = useSelector(state => state.application.theme);
  
  const addressData = useSelector(state => state.salesOrdersReducer.salesOrdersData);

  const dispatch = useDispatch();
  const [card, setCard] = useState('');
  const [valid, setValid] = useState('');
  const [digit, setDigit] = useState('');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });
 
useEffect(() => {
    dispatch(salesOrdersCountRequest());

}, [dispatch])

useEffect(() => {
  dispatch(salesOrdersGetRequest());

}, [dispatch])

const onFindOneSalesOrders = () => {
  setLoading(true);
  dispatch(salesOrdersFindOneRequest());
  // setTimeout(() => {
  //   navigation.goBack();
  // }, 1000);
};


const onAddSalesOrders = () => {
  setLoading(true);
  dispatch(salesOrdersPostRequest());
  // setTimeout(() => {
  //   navigation.goBack();
  // }, 1000);
};

const onDeleteSalesOrders = () => {
  setLoading(true);
  dispatch(salesOrdersDeleteRequest());
  // setTimeout(() => {
  //   navigation.goBack();
  // }, 1000);
};

const onUpdateSalesOrders = () => {
  setLoading(true);
  dispatch( salesOrdersUpdateRequest());
  // setTimeout(() => {
  //   navigation.goBack();
  // }, 1000);
};


  return(
<View style={{flex: 1}}>
      <Header
        title={t('add_payment_method')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{flex: 1}}>
          <ScrollView contentContainerStyle={{padding: 20}}>
            <Text headline>{t('card_information')}</Text>
            </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
  </View>
  )
}