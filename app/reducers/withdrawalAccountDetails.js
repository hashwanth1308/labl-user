import {
  WITHDRAWALACCOUNTDETAILS_POST_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_SET_COUNT,
  WITHDRAWALACCOUNTDETAILS_COUNT_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_DELETE_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_FINDONE_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_SET_ONE_DATA,
  WITHDRAWALACCOUNTDETAILS_GET_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_SET_DATA,
  WITHDRAWALACCOUNTDETAILS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  withdrawalAccountDetailsPostCheckSuccess: false,
  withdrawalAccountDetailsCount: 0,
  withdrawalAccountDetailsCountCheckSuccess : false,
  withdrawalAccountDetailsDeleteCheckSuccess: false,
  withdrawalAccountDetailsFindOneCheckSuccess: false,
  withdrawalAccountDetailsOneData: [],
  withdrawalAccountDetailsGetCheckSuccess: false,
  withdrawalAccountDetailsData: [],
  withdrawalAccountDetailsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case WITHDRAWALACCOUNTDETAILS_POST_CHECK_SUCCESS:
      return {
        ...state,
        withdrawalAccountDetailsPostCheckSuccess: action.payload,
      };
      case WITHDRAWALACCOUNTDETAILS_SET_COUNT:
      return {
        ...state,
        withdrawalAccountDetailsCount: action.payload,
      };
      case WITHDRAWALACCOUNTDETAILS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        withdrawalAccountDetailsCountCheckSuccess: action.payload,
      };
      case WITHDRAWALACCOUNTDETAILS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        withdrawalAccountDetailsDeleteCheckSuccess: action.payload,
      };
      case WITHDRAWALACCOUNTDETAILS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        withdrawalAccountDetailsFindOneCheckSuccess: action.payload,
          };
      case WITHDRAWALACCOUNTDETAILS_SET_ONE_DATA:
      return {
        ...state,
        withdrawalAccountDetailsOneData: action.payload,
          };
      case WITHDRAWALACCOUNTDETAILS_GET_CHECK_SUCCESS:
      return {
        ...state,
        withdrawalAccountDetailsGetCheckSuccess: action.payload,
          };
      case WITHDRAWALACCOUNTDETAILS_SET_DATA:
      return {
        ...state,
        withdrawalAccountDetailsData: action.payload,
          };
      case WITHDRAWALACCOUNTDETAILS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        withdrawalAccountDetailsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
