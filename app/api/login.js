import axios from 'axios';
import { Alert } from "react-native";
import { loginFailure, loginLoading, loginSuccess } from '../actions';
import { setAccessToken, setUserData } from '../actions';
// import AsyncStorage from '@react-native-community/async-storage';

export const loginRequest = (mobile, password) => {
 
  let data = {
    identifier: mobile,
    password: password
  };
  console.log("login data from api",data)
  return dispatch => {
    dispatch(loginLoading(false));
    dispatch(loginSuccess(true));
    dispatch(loginFailure(false));
    const axiosLogin = axios.create();
    delete axiosLogin.defaults.headers;
    axiosLogin({
      method: 'post',
      url: 'auth/local',
      data: data,
      headers: {
        "Content-Type": "application/json"
      },
    }).then(response => {
      if (response.status !== 200) {
        throw Error(response.statusText);
      }
      return response.data;
    })
      .then(async data => {
        // AsyncStorage.setItem('userid',data.data.user.user_id);
        console.log("login after @@@@@@@@", data)
        dispatch(setUserData(data));
        dispatch(setAccessToken(data?.jwt));
        dispatch(loginLoading(false));
        dispatch(loginSuccess(true));
        dispatch(loginFailure(false));
      }
      ).catch(
        async err => {
          await dispatch(loginLoading(false));
          dispatch(loginSuccess(false));
          dispatch(loginFailure(true));
          console.log("error", err.response);
          if (err.response?.status === 400) {
            Alert.alert(err.response.data.message[0].messages[0].message)
          }
          // else {
          //   alert("Something Went Wrong")
          // }
        }
      )
  };
};

// export const loginRequest = (phone, password) => {
//   let data = {};
//   console.log("ygdjds")
//   data.identifier = phone;
//   data.password = password;
//   return dispatch => {
//     dispatch(loginLoading(true));
//     dispatch(loginSuccess(false));
//     dispatch(loginFailure(false));
//     const axiosLogin = axios.create();
//     delete axiosLogin.defaults.headers;
//     axiosLogin({
//       method: 'POST',
//       url: 'auth/local',
//       data: data
//     }).then(response => {
//       if (response.status !== 200) {
//         throw Error(response.statusText);
//       }
//       return response.data;
//     })
//       .then(async data => {
//         // AsyncStorage.setItem('userid',data.data.user.user_id);
//         console.log("loooogin afterrrr", data)
//         dispatch(setUserData(data));
//         dispatch(setAccessToken(data.jwt));
//         dispatch(loginLoading(false));
//         dispatch(loginSuccess(true));
//         dispatch(loginFailure(false));
//       }
//       ).catch(
//         async err => {
//           await dispatch(loginLoading(false));
//           dispatch(loginSuccess(false));
//           dispatch(loginFailure(true));
//           console.log(err)
//           if (err.response?.status === 400) {
//             alert("Mobile or password invalid")
//           } else {
//             alert("Something Went Wrong")
//           }
//         }
//       )
//   };
// };

export const loginGetRequest = () => {
  return dispatch => {
    const axiosLogin = axios.create();
    axiosLogin({
      method: 'get',
      url: 'auth/local',
    }).then(response => {
      console.log(response)
      if (response.status !== 200) {
        throw Error(response.statusText);
      }
      return response.data;
    })
      .then(async data => {
        // AsyncStorage.setItem('userid',data.data.user.user_id);
        console.log("loooogin afterrrr", data)
        dispatch(setUserData(data));
      }
      ).catch(
        err => {
          console.log(err)
        }
      )
  };
};
