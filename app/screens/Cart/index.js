import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
  ActivityIndicator,
  Dimensions,
  RefreshControl,
} from 'react-native';
import {
  Image,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  EventCard,
  Header,
  CartCard,
  TextInput,
  Icon,
} from '@components';
import { Badge, VStack, Box, Center, NativeBaseProvider } from 'native-base';
import _ from 'lodash';
import Modal from 'react-native-modal';
import RazorpayCheckout from 'react-native-razorpay';
import CheckBox from '@react-native-community/checkbox';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { PromotionData, TourData, HotelData, BestData } from '@data';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { cartsGetRequest, cartsDeleteRequest } from '../../api/carts';
import { productsGetRequest, productsPostRequest } from '../../api/products';
import { color } from 'react-native-reanimated';
import { modelOrdersPostRequest } from '../../api/modelOrders';
import Icon1 from 'react-native-vector-icons/Octicons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { PropTypes } from 'prop-types';
import { modelsGetRequest } from '../../api/models';
import { userCartsUpdateRequest, userCartsDeleteRequest, userCartsGetRequest } from '../../api/userCarts';
import axios from 'axios';
import { ordersItemsUpdateRequest, ordersUpdateRequest } from '../../api/orders';
import {
  bookmarksGetRequestByUser,
  bookmarksDeleteRequest,
  bookmarksPostRequest,
} from '../../api/bookmarks';
import { addressGetRequest } from '../../api';
import { Picker } from '@react-native-picker/picker';

export default function Cart({ navigation }) {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [orderId, setOrderId] = React.useState('');
  const [load, setLoad] = useState(false);
  console.log(orderId, 'orderId');
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const [modalVisible, setModalVisible] = useState(false);
  const [checked, setChecked] = useState(null)
  const { colors } = useTheme();
  const [imageData] = useState([]);
  const [adults, setAdults] = useState(2);
  const [isRefreshing, setIsRefreshing] = useState(false);
  console.log("isRefreshing", isRefreshing);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  const dispatch = useDispatch();
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  useEffect(() => {
    const obj = {};
    obj.user = UserData?.user.id;
    dispatch(userCartsGetRequest(obj));
    setLoad(false);
  }, [load, userCart, loading1]);
  const userCartData = useSelector(
    state => state.userCartsReducer.userCartsData,
  );
  console.log('userCartData', userCartData);
  const userCart = useSelector(
    state => state.userCartsReducer.userCartsDeleteCheckSuccess,
  );
  const loginModelData = useSelector(state => state.modelsReducer.modelsData);

  console.log('loginModelData', loginModelData);

  useEffect(() => {
    const obj = {};
    obj.user = UserData?.user?.id;
    dispatch(addressGetRequest(obj));

  }, [loading1]);

  const UserAddress = useSelector(state => state.addressReducer.addressData);
  console.log("address", UserAddress);

  const defaultAddress = UserAddress?.length > 0 && UserAddress?.filter((item) => item?.typeOfAddress === true)


  const modelId = loginModelData[0]?.id;

  const onRefresh = () => {
    //set isRefreshing to true
    setIsRefreshing(true)
    setLoading1(true)
    const obj = {};
    obj.user = UserData?.user.id;
    userCartsGetRequest(obj);
    setIsRefreshing(false);
    setLoading1(false)
    // and set isRefreshing to false at the end of your callApiMethod()
  }

  useEffect(() => {
    dispatch(bookmarksGetRequestByUser(UserData?.user?.id));
  }, []);
  const WhistList = useSelector(
    state => state.bookMarksReducer.bookmarksDataByUser,
  );
  const bookmark = useSelector(
    state => state.bookMarksReducer.bookmarksPostCheckSuccess,
  );
  console.log(WhistList, 'whishlist');
  // const loginModelData = useSelector(state => state.modelsReducer.modelsData);
  // const modelId=loginModelData[0]?.id;
  // useEffect(() => {
  //   dispatch(modelsGetRequest(modelId));
  // }, [dispatch])
  useEffect(() => {
    dispatch(productsGetRequest());
  }, [dispatch, userCart]);

  // useEffect(() => {
  //   dispatch(cartsGetRequest(modelId));
  // }, [dispatch]);
  // const cartsData = useSelector(state => state.cartsReducer.cartsData);
  // console.log(cartsData, 'cartsData');
  const productsData = useSelector(state => state.productsReducer.productsData);

  // console.log('cartsData', cartsData);
  // console.log("UserToken", userToken);
  const products = [];
  const sumValue = _.sumBy(userCartData, item =>
    Number(item?.product?.productPrice),
  );
  // const productArray = _.forEach(cartsData, i =>
  //   products.push({ _id: i?.product?.id }),
  // );
  console.log(sumValue, products, modelId);

  const handleSubmit = order => {
    const options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: 'INR',
      key: 'rzp_test_IJExsYXcfYSi2s',
      key_secret: 'kz8sE2zqgfJVDLAaEy0y5Twz',
      amount: totalSum * 100,
      name: 'LABAL',
      prefill: {
        email: 'ab@websoc.co.in',
        contact: '9191919191',
        name: 'Ashok',
      },
      theme: { color: '#53a20e' },
    };
    RazorpayCheckout.open(options)
      .then(data => {
        // handle success
        console.log('data razorpay', data);
        alert(`Success: ${data.razorpay_payment_id}`);
        const orderData = {};
        orderData.razerPayid = data.razorpay_payment_id;
        orderData.status = 'Payment success';
        const orderItemsData = {
          status: 'ordered',
        };
        userCartData.map(item => {
          dispatch(ordersItemsUpdateRequest(orderItemsData, item?.id));
        });
        dispatch(ordersUpdateRequest(orderData, order));
        navigation.navigate("OrdersList");
      })
      .catch(error => {
        // handle failure
        const orderData = {
          status: 'order not placed',
        };
        orderData.status = 'Payment failed';
        const orderItemsData = {};
        userCartData.map(item => {
          dispatch(ordersItemsUpdateRequest(orderItemsData, item?.id));
        });
        dispatch(ordersUpdateRequest(orderData, order));
        alert(`Error: ${error.code} | ${error.description}`);
      });
    userCartData.map(item => {
      dispatch(userCartsDeleteRequest(item?.id));
    });
  };
  // const PostOrderItems = order => {
  //   // eslint-disable-next-line array-callback-return
  //   userCartData?.length > 0 &&
  //     userCartData?.map(item => {
  //       const object = {};
  //       object.order = order;
  //       object.modelProducts = item?.modelProduct?.id;
  //       object.status = 'Received';
  //       console.log('itemitem', object);
  //       axios
  //         .post('https://staging-backend.labl.store/order-items', object, {
  //           headers: {
  //             'content-type': 'application/json',
  //             Authorization: `Bearer ${UserData.jwt}`,
  //           },
  //         })
  //         .then(({ data }) => {
  //           console.log(data, 'orderitemdata');
  //           // setOrderId(data.id);
  //           // PostOrderItems();
  //         })
  //         .catch(() => { });
  //     });
  //   handleSubmit(order);
  // };
  // const PostOrder = () => {
  //   const orderObject = {};
  //   orderObject.currency = 'INR';
  //   orderObject.amount = Number(totalSum);
  //   orderObject.users_permissions_user = UserData?.user.id;
  //   orderObject.status = 'Payment pending';
  //   console.log(orderObject, 'orderObject');
  //   axios
  //     .post('https://staging-backend.labl.store/orders', orderObject, {
  //       headers: {
  //         'content-type': 'application/json',
  //         Authorization: `Bearer ${UserData.jwt}`,
  //       },
  //     })
  //     .then(({ data }) => {
  //       console.log(data);
  //       setOrderId(data.id);
  //       PostOrderItems(data.id);
  //     })
  //     .catch(() => { });
  // };

  const UserOrder =
    userCartData?.length > 0 &&
    userCartData?.map(item => ({
      status: 'order not placed',
      modelProduct: item?.modelProduct?.id,
      productVarient: item?.productVarient?.id,
      label: item?.modelProduct?.product?.label?.id,
      user: UserData?.user?.id,
      model: item?.modelProduct?.model?.id,
      quantity: item?.quantity,
    }));
  console.log(UserOrder, 'userOrderObject');

  const postUserOrder = () => {
    const userOrderObject = {};
    userOrderObject.status = 'Payment pending';
    userOrderObject.orderObjects = UserOrder;
    userOrderObject.user = UserData?.user?.id;
    userOrderObject.address = checked?.id
    console.log(userOrderObject, "userOrderObject");
    axios
      .post('https://staging-backend.labl.store/orders', userOrderObject, {
        headers: {
          'content-type': 'application/json',
          Authorization: `Bearer ${UserData.jwt}`,
        },
      })
      .then(({ data }) => {
        console.log(data, 'orderuser');
        setOrderId(data.id);
        handleSubmit(data.id);
      })
      .catch(() => { });
  };

  // console.log(postUserOrder, 'postUserOrder');
  // cartsData?.forEach((i) => {
  //   products.push({ _id: i?.product?.id });
  // })

  function Example() {
    return (
      <Box alignItems="center">
        <VStack>
          <Badge
            bg="#fff"
            // colorScheme="danger"
            rounded="full"
            mb={-4}
            mr={-4}
            zIndex={1}
            variant="solid"
            alignSelf="flex-end"
            _text={{
              fontSize: 12,
              color: "#000"
            }}>
            {WhistList?.length ? WhistList?.length : "0"}
          </Badge>
          <TouchableOpacity
            style={{
              height: 35,
              width: 35,
              borderRadius: 30,
              backgroundColor: '#2F2F2F',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../assets/images/Vector.png')}
              style={{ height: 17, width: 20 }}
            />
          </TouchableOpacity>
        </VStack>
      </Box>
    );
  }

  const toggleModal = (item) => {
    setChecked(item);
    setModalVisible(!modalVisible)
  }
  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const [isSelected, setSelection] = useState(false);

  const heightImageBanner = Utils.scaleWithPixel(60);
  const marginTopBanner = heightImageBanner - heightHeader;

  const productPrice =
    userCartData.length > 0 &&
    userCartData?.map(item => item?.lablExclusive || item?.lablFranchise || item?.lablPromoted ? (item?.product?.productPrice * item?.quantity) : (item?.modelProduct?.priceOfProductWithModelCommission * item?.quantity));
  console.log(productPrice, 'productPrice');
  const totalSum =
    productPrice.length > 0 &&
    productPrice.reduce((partialSum, a) => Number(partialSum) + Number(a), 0);
  console.log(totalSum, 'totalSum');

  const [newPrice, setNewPrice] = useState(null);
  console.log("newPrice", newPrice);
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 500)
  }, [userCart, isRefreshing, bookmark]);

  const upDatePnQ = (id, q, p) => {
    const object = {}
    object.quantity = q
    dispatch(userCartsUpdateRequest(object, id));
    setLoad(true);
  };

  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('Shopping Bag')}
        style={{ height: 80, backgroundColor: '#000' }}
        renderLeft={() => {
          return (
            <View
              style={{
                flexDirection: 'row',
                width: 200,
                // justifyContent: 'space-between',
                alignSelf: 'flex-start',
                top: -10,
              }}>
              <View
                style={{
                  height: 30,
                  width: 30,
                  borderRadius: 30,
                  backgroundColor: '#fff',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
              <Text
                style={{
                  alignSelf: 'center',
                  fontFamily: 'Roboto-Bold',
                  fontSize: 20,
                  color: '#fff',
                  // right: 20,
                  marginHorizontal: 20
                }}>
                Shopping Bag
              </Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
        renderRight={() => {
          return (
            <NativeBaseProvider>
              <Center flex={1} px="3">
                <Example />
              </Center>
            </NativeBaseProvider>
          );
        }}
        // renderRightSecond={() => {
        //   return <TouchableOpacity style={{ height: 35, width: 35, borderRadius: 30, backgroundColor: "#2F2F2F", justifyContent: "center", alignItems: "center" }}>
        //     <Image source={require('../../assets/images/trash.png')} style={{ height: 17, width: 17 }} />
        //   </TouchableOpacity>
        // }}
        onPressRight={() => {
          navigation.navigate('Wishlist');
        }}
      />

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        {userCartData.length > 0 ? (
          <ScrollView
            onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
            refreshControl={
              <RefreshControl
                refreshing={isRefreshing}
                onRefresh={onRefresh}
                tintColor="#F8852D" />
            }
            scrollEventThrottle={8}>
            <View style={{ flex: 1, padding: 20 }}>
              {UserAddress?.length > 0 ?
                checked ?
                  <>
                    <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20, marginBottom: 10, marginHorizontal: 20 }}>Delivery Address :- </Text>
                    <View style={{ width: "90%", borderWidth: 1, borderColor: "#000", borderTopLeftRadius: 10, borderTopRightRadius: 10, alignSelf: "center" }}>
                      <View style={{ flexDirection: "row", padding: 10, justifyContent: "space-around" }}>
                        <View style={{ flexDirection: "column" }}>
                          <View style={{ flexDirection: "row" }}>
                            <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 20, marginVertical: 5 }}>{checked?.fullName}</Text>
                          </View>
                          <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 16, marginVertical: 10 }}>{checked?.houseNo},{checked?.areaColony},{checked?.city},{'\n'}Pincode: {checked?.pincode}</Text>
                          <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 16, marginVertical: 10 }}>Phone : {checked?.mobileNumber}</Text>
                        </View>
                      </View>
                    </View>
                    <TouchableOpacity onPress={() => setModalVisible(true)} style={{ width: "90%", borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderWidth: 1, padding: 10, borderColor: "#000", justifyContent: "center", alignItems: "center", alignSelf: "center" }}>
                      <Text style={{ fontFamily: "Roboto-Medium", fontSize: 18 }}>Change</Text>
                    </TouchableOpacity>
                  </> :
                  <>
                    <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20, marginBottom: 10, marginHorizontal: 20 }}>Delivery Address :- </Text>
                    <View style={{ width: "90%", borderWidth: 1, borderColor: "#000", borderTopLeftRadius: 10, borderTopRightRadius: 10, alignSelf: "center" }}>
                      <View style={{ flexDirection: "row", padding: 10, justifyContent: "space-around" }}>
                        <View style={{ flexDirection: "column" }}>
                          <View style={{ flexDirection: "row" }}>
                            <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 20, marginVertical: 5 }}>{defaultAddress[0]?.fullName}</Text>
                          </View>
                          <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 16, marginVertical: 10 }}>{defaultAddress[0]?.houseNo},{defaultAddress[0]?.areaColony},{defaultAddress[0]?.city},{'\n'}Pincode: {defaultAddress[0]?.pincode}</Text>
                          <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 16, marginVertical: 10 }}>Phone : {defaultAddress[0]?.mobileNumber}</Text>
                        </View>
                      </View>
                    </View>
                    <TouchableOpacity onPress={() => setModalVisible(true)} style={{ width: "90%", borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderWidth: 1, padding: 10, borderColor: "#000", justifyContent: "center", alignItems: "center", alignSelf: "center" }}>
                      <Text style={{ fontFamily: "Roboto-Medium", fontSize: 18 }}>Change</Text>
                    </TouchableOpacity>
                  </>
                : null}
            </View>
            <FlatList
              vertical
              // numColumns={2}
              data={userCartData}
              // style={{ margin: 10 }}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => {
                return (
                  item?.lablExclusive || item?.lablFranchise || item?.lablPromoted ?
                    (<CartComponent
                      image={
                        item?.product?.productImages[0]?.url
                      }
                      name={item?.product?.productBrandName}
                      description={item?.product?.productName}
                      price={item?.product?.productPrice}
                      Pcolor={item?.productVarient?.color?.color}
                      size={item?.productVarient?.size?.type}
                      // qty={item?.quantity}
                      id={item?.id}
                      object={item}
                      upDatePnQ={(id, q, p) => upDatePnQ(id, q, p)}
                    />) :
                    (<CartComponent
                      image={
                        item?.modelProduct?.product?.productImages[0]?.url
                      }
                      name={item?.modelProduct?.product?.productBrandName}
                      description={item?.modelProduct?.product?.productName}
                      price={item?.modelProduct?.priceOfProductWithModelCommission}
                      Pcolor={item?.productVarient?.color?.color}
                      size={item?.productVarient?.size?.type}
                      // qty={item?.quantity}
                      id={item?.id}
                      object={item}
                      upDatePnQ={(id, q, p) => upDatePnQ(id, q, p)}
                    />)
                );
              }}
            />
            <View style={{ padding: 20 }}>
              <Text
                style={{
                  fontFamily: 'Roboto-Bold',
                  fontSize: 16,
                  marginVertical: 10,
                }}>
                Order Details
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginVertical: 5,
                }}>
                <Text style={{ fontFamily: 'Roboto-Regluar', fontSize: 14 }}>
                  Total MRP
                </Text>
                <Text style={{ fontFamily: 'Roboto-Regluar', fontSize: 14 }}>
                  ₹ {Number(totalSum)}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginVertical: 5,
                }}>
                <Text style={{ fontFamily: 'Roboto-Regluar', fontSize: 14 }}>
                  Bag Savings
                </Text>
                <Text style={{ fontFamily: 'Roboto-Regluar', fontSize: 14 }}>
                  ₹ 0
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginVertical: 5,
                }}>
                <Text style={{ fontFamily: 'Roboto-Regluar', fontSize: 14 }}>
                  Delivery
                </Text>
                <Text style={{ fontFamily: 'Roboto-Regluar', fontSize: 14 }}>
                  Free
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  borderWidth: 0.5,
                  borderColor: '#CDCDCD',
                  marginVertical: 10,
                }}
              />
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginVertical: 5,
                }}>
                <Text style={{ fontFamily: 'Roboto-Regluar', fontSize: 14 }}>
                  Amount Payable
                </Text>
                <Text style={{ fontFamily: 'Roboto-Regluar', fontSize: 14 }}>
                  ₹ {Number(totalSum)}
                </Text>
              </View>
            </View>

            <Modal
              isVisible={modalVisible}
              onBackdropPress={() => setModalVisible(false)}
              onSwipeComplete={() => setModalVisible(false)}
              swipeDirection={['down']}
              style={{ justifyContent: "flex-end", margin: 10 }}>
              <View style={{ flex: 1, backgroundColor: "#fff", padding: 20 }}>

                <TouchableOpacity onPress={() => setModalVisible(!modalVisible)} >
                  <Entypo name="cross" size={30} color="#000" style={{ alignSelf: "flex-end" }} />
                </TouchableOpacity>
                <ScrollView>
                  {UserAddress.map((item) => (
                    <TouchableOpacity onPress={() => toggleModal(item)} style={{ flex: 1, width: "100%", borderWidth: 1, borderColor: "#909090", borderRadius: 10, margin: 20, alignSelf: "center", marginBottom: 10, padding: 10 }}>
                      <View style={{ flexDirection: "row", padding: 10, justifyContent: "space-around" }}>
                        <View style={{ flexDirection: "column" }}>
                          <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 22, marginVertical: 5 }}>{item?.fullName}</Text>
                          </View>
                          <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 20, marginVertical: 0 }}>{item?.houseNo},{item?.areaColony},{item?.city},Pincode: {item?.pincode}</Text>
                          <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 20, marginVertical: 0 }}>Phone : {item?.mobileNumber}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  ))}
                </ScrollView>
              </View>
            </Modal>
            <View
              style={{
                paddingTop: 0,
                paddingLeft: 30,
                justifyContent: 'center',
                flexDirection: 'row',
              }}></View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                marginVertical: 20,
              }}>
              <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                <Icon1
                  name="verified"
                  size={32}
                  color="#CDCDCD"
                  style={{ marginVertical: 10 }}
                />
                <Text
                  style={{
                    fontFamily: 'Roboto-Medium',
                    fontSize: 18,
                    textAlign: 'center',
                    color: '#CDCDCD',
                  }}>
                  Genuine{'\n'}Product
                </Text>
              </View>
              <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                <Icon2
                  name="smiley"
                  size={32}
                  color="#CDCDCD"
                  style={{ marginVertical: 10 }}
                />
                <Text
                  style={{
                    fontFamily: 'Roboto-Medium',
                    fontSize: 18,
                    textAlign: 'center',
                    color: '#CDCDCD',
                  }}>
                  Happy{'\n'}Customers
                </Text>
              </View>
              <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                <Icon3
                  name="shield-check-outline"
                  size={32}
                  color="#CDCDCD"
                  style={{ marginVertical: 10 }}
                />
                <Text
                  style={{
                    fontFamily: 'Roboto-Medium',
                    fontSize: 18,
                    textAlign: 'center',
                    color: '#CDCDCD',
                  }}>
                  Quality{'\n'}Checked
                </Text>
              </View>
            </View>
            {/* <View style={[styles.line, { backgroundColor: colors.border }]} /> */}
            <TouchableOpacity
              style={{
                marginVertical: 20,
                backgroundColor: '#000',
                height: 50,
                width: "40%",
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 10,
              }}
              onPress={postUserOrder}>
              <Text
                style={{
                  color: '#fff',
                  fontFamily: 'Roboto-Bold',
                  fontSize: 16,
                }}>
                {t('PLACE ORDER')}
              </Text>
            </TouchableOpacity>
          </ScrollView>
        ) : (
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              flexDirection: 'column',
              top: 0,
              justifyContent: 'center',
            }}>
            <Image
              source={require('../../assets/images/bag-1.png')}
              style={{ height: 100, width: 100, marginVertical: 30 }}
            />
            <Text
              style={{
                alignSelf: 'center',
                fontFamily: 'Roboto-Bold',
                fontSize: 20,
                color: '#000',
                marginVertical: 0,
              }}>
              Your Bag is empty
            </Text>
            <Text
              style={{
                alignSelf: 'center',
                fontFamily: 'Roboto-Medium',
                fontSize: 16,
                color: '#000',
                textAlign: 'center',
                marginVertical: 20,
              }}>
              There is nothing in your bag,{'\n'}Lets add some items.
            </Text>
            <TouchableOpacity
              onPress={() => navigation.navigate("HomeScreenUserLABL")}
              style={{
                height: 40,
                width: 160,
                backgroundColor: '#000',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
                marginVertical: 20,
              }}>
              <Text
                style={{
                  fontFamily: 'Roboto-SemiBold',
                  fontSize: 18,
                  color: '#fff',
                }}>
                Shop now
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}

function CartComponent(props) {
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const [modalVisible, setModalVisible] = useState(false);
  const [NP, setNP] = useState(null);
  const { image, name, description, price, size, Pcolor, id, object } = props;
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log('object', object);
  const toggleModal = () => {
    console.log('hello world');
    setModalVisible(!modalVisible);
  };
  const [value, setValue] = useState(1)
  // useEffect(() => {
  // }, [])

  const onChange = (type) => {
    if (type == 'up') {
      props.upDatePnQ(id, value + 1, price * (value + 1))
      setValue(value + 1);
      setNP(price * (value + 1));
    } else {
      props.upDatePnQ(id, value - 1 > 0 ? value - 1 : 0, price * (value - 1 > 0 ? value - 1 : 0))
      setValue(value - 1 > 0 ? value - 1 : 0);
      setNP(price * (value - 1 > 0 ? value - 1 : 0));
    }
  };

  const deleteProduct = id => {
    console.log('del button pressed', id);
    dispatch(userCartsDeleteRequest(id));
    setModalVisible(!modalVisible);
  };

  const WhistList = useSelector(
    state => state.bookMarksReducer.bookmarksDataByUser,
  );
  const [productId, setProductId] = useState('');
  const [productmarked, setProductmarked] = useState(false);

  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) {
      return o.modelProduct?.id === object?.modelProduct?.id;
    });
    console.log(whishlistitems, 'whishlistitems');
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true);
    }
  }, []);

  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) {
      return o.modelProduct?.id === object?.modelProduct?.id;
    });
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true);
    }
  }, [WhistList]);
  const addWhishlist = async () => {
    if (productmarked) {
      setProductmarked(false);
      await dispatch(bookmarksDeleteRequest(productId));
    } else {
      setProductmarked(true);
      const objectValue = {};
      objectValue.user = UserData?.user?.id;
      objectValue.modelProduct = object?.modelProduct?.id;
      console.log('bookmarkpost', objectValue);
      // setLoading(true);

      await dispatch(bookmarksPostRequest(objectValue));
      dispatch(userCartsDeleteRequest(id));
      setModalVisible(!modalVisible);
    }
  };
  return (
    <View
      style={{
        // height: 230,
        width: 300,
        borderWidth: 1,
        borderColor: '#CDCDCD',
        borderRadius: 10,
        margin: 20,
        alignSelf: 'center',
        marginBottom: 10,
      }}>
      <View
        style={{
          flexDirection: 'row',
          padding: 10,
          justifyContent: 'space-around',
        }}>
        <Image
          source={{ uri: image }}
          style={{ height: 160, width: 120, borderRadius: 10 }}
        />
        <View style={{ flexDirection: 'column', marginVertical: 5 }}>
          <Text
            style={{
              width: 130,
              fontFamily: 'Roboto-Bold',
              fontSize: 14,
              marginVertical: 3,
            }}>
            {name}
          </Text>
          <Text
            // numberOfLines={2}
            style={{
              width: 120,
              fontFamily: 'Roboto-Regular',
              fontSize: 12,
              marginVertical: 3,
            }}>
            {description}
          </Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 14 }}>
            Size : {size}
          </Text>
          {/* <View style={{flexDirection:"row"}}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 13, marginVertical: 10 }}>
              Color : 
            </Text>
            <View style={{ margin: 5, elevation: 5, backgroundColor: `${Pcolor.toLowerCase()}`, borderRadius: 20, width: 25, height: 25, justifyContent: "center", alignItems: "center" }}/>
          </View> */}
          <View
            style={{
              // height: 25,
              // width: 55,
              flexDirection: "row",
              // backgroundColor: '#F3F3F3',
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 7,
            }}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 14 }}>
              Qty
            </Text>
            <View style={styles.contentPicker}>
              {/* <Text body1 numberOfLines={1} style={{ marginBottom: 5, fontFamily: "Roboto-SemiBold", fontSize: 22, marginHorizontal: 0 }}>
                Qty -
              </Text> */}

              {value > 1 ?
                <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => onChange('down')}>
                  <Icon name="minus-circle" size={22} color={BaseColor.grayColor} />
                </TouchableOpacity> :
                <TouchableOpacity disabled={true} style={{ marginHorizontal: 5 }} onPress={() => onChange('down')}>
                  <Icon name="minus-circle" size={22} color={BaseColor.grayColor} />
                </TouchableOpacity>}
              <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20 }}>{value}</Text>
              <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => onChange('up')}>
                <Icon name="plus-circle" size={22} color={colors.primary} />
              </TouchableOpacity>
            </View>
          </View>
          {value > 1 ?
            <Text
              style={{
                fontFamily: 'Roboto-SemiBold',
                fontSize: 18,
                marginVertical: 10,
              }}>
              ₹ {NP}
            </Text> :
            <Text
              style={{
                fontFamily: 'Roboto-Medium',
                fontSize: 15,
                marginVertical: 0,
              }}>
              ₹ {price}
            </Text>}

        </View>
      </View>
      <View
        style={{
          width: 300,
          borderTopWidth: 0.5,
          borderTopColor: '#CDCDCD',
          alignItems: "center",
          justifyContent:"center",
          marginVertical: 10,
        }}
      >
        <TouchableOpacity onPress={toggleModal} style={{marginTop:5}}>
          <Text
            style={{
              fontFamily: 'Roboto-Medium',
              fontSize: 18,
              alignSelf: 'center',
            }}>
            Remove
          </Text>
        </TouchableOpacity>
      </View>
      <View>
        <Modal
          isVisible={modalVisible}
          onBackdropPress={() => setModalVisible(false)}
          onSwipeComplete={() => setModalVisible(false)}
          swipeDirection={['down']}
          style={styles.bottomModal}>
          <View style={styles.contain}>
            <View
              style={{
                paddingTop: 20,
                paddingLeft: 30,
                justifyContent: 'center',
                flexDirection: 'row',
              }}>
              <Image
                source={{ uri: image }}
                style={{ height: 150, width: 120, borderRadius: 10 }}
              />
              <View style={{ flexDirection: 'column', padding: 20 }}>
                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 22 }}>
                  Remove from bag?
                </Text>
                <Text
                  style={{
                    fontFamily: 'Roboto-Regular',
                    fontSize: 17,
                    marginVertical: 20,
                  }}>
                  You can save products to your{'\n'}wishlist to use later.
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
                height: 90,
                width: '100%',
                backgroundColor: '#fff',
              }}>
              <TouchableOpacity
                onPress={() => deleteProduct(id)}
                style={{
                  backgroundColor: '#D9D9D9',
                  height: 50,
                  width: 150,
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{ fontFamily: 'Roboto-SemiBold', fontSize: 20 }}>
                  Remove
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => addWhishlist()}
                style={{
                  backgroundColor: '#000',
                  height: 50,
                  width: 190,
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'Roboto-SemiBold',
                    fontSize: 20,
                    color: '#fff',
                  }}>
                  Move to Wishlist
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
}

CartComponent.propTypes = {
  image: PropTypes.node.isRequired,
  name: PropTypes.string,
  price: PropTypes.string,
  description: PropTypes.string,
};

CartComponent.defaultProps = {
  image: '',
  name: '',
  price: '',
  description: '',
};
