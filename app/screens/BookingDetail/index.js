import React, { useState } from 'react';
import { View, ScrollView, FlatList ,Button} from 'react-native';
import { BaseStyle, BaseColor, useTheme, Images } from '@config';
import { Header, SafeAreaView, Icon, Text, OrderCard, PostListItem } from '@components';
import { TabView, TabBar } from 'react-native-tab-view';
import { useTranslation } from 'react-i18next';
import styles from './styles';
import { ListItem, Avatar } from 'react-native-elements'

export default function BookingDetail({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [index, setIndex] = useState(0);

  const [routes] = useState([
    { key: 'preview', title: t('Order ID') },
    { key: 'confirm', title: t('ORDER DATE') },
    { key: 'complete', title: t('ADDRESS') },
    { key: 'detail', title: t('ORDER TOTAL') },

  ]);

  // When tab is activated, set what's index value
  const handleIndexChange = index => {
    setIndex(index);
  };

  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, { backgroundColor: colors.primary }]}
      style={[styles.tabbar, { backgroundColor: colors.background }]}
      tabStyle={styles.tab}
      inactiveColor={BaseColor.grayColor}
      activeColor={colors.text}
      renderLabel={({ route, focused, color }) => (
        <View style={{ flex: 1, alignItems: 'center', width: 100 }}>
          <Text headline semibold={focused} style={{ color }}>
            {route.title}
          </Text>
        </View>
      )}
    />
  );

  // Render correct screen container when tab is activated
  const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'preview':
        return <PreviewTab jumpTo={jumpTo} navigation={navigation} />;
      case 'confirm':
        return <ConfirmTab jumpTo={jumpTo} navigation={navigation} />;
      case 'complete':
        return <CompleteTab jumpTo={jumpTo} navigation={navigation} />;
      case 'detail':
        return <DetailTab jumpTo={jumpTo} navigation={navigation} />;

    }
  };


  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('Order Details')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}

        // renderRight={() => {
        //   return (
        //     <Text headline primaryColor numberOfLines={1}>
        //       {t('save')}
        //     </Text>
        //   );
        // }}
        onPressLeft={() => {
          navigation.goBack();
        }}
        onPressRight={() => {
          navigation.navigate('Home');
        }}
      />


      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <TabView
          lazy
          navigationState={{ index, routes }}
          renderScene={renderScene}
          renderTabBar={renderTabBar}
          onIndexChange={handleIndexChange}
        />
      </SafeAreaView>
    </View>
  );
}

/**
 * @description Show when tab Preview activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function PreviewTab() {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [bookID] = useState('01233');
  const [Booking] = useState([
    {
      id: '0',
      image: Images.room20,
      title: 'Mens T-shirt',
      time: '$1300',
      location: '',
    },
    {
      id: '1',
      image: Images.room21,
      title: 'Mens hoodie',
      time: '$4000',
      location: '',

    },
    {
      id: '0',
      image: Images.room22,
      title: 'womens croptop',
      time: ' $3000',
      location: '',
    },
  ]);




  return (
    <ScrollView contentContainerStyle={{ padding: 18, }}>

      <View>
        <FlatList
          contentContainerStyle={{
            paddingRight: 25,
            paddingLeft: 0,
          }}
          vertical={true}
          data={Booking}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) => (
            <PostListItem
              description="Diffie on Friday announced he had contracted the coronavirus, becoming the first country star to go public with such a diagnosis."
              title="Top 15 Things Must To Do"
              style={{ marginTop: 10, width: '100%' }}
              image={Images.trip8}
              
              onPress={() => {
                navigation.navigate('Post');
              }}
              
            />
            
          )}
        />
          <Button
        title="Press me"
        
        onPress={() => navigation.navigate('Post')}
      />
      </View>

      {/* <Icon
        name="copy"
        size={72}
        color={colors.primaryLight}
        style={{paddingTop: 50, paddingBottom: 20}}
      /> */}




      {/* <Text title3 style={{marginVertical: 25}} semibold>
        {t('booking_id')} {bookID}
      </Text> */}
      {/* <Text body1 grayColor style={{textAlign: 'center'}}>
        {t('payment_completed_text')}
      </Text> */}
    </ScrollView>
  );
}


/**
 * @description Show when tab Confirm activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function ConfirmTab() {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [bookID] = useState('01233');
  const [Booking] = useState([
    {
      id: '0',
      image: Images.room23,
      title: 'Womens Cotton Paint',
      time: '$1300',
      location: '',
    },
    {
      id: '1',
      image: Images.room24,
      title: 'Womens crop top',
      time: '$4000',
      location: '',
    },
    {
      id: '0',
      image: Images.room25,
      title: 'cold shoulder top',
      time: ' $3000',
      location: '',
    },
  ]);
  const list = [
    {
      orderdate: 'september 4,2021',

    },
    {
      orderdate: 'september 3,2021',

    },
    {
      orderdate: 'september 2,2021',

    },
    {
      orderdate: 'september 1,2021',

    },
    {
      orderdate: 'october 30,2021',

    },
    {
      orderdate: 'october 29,2021',

    },
  ]
  return (
    <ScrollView contentContainerStyle={{ padding: 20, }}>
      <View>
        {
          list.map((l, i) => (
            <ListItem key={i} bottomDivider>
              <ListItem.Content>
                <ListItem.Title>{l.orderdate}</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          ))
        }
      </View>
      {/* <View>
                <FlatList
                  contentContainerStyle={{
                    paddingRight: 20,
                    paddingLeft: 0,
              
                  }}
                  vertical={true}
                  data={Booking}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({item, index}) => (
                    <OrderCard
                      image={item.image}
                      title={item.title}
                      time={item.time}
                      location={item.location}
                      onPress={() => navigation.navigate('EarnDetail')}
                      style={{marginLeft: 15,height:-10,}}
                    />
                  )}
                />
              </View> */}



    </ScrollView>
  );
}

/**
 * @description Show when tab Detail activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function CompleteTab() {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [bookID] = useState('01233');
  const [Booking] = useState([
    {
      id: '0',
      image: Images.room26,
      title: 'crop top ',
      time: '$1300',
      location: '',
    },
    {
      id: '1',
      image: Images.room27,
      title: 'Stylish women wear',
      time: '$4000',
      location: '',
    },
    {
      id: '0',
      image: Images.room28,
      title: 'Black&white checks ',
      time: ' $3000',
      location: '',
    },
  ]);
  const list = [
    {
      address: 'Sanali Info Park, 8 2, C Block, Road 2, Banjara Hils ,HYDERABAD, TELANGANA 500034,India',

    },
    {
      address: 'Sanali Info Park, 8 2, C Block, Road 2, Banjara Hils ,HYDERABAD, TELANGANA 500034,India',

    },
    {
      address: 'Sanali Info Park, 8 2, C Block, Road 2, Banjara Hils ,HYDERABAD, TELANGANA 500034,India',

    },
    {
      address: 'Sanali Info Park, 8 2, C Block, Road 2, Banjara Hils ,HYDERABAD, TELANGANA 500034,India',

    },
    {
      address: 'Sanali Info Park, 8 2, C Block, Road 2, Banjara Hils ,HYDERABAD, TELANGANA 500034,India',

    },

  ]
  return (
    <ScrollView contentContainerStyle={{ padding: 20, }}>
      <View>
        {
          list.map((l, i) => (
            <ListItem key={i} bottomDivider>
              <ListItem.Content>
                <ListItem.Title>{l.address}</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          ))
        }
      </View>
      {/* <View>
                <FlatList
                  contentContainerStyle={{
                    paddingRight: 20,
                    paddingLeft: 0,
              
                  }}
                  vertical={true}
                  data={Booking}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({item, index}) => (
                    <OrderCard
                      image={item.image}
                      title={item.title}
                      time={item.time}
                      location={item.location}
                      onPress={() => navigation.navigate('EarnDetail')}
                      style={{marginLeft: 15,height:-10,}}
                    />
                  )}
                />
              </View> */}

    </ScrollView>
  );
}

/**
 * @description Show when tab Complete activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function DetailTab() {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [bookID] = useState('01233');
  const [Booking] = useState([
    {
      id: '0',
      image: Images.room29,
      title: 'Floral Top',
      time: '$1300',
      location: '',
    },
    {
      id: '1',
      image: Images.room30,
      title: 'stylish Top',
      time: '$4000',
      location: '',
    },
    {
      id: '0',
      image: Images.room31,
      title: 'Stylish Skirt',
      time: ' $3000',
      location: '',
    },
  ]);
  const list = [
    {
      total: '$3702',

    },
    {
      total: '$3702',

    },
    {
      total: '$3702',

    },
    {
      total: '3702',

    },
    {
      total: '3702',

    },

  ]
  return (
    <ScrollView contentContainerStyle={{ padding: 20, }}>
      <View>
        {
          list.map((l, i) => (
            <ListItem key={i} bottomDivider>
              <ListItem.Content>
                <ListItem.Title>{l.total}</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          ))
        }
      </View>
      {/* <View>
                <FlatList
                  contentContainerStyle={{
                    paddingRight: 20,
                    paddingLeft: 0,
              
                  }}
                  vertical={true}
                  data={Booking}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({item, index}) => (
                    <OrderCard
                      image={item.image}
                      title={item.title}
                      time={item.time}
                      location={item.location}
                      onPress={() => navigation.navigate('EarnDetail')}
                      style={{marginLeft: 15,height:-10,}}
                    />
                  )}
                />
              </View> */}
    </ScrollView>
  );
}
/**
 * @description Show when tab Preview activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function RefundTab() {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [bookID] = useState('01233');
  const [Booking] = useState([
    {
      id: '0',
      image: Images.room32,
      title: 'Green Kurti',
      time: '$1300',
      location: '',
    },
    {
      id: '1',
      image: Images.room33,
      title: 'Long Frok',
      time: '$4000',
      location: '',
    },
    {
      id: '0',
      image: Images.room34,
      title: 'Stylish top',
      time: ' $3000',
      location: '',
    },
  ]);
  return (
    <ScrollView contentContainerStyle={{ padding: 20, alignItems: 'center' }}>
      {/* <View>
                <FlatList
                  contentContainerStyle={{
                    paddingRight: 20,
                    paddingLeft: 0,
              
                  }}
                  vertical={true}
                  data={Booking}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({item, index}) => (
                    <OrderCard
                      image={item.image}
                      title={item.title}
                      time={item.time}
                      location={item.location}
                      onPress={() => navigation.navigate('EarnDetail')}
                      style={{marginLeft: 15,height:-10,}}
                    />
                  )}
                />
              </View> */}
    </ScrollView>
  );
}
