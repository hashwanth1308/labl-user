import {
  ORDERMESSAGES_POST_CHECK_SUCCESS,
  ORDERMESSAGES_SET_COUNT,
  ORDERMESSAGES_COUNT_CHECK_SUCCESS,
  ORDERMESSAGES_DELETE_CHECK_SUCCESS,
  ORDERMESSAGES_FINDONE_CHECK_SUCCESS,
  ORDERMESSAGES_SET_ONE_DATA,
  ORDERMESSAGES_GET_CHECK_SUCCESS,
  ORDERMESSAGES_SET_DATA,
  ORDERMESSAGES_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function orderMessagesPostCheckSuccess(bool = false) {
  return {
    type: ORDERMESSAGES_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function orderMessagesSetCount(object) {
  return {
    type: ORDERMESSAGES_SET_COUNT,
    payload: object,
  };
}
export function orderMessagesCountCheckSuccess(bool = false) {
  return {
    type: ORDERMESSAGES_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function orderMessagesDeleteCheckSuccess(bool = false) {
  return {
    type: ORDERMESSAGES_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function orderMessagesFindOneCheckSuccess(bool = false) {
  return {
    type: ORDERMESSAGES_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function orderMessagesSetOneData(object) {
  return {
    type: ORDERMESSAGES_SET_ONE_DATA,
    payload: object,
  };
}
export function orderMessagesGetCheckSuccess(bool = false) {
  return {
    type: ORDERMESSAGES_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function orderMessagesSetData(object) {
  return {
    type: ORDERMESSAGES_SET_DATA,
    payload: object,
  };
}

export function orderMessagesUpdateCheckSuccess(bool = false) {
  return {
    type: ORDERMESSAGES_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

