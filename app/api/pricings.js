import{ 
    
  pricingsPostCheckSuccess,
  pricingsSetCount,
  pricingsCountCheckSuccess,
  pricingsDeleteCheckSuccess,
  pricingsFindOneCheckSuccess,
  pricingsSetOneData,
  pricingsGetCheckSuccess,
  pricingsSetData,
  pricingsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function pricingsPostRequest(element) {
      return dispatch => {
        dispatch(pricingsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `pricings`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(pricingsSetData(a));
            dispatch(pricingsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(pricingsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function pricingsCountRequest(element) {
      return dispatch => {
          dispatch(pricingsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `pricings/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(pricingsSetCount(data));
              dispatch(pricingsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(pricingsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function pricingsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(pricingsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `pricings/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(pricingsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(pricingsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function pricingsFindOneRequest(id) {
      return dispatch => {
        dispatch(pricingsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `pricings/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(pricingsOneData(data));
            dispatch(pricingsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(pricingsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function pricingsGetRequest(element) {
  return dispatch => {
    dispatch(pricingsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `pricings`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(pricingsSetOneData(data));
        dispatch(pricingsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(pricingsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function pricingsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(pricingsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `pricings/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(pricingsSetData(data));
              dispatch(pricingsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(pricingsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 