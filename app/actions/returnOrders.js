import {
  RETURNORDERS_POST_CHECK_SUCCESS,
  RETURNORDERS_SET_COUNT,
  RETURNORDERS_COUNT_CHECK_SUCCESS,
  RETURNORDERS_DELETE_CHECK_SUCCESS,
  RETURNORDERS_FINDONE_CHECK_SUCCESS,
  RETURNORDERS_SET_ONE_DATA,
  RETURNORDERS_GET_CHECK_SUCCESS,
  RETURNORDERS_SET_DATA,
  RETURNORDERS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function returnOrdersPostCheckSuccess(bool = false) {
  return {
    type: RETURNORDERS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function returnOrdersSetCount(object) {
  return {
    type: RETURNORDERS_SET_COUNT,
    payload: object,
  };
}
export function returnOrdersCountCheckSuccess(bool = false) {
  return {
    type: RETURNORDERS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function returnOrdersDeleteCheckSuccess(bool = false) {
  return {
    type: RETURNORDERS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function returnOrdersFindOneCheckSuccess(bool = false) {
  return {
    type: RETURNORDERS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function returnOrdersSetOneData(object) {
  return {
    type: RETURNORDERS_SET_ONE_DATA,
    payload: object,
  };
}
export function returnOrdersGetCheckSuccess(bool = false) {
  return {
    type: RETURNORDERS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function returnOrdersSetData(object) {
  return {
    type: RETURNORDERS_SET_DATA,
    payload: object,
  };
}

export function returnOrdersUpdateCheckSuccess(bool = false) {
  return {
    type: RETURNORDERS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

