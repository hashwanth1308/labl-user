import {
  PRODUCTRATING_POST_CHECK_SUCCESS,
  PRODUCTRATING_SET_COUNT,
  PRODUCTRATING_COUNT_CHECK_SUCCESS,
  PRODUCTRATING_DELETE_CHECK_SUCCESS,
  PRODUCTRATING_FINDONE_CHECK_SUCCESS,
  PRODUCTRATING_SET_ONE_DATA,
  PRODUCTRATING_GET_CHECK_SUCCESS,
  PRODUCTRATING_SET_DATA,
  PRODUCTRATING_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  productRatingPostCheckSuccess: false,
  productRatingCount: 0,
  productRatingCountCheckSuccess : false,
  productRatingDeleteCheckSuccess: false,
  productRatingFindOneCheckSuccess: false,
  productRatingOneData: [],
  productRatingGetCheckSuccess: false,
  productRatingData: [],
  productRatingUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCTRATING_POST_CHECK_SUCCESS:
      return {
        ...state,
        productRatingPostCheckSuccess: action.payload,
      };
      case PRODUCTRATING_SET_COUNT:
      return {
        ...state,
        productRatingCount: action.payload,
      };
      case PRODUCTRATING_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        productRatingCountCheckSuccess: action.payload,
      };
      case PRODUCTRATING_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        productRatingDeleteCheckSuccess: action.payload,
      };
      case PRODUCTRATING_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        productRatingFindOneCheckSuccess: action.payload,
          };
      case PRODUCTRATING_SET_ONE_DATA:
      return {
        ...state,
        productRatingOneData: action.payload,
          };
      case PRODUCTRATING_GET_CHECK_SUCCESS:
      return {
        ...state,
        productRatingGetCheckSuccess: action.payload,
          };
      case PRODUCTRATING_SET_DATA:
      return {
        ...state,
        productRatingData: action.payload,
          };
      case PRODUCTRATING_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        productRatingUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
