import {
  VENDORS_POST_CHECK_SUCCESS,
  VENDORS_SET_COUNT,
  VENDORS_COUNT_CHECK_SUCCESS,
  VENDORS_DELETE_CHECK_SUCCESS,
  VENDORS_FINDONE_CHECK_SUCCESS,
  VENDORS_SET_ONE_DATA,
  VENDORS_GET_CHECK_SUCCESS,
  VENDORS_SET_DATA,
  VENDORS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function vendorsPostCheckSuccess(bool = false) {
  return {
    type: VENDORS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorsSetCount(object) {
  return {
    type: VENDORS_SET_COUNT,
    payload: object,
  };
}
export function vendorsCountCheckSuccess(bool = false) {
  return {
    type: VENDORS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorsDeleteCheckSuccess(bool = false) {
  return {
    type: VENDORS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorsFindOneCheckSuccess(bool = false) {
  return {
    type: VENDORS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorsSetOneData(object) {
  return {
    type: VENDORS_SET_ONE_DATA,
    payload: object,
  };
}
export function vendorsGetCheckSuccess(bool = false) {
  return {
    type: VENDORS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorsSetData(object) {
  return {
    type: VENDORS_SET_DATA,
    payload: object,
  };
}

export function vendorsUpdateCheckSuccess(bool = false) {
  return {
    type: VENDORS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

