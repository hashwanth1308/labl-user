import React, {useState,useEffect} from 'react';
import {View, KeyboardAvoidingView, Platform,ActivityIndicator,Dimensions} from 'react-native';
import {BaseStyle, useTheme} from '@config';
import {Header, SafeAreaView, Icon, TextInput, Button,Text} from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { usersUpdateRequest } from '../../api/users';

export default function ResetPassword({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const dispatch = useDispatch();
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('')
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState({ confirmPassword: true });

  const fpData = useSelector(state => state.usersReducer.fpData);
  console.log(fpData);
  const id = fpData?.id;

  // useEffect(() => {
  //   dispatch(usersUpdateRequest(fpData?.id))
  // }, [dispatch])

  /**
   * call when action reset pass
   */
  const onReset = () => {
    if (password != confirmPassword) {
      alert("Passwords you have entered doesn't match")
    } else {
      setLoading(true);
      dispatch(usersUpdateRequest(id,password))
      setTimeout(() => {
        setLoading(false);
        navigation.navigate('SignIn');
      }, 500);
    }
  };

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev)=>(!prev))
    ), 1000)
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('reset_password')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              padding: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TextInput
              onChangeText={text => setPassword(text)}
              style={{ marginTop: 20, width: 322, height: 52, elevation: 5, borderRadius: 30, marginVertical: 20 }}
              onFocus={() => {
                setSuccess({
                  ...success,
                  password: true,
                });
              }}
              placeholder={t('Password')}
              success={success.password}
              value={password}
            />
            <TextInput
              onChangeText={text => setConfirmPassword(text)}
              style={{ marginTop: 20, width: 322, height: 52, elevation: 5, borderRadius: 30, marginVertical: 20 }}
              onFocus={() => {
                setSuccess({
                  ...success,
                  confirmPassword: true,
                });
              }}
              placeholder={t('Confirm password')}
              success={success.confirmPassword}
              value={confirmPassword}
            />
            <Button
              style={{ marginTop: 20, width: 200 }}
              // full
              onPress={() => {
                onReset();
              }}
              loading={loading}>
              {t('reset_password')}
            </Button>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
      {loading1  &&
        <View style={{ width: width, height:height,backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor:"#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}
