import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
  Dimensions,
  ActivityIndicator,
  RefreshControl
} from 'react-native';
import {
  Image,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  EventCard,
  Header,
  CartCard,
  TextInput,
  Icon,
  QuantityPicker
} from '@components';
import _ from "lodash";
import Modal from 'react-native-modal';
import RazorpayCheckout from 'react-native-razorpay';
import CheckBox from '@react-native-community/checkbox';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { PromotionData, TourData, HotelData, BestData } from '@data';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { cartsGetRequest, cartsDeleteRequest, cartsUpdateRequest } from '../../api/carts';
import { productsGetRequest, productsPostRequest } from '../../api/products';
import { color } from 'react-native-reanimated';
import { modelOrdersPostRequest, modelOrdersUpdateRequest } from '../../api/modelOrders';
import Icon1 from "react-native-vector-icons/Octicons";
import Icon2 from "react-native-vector-icons/Fontisto";
import Icon3 from "react-native-vector-icons/MaterialCommunityIcons";
import PropTypes from 'prop-types';
import { modelsGetRequest } from '../../api/models';
import axios from 'axios';
import { bookmarksGetRequestByModelUser } from '../../api/bookmarks';

export default function CartModelLABL({ navigation }) {
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const { colors } = useTheme();
  const [imageData] = useState([]);
  const [load, setLoad] = useState(false);
  const [adults, setAdults] = useState(2);
  const [orderId, setOrderId] = React.useState('');
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const dispatch = useDispatch();
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const modalCartData = useSelector(state => state?.cartsReducer?.cartsData);
  console.log(modalCartData, "userCartData");
  const [value, setValue] = useState();
  const loginModelData = useSelector(state => state.modelsReducer.modelsData);
  useEffect(() => {
    const obj = {};
    obj.model = UserData?.user?.model?.id;
    dispatch(cartsGetRequest(obj));
    setLoad(false);
  }, [load]);

  console.log("loginModelData", loginModelData);

  const modelId = loginModelData[0]?.id;
  // const loginModelData = useSelector(state => state.modelsReducer.modelsData);
  // const modelId=loginModelData[0]?.id;
  // useEffect(() => {
  //   dispatch(modelsGetRequest(modelId));
  // }, [dispatch])
  useEffect(() => {
    dispatch(productsGetRequest());

  }, [dispatch])

  useEffect(() => {
    dispatch(bookmarksGetRequestByModelUser(UserData?.user?.model?.id));
  }, [dispatch]);
  // useEffect(() => {
  //   dispatch(cartsGetRequest(modelId));

  // }, [dispatch])
  const cartsData = useSelector(state => state.cartsReducer.cartsData)
  const productsData = useSelector(state => state.productsReducer.productsData)


  console.log("cartsData", cartsData);
  // console.log("UserToken", userToken);
  const products = [];
  const sumValue = _.sumBy(cartsData, item => Number(item?.product?.productPrice));
  const productArray = _.forEach(cartsData, i => products.push({ _id: i?.product?.id }));
  console.log(sumValue, products, modelId);

  const handleSubmit = order => {
    const options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: 'INR',
      key: 'rzp_test_r0Ec2tn9SG0Ueu',
      key_secret: 'nXgVoTQ27RE0suRAzQ0pbk4z',
      amount: totalSum * 100,
      order_id:orderId,
      name: 'LABAL',
      prefill: {
        email: 'ab@websoc.co.in',
        contact: '9191919191',
        name: 'Ashok',
      },
      theme: { color: '#53a20e' },
    };
    RazorpayCheckout.open(options)
      .then(data => {
        // handle success
        console.log('data razorpay', data);
        alert(`Success: ${data.razorpay_payment_id}`);
        const orderData = {};
        orderData.razerPayid = data.razorpay_payment_id;
        orderData.status = 'Payment success';
        dispatch(modelOrdersUpdateRequest(orderData, order));
        navigation.navigate("ThankyouOrderPlaced");
      })
      .catch(error => {
        // handle failure
        const orderData = {};
        orderData.status = 'Payment failed';
        dispatch(modelOrdersUpdateRequest(orderData, order));
        alert(`Error: ${error.code} | ${error.description}`);
      });
    cartsData.map(async(item) => {
      await dispatch(cartsDeleteRequest(item?.id));
    });
  
  };
  const modelOrder = modalCartData?.length > 0 && modalCartData?.map(item => ({
    status: 'Ordered',
    product: item?.product?.id,
    productVarient: item?.productVarient?.id,
    model: UserData?.user?.model?.id,
    label: item?.product?.label?.id,
    quantity: item?.quantity,
  }));
  console.log(modelOrder, "userOrderObject");
  const postModalOrder = () => {
    const userOrderObject = {};
    userOrderObject.status = 'Payment pending';
    userOrderObject.modelorder = modelOrder;
    userOrderObject.model = UserData?.user?.model?.id;
    userOrderObject.address = modalCartData[0]?.address?.id
    // console.log(userOrderObject, "userOrderObject");
    axios
      .post('https://staging-backend.labl.store/model-orders', userOrderObject, {
        headers: {
          'content-type': 'application/json',
          Authorization: `Bearer ${UserData.jwt}`,
        },
      })
      .then(({ data }) => {
        console.log(data, "orderuser");
        setOrderId(data.id);
        handleSubmit(data.id);
      })
      .catch(() => { });
  }
  const [refreshing, setRefreshing] = useState(false);
  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const [isSelected, setSelection] = useState(false);

  const heightImageBanner = Utils.scaleWithPixel(60);
  const marginTopBanner = heightImageBanner - heightHeader;

  const productPrice = modalCartData.length > 0 && modalCartData?.map((item) => item?.product?.productPrice * item?.quantity);
  console.log(productPrice, "productPrice");
  const totalSum =
    productPrice.length > 0 &&
    productPrice.reduce((partialSum, a) => Number(partialSum) + Number(a), 0);
  console.log(totalSum, 'totalSum');
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  const upDatePnQ = (id, q, p) => {
    console.log(id, q, p);
    const object = {}
    object.quantity = q
    dispatch(cartsUpdateRequest(object, id));
    setLoad(true);
  };

  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('Shopping Bag')}
        style={{ height: 100, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flexDirection: "row", width: 200, justifyContent: "space-between", alignSelf: "flex-start", top: -10 }}>
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
              <Text style={{ alignSelf: "center", fontFamily: "Roboto-Bold", fontSize: 20, color: "#fff", right: 20 }}>Shopping Bag</Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

        renderRight={() => {
          return <TouchableOpacity onPress={() => navigation.navigate('WishlistModelLABL')} style={{ height: 35, width: 35, borderRadius: 30, backgroundColor: "#2F2F2F", justifyContent: "center", alignItems: "center" }}>
            <Image source={require('../../assets/images/Vector.png')} style={{ height: 17, width: 20 }} />
          </TouchableOpacity>
        }}
        // renderRightSecond={() => {
        //   return <TouchableOpacity style={{ height: 35, width: 35, borderRadius: 30, backgroundColor: "#2F2F2F", justifyContent: "center", alignItems: "center" }}>
        //     <Image source={require('../../assets/images/trash.png')} style={{ height: 17, width: 17 }} />
        //   </TouchableOpacity>
        // }}
        // onPressRight={() => {
        //   navigation.navigate('WishlistModelLABL');
        // }}
      />

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        {modalCartData.length > 0 ?
          <ScrollView
            onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
            scrollEventThrottle={8}>
            <FlatList
              vertical
              // numColumns={2}
              data={modalCartData}
              // style={{ margin: 10 }}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => {
                return (
                  <CartComponent object={item} qty={item?.quantity} id={item?.id} upDatePnQ={(id, q, p) => upDatePnQ(id, q, p)} image={item?.product?.productImages[0]?.url} name={item?.product?.productBrandName} description={item?.product?.productName} price={item?.product?.productPrice} size={item?.productVarient?.size?.type} Pcolor={item?.productVarient?.color?.color} />
                )
              }}
            />
            <View style={{ padding: 20 }}>
              <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20, marginVertical: 10 }}>Order Details</Text>
              <View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: 5 }}>
                <Text style={{ fontFamily: "Roboto-Regluar", fontSize: 20 }}>Total MRP</Text>
                <Text style={{ fontFamily: "Roboto-Regluar", fontSize: 20 }}>₹ {Number(totalSum)}</Text>
              </View>
              <View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: 5 }}>
                <Text style={{ fontFamily: "Roboto-Regluar", fontSize: 20 }}>Bag Savings</Text>
                <Text style={{ fontFamily: "Roboto-Regluar", fontSize: 20 }}>₹ 0</Text>
              </View>
              <View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: 5 }}>
                <Text style={{ fontFamily: "Roboto-Regluar", fontSize: 20 }}>Delivery</Text>
                <Text style={{ fontFamily: "Roboto-Regluar", fontSize: 20 }}>Free</Text>
              </View>
              <View style={{ width: "100%", borderWidth: 0.5, borderColor: "#CDCDCD", marginVertical: 10 }}></View>
              <View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: 5 }}>
                <Text style={{ fontFamily: "Roboto-Regluar", fontSize: 20 }}>Amount Payable</Text>
                <Text style={{ fontFamily: "Roboto-Regluar", fontSize: 20 }}>₹ {Number(totalSum)}</Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly", marginVertical: 20 }}>
              <View style={{ flexDirection: "column", alignItems: "center" }}>
                <Icon1 name="verified" size={32} color="#CDCDCD" style={{ marginVertical: 10 }} />
                <Text style={{ fontFamily: "Roboto-Medium", fontSize: 18, textAlign: "center", color: "#CDCDCD" }}>Genuine{'\n'}Product</Text>
              </View>
              <View style={{ flexDirection: "column", alignItems: "center" }}>
                <Icon2 name="smiley" size={32} color="#CDCDCD" style={{ marginVertical: 10 }} />
                <Text style={{ fontFamily: "Roboto-Medium", fontSize: 18, textAlign: "center", color: "#CDCDCD" }}>Happy{'\n'}Customers</Text>
              </View>
              <View style={{ flexDirection: "column", alignItems: "center" }}>
                <Icon3 name="shield-check-outline" size={32} color="#CDCDCD" style={{ marginVertical: 10 }} />
                <Text style={{ fontFamily: "Roboto-Medium", fontSize: 18, textAlign: "center", color: "#CDCDCD" }}>Quality{'\n'}Checked</Text>
              </View>
            </View>
            {/* <View style={[styles.line, { backgroundColor: colors.border }]} /> */}
            <TouchableOpacity
              style={{ marginVertical: 20, backgroundColor: "#000", height: 50, width: 250, alignSelf: "center", alignItems: "center", justifyContent: "center", borderRadius: 10, }}
              // onPress={() => {
              //   navigation.navigate('CheckOut');
              // }}
              onPress={postModalOrder}
            >
              <Text style={{ color: "#fff", fontFamily: "Roboto-Bold", fontSize: 22 }}>
                {t('PLACE ORDER')}
              </Text>
            </TouchableOpacity>

          </ScrollView>
          :
          <View style={{ flex: 1, alignItems: "center", flexDirection: "column", top: 0, justifyContent: "center" }}>
            <Image source={require('../../assets/images/bag-1.png')} style={{ height: 100, width: 100, marginVertical: 30 }} />
            <Text style={{ alignSelf: "center", fontFamily: "Roboto-Bold", fontSize: 20, color: "#000", marginVertical: 0 }}>Your Bag is empty</Text>
            <Text style={{ alignSelf: "center", fontFamily: "Roboto-Medium", fontSize: 16, color: "#000", textAlign: "center", marginVertical: 20 }}>There is nothing in your bag,{'\n'}Lets add some items.</Text>
            <TouchableOpacity onPress={() => navigation.navigate("CategoriesScreenModelLABL")} style={{ height: 40, width: 160, backgroundColor: "#000", justifyContent: "center", alignItems: "center", borderRadius: 10, marginVertical: 20 }}>
              <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 18, color: "#fff" }}>Shop now</Text>
            </TouchableOpacity>
          </View>
        }
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}

function CartComponent(props) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [loading1, setLoading1] = useState(false);
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const [modalVisible, setModalVisible] = useState(false);
  const [NP, setNP] = useState(null);
  console.log("NP", NP);

  const { image, name, description, price, size, Pcolor, id, object } = props;
  console.log(value, "value");
  const toggleModal = () => {
    setModalVisible(!modalVisible);
  };

  const [value, setValue] = useState(1);
  // useEffect(() => {
  //   // cartsDeleteRequest(id);
  //   setTimeout(() => (
  //     setLoading1((prev) => (!prev))
  //   ), 1000)
  //   {
  //     loading1 &&
  //       <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
  //         <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
  //           <View>
  //             <ActivityIndicator size={"large"} color="#000" />
  //           </View>
  //           <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
  //         </View>
  //       </View>
  //   }
  // }, [])

  const onChange = (type) => {
    if (type == 'up') {
      props.upDatePnQ(id, value + 1, price * (value + 1))
      setValue(value + 1);
      setNP(price * (value + 1));
    } else {
      props.upDatePnQ(id, value - 1 > 0 ? value - 1 : 0, price * (value - 1 > 0 ? value - 1 : 0))
      setValue(value - 1 > 0 ? value - 1 : 0);
      setNP(price * (value - 1 > 0 ? value - 1 : 0));
    }
  };


  const deleteProduct = (id) => {
    // setLoading1(true);
    console.log("del button pressed", id);
    dispatch(cartsDeleteRequest(id));
    setModalVisible(!modalVisible);
    // setLoading1(false);
  };

  const WhistList = useSelector(
    state => state.bookMarksReducer.bookmarksDataByUser,
  );
  const [productId, setProductId] = useState('');
  const [productmarked, setProductmarked] = useState(false);

  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) { return o.product?.id === object?.product?.id });
    console.log(whishlistitems, "whishlistitems");
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true)
    }
  }, []);

  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) { return o.product.id === object?.product?.id });
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true)
    }
  }, [WhistList]);
  const addWhishlist = async () => {
    if (productmarked) {
      setProductmarked(false);
      await dispatch(bookmarksDeleteRequest(productId));
    }
    else {
      setProductmarked(true);
      const objectValue = {};
      objectValue.model = UserData?.user?.model?.id;
      objectValue.product = object?.product?.id;
      console.log("bookmarkpost", objectValue);
      // setLoading(true);

      await dispatch(bookmarksPostRequest(objectValue));
    }
  }


  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  return (
    <View style={{
      width: 300,
      borderWidth: 1,
      borderColor: '#CDCDCD',
      borderRadius: 10,
      margin: 20,
      alignSelf: 'center',
      marginBottom: 10,
    }}>
      <View style={{ flexDirection: "row", padding: 10, justifyContent: "space-around" }}>
        <Image source={{uri : image}} style={{ height: 150, width: 120, borderRadius: 10 }} />
        <View style={{ flexDirection: "column" }}>
          <Text numberOfLines={1} style={{ fontFamily: "Roboto-SemiBold", fontSize: 20, marginVertical: 5, width: 130 }}>{name}</Text>
          <Text numberOfLines={1} style={{ width: 120, fontFamily: "Roboto-Regular", fontSize: 18, marginVertical: 5 }}>{description}</Text>
          {/* <View style={{ height:50 }}> */}
          {/* <View
            style={{
              // height: 25,
              // width: 55,
              flexDirection: "row",
              // backgroundColor: '#F3F3F3',
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 5,
            }}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 18 }}>
              Qty
            </Text>
            <View style={styles.contentPicker}>
              {value > 1 ?
                <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => onChange('down')}>
                  <Icon name="minus-circle" size={24} color={BaseColor.grayColor} />
                </TouchableOpacity> :
                <TouchableOpacity disabled={true} style={{ marginHorizontal: 5 }} onPress={() => onChange('down')}>
                  <Icon name="minus-circle" size={24} color={BaseColor.grayColor} />
                </TouchableOpacity>}
              <Text style={{ fontFamily: "Roboto-Bold", fontSize: 20 }}>{value}</Text>
              <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => onChange('up')}>
                <Icon name="plus-circle" size={24} color={colors.primary} />
              </TouchableOpacity>
            </View>
          </View> */}
          {/* </View> */}
          <Text
            style={{
              fontFamily: 'Roboto-SemiBold',
              fontSize: 18,
              marginVertical: 10,
            }}>
            ₹ {price}
          </Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 17 }}>
            Size : {size}
          </Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 17, marginVertical: 10 }}>
            Color : {Pcolor}
          </Text>
        </View>
      </View>
      <View
        style={{
          width: 300,
          borderWidth: 0.5,
          borderColor: '#CDCDCD',
          marginVertical: 10,
        }}
      />
      <TouchableOpacity onPress={toggleModal} style={{ marginVertical: 5 }}>
        <Text style={{ fontFamily: "Roboto-Medium", fontSize: 18, alignSelf: "center" }}>Remove</Text>
      </TouchableOpacity>
      <View>
        <Modal
          isVisible={modalVisible}
          onBackdropPress={() => setModalVisible(false)}
          onSwipeComplete={() => setModalVisible(false)}
          swipeDirection={['down']}
          style={styles.bottomModal}>
          <View style={styles.contain}>
            <View style={{
              paddingTop: 20,
              paddingLeft: 30,
              justifyContent: 'center',
              flexDirection: "row",
            }}>
              <Image source={{uri :image}} style={{ height: 150, width: 120, borderRadius: 10 }} />
              <View style={{ flexDirection: "column", padding: 20 }}>
                <Text style={{ fontFamily: "Roboto-Bold", fontSize: 22 }}>Remove from bag?</Text>
                <Text style={{ fontFamily: "Roboto-Regular", fontSize: 17, marginVertical: 20 }}>You can save products to your{'\n'}wishlist to use later.</Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-around", height: 90, width: "100%", backgroundColor: "#fff" }}>
              <TouchableOpacity onPress={() => deleteProduct(id)} style={{ backgroundColor: "#D9D9D9", height: 50, width: 150, borderRadius: 10, justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 20 }}>Remove</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => addWhishlist()} style={{ backgroundColor: "#000", height: 50, width: 190, borderRadius: 10, justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontFamily: "Roboto-SemiBold", fontSize: 20, color: "#fff" }}>Move to Wishlist</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    </View >

  )
}

CartComponent.propTypes = {
  image: PropTypes.node.isRequired,
  name: PropTypes.string,
  price: PropTypes.string,
  description: PropTypes.string,
  value: PropTypes.number,
  onChange: PropTypes.func,
};

CartComponent.defaultProps = {
  image: '',
  name: '',
  price: '',
  value: 1,
  description: '',
  onChange: () => { },
};
