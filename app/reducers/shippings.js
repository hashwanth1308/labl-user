import {
  SHIPPINGS_POST_CHECK_SUCCESS,
  SHIPPINGS_SET_COUNT,
  SHIPPINGS_COUNT_CHECK_SUCCESS,
  SHIPPINGS_DELETE_CHECK_SUCCESS,
  SHIPPINGS_FINDONE_CHECK_SUCCESS,
  SHIPPINGS_SET_ONE_DATA,
  SHIPPINGS_GET_CHECK_SUCCESS,
  SHIPPINGS_SET_DATA,
  SHIPPINGS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  shippingsPostCheckSuccess: false,
  shippingsCount: 0,
  shippingsCountCheckSuccess : false,
  shippingsDeleteCheckSuccess: false,
  shippingsFindOneCheckSuccess: false,
  shippingsOneData: [],
  shippingsGetCheckSuccess: false,
  shippingsData: [],
  shippingsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SHIPPINGS_POST_CHECK_SUCCESS:
      return {
        ...state,
        shippingsPostCheckSuccess: action.payload,
      };
      case SHIPPINGS_SET_COUNT:
      return {
        ...state,
        shippingsCount: action.payload,
      };
      case SHIPPINGS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        shippingsCountCheckSuccess: action.payload,
      };
      case SHIPPINGS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        shippingsDeleteCheckSuccess: action.payload,
      };
      case SHIPPINGS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        shippingsFindOneCheckSuccess: action.payload,
          };
      case SHIPPINGS_SET_ONE_DATA:
      return {
        ...state,
        shippingsOneData: action.payload,
          };
      case SHIPPINGS_GET_CHECK_SUCCESS:
      return {
        ...state,
        shippingsGetCheckSuccess: action.payload,
          };
      case SHIPPINGS_SET_DATA:
      return {
        ...state,
        shippingsData: action.payload,
          };
      case SHIPPINGS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        shippingsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
