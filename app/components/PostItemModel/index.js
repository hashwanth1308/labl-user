import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Image, Icon } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { BaseColor, useTheme } from '@config';
import Icon1 from 'react-native-vector-icons/Ionicons';
import ProfileGroupSmall from '../ProfileGroupSmall';
import { UserData } from '../../data/user';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import { useDispatch, useSelector } from 'react-redux';
import _, { set } from 'lodash';
import { useNavigation } from '@react-navigation/native';
import { bookmarksDeleteRequest, bookmarksGetRequestByModelUser, bookmarksGetRequestByUser, bookmarksPostRequest } from '../../api/bookmarks';
import { likeDeleteRequest, likeGetRequest, likeGetRequestByModelUser, likePostRequest } from '../../api/like';

export default function PostItemModel(props) {
  const { style, children, title, description, onPress, image, likes, commentsLength, object } = props;
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const [users] = useState(UserData);

  const navigation = useNavigation();
  const commentNavigate = (object) => {
    navigation.navigate("CommentsModel", { object })
  }

  useEffect(() => {
    dispatch(likeGetRequestByModelUser(userData?.user?.model?.id));
  }, []);

  const LikedList = useSelector(
    state => state.likeReducer.likeDataByUser,
  );
  const [likecount, setLikeCount] = useState(object?.likes?.length);
  const [liked, setLiked] = useState(false);
  const [likedId, setLikedId] = useState('');
  const userData = useSelector(state => state.accessTokenReducer.userData);
  const userId = userData?.user?.model?.id;
  const product = object?.id;
  // console.log(object, "object");
  useEffect(() => {
    const LikedListitems = _.find(LikedList, function (o) {
      return o?.product?.id === object?.id;
    });
    // console.log(LikedListitems, 'LikedList');
    if (LikedListitems) {
      setLikedId(LikedListitems.id);
      setLiked(true);
    }
  }, []);

  useEffect(() => {
    const LikedListitems = _.find(LikedList, function (o) {
      return o?.product?.id === object?.id;
    });
    if (LikedListitems) {
      setLikedId(LikedListitems.id);
      setLiked(true);
    }
  }, [LikedList]);
  const addLike = async () => {
    if (liked) {
      setLiked(false);
      setLikeCount(likecount - 1);
      await dispatch(likeDeleteRequest(likedId));
      await dispatch(likeGetRequest())
    } else {
      setLiked(true);
      const objectValue = {};
      objectValue.model = userData?.user?.model?.id;
      objectValue.product = object?.id;
      // console.log('bookmarkpost', objectValue);
      // setLoading(true);

      await dispatch(likePostRequest(objectValue));
      await dispatch(likeGetRequestByModelUser(userId, product))
      setLikeCount(likecount + 1);
    }
  };

  useEffect(() => {
    dispatch(bookmarksGetRequestByModelUser(userData?.user?.model?.id));
  }, []);

  const WhistList = useSelector(
    state => state.bookMarksReducer.bookmarksDataByUser,
  );

  const [productId, setProductId] = useState('');
  const [productmarked, setProductmarked] = useState(false);
  // console.log(productmarked, "productmarked")
  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) {
      return o?.product?.id === object?.id;
    });
    // console.log(whishlistitems, 'whishlistitems');
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true);
    }
  }, []);

  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) {
      return o?.product?.id === object?.id;
    });
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true);
    }
  }, [WhistList]);
  const addWhishlist = async () => {
    if (productmarked) {
      setProductmarked(false);
      await dispatch(bookmarksDeleteRequest(productId));
    } else {
      setProductmarked(true);
      const objectValue = {};
      objectValue.model = userData?.user?.model?.id;
      objectValue.product = object?.id;
      // console.log('bookmarkpost', objectValue);
      // setLoading(true);

      await dispatch(bookmarksPostRequest(objectValue));
    }
  };
  return (
    <View style={style}>
      <View style={{padding:5}}>
        {children}
      </View>
      <View style={{ width: "100%", borderWidth: 0.5, borderColor: "#CDCDCD", marginBottom: 5 }}></View>
      <TouchableOpacity onPress={onPress} activeOpacity={0.9}>
        <Image source={{ uri: image ? image : "" }} style={styles.imageBanner} />
      </TouchableOpacity>
      <View style={{ flexDirection: "row", marginVertical: 20, justifyContent: "space-around", marginLeft: 0 }}>
        <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => addLike()}>
          {liked ? <Icon2
            name="thumbs-up"
            solid
            size={24}
            color={BaseColor.blackColor}
            style={{ right: 20 }}
          /> : <Icon2
            name="thumbs-o-up"
            size={24}
            color={BaseColor.blackColor}
            style={{ right: 20 }}
          />}
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 22, right: 10 }}>{likecount}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => commentNavigate(object)}>
          <Icon1
            name="chatbubble-ellipses"
            solid
            size={24}
            color={commentsLength?.length === 0 ? "#CDCDCD" : BaseColor.blackColor}
            style={{ right: 30 }}
          />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 22, right: 20, color: "#CDCDCD" }}>{commentsLength?.length}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => addWhishlist()}>
          {productmarked ? <Icon
            name="bookmark"
            solid
            size={24}
            color={BaseColor.blackColor}
            style={{ left: 20 }}
          /> : <Icon
            name="bookmark"
            size={24}
            color={BaseColor.blackColor}
            style={{ left: 20 }}
          />}
        </TouchableOpacity>
      </View>
      {/* <View style={[styles.content, { borderBottomColor: colors.border }]}>
        <Text headline semibold style={{ marginBottom: 6 }}>
          {title}
        </Text>
        <Text body2>{description}</Text>
      </View> */}
      {/* <View style={{ flexDirection: "row", marginLeft: 20 }}>
        {commentsLength?.length > 0 && <ProfileGroupSmall />}

        {commentsLength?.length > 0 ? commentsLength?.slice(0, 2)?.map((item) =>
          (<Text numberOfLines={1} style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#000", alignSelf: "center", left: 10 }}>{item?.user?.username},</Text>)) : "hii"}
        <TouchableOpacity style={{ height: 16, width: 75, backgroundColor: "#CDCDCD", borderRadius: 10, alignSelf: "center", left: 20 }} onPress={commentNavigate}>
          <Text style={{ fontFamily: "Montserrat-Regular", fontSize: 14, alignSelf: "center" }}>
            View more
          </Text>
        </TouchableOpacity>
      </View> */}
      {/* <View style={{ width: 370, borderWidth: 0.8, borderColor: "#CDCDCD", alignSelf: "center", marginVertical: 20 }}></View>
      <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
        <View style={{ flexDirection: "column", alignItems: "center" }}>
          <Icon2 name="shopping-bag" size={20} color="#000" />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000", top: 10 }}>Buy Combo</Text>
        </View>
        <View style={{ width: 1, height: 60, borderWidth: 0.8, borderColor: "#CDCDCD", alignSelf: "center", top: -10 }}></View>
        <View style={{ flexDirection: "column", alignItems: "center" }}>
          <Icon2 name="sunglasses-alt" size={20} color="#000" />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000", top: 10 }}>Glass</Text>
        </View>
        <View style={{ width: 1, height: 60, borderWidth: 0.8, borderColor: "#CDCDCD", alignSelf: "center", top: -10 }}></View>
        <View style={{ flexDirection: "column", alignItems: "center" }}>
          <Icon name="tshirt" size={20} color="#000" />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000", top: 10 }}>T-Shirt</Text>
        </View>
        <View style={{ width: 1, height: 60, borderWidth: 0.8, borderColor: "#CDCDCD", alignSelf: "center", top: -10 }}></View>
        <View style={{ flexDirection: "column", alignItems: "center" }}>
          <Icon1 name="ios-watch" size={20} color="#000" />
          <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000", top: 10 }}>Watch</Text>
        </View>
      </View> */}
    </View>
  );
}

PostItemModel.propTypes = {
  image: PropTypes.node.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  title: PropTypes.string,
  likes: PropTypes.number,
  commentsLength: PropTypes.array,
  description: PropTypes.string,
  onPress: PropTypes.func,
};

PostItemModel.defaultProps = {
  image: '',
  title: '',
  likes: '',
  commentsLength: '',
  description: '',
  style: {},
  onPress: () => { },
};
