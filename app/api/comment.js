import {
    commentGetCheckSuccess,
    commentGetCheckSuccessByUser,
    commentSetData,
    commentSetDataByUser,
    commentPostCheckSuccess,
    commentUpdateCheckSuccess,
    commentDeleteCheckSuccess,
    commentGetCheckSuccessForFeed,
    commentSetDataForFeed,
} from '../actions';
import axios from 'axios';
let a = [];

export function commentGetRequest(element, token) {
    console.log('bookmarks get request');
    return dispatch => {
        dispatch(commentGetCheckSuccess(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'get',
            url: `comments`,
            headers: {
                //  Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('get bookmarks from api', response);
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log(data)
                // console.log(element,"api data",data.data[0])
                // const propertyValues = Object.values(data.data);
                // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                // a.push(obj)
                dispatch(commentSetData(data));
                dispatch(commentGetCheckSuccess(true));
            })
            .catch(error => {
                console.log(error, axiosLogin, 'furnishings');
                dispatch(commentGetCheckSuccess(false));
            });
    };
}
export function commentGetRequestByUser(ProductId) {
    console.log(ProductId, "bookmarksuserid");
    return dispatch => {
        dispatch(commentGetCheckSuccessByUser(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'get',
            url: `comments?modelProduct=${ProductId}`,
            headers: {
                //  Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('get bookmarks from api', response);
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                console.log(data, "response")
                // console.log(element,"api data",data.data[0])
                // const propertyValues = Object.values(data.data);
                // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                // a.push(obj)
                dispatch(commentSetDataByUser(data));
                dispatch(commentGetCheckSuccessByUser(true));
            })
            .catch(error => {
                console.log(error, axiosLogin, 'furnishings');
                dispatch(commentGetCheckSuccessByUser(false));
            });
    };
}

export function commentGetRequestByModelUser(ProductId) {
    console.log(ProductId, "bookmarksuserid");
    return dispatch => {
        dispatch(commentGetCheckSuccessByUser(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'get',
            url: `comments?product=${ProductId}`,
            headers: {
                //  Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('get bookmarks from api', response);
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                console.log(data, "response")
                // console.log(element,"api data",data.data[0])
                // const propertyValues = Object.values(data.data);
                // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                // a.push(obj)
                dispatch(commentSetDataByUser(data));
                dispatch(commentGetCheckSuccessByUser(true));
            })
            .catch(error => {
                console.log(error, axiosLogin, 'furnishings');
                dispatch(commentGetCheckSuccessByUser(false));
            });
    };
}
// export function bookmarksGetRequestForFeed(userId) {
//     const objectvalue = {};
//     objectvalue.bookmarkedBy = userId;
//     objectvalue.postType = 'feed';

//     console.log('bookmarks get request', objectvalue);
//     return dispatch => {
//         dispatch(bookmarksGetCheckSuccessForFeed(false));
//         const axiosLogin = axios.create();
//         return axiosLogin({
//             method: 'get',
//             url: `book-marks`,
//             params: objectvalue,
//             headers: {
//                 //  Authorization: `Bearer ${token}`,
//                 'Content-Type': 'application/json',
//             },
//         })
//             .then(response => {
//                 console.log('get bookmarks from api', response);
//                 if (response.status !== 200) {
//                     throw Error(response.statusText);
//                 }
//                 return response.data;
//             })
//             .then(data => {
//                 // console.log(data)
//                 // console.log(element,"api data",data.data[0])
//                 // const propertyValues = Object.values(data.data);
//                 // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
//                 // a.push(obj)
//                 dispatch(bookmarksSetDataForFeed(data));
//                 dispatch(bookmarksGetCheckSuccessForFeed(true));
//             })
//             .catch(error => {
//                 console.log(error, axiosLogin, 'furnishings');
//                 dispatch(bookmarksGetCheckSuccessForFeed(false));
//             });
//     };
// }

export function commentPostRequest(objectValue) {
    console.log('objectValue', objectValue);
    return dispatch => {
        // dispatch(bookmarksPostCheckSuccess(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'post',
            url: 'comments',
            data: objectValue,
            headers: {
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('response', response);
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // const propertyValues = Object.values(data.data);
                // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
                // a.push(obj)
                dispatch(commentSetDataByUser(data));
                console.log(data, "successdata");
                // dispatch(bookmarksPostCheckSuccess(true));
            })
            .catch(error => {
                console.log(error, axiosLogin);
                // dispatch(bookmarksPostCheckSuccess(false));
            });
    };
}

export function commentUpdateRequest(element, id) {
    console.log(element.accessToken);
    const accessToken = element.accessToken;
    delete element.accessToken;
    return dispatch => {
        dispatch(commentSetData(element));
        dispatch(commentUpdateCheckSuccess(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'put',
            url: `comments/${id}`,
            headers: {
                Authorization: `Bearer ${accessToken}`,
                'Content-Type': 'application/json',
            },
            data: element,
        })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(commentSetData(a));
                dispatch(commentUpdateCheckSuccess(true));
            })
            .catch(error => {
                console.log(error, axiosLogin);
                dispatch(commentUpdateCheckSuccess(false));
            });
    };
}

export function commentDeleteRequest(id) {
    return dispatch => {
        dispatch(commentDeleteCheckSuccess(false));
        const axiosLogin = axios.create();
        return axiosLogin({
            method: 'delete',
            url: `comments/${id}`,
            headers: {
                // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                dispatch(commentSetData(data));
                dispatch(commentDeleteCheckSuccess(true));
            })
            .catch(error => {
                console.log(error, axiosLogin);
                dispatch(commentDeleteCheckSuccess(false));
            });
    };
}
