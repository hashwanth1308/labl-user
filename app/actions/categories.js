import {
  CATEGORIES_POST_CHECK_SUCCESS,
  CATEGORIES_SET_COUNT,
  CATEGORIES_COUNT_CHECK_SUCCESS,
  CATEGORIES_DELETE_CHECK_SUCCESS,
  CATEGORIES_FINDONE_CHECK_SUCCESS,
  CATEGORIES_SET_ONE_DATA,
  CATEGORIES_GET_CHECK_SUCCESS,
  CATEGORIES_SET_DATA,
  CATEGORIES_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function categoriesPostCheckSuccess(bool = false) {
  return {
    type: CATEGORIES_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function categoriesSetCount(object) {
  return {
    type: CATEGORIES_SET_COUNT,
    payload: object,
  };
}
export function categoriesCountCheckSuccess(bool = false) {
  return {
    type: CATEGORIES_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function categoriesDeleteCheckSuccess(bool = false) {
  return {
    type: CATEGORIES_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function categoriesFindOneCheckSuccess(bool = false) {
  return {
    type: CATEGORIES_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function categoriesSetOneData(object) {
  return {
    type: CATEGORIES_SET_ONE_DATA,
    payload: object,
  };
}
export function categoriesGetCheckSuccess(bool = false) {
  return {
    type: CATEGORIES_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function categoriesSetData(object) {
  return {
    type: CATEGORIES_SET_DATA,
    payload: object,
  };
}

export function categoriesUpdateCheckSuccess(bool = false) {
  return {
    type: CATEGORIES_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

