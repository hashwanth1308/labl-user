import {
  PAYMENTS_POST_CHECK_SUCCESS,
  PAYMENTS_SET_COUNT,
  PAYMENTS_COUNT_CHECK_SUCCESS,
  PAYMENTS_DELETE_CHECK_SUCCESS,
  PAYMENTS_FINDONE_CHECK_SUCCESS,
  PAYMENTS_SET_ONE_DATA,
  PAYMENTS_GET_CHECK_SUCCESS,
  PAYMENTS_SET_DATA,
  PAYMENTS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  paymentsPostCheckSuccess: false,
  paymentsCount: 0,
  paymentsCountCheckSuccess : false,
  paymentsDeleteCheckSuccess: false,
  paymentsFindOneCheckSuccess: false,
  paymentsOneData: [],
  paymentsGetCheckSuccess: false,
  paymentsData: [],
  paymentsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PAYMENTS_POST_CHECK_SUCCESS:
      return {
        ...state,
        paymentsPostCheckSuccess: action.payload,
      };
      case PAYMENTS_SET_COUNT:
      return {
        ...state,
        paymentsCount: action.payload,
      };
      case PAYMENTS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        paymentsCountCheckSuccess: action.payload,
      };
      case PAYMENTS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        paymentsDeleteCheckSuccess: action.payload,
      };
      case PAYMENTS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        paymentsFindOneCheckSuccess: action.payload,
          };
      case PAYMENTS_SET_ONE_DATA:
      return {
        ...state,
        paymentsOneData: action.payload,
          };
      case PAYMENTS_GET_CHECK_SUCCESS:
      return {
        ...state,
        paymentsGetCheckSuccess: action.payload,
          };
      case PAYMENTS_SET_DATA:
      return {
        ...state,
        paymentsData: action.payload,
          };
      case PAYMENTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        paymentsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
