import React, { useState } from 'react';
import { RefreshControl, FlatList, View } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { useTranslation } from 'react-i18next';
import { Header, SafeAreaView, Icon, ListThumbCircle, Text } from '@components';
import styles from './styles';
import { NotificationData } from '@data';

export default function Notification({ navigation }) {
  const { t } = useTranslation();
  const { colors } = useTheme();

  const [refreshing] = useState(false);
  const [notification] = useState(NotificationData);

  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('Upload Products')}
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flexDirection: "row", width: 200, justifyContent: "space-between", alignSelf: "flex-start" }}>
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={"#000"}
                  enableRTL={true}
                />
              </View>
              <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 18, color: "#fff", right: 50 }}>Notifications</Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        {notification.length == 0 ?
          <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
            <Text style={{fontFamily:"Montserrat-Bold",fontSize:22,marginVertical:20}}>No Notifications</Text>
            <Text style={{fontFamily:"Montserrat-Medium",fontSize:17,textAlign:"center"}}>We will notify you once we have {'\n'} something for you</Text>
          </View> :
          <FlatList
            contentContainerStyle={{ paddingHorizontal: 20, paddingVertical: 10 }}
            refreshControl={
              <RefreshControl
                colors={[colors.primary]}
                tintColor={colors.primary}
                refreshing={refreshing}
                onRefresh={() => { }}
              />
            }
            data={notification}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item, index }) => (
              <ListThumbCircle
                image={item.image}
                txtLeftTitle={item.title}
                txtContent={item.description}
                txtRight={item.date}
                style={{ marginBottom: 5 }}
              />
            )}
          />}
      </SafeAreaView>
    </View>
  );
}
