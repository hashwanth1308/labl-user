import {
  ADDRESS_POST_CHECK_SUCCESS,
  ADDRESS_SET_COUNT,
  ADDRESS_COUNT_CHECK_SUCCESS,
  ADDRESS_DELETE_CHECK_SUCCESS,
  ADDRESS_FINDONE_CHECK_SUCCESS,
  ADDRESS_SET_ONE_DATA,
  ADDRESS_GET_CHECK_SUCCESS,
  ADDRESS_SET_DATA,
  ADDRESS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';

export function addressPostCheckSuccess(bool = false) {
  return {
    type: ADDRESS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function addressSetCount(object) {
  return {
    type: ADDRESS_SET_COUNT,
    payload: object,
  };
}
export function addressCountCheckSuccess(bool = false) {
  return {
    type: ADDRESS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function addressDeleteCheckSuccess(bool = false) {
  return {
    type: ADDRESS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function addressFindOneCheckSuccess(bool = false) {
  return {
    type: ADDRESS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function addressSetOneData(object) {
  return {
    type: ADDRESS_SET_ONE_DATA,
    payload: object,
  };
}
export function addressGetCheckSuccess(bool = false) {
  return {
    type: ADDRESS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function addressSetData(object) {
  return {
    type: ADDRESS_SET_DATA,
    payload: object,
  };
}

export function addressUpdateCheckSuccess(bool = false) {
  return {
    type: ADDRESS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}
