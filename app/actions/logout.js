import {SET_ACCESS_TOKEN} from './actionTypes';

export function logout() {
  return {
    type: SET_ACCESS_TOKEN,
  };
}
