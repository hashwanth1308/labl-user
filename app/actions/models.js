import {
  MODELS_POST_CHECK_SUCCESS,
  MODELS_SET_COUNT,
  MODELS_COUNT_CHECK_SUCCESS,
  MODELS_DELETE_CHECK_SUCCESS,
  MODELS_FINDONE_CHECK_SUCCESS,
  MODELS_SET_ONE_DATA,
  MODELS_GET_CHECK_SUCCESS,
  MODELS_SET_DATA,
  MODELS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function modelsPostCheckSuccess(bool = false) {
  return {
    type: MODELS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelsSetCount(object) {
  return {
    type: MODELS_SET_COUNT,
    payload: object,
  };
}
export function modelsCountCheckSuccess(bool = false) {
  return {
    type: MODELS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelsDeleteCheckSuccess(bool = false) {
  return {
    type: MODELS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelsFindOneCheckSuccess(bool = false) {
  return {
    type: MODELS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelsSetOneData(object) {
  return {
    type: MODELS_SET_ONE_DATA,
    payload: object,
  };
}
export function modelsGetCheckSuccess(bool = false) {
  return {
    type: MODELS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelsSetData(object) {
  return {
    type: MODELS_SET_DATA,
    payload: object,
  };
}

export function modelsUpdateCheckSuccess(bool = false) {
  return {
    type: MODELS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

