import {
  CATEGORIES_POST_CHECK_SUCCESS,
  CATEGORIES_SET_COUNT,
  CATEGORIES_COUNT_CHECK_SUCCESS,
  CATEGORIES_DELETE_CHECK_SUCCESS,
  CATEGORIES_FINDONE_CHECK_SUCCESS,
  CATEGORIES_SET_ONE_DATA,
  CATEGORIES_GET_CHECK_SUCCESS,
  CATEGORIES_SET_DATA,
  CATEGORIES_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  categoriesPostCheckSuccess: false,
  categoriesCount: 0,
  categoriesCountCheckSuccess : false,
  categoriesDeleteCheckSuccess: false,
  categoriesFindOneCheckSuccess: false,
  categoriesOneData: [],
  categoriesGetCheckSuccess: false,
  categoriesData: [],
  categoriesUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CATEGORIES_POST_CHECK_SUCCESS:
      return {
        ...state,
        categoriesPostCheckSuccess: action.payload,
      };
      case CATEGORIES_SET_COUNT:
      return {
        ...state,
        categoriesCount: action.payload,
      };
      case CATEGORIES_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        categoriesCountCheckSuccess: action.payload,
      };
      case CATEGORIES_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        categoriesDeleteCheckSuccess: action.payload,
      };
      case CATEGORIES_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        categoriesFindOneCheckSuccess: action.payload,
          };
      case CATEGORIES_SET_ONE_DATA:
      return {
        ...state,
        categoriesOneData: action.payload,
          };
      case CATEGORIES_GET_CHECK_SUCCESS:
      return {
        ...state,
        categoriesGetCheckSuccess: action.payload,
          };
      case CATEGORIES_SET_DATA:
      return {
        ...state,
        categoriesData: action.payload,
          };
      case CATEGORIES_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        categoriesUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
