import {
    FURNISHING_POST_CHECK_SUCCESS,
    FURNISHING_SET_COUNT,
    FURNISHING_COUNT_CHECK_SUCCESS,
    FURNISHING_DELETE_CHECK_SUCCESS,
    FURNISHING_FINDONE_CHECK_SUCCESS,
    FURNISHING_SET_ONE_DATA,
    FURNISHING_GET_CHECK_SUCCESS,
    FURNISHING_SET_DATA,
    FURNISHING_UPDATE_CHECK_SUCCESS,
  } from './actionTypes';

 
  export function furnishingsPostCheckSuccess(bool = false) {
    return {
      type: FURNISHING_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function furnishingsSetCount(object) {
    return {
      type: FURNISHING_SET_COUNT,
      payload: object,
    };
  }
  export function furnishingsCountCheckSuccess(bool = false) {
    return {
      type: FURNISHING_COUNT_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function furnishingsDeleteCheckSuccess(bool = false) {
    return {
      type: FURNISHING_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function furnishingsFindOneCheckSuccess(bool = false) {
    return {
      type: FURNISHING_FINDONE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function furnishingsSetOneData(object) {
    return {
      type: FURNISHING_SET_ONE_DATA,
      payload: object,
    };
  }
  export function furnishingsGetCheckSuccess(bool = false) {
    return {
      type: FURNISHING_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function furnishingsSetData(object) {
    return {
      type: FURNISHING_SET_DATA,
      payload: object,
    };
  }

  export function furnishingsUpdateCheckSuccess(bool = false) {
    return {
      type: FURNISHING_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  