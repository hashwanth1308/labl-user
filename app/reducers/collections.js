import {
  COLLECTIONS_POST_CHECK_SUCCESS,
  COLLECTIONS_SET_COUNT,
  COLLECTIONS_COUNT_CHECK_SUCCESS,
  COLLECTIONS_DELETE_CHECK_SUCCESS,
  COLLECTIONS_FINDONE_CHECK_SUCCESS,
  COLLECTIONS_SET_ONE_DATA,
  COLLECTIONS_GET_CHECK_SUCCESS,
  COLLECTIONS_SET_DATA,
  COLLECTIONS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  collectionsPostCheckSuccess: false,
  collectionsCount: 0,
  collectionsCountCheckSuccess : false,
  collectionsDeleteCheckSuccess: false,
  collectionsFindOneCheckSuccess: false,
  collectionsOneData: [],
  collectionsGetCheckSuccess: false,
  collectionsData: [],
  collectionsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case COLLECTIONS_POST_CHECK_SUCCESS:
      return {
        ...state,
        collectionsPostCheckSuccess: action.payload,
      };
      case COLLECTIONS_SET_COUNT:
      return {
        ...state,
        collectionsCount: action.payload,
      };
      case COLLECTIONS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        collectionsCountCheckSuccess: action.payload,
      };
      case COLLECTIONS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        collectionsDeleteCheckSuccess: action.payload,
      };
      case COLLECTIONS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        collectionsFindOneCheckSuccess: action.payload,
          };
      case COLLECTIONS_SET_ONE_DATA:
      return {
        ...state,
        collectionsOneData: action.payload,
          };
      case COLLECTIONS_GET_CHECK_SUCCESS:
      return {
        ...state,
        collectionsGetCheckSuccess: action.payload,
          };
      case COLLECTIONS_SET_DATA:
      return {
        ...state,
        collectionsData: action.payload,
          };
      case COLLECTIONS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        collectionsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
