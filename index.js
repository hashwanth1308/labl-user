/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
 import 'react-native-gesture-handler';
 import { AppRegistry } from 'react-native';
 import App from './app/index';
 import React from 'react';
 // import { name as appName } from './app.json';
 import { BaseSetting } from '@config';
 import { Provider } from 'react-redux';
 import { store, persistor } from './app/store';
 import axios from 'axios';
 import { PersistGate } from 'redux-persist/lib/integration/react';
 
 axios.defaults.baseURL = 'https://staging-backend.labl.store/';
 
 axios.interceptors.request.use(
     request => {
         return request;
     },
     error => {
         return Promise.reject(error);
     },
 );
 
 
 const RNRedux = () => (
     <Provider store={store}>
         <PersistGate persistor={persistor}>
             <App />
         </PersistGate>
     </Provider>
 );
 
 
 // AppRegistry.registerComponent(appName, () => RNRedux);
 
 
 
 
 AppRegistry.registerComponent(BaseSetting.name, () => RNRedux);
 
 