import {
  USERS_POST_CHECK_SUCCESS,
  FP_POST_CHECK_SUCCESS,
  USERS_SET_COUNT,
  USERS_COUNT_CHECK_SUCCESS,
  USERS_DELETE_CHECK_SUCCESS,
  USERS_FINDONE_CHECK_SUCCESS,
  USERS_SET_ONE_DATA,
  USERS_GET_CHECK_SUCCESS,
  USERS_SET_DATA,
  FP_SET_DATA,
  USERS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  usersPostCheckSuccess: false,
  fpPostCheckSuccess: false,
  usersCount: 0,
  usersCountCheckSuccess: false,
  usersDeleteCheckSuccess: false,
  usersFindOneCheckSuccess: false,
  usersOneData: [],
  usersGetCheckSuccess: false,
  usersData: [],
  fpData: [],
  usersUpdateCheckSuccess: false,


};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case USERS_POST_CHECK_SUCCESS:
      return {
        ...state,
        usersPostCheckSuccess: action.payload,
      };
    case FP_POST_CHECK_SUCCESS:
      return {
        ...state,
        fpPostCheckSuccess: action.payload,
      };
    case USERS_SET_COUNT:
      return {
        ...state,
        usersCount: action.payload,
      };
    case USERS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        usersCountCheckSuccess: action.payload,
      };
    case USERS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        usersDeleteCheckSuccess: action.payload,
      };
    case USERS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        usersFindOneCheckSuccess: action.payload,
      };
    case USERS_SET_ONE_DATA:
      return {
        ...state,
        usersOneData: action.payload,
      };
    case USERS_GET_CHECK_SUCCESS:
      return {
        ...state,
        usersGetCheckSuccess: action.payload,
      };
    case USERS_SET_DATA:
      return {
        ...state,
        usersData: action.payload,
      };
    case FP_SET_DATA:
      return {
        ...state,
        fpData: action.payload,
      };
    case USERS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        usersUpdateCheckSuccess: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
