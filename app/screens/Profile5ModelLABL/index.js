import React, { useState, useEffect, useMemo } from 'react';
import { View, ScrollView, Animated, ImageBackground, TouchableOpacity, FlatList, Dimensions, ActivityIndicator } from 'react-native';
import { Images, useTheme, BaseStyle } from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  ProfilePerformanceModelLABL,
  HotelItem
} from '@components';
import * as Utils from '@utils';
import styles from './styles';
import { UserData, HotelData } from '@data';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { modelFollowingGetRequest } from '../../api/following';
import { modelsFindOneRequest } from '../../api/models';
import { userFollowingGetRequest } from '../../api/following';
import { modelProductsGetRequest } from '../../api';

export default function Profile5ModelLABL({ navigation }) {
  const deltaY = new Animated.Value(0);
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [hotels] = useState(HotelData);
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;

  const [showItemCount, setShowItemCount] = useState(9);
  const [userData] = useState(UserData[0]);
  const dispatch = useDispatch();
  const userdata = useSelector(state => state.accessTokenReducer.userData);
  console.log(userdata);
  const obj = {};
  obj.model = userdata?.user?.model?.id;
  useEffect(() => {
    dispatch(modelProductsGetRequest(obj))
  }, [dispatch, loading1])
  const modelProducts = useSelector(state => state.modelProductsReducer.modelProductsData);

  const productsToShow = useMemo(() => modelProducts?.slice(0, showItemCount), [modelProducts, showItemCount]);


  useEffect(() => {
    dispatch(modelFollowingGetRequest(obj));
  }, [dispatch])
  useEffect(() => {
    dispatch(modelsFindOneRequest(userdata?.user?.model?.id));
  }, [dispatch])
  const model = useSelector(state => state.modelsReducer.modelsOneData);
  console.log("model", model);

  useEffect(() => {
    dispatch(userFollowingGetRequest(obj))
  }, [dispatch])

  const modelfollows = useSelector(state => state.followingReducer.userFollowingData)
  console.log("modelfollows", modelfollows);

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const heightImageBanner = Utils.scaleWithPixel(335, 1);
  const marginTopBanner = heightImageBanner - heightHeader - 120;
  const modelfollowings = useSelector(state => state.followingReducer.modelFollowingData);
  console.log("modelfollowings", modelfollowings);
  const performance = [
    { value: modelfollows?.length, title: 'Followers', route: "FollowersScreenModel" },
    { value: modelfollowings?.length, title: 'Following', route: "VendorsScreenLABL" },
    { value: modelProducts?.length, title: 'Posts' },
  ];
  

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  return (
    // <ImageBackground
    //   source={{ uri: userdata?.user?.image ? userdata?.user?.image : "" }}
    //   style={[
    //     styles.imgBanner,
    //   ]}
    // >
    <View style={{ flex: 1 }}>
      {/* <Header
        title=""
        renderLeft={() => {
          return (
            <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center", margin:0 }}>
              <Icon
                name="arrow-left"
                size={20}
                color={colors.primary}
                enableRTL={true}
              />
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}

      <SafeAreaView style={{ flex: 1 }} edges={['right', 'left', 'bottom']}>
        <View style={{ flex: 1 }}>
          <ScrollView
            onScroll={Animated.event([
              {
                nativeEvent: {
                  contentOffset: { y: deltaY },
                },
              },
            ])}
            onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
            scrollEventThrottle={8}>
            <View style={[styles.contentUser]}>
              {/* <Image
                source={Images.profile2}
                style={styles.imgUser}
                resizeMode="cover"
              /> */}
              <View style={styles.contentLeftUser}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 23, color: "#000", marginVertical: 20 }}>
                  {userdata?.user?.username}
                </Text>
                <View style={{ backgroundColor: "#000", height: 40, width: 150, borderRadius: 30, justifyContent: "center", alignItems: "center" }}>
                  <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#fff" }}>Model</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text
                    footnote
                    primaryColor
                    numberOfLines={1}
                    style={{ marginHorizontal: 5 }}>
                    {userData.address}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  padding: 20,
                }}>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18 }}>
                  {t('My self -')}
                </Text>
                <Text body2 numberOfLines={5} style={{ marginTop: 10, width: 350, fontFamily: "Montserrat-Medium", fontSize: 16 }}>
                  {userData.about}.
                </Text>
              </View>
              <View style={{ paddingHorizontal: 20, marginVertical: 30 }}>
                <ProfilePerformanceModelLABL data={performance} user={userdata} />
              </View>
              <View style={{ width: "100%", borderWidth: 0.8, borderColor: "#CDCDCD", marginVertical: 0 }}></View>
              {/* <View onLayout={onLayout}> */}
              <FlatList
                contentContainerStyle={{
                  paddingRight: 13,
                  marginRight: 10,
                  margin: 5,
                  // height: height
                }}
                numColumns={3}
                data={productsToShow}
                keyExtractor={(item, index) => item.id}
                // onEndReached={() => setShowItemCount(showItemCount + 9)}
                // onEndReachedThreshold={0.5}
                renderItem={({ item, index }) => (
                  <HotelItem
                    // grid
                    // style={{padding:5}}
                    image={item?.imagesOfProductByModel[0]?.url}
                  />
                )}
              />
              {/* </View> */}
            </View>
            {showItemCount < modelProducts.length && (
              <TouchableOpacity style={{ marginVertical: 10, backgroundColor: "#000",alignSelf:"center",justifyContent:"center",alignItems:"center", height: 40, width: 100 }} onPress={() => setShowItemCount(showItemCount + 9)} >
                <Text style={{color:"#fff"}}>Load More</Text>
              </TouchableOpacity>
            )}
          </ScrollView>
        </View>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }


    </View>
  );
}
