import * as React from 'react';
import {
  View,
  TouchableOpacity,
  StatusBar,
  Text,
  PermissionsAndroid,
  ActivityIndicator,
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useSelector } from 'react-redux';
import { BaseColor, useTheme, useFont } from '@config';
import { useTranslation } from 'react-i18next';
import { Icon, Image } from '@components';
import ImagePicker from 'react-native-image-crop-picker';
import Icon1 from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/Foundation';
import Icon3 from 'react-native-vector-icons/Feather';
import * as Utils from '@utils';
import { uploadFilesPostRequest } from '../api/uploadApi';
import { statusPostRequest } from '../api/status';
import { useDispatch } from 'react-redux';
import {
  Menu,
  Button,
  VStack,
  Select,
  CheckIcon,
  Center,
  NativeBaseProvider,
} from 'native-base';

import IconSimple from 'react-native-vector-icons/SimpleLineIcons';

/* Stack Screen */
import Comments from '@screens/Comments';
import HomeScreenModelLABL from '@screens/HomeScreenModelLABL';
import HomeScreenUserLABL from '@screens/HomeScreenUserLABL';
import Profile1 from '@screens/Profile1';
import Profile2 from '@screens/Profile2';
import Profile3 from '@screens/Profile3';
import Profile4 from '@screens/Profile4';
import Profile5 from '@screens/Profile5';
import Profile6 from '@screens/Profile6';
import Profile7 from '@screens/Profile7';
import Profile8 from '@screens/Profile8';
import More from '@screens/More';
import Tour from '@screens/Tour';
import Car from '@screens/Car';
import OverViewCar from '@screens/OverViewCar';
import Hotel from '@screens/Hotel';
import Review from '@screens/Review';
import Feedback from '@screens/Feedback';
import Messages from '@screens/Messages';
import Notification from '@screens/Notification';
import Walkthrough from '@screens/Walkthrough';
import SignUpScreenModelLABL from '@screens/SignUpScreenModelLABL';
import SignUpScreenUserLABL from '@screens/SignUpScreenUserLABL';
import UploadScreenModelLABL from '@screens/UploadScreenModelLABL';
import UpdateAddressScreenLABL from '@screens/AddressScreenLABL/addressUpdate';
import AddAddressScreenModelLABL from '@screens/AddressScreenModelLABL/address';
import AddressScreenModelLABL from '@screens/AddressScreenModelLABL';
import UpdateAddressScreenModelLABL from '@screens/AddressScreenModelLABL/addressUpdate';
import CollectionsScreenModelLABL from '@screens/CollectionsScreenModelLABL';
import CategoriesScreenUserLABL from '@screens/CategoriesScreenUserLABL';
import SignIn from '@screens/SignIn';
import ResetPassword from '@screens/ResetPassword';
import ChangePassword from '@screens/ChangePassword';
import ProfileEdit from '@screens/ProfileEdit';
import ProfileExample from '@screens/ProfileExample';
import ChangeLanguage from '@screens/ChangeLanguage';
import HotelInformation from '@screens/HotelInformation';
import CheckOut from '@screens/CheckOut';
import Currency from '@screens/Currency';
import Coupons from '@screens/Coupons';
import HotelDetail from '@screens/HotelDetail';
import ContactUs from '@screens/ContactUs';
import PreviewBooking from '@screens/PreviewBooking';
import PricingTable from '@screens/PricingTable';
import PricingTableIcon from '@screens/PricingTableIcon';
import BookingDetail from '@screens/BookingDetail';
import PostDetail from '@screens/PostDetail';
import TourDetail from '@screens/TourDetail';
import CarDetail from '@screens/CarDetail';
import AboutUs from '@screens/AboutUs';
import OurService from '@screens/OurService';
import FlightSearch from '@screens/FlightSearch';
import SelectFlight from '@screens/SelectFlight';
import FlightResult from '@screens/FlightResult';
import FlightSummary from '@screens/FlightSummary';
import FlightTicket from '@screens/FlightTicket';
import CruiseSearch from '@screens/CruiseSearch';
import Cruise from '@screens/Cruise';
import CruiseDetail from '@screens/CruiseDetail';
import BusSearch from '@screens/BusSearch';
import BusList from '@screens/BusList';
import BusFilter from '../screens/BusFilter';
import BusSelectSeat from '@screens/BusSelectSeat';
import PreviewBusBooking from '@screens/PreviewBusBooking';
import BusTicket from '@screens/BusTicket';
import Event from '@screens/Event';
import EventDetail from '@screens/EventDetail';
import EventPreviewBooking from '@screens/EventPreviewBooking';
import DashboardEvent from '@screens/DashboardEvent';
import EventTicket from '@screens/EventTicket';
import PaymentMethod from '@screens/PaymentMethod';
import MyPaymentMethod from '@screens/MyPaymentMethod';
import AddPayment from '@screens/AddPayment';
import PaymentMethodDetail from '@screens/PaymentMethodDetail';
import PreviewPayment from '@screens/PreviewPayment';
import Setting from '@screens/Setting';
import ThemeSetting from '@screens/ThemeSetting';
import NotFound from '@screens/NotFound';
import EarnDetail from '../screens/EarnDetail';
import OrdersList from '../screens/OrdersList';
import Cart from '../screens/Cart';
import Wishlist from '@screens/Wishlist';
import WishlistModelLABL from '@screens/WishlistModelLABL';
import CollectionlistModelLABL from '@screens/CollectionlistModelLABL';
import ModelProductsScreenLABL from '../screens/ModelProductsScreenLABL';
import UserSelectedProductScreenLABL from '../screens/UserSelectedProductScreenLABL';
import UserSelectedProductDetails from '../screens/UserSelectedProductDetails';
import UserCart from '../screens/UserCart';
import ModelOrdersScreenLABL from '../screens/ModelOrdersScreenLABL';
import SubCategories from '../screens/SubCategories';
import UserProductsScreenLABL from '../screens/UserProductsScreenLABL';
import ModelProductsDetailScreenLABL from '../screens/ModelProductsDetailScreenLABL';
import PreviewImage from '../screens/PreviewImage';
import Walkthrough1 from '@screens/Walkthrough/walkthrough1';
import FollowersScreen from '@screens/FollowersScreenUserLABL';
import AddAddressScreenLABL from '@screens/AddressScreenLABL/address';
import SignInModelLABL from '@screens/SignInModelLABL';
import ProfileModelLABL from '@screens/ProfileModelLABL';
import Profile5ModelLABL from '@screens/Profile5ModelLABL';
import FollowersScreenModel from '@screens/FollowersScreenModelLABL';
import OrdersListModelLABL from '@screens/OrdersListModelLABL';
import EarningsScreenModelLABL from '@screens/EarningsScreenModelLABL';
import VendorsScreenLABL from '@screens/VendorsScreenLABL';
import VendorProfile from '@screens/VendorsScreenLABL/VendorProfile';
import CartModelLABL from '@screens/CartModelLABL';
import PostDetailModelLABL from '@screens/PostDetailModelLABL';
import CollectionlistUserLABL from '@screens/CollectionlistUserLABL';
import CollectionsScreenUserLABL from '@screens/CollectionsScreenUserLABL';
import VendorCommonProfile from '@screens/VendorsScreenLABL/VendorCommonProfile';
import CommentsModel from '@screens/CommentsModel';
import ForgotPassword from '@screens/ResetPassword/ForgotPassword';
import ThankyouScreen from '@screens/UploadScreenModelLABL/ThankyouScreen';
import OrdersScreenLABL from '@screens/OrdersScreenLABL';
import CollectionlistUserSearch from '@screens/CollectionlistUserSearch';
import CollectionsScreenUserSearch from '@screens/CollectionsScreenUserSearch';
import SearchHistoryModel from '@screens/SearchHistoryModel';
import CollectionlistModelSearch from '@screens/CollectionlistModelSearch';
import CollectionsScreenModelSearch from '@screens/CollectionsScreenModelSearch';
import OrdersScreenModelLABL from '@screens/OrdersScreenModelLABL';
import BottomTabNavigator from './bottomTabUser';
import ThankyouOrderPlaced from '../screens/CartModelLABL/ThankyouOrderPlaced';
//by ashok
import Otp from '@screens/Otp';
import OnboardingScreen2 from '@screens/OnboardingScreen2';
import OnboardingScreen3 from '@screens/OnboardingScreen3';

/* Bottom Screen */
// import Home from '@screens/Home';
import Messenger from '@screens/Messenger';
import Post from '@screens/Post';
import Profile from '@screens/Profile';
import CategoriesScreenModelLABL from '@screens/CategoriesScreenModelLABL';
import AddressScreenLABL from '@screens/AddressScreenLABL';
import Loading from '../screens/Loading';

const MainStack = createStackNavigator();
const BottomTab = createBottomTabNavigator();

export default function Main() {
  const accessToken = useSelector(
    state => state.accessTokenReducer.accessToken,
  );
  const userData = useSelector(state => state.accessTokenReducer.userData);
  return (
    <MainStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={userData?.user?.isModel === false && userData?.user?.isVendor === false ? "BottomTabNavigator" : "BottomTabNavigator"}>
      {userData?.user?.isModel === false && userData?.user?.isVendor === false ?
        <MainStack.Screen
          tabBarOptions={{
            style: {
              backgroundColor: 'red',
              height: Utils.scaleWithPixel(80),
              width: '100%',
            },
          }}
          name="BottomTabNavigator"
          component={accessToken ? BottomTabNavigator : Walkthrough}
        /> :
        <MainStack.Screen
          tabBarOptions={{
            style: {
              backgroundColor: 'red',
              height: Utils.scaleWithPixel(80),
              width: '100%',
            },
          }}
          name="BottomTabNavigator1"
          component={accessToken ? BottomTabNavigator1 : Walkthrough}
        />}
      <MainStack.Screen name="Otp" component={Otp} />
      <MainStack.Screen name="SearchHistoryModel" component={SearchHistoryModel} />
      <MainStack.Screen name="Comments" component={Comments} />
      <MainStack.Screen name="Loading" component={Loading} />
      <MainStack.Screen name="CommentsModel" component={CommentsModel} />
      <MainStack.Screen name="CategoriesScreenModelLABL" component={CategoriesScreenModelLABL} />
      <MainStack.Screen name="CollectionlistUserLABL" component={CollectionlistUserLABL} />
      <MainStack.Screen name="CollectionsScreenUserLABL" component={CollectionsScreenUserLABL} />
      <MainStack.Screen name="OnboardingScreen2" component={OnboardingScreen2} />
      <MainStack.Screen name="OnboardingScreen3" component={OnboardingScreen3} />
      <MainStack.Screen name="HomeScreenModelLABL" component={HomeScreenModelLABL} />
      <MainStack.Screen name="UploadScreenModelLABL" component={UploadScreenModelLABL} />
      <MainStack.Screen name="UpdateAddressScreenLABL" component={UpdateAddressScreenLABL} />
      <MainStack.Screen name="ForgotPassword" component={ForgotPassword} />
      <MainStack.Screen name="Profile1" component={Profile1} />
      <MainStack.Screen name="Profile2" component={Profile2} />
      <MainStack.Screen name="Profile3" component={Profile3} />
      <MainStack.Screen name="Profile4" component={Profile4} />
      <MainStack.Screen name="Profile5" component={Profile5} />
      <MainStack.Screen name="Profile6" component={Profile6} />
      <MainStack.Screen name="Profile7" component={Profile7} />
      <MainStack.Screen name="Profile8" component={Profile8} />
      <MainStack.Screen name="More" component={More} />
      <MainStack.Screen name="Tour" component={Tour} />
      <MainStack.Screen name="Car" component={Car} />
      <MainStack.Screen name="OverViewCar" component={OverViewCar} />
      <MainStack.Screen name="Hotel" component={Hotel} />
      <MainStack.Screen name="Review" component={Review} />
      <MainStack.Screen name="Feedback" component={Feedback} />
      <MainStack.Screen name="Messages" component={Messages} />
      <MainStack.Screen name="Notification" component={Notification} />
      <MainStack.Screen name="Walkthrough" component={Walkthrough} />
      <MainStack.Screen name="ProfileModelLABL" component={ProfileModelLABL} />
      <MainStack.Screen name="ThankyouScreen" component={ThankyouScreen} />
      <MainStack.Screen name="OrdersScreenLABL" component={OrdersScreenLABL} />
      <MainStack.Screen name="CollectionlistUserSearch" component={CollectionlistUserSearch} />
      <MainStack.Screen name="CollectionsScreenUserSearch" component={CollectionsScreenUserSearch} />
      <MainStack.Screen name="CollectionlistModelSearch" component={CollectionlistModelSearch} />
      <MainStack.Screen name="CollectionsScreenModelSearch" component={CollectionsScreenModelSearch} />
      <MainStack.Screen name="OrdersScreenModelLABL" component={OrdersScreenModelLABL} />
      <MainStack.Screen name="ThankyouOrderPlaced" component={ThankyouOrderPlaced} />
      <MainStack.Screen
        name="VendorCommonProfile"
        component={VendorCommonProfile}
      />
      <MainStack.Screen name="SignInModelLABL" component={SignInModelLABL} />
      <MainStack.Screen
        name="AddAddressScreenLABL"
        component={AddAddressScreenLABL}
      />
      <MainStack.Screen
        name="AddressScreenLABL"
        component={AddressScreenLABL}
      />
      <MainStack.Screen name="FollowersScreen" component={FollowersScreen} />
      <MainStack.Screen
        name="SignUpScreenModelLABL"
        component={SignUpScreenModelLABL}
      />
      {/* <MainStack.Screen name="SignUpScreenUserLABL" component={SignUpScreenUserLABL} /> */}
      <MainStack.Screen
        name="SignUpScreenUserLABL"
        component={SignUpScreenUserLABL}
      />
      <MainStack.Screen name="SignIn" component={SignIn} />
      <MainStack.Screen name="ResetPassword" component={ResetPassword} />
      <MainStack.Screen name="ChangePassword" component={ChangePassword} />
      <MainStack.Screen name="ProfileEdit" component={ProfileEdit} />
      <MainStack.Screen name="ProfileExample" component={ProfileExample} />
      <MainStack.Screen name="ChangeLanguage" component={ChangeLanguage} />
      <MainStack.Screen name="HotelInformation" component={HotelInformation} />
      <MainStack.Screen name="CheckOut" component={CheckOut} />
      <MainStack.Screen name="Currency" component={Currency} />
      <MainStack.Screen name="Coupons" component={Coupons} />
      <MainStack.Screen name="HotelDetail" component={HotelDetail} />
      <MainStack.Screen name="ContactUs" component={ContactUs} />
      <MainStack.Screen name="PreviewBooking" component={PreviewBooking} />
      <MainStack.Screen name="PricingTable" component={PricingTable} />
      <MainStack.Screen name="PricingTableIcon" component={PricingTableIcon} />
      <MainStack.Screen name="BookingDetail" component={BookingDetail} />
      <MainStack.Screen name="PostDetail" component={PostDetail} />
      <MainStack.Screen
        name="PostDetailModelLABL"
        component={PostDetailModelLABL}
      />
      <MainStack.Screen name="TourDetail" component={TourDetail} />
      <MainStack.Screen name="CarDetail" component={CarDetail} />
      <MainStack.Screen name="AboutUs" component={AboutUs} />
      <MainStack.Screen name="OurService" component={OurService} />
      <MainStack.Screen name="FlightSearch" component={FlightSearch} />
      <MainStack.Screen name="SelectFlight" component={SelectFlight} />
      <MainStack.Screen name="FlightResult" component={FlightResult} />
      <MainStack.Screen name="FlightSummary" component={FlightSummary} />
      <MainStack.Screen name="FlightTicket" component={FlightTicket} />
      <MainStack.Screen name="CruiseSearch" component={CruiseSearch} />
      <MainStack.Screen name="Cruise" component={Cruise} />
      <MainStack.Screen name="CruiseDetail" component={CruiseDetail} />
      <MainStack.Screen name="BusSearch" component={BusSearch} />
      <MainStack.Screen name="BusList" component={BusList} />
      <MainStack.Screen name="BusFilter" component={BusFilter} />
      <MainStack.Screen name="BusSelectSeat" component={BusSelectSeat} />
      <MainStack.Screen name="OrdersList" component={OrdersList} />
      <MainStack.Screen name="Cart" component={Cart} />
      <MainStack.Screen name="Wishlist" component={Wishlist} />
      <MainStack.Screen
        name="WishlistModelLABL"
        component={WishlistModelLABL}
      />
      <MainStack.Screen
        name="CollectionlistModelLABL"
        component={CollectionlistModelLABL}
      />
      <MainStack.Screen
        name="ModelProductsScreenLABL"
        component={ModelProductsScreenLABL}
      />
      <MainStack.Screen
        name="UserSelectedProductScreenLABL"
        component={UserSelectedProductScreenLABL}
      />
      <MainStack.Screen
        name="UserSelectedProductDetails"
        component={UserSelectedProductDetails}
      />
      <MainStack.Screen name="UserCart" component={UserCart} />
      <MainStack.Screen
        name="ModelOrdersScreenLABL"
        component={ModelOrdersScreenLABL}
      />
      <MainStack.Screen name="SubCategories" component={SubCategories} />
      <MainStack.Screen
        name="UserProductsScreenLABL"
        component={UserProductsScreenLABL}
      />
      <MainStack.Screen
        name="ModelProductsDetailScreenLABL"
        component={ModelProductsDetailScreenLABL}
      />
      <MainStack.Screen
        name="Profile5ModelLABL"
        component={Profile5ModelLABL}
      />
      <MainStack.Screen name="PreviewImage" component={PreviewImage} />
      <MainStack.Screen name="Walkthrough1" component={Walkthrough1} />
      <MainStack.Screen name="HomeScreenUserLABL" component={HomeScreenUserLABL} />
      <MainStack.Screen
        name="FollowersScreenModel"
        component={FollowersScreenModel}
      />
      <MainStack.Screen
        name="OrdersListModelLABL"
        component={OrdersListModelLABL}
      />
      <MainStack.Screen
        name="EarningsScreenModelLABL"
        component={EarningsScreenModelLABL}
      />
      <MainStack.Screen
        name="VendorsScreenLABL"
        component={VendorsScreenLABL}
      />
      <MainStack.Screen name="VendorProfile" component={VendorProfile} />
      <MainStack.Screen
        name="AddAddressScreenModelLABL"
        component={AddAddressScreenModelLABL}
      />
      <MainStack.Screen
        name="AddressScreenModelLABL"
        component={AddressScreenModelLABL}
      />
      <MainStack.Screen
        name="UpdateAddressScreenModelLABL"
        component={UpdateAddressScreenModelLABL}
      />
      <MainStack.Screen name="CartModelLABL" component={CartModelLABL} />
      <MainStack.Screen
        name="CollectionsScreenModelLABL"
        component={CollectionsScreenModelLABL}
      />
      <MainStack.Screen
        name="CategoriesScreenUserLABL"
        component={CategoriesScreenUserLABL}
      />
      <MainStack.Screen
        name="PreviewBusBooking"
        component={PreviewBusBooking}
      />
      <MainStack.Screen name="BusTicket" component={BusTicket} />
      <MainStack.Screen name="Event" component={Event} />
      <MainStack.Screen name="EventDetail" component={EventDetail} />
      <MainStack.Screen name="EarnDetail" component={EarnDetail} />

      <MainStack.Screen
        name="EventPreviewBooking"
        component={EventPreviewBooking}
      />
      <MainStack.Screen name="DashboardEvent" component={DashboardEvent} />
      <MainStack.Screen name="EventTicket" component={EventTicket} />
      <MainStack.Screen name="PaymentMethod" component={PaymentMethod} />
      <MainStack.Screen name="MyPaymentMethod" component={MyPaymentMethod} />
      <MainStack.Screen name="AddPayment" component={AddPayment} />
      <MainStack.Screen
        name="PaymentMethodDetail"
        component={PaymentMethodDetail}
      />
      <MainStack.Screen name="PreviewPayment" component={PreviewPayment} />
      <MainStack.Screen name="Setting" component={Setting} />
      <MainStack.Screen name="ThemeSetting" component={ThemeSetting} />
      <MainStack.Screen name="NotFound" component={NotFound} />
      <MainStack.Screen name="Profile" component={Profile} />
    </MainStack.Navigator>
  );
}

function BottomTabNavigator1({ navigation }) {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const font = useFont();
  const auth = useSelector(state => state.auth);
  const login = auth.login.success;
  const accessToken = useSelector(
    state => state.accessTokenReducer.accessToken,
  );
  const userData = useSelector(state => state.accessTokenReducer.userData);
  const dispatch = useDispatch();
  const [shouldOverlapWithTrigger] = React.useState(false);
  const [position, setPosition] = React.useState('auto');

  React.useEffect(() => {
    setPosition('auto');
  }, []);

  const [image, setImage] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  const selectImage = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, response => {
      console.log(response.assets, 'all response', response.assets.uri);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        console.log(source);
        setImage(source);
      }
    });
  };
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write camera err', err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  console.log(image, 'image.......');
  const captureImage = async type => {
    let options = {
      storageOptions: {
        //   skipBackup: true,
        //   path: 'images',
      },
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      let formData = new FormData();
      ImagePicker.openCamera({
        multiple: false,
        cropping: true,
      }).then(images => {
        // console.log('imgApi');

        toNextPage(images);
      });
    }
  };

  const toNextPage = async images => {
    // console.log('imgApi2', images);
    let formData = new FormData();
    setLoading(true);

    console.log('lopala', images);

    formData.append('files', {
      name: images.modificationDate,
      type: images.mime,
      uri: images.path,
    });
    console.log(formData);

    let imgApi = await uploadFilesPostRequest(formData);
    console.log('imgApi', imgApi);
    const obj = {};
    obj.image = imgApi[0]?.id;
    obj.model = userData?.user?.model?.id;
    dispatch(statusPostRequest(obj));
    setLoading(false);
  };
  if (loading) {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator
          size={'large'}
          style={{ backgroundColor: 'transparent' }}
        />
        <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 20 }}>
          Loading
        </Text>
      </View>
    );
  }

  console.log('in main.js file before nav', accessToken, userData);
  return (
    <BottomTab.Navigator
      initialRouteName="HomeScreenModelLABL"
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: colors.primary,
        inactiveTintColor: BaseColor.grayColor,
        tabBarShowLabel: true,
        tabBarShowIcon: true,
        style: {
          height: Utils.scaleWithPixel(100),
          backgroundColor: 'red',
        },
        tabBarLabelStyle: {
          fontSize: 12,
          fontFamily: font,
        },
        tabBarStyle: { borderTopWidth: 1 },
      }}>
      <BottomTab.Screen
        name="HomeScreenModelLABL"
        component={login ? HomeScreenModelLABL : Walkthrough}
        //   options={{
        //     tabBarStyle: { display: "none" },
        //  }}

        options={{
          tabBarStyle: login ? { display: 'flex' } : { display: 'none' },
          // tabBarVisible: false,
          // tabBarVisible: login ? false : true,
          title: t('Home'),
          tabBarIcon: ({ color }) => {
            return <Icon2 name="home" color={color} size={25} solid />;
          },
        }}
      />
      <BottomTab.Screen
        name="fav"
        component={login ? CategoriesScreenModelLABL : Walkthrough}
        options={{
          tabBarStyle: login ? { display: 'flex' } : { display: 'none' },

          title: t('Categories'),
          tabBarIcon: ({ color }) => {
            return <Icon color={color} name="list" size={20} solid />;
          },
        }}
      />
      <BottomTab.Screen
        name="upload"
        component={login ? UploadScreenModelLABL : Walkthrough}
        options={{
          tabBarStyle: login ? { display: 'flex' } : { display: 'none' },

          title: t('Upload'),
          tabBarIcon: ({ color }) => {
            return (
              <Icon
                name="plus-circle"
                size={30}
                color={color}
              />
              // <View style={{ marginBottom: 0, alignSelf: 'center' }}>
              //   <NativeBaseProvider>
              //     <Center flex={1} px="3">
              //       <VStack space={6} alignSelf="flex-start" w="100%">
              //         <Menu
              //           w="130"
              //           height="130"
              //           borderRadius={10}
              //           shouldOverlapWithTrigger={shouldOverlapWithTrigger} // @ts-ignore
              //           placement={position == 'auto' ? 'top' : position}
              //           trigger={triggerProps => {
              //             return (
              //               <TouchableOpacity
              //                 {...triggerProps}
              //                 style={{
              //                   // flexDirection: "row",
              //                   alignItems: 'center',
              //                   justifyContent: 'center',
              //                 }}>
              //                 <Icon
              //                   name="plus-circle"
              //                   size={30}
              //                   color={color}
              //                 />
              //               </TouchableOpacity>
              //             );
              //           }}>
              //           <Menu.Item>
              //             <TouchableOpacity
              //               onPress={() =>
              //                 navigation.navigate('UploadScreenModelLABL')
              //               }
              //               style={{
              //                 flexDirection: 'row',
              //                 justifyContent: 'space-between',
              //                 marginVertical: 10,
              //               }}>
              //               <Text
              //                 style={{
              //                   fontFamily: 'Montserrat-Bold',
              //                   fontSize: 18,
              //                 }}>
              //                 Post
              //               </Text>
              //               <Icon3 name="upload" size={20} color="#000" />
              //             </TouchableOpacity>
              //           </Menu.Item>
              //           <View
              //             style={{
              //               borderWidth: 1,
              //               width: 135,
              //               borderColor: '#CDCDCD',
              //               alignSelf: 'center',
              //             }}></View>
              //           <Menu.Item>
              //             <TouchableOpacity
              //               onPress={() => captureImage()}
              //               style={{
              //                 flexDirection: 'row',
              //                 justifyContent: 'space-between',
              //                 marginVertical: 10,
              //               }}>
              //               <Text
              //                 style={{
              //                   fontFamily: 'Montserrat-Bold',
              //                   fontSize: 18,
              //                   alignSelf: 'center',
              //                 }}>
              //                 Story
              //               </Text>
              //               <Image
              //                 source={require('../assets/images/story.png')}
              //                 style={{ height: 25, width: 25 }}
              //               />
              //             </TouchableOpacity>
              //           </Menu.Item>
              //         </Menu>
              //       </VStack>
              //     </Center>
              //   </NativeBaseProvider>
              // </View>
            );
          },
        }}
        // listeners={({ navigation }) => ({
        //   tabPress: e => {
        //     e.preventDefault();
        //     // navigation.navigate('CreateNew');
        //   },
        // })}
      />
      <BottomTab.Screen
        name="Earnings"
        component={login ? EarningsScreenModelLABL : Walkthrough}
        options={{
          tabBarStyle: login ? { display: 'flex' } : { display: 'none' },

          title: t('Earnings'),
          tabBarIcon: ({ color }) => {
            return <Icon1 color={color} name="wallet" size={20} solid />;
          },
        }}
      />
      {/* <BottomTab.Screen
        name="Post"
        component={Post}
        options={{
          title: t('Bag'),
          tabBarIcon: ({color}) => {
            return <Icon color={color} name="shopping-bag" size={20} solid />;
          },
        }}
      /> */}
      <BottomTab.Screen
        name="Profile"
        component={login ? Profile5ModelLABL : Walkthrough}
        options={{
          tabBarStyle: login ? { display: 'flex' } : { display: 'none' },
          title: t('Profile'),
          tabBarIcon: ({ color }) => {
            return <Icon solid color={color} name="user-circle" size={20} />;
          },
        }}
      />
    </BottomTab.Navigator>
  );
}