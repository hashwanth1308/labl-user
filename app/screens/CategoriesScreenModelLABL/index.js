import React, { useEffect, useState } from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  FlatList,
  TouchableHighlight,
  RefreshControl,
  Animated,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { BaseStyle, useTheme } from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  Image,
  TextInput,
  Button,
} from '@components';
import { useTranslation } from 'react-i18next';
import styles from './styles';
import {
  categoriesGetRequest,
  parentCategoriesGetRequest,
} from '../../api/categories';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';

export default function CategoriesScreenModelLABL({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const themeStorage = useSelector(state => state.application.theme);
  useEffect(() => {
    dispatch(categoriesGetRequest());
  }, [dispatch]);
  const addressData = useSelector(
    state => state.collectionsReducer.collectionsData,
  );
  const categoriesData = useSelector(
    state => state.categoriesReducer.categoriesData,
  );
  const subCategoriesData = useSelector(
    state => state.categoriesReducer.parentCategoriesData,
  );
  console.log('categoriesData', categoriesData);
  console.log('subCategoriesData', subCategoriesData);
  const dispatch = useDispatch();
  const [refreshing, setRefreshing] = useState(false);
  const [card, setCard] = useState('');
  const [valid, setValid] = useState('');
  const [digit, setDigit] = useState('');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });


  const [expanded, setExpanded] = useState(null);
  const handleClick = i => {
    if (expanded == i) {
      setExpanded(null);
    } else {
      setExpanded(i);
    }
  };
  // useEffect(() => {
  //   dispatch(collectionsCountRequest());

  // }, [dispatch])
  // useEffect(() => {
  //   dispatch(collectionsGetRequest());

  // }, [dispatch])

  // const onFindOneCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onAddCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => setLoading1(prev => !prev), 1000);
  }, []);
  return (
    <View style={{ flex: 1 }}>
      {/* <Header
        title={t('add_payment_method')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ padding: 0 }}>
            <FlatList
              // contentContainerStyle={{
              //   paddingLeft: 5,
              //   paddingRight: 20,
              //   paddingTop: 10,
              // }}
              // numColumns={3}
              refreshControl={
                <RefreshControl
                  colors={[colors.primary]}
                  tintColor={colors.primary}
                  refreshing={refreshing}
                  onRefresh={() => {
                    setRefreshing(true);
                  }}
                />
              }
              horizontal={false}
              data={categoriesData}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <>
                  <View
                    style={{
                      backgroundColor: '#000',
                      height: 120,
                      width: 400,
                      flexDirection: 'row',
                      padding: 20,
                      marginBottom: 5,
                      justifyContent: 'space-around',
                    }}>
                    <View
                      style={{
                        flexDirection: 'column',
                        justifyContent: 'space-evenly',
                      }}>
                      {expanded == index ? (
                        <TouchableOpacity
                          onPress={() => setExpanded(null)}
                          style={{ flexDirection: 'row' }}>
                          <Text
                            style={{
                              fontFamily: 'Montserrat-Bold',
                              fontSize: 24,
                              color: '#fff',
                            }}>
                            {item?.name}
                          </Text>
                          <Icon1
                            name={'chevron-up'}
                            size={28}
                            color="#fff"
                            style={{ alignSelf: 'center' }}
                          />
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          onPress={() => handleClick(index)}
                          style={{ flexDirection: 'row' }}>
                          <Text
                            style={{
                              fontFamily: 'Montserrat-Bold',
                              fontSize: 24,
                              color: '#fff',
                            }}>
                            {item?.name}
                          </Text>
                          <Icon1
                            name={'chevron-down'}
                            size={28}
                            color="#fff"
                            style={{ alignSelf: 'center' }}
                          />
                        </TouchableOpacity>
                      )}
                      <Text
                        style={{
                          fontFamily: 'Montserrat-Medium',
                          fontSize: 16,
                          color: '#fff',
                        }}>
                        {item?.description}
                      </Text>
                    </View>
                    <Image
                      source={{ uri :item?.image}}
                      style={{ height: 100, width: 100 }}
                    />
                  </View>
                  {expanded == index ? (
                    <View
                      style={{
                        // height: height / 2.8,
                        flex:1,
                        width: '100%',
                        marginHorizontal: 20,
                      }}>
                      {item?.parentCategories.map(item => (
                        <TouchableOpacity
                          onPress={() =>
                            navigation.navigate('CollectionsScreenModelLABL', {
                              prodObj: item,
                            })
                          }>
                          <Text
                            style={{
                              color: '#000',
                              fontFamily: 'Montserrat-Medium',
                              fontSize: 18,
                              margin: 10,
                            }}>
                            {item?.name}
                          </Text>
                        </TouchableOpacity>
                      ))}
                    </View>
                  ) : null}
                </>
              )}
            />
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      {loading1 && (
        <View
          style={{
            width: width,
            height: height,
            backgroundColor: 'rgba(0,0,0,0.8)',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
          }}>
          <View
            style={{
              width: '60%',
              paddingVertical: 20,
              backgroundColor: '#fff',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <View>
              <ActivityIndicator size={'large'} color="#000" />
            </View>
            <Text
              style={{
                marginLeft: 10,
                fontSize: 30,
                color: '#000',
                fontWeight: 'bold',
              }}>
              Loading..
            </Text>
          </View>
        </View>
      )}
    </View>
  );
}
