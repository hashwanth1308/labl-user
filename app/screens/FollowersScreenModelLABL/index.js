import React, { useState, useEffect } from 'react';
import {
    View,
    Animated,
    RefreshControl,
    TouchableOpacity,
    FlatList,
    ScrollView,
    Dimensions,
    SafeAreaView,
    ActivityIndicator
} from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import {
    Text,
    HotelItem,
    Image,
    Header,
    ListThumbCircle,
    PostItem,
    Icon,
    ProfileAuthor
} from '@components';
import PropTypes from 'prop-types';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { UserData } from '../../data/user';
import { HotelData } from '../../data/hotel';
import { useDispatch, useSelector } from 'react-redux';
import { userFollowingGetRequest } from '../../api/following';

export default function FollowersScreenModel({ navigation, route }) {
    const { t } = useTranslation();
    const { colors } = useTheme();
    const dispatch = useDispatch();
    const height = Dimensions.get('window').height;
    const width = Dimensions.get('window').width;
    const { user } = route.params;
    console.log("u------", user);
    const obj = {};
    obj.model = user?.user?.model?.id;

    useEffect(() => {
        dispatch(userFollowingGetRequest(obj))
    }, [dispatch])

    const modelfollows = useSelector(state => state.followingReducer.userFollowingData)
    console.log("modelfollows", modelfollows);

    const [loading1, setLoading1] = useState(true);
    useEffect(() => {
        setTimeout(() => (
            setLoading1((prev) => (!prev))
        ), 1000)
    }, []);
    return (
        <View style={{ flex: 1 }} >
            <Header
                // title={t('Shopping Bag')}
                style={{ height: 80, backgroundColor: "#000" }}
                renderLeft={() => {
                    return (
                        <View style={{ flexDirection: "row", width: 200, justifyContent: "space-between", alignSelf: "flex-start", top: -10 }}>
                            <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                                <Icon
                                    name="arrow-left"
                                    size={20}
                                    color={colors.primary}
                                    enableRTL={true}
                                />
                            </View>
                            <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", right: 40 }}>Followers</Text>
                            {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
                        </View>
                    );
                }}
                onPressLeft={() => {
                    navigation.goBack();
                }}
            />
            <ScrollView style={{ height: "100%" }}>
                <View style={{ padding: 10 }}>
                    <FlatList
                        contentContainerStyle={{
                            paddingLeft: 5,
                            paddingRight: 20,
                            paddingTop: 20,
                        }}
                        // numColumns={3}
                        data={modelfollows}
                        keyExtractor={(item, index) => item.id}
                        renderItem={({ item, index }) => (
                            <FollowersComponent image={item?.user?.image} name={item?.user?.username} />
                        )}
                    />
                </View>
            </ScrollView>
            {loading1 &&
        <View style={{ width: width, height:height,backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor:"#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
        </View>
    )
}


function FollowersComponent(props) {
    const { image, name, id } = props;
    console.log("id", id);
    // const [follow, setFollow] = useState(false);
    // const handleClick = () => {
    //     setFollow(true);
    // }
    // const handleClick1 = () => {
    //     setFollow(false);
    // }
    return (
        <View style={{ flexDirection: "row", marginVertical: 10, left: 10 }}>
            <Image source={{uri : "https://smallimg.pngkey.com/png/small/52-522921_kathrine-vangen-profile-pic-empty-png.png"}} style={{ height: 50, width: 50, borderRadius: 40 }} />
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#000", width: 120, alignSelf: "center",marginHorizontal:20 }}>{name}</Text>
            {/* {follow ?
                <TouchableOpacity onPress={handleClick1} style={{ height: 35, width: 140, borderWidth: 1, borderColor: "#000", borderRadius: 10, justifyContent: "center", alignItems: "center", alignSelf: "center" }}>
                    <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#000" }}>Following</Text>
                </TouchableOpacity> :
                <TouchableOpacity onPress={handleClick} style={{ height: 35, width: 140, backgroundColor: "#000", borderRadius: 10, justifyContent: "center", alignItems: "center", alignSelf: "center" }}>
                    <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#fff" }}>Follow</Text>
                </TouchableOpacity>} */}
        </View>
    )
}

FollowersComponent.propTypes = {
    image: PropTypes.node.isRequired,
    name: PropTypes.string,
};

FollowersComponent.defaultProps = {
    image: '',
    name: '',
};