import {
  ORDERMESSAGES_POST_CHECK_SUCCESS,
  ORDERMESSAGES_SET_COUNT,
  ORDERMESSAGES_COUNT_CHECK_SUCCESS,
  ORDERMESSAGES_DELETE_CHECK_SUCCESS,
  ORDERMESSAGES_FINDONE_CHECK_SUCCESS,
  ORDERMESSAGES_SET_ONE_DATA,
  ORDERMESSAGES_GET_CHECK_SUCCESS,
  ORDERMESSAGES_SET_DATA,
  ORDERMESSAGES_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  orderMessagesPostCheckSuccess: false,
  orderMessagesCount: 0,
  orderMessagesCountCheckSuccess : false,
  orderMessagesDeleteCheckSuccess: false,
  orderMessagesFindOneCheckSuccess: false,
  orderMessagesOneData: [],
  orderMessagesGetCheckSuccess: false,
  orderMessagesData: [],
  orderMessagesUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ORDERMESSAGES_POST_CHECK_SUCCESS:
      return {
        ...state,
        orderMessagesPostCheckSuccess: action.payload,
      };
      case ORDERMESSAGES_SET_COUNT:
      return {
        ...state,
        orderMessagesCount: action.payload,
      };
      case ORDERMESSAGES_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        orderMessagesCountCheckSuccess: action.payload,
      };
      case ORDERMESSAGES_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        orderMessagesDeleteCheckSuccess: action.payload,
      };
      case ORDERMESSAGES_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        orderMessagesFindOneCheckSuccess: action.payload,
          };
      case ORDERMESSAGES_SET_ONE_DATA:
      return {
        ...state,
        orderMessagesOneData: action.payload,
          };
      case ORDERMESSAGES_GET_CHECK_SUCCESS:
      return {
        ...state,
        orderMessagesGetCheckSuccess: action.payload,
          };
      case ORDERMESSAGES_SET_DATA:
      return {
        ...state,
        orderMessagesData: action.payload,
          };
      case ORDERMESSAGES_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        orderMessagesUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
