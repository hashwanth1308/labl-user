import {
  EARNINGS_POST_CHECK_SUCCESS,
  EARNINGS_SET_COUNT,
  EARNINGS_COUNT_CHECK_SUCCESS,
  EARNINGS_DELETE_CHECK_SUCCESS,
  EARNINGS_FINDONE_CHECK_SUCCESS,
  EARNINGS_SET_ONE_DATA,
  EARNINGS_GET_CHECK_SUCCESS,
  EARNINGS_SET_DATA,
  EARNINGS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  earningsPostCheckSuccess: false,
  earningsCount: 0,
  earningsCountCheckSuccess : false,
  earningsDeleteCheckSuccess: false,
  earningsFindOneCheckSuccess: false,
  earningsOneData: [],
  earningsGetCheckSuccess: false,
  earningsData: [],
  earningsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case EARNINGS_POST_CHECK_SUCCESS:
      return {
        ...state,
        earningsPostCheckSuccess: action.payload,
      };
      case EARNINGS_SET_COUNT:
      return {
        ...state,
        earningsCount: action.payload,
      };
      case EARNINGS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        earningsCountCheckSuccess: action.payload,
      };
      case EARNINGS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        earningsDeleteCheckSuccess: action.payload,
      };
      case EARNINGS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        earningsFindOneCheckSuccess: action.payload,
          };
      case EARNINGS_SET_ONE_DATA:
      return {
        ...state,
        earningsOneData: action.payload,
          };
      case EARNINGS_GET_CHECK_SUCCESS:
      return {
        ...state,
        earningsGetCheckSuccess: action.payload,
          };
      case EARNINGS_SET_DATA:
      return {
        ...state,
        earningsData: action.payload,
          };
      case EARNINGS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        earningsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
