import {StyleSheet,Dimensions} from 'react-native';
import * as Utils from '@utils';
import {BaseColor} from '@config';

const height = Dimensions.get("screen").height;
const width = Dimensions.get("screen").width;
export default StyleSheet.create({
  //block css
  blockImage: {
    height: Utils.scaleWithPixel(115),
    width: 115,
    margin:3
  },
  blockContentAddress: {
    flexDirection: 'row',
    marginTop: 3,
    alignItems: 'center',
  },
  blockContentDetail: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginTop: 10,
  },
  blockListContentIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 50,
    width: '100%',
    marginTop: 4,
  },
  contentService: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  serviceItemBlock: {
    alignItems: 'center',
    justifyContent: 'center',

    width: 60,
  },
  //list css
  listImage: {
    height: Utils.scaleWithPixel(120),
    width: Utils.scaleWithPixel(110),
    borderRadius: 0,
  },
  listContent: {
    flexDirection: 'row',
  },
  listContentRight: {
    paddingHorizontal: 10,
    paddingVertical: 2,
    flex: 1,
  },
  listContentRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  //gird css
  girdImage: {
    borderRadius: 0,
    height: Utils.scaleWithPixel(height/7),
    width: width/3,
    alignSelf:"center"
    // marginTop: 5,
  },
  girdContent: {
    flex: 1,
    marginLeft: 15,
  },
  girdContentLocation: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 5,
  },
  girdContentRate: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
});
