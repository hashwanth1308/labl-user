import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { useNavigation } from '@react-navigation/native';

export default function ProfilePerformanceVendor(props) {
  const navigation = useNavigation();
  const renderValue = (type, value) => {
    switch (type) {
      case 'primary':
        return (
          <Text title3 semibold primaryColor>
            {value}
          </Text>
        );
      case 'small':
        return (
          <Text body1 semibold>
            {value}
          </Text>
        );
      default:
        return (
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22, marginVertical: 10,color:"#000" }} >
            {value}
          </Text>
        );
    }
  };

  const renderTitle = (type, value) => {
    switch (type) {
      case 'primary':
        return (
          <Text body2 grayColor>
            {value}
          </Text>
        );
      case 'small':
        return (
          <Text caption1 grayColor>
            {value}
          </Text>
        );
      default:
        return (
          <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 14,color:"#000" }}>
            {value}
          </Text>
        );
    }
  };

  const {
    style,
    contentLeft,
    contentCenter,
    contentRight,
    data,
    type,
    flexDirection,
  } = props;
  const [loading, setLoading] = useState();

  const success = async () => {
    setLoading(true);
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    navigation.navigate('FollowerScreen')
    setLoading(false);

  };
  // const success1 = async () => {
  //   setLoading(true);
  //   // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
  //   navigation.navigate('FollowerScreen', { profileData })
  //   setLoading(false);

  // };

  switch (flexDirection) {
    case 'row':
      return (
        <View style={[styles.contain, style]}>
          {data.map((item, index) => {
            if (index == 0) {
              return (
                <TouchableOpacity onPress={() => success()}>
                  <View style={[styles.contentLeft]} key={index}>
                    {renderValue(type, item.value)}
                    {renderTitle(type, item.title)}
                  </View>
                </TouchableOpacity>
              );
            } else if (index == data.length - 1) {
              return (
                <TouchableOpacity>
                  <View style={[styles.contentRight]} key={index}>
                    {renderValue(type, item.value)}
                    {renderTitle(type, item.title)}
                  </View>
                </TouchableOpacity>
              );
            }
          })}
        </View>
      );
    case 'column':
      return (
        <View style={[{ flex: 1 }, style]}>
          {data.map((item, index) => (
            <View key={index}>
              {renderTitle(type, item.title)}
              {renderValue(type, item.value)}
            </View>
          ))}
        </View>
      );
  }
}

ProfilePerformanceVendor.propTypes = {
  flexDirection: PropTypes.string,
  type: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  data: PropTypes.array,
  contentLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  contentCenter: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  contentRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

ProfilePerformanceVendor.defaultProps = {
  flexDirection: 'row',
  type: 'medium',
  style: {},
  data: [],
  contentLeft: {},
  contentCenter: {},
  contentRight: {},
};
