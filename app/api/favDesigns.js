import{ 
    
  favDesignsPostCheckSuccess,
  favDesignsSetCount,
  favDesignsCountCheckSuccess,
  favDesignsDeleteCheckSuccess,
  favDesignsFindOneCheckSuccess,
  favDesignsSetOneData,
  favDesignsGetCheckSuccess,
  favDesignsSetData,
  favDesignsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function favDesignsPostRequest(element) {
      return dispatch => {
        dispatch(favDesignsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `fav-designs`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(favDesignsSetData(a));
            dispatch(favDesignsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(favDesignsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function favDesignsCountRequest(element) {
      return dispatch => {
          dispatch(favDesignsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `fav-designs/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(favDesignsSetCount(data));
              dispatch(favDesignsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(favDesignsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function favDesignsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(favDesignsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `fav-designs/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(favDesignsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(favDesignsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function favDesignsFindOneRequest(id) {
      return dispatch => {
        dispatch(favDesignsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `fav-designs/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(favDesignsOneData(data));
            dispatch(favDesignsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(favDesignsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function favDesignsGetRequest(element) {
  return dispatch => {
    dispatch(favDesignsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `fav-designs`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(favDesignsSetOneData(data));
        dispatch(favDesignsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(favDesignsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function favDesignsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(favDesignsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `fav-designs/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(favDesignsSetData(data));
              dispatch(favDesignsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(favDesignsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 