import {
  LABELS_POST_CHECK_SUCCESS,
  LABELS_SET_COUNT,
  LABELS_COUNT_CHECK_SUCCESS,
  LABELS_DELETE_CHECK_SUCCESS,
  LABELS_FINDONE_CHECK_SUCCESS,
  LABELS_SET_ONE_DATA,
  LABELS_GET_CHECK_SUCCESS,
  LABELS_SET_DATA,
  LABELS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  labelsPostCheckSuccess: false,
  labelsCount: 0,
  labelsCountCheckSuccess : false,
  labelsDeleteCheckSuccess: false,
  labelsFindOneCheckSuccess: false,
  labelsOneData: [],
  labelsGetCheckSuccess: false,
  labelsData: [],
  labelsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LABELS_POST_CHECK_SUCCESS:
      return {
        ...state,
        labelsPostCheckSuccess: action.payload,
      };
      case LABELS_SET_COUNT:
      return {
        ...state,
        labelsCount: action.payload,
      };
      case LABELS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        labelsCountCheckSuccess: action.payload,
      };
      case LABELS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        labelsDeleteCheckSuccess: action.payload,
      };
      case LABELS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        labelsFindOneCheckSuccess: action.payload,
          };
      case LABELS_SET_ONE_DATA:
      return {
        ...state,
        labelsOneData: action.payload,
          };
      case LABELS_GET_CHECK_SUCCESS:
      return {
        ...state,
        labelsGetCheckSuccess: action.payload,
          };
      case LABELS_SET_DATA:
      return {
        ...state,
        labelsData: action.payload,
          };
      case LABELS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        labelsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
