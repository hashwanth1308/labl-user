import {
    UPLOADFILES_GET_CHECK_SUCCESS,
    UPLOADFILES_POST_CHECK_SUCCESS,
    UPLOADFILES_UPDATE_CHECK_SUCCESS,
    UPLOADFILES_DELETE_CHECK_SUCCESS,
    UPLOADFILES_SET_DATA,
  } from './actionTypes';
  
  
  
  export function uploadFilesGetCheckSuccess(bool = false) {
    return {
      type: UPLOADFILES_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function uploadFilesPostCheckSuccess(bool = false) {
    return {
      type: UPLOADFILES_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function uploadFilesUpdateCheckSuccess(bool = false) {
    return {
      type: UPLOADFILES_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  export function uploadFilesDeleteCheckSuccess(bool = false) {
    return {
      type: UPLOADFILES_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
  
  
  export function uploadFilesSetData(object) {
    return {
      type: UPLOADFILES_SET_DATA,
      payload: object,
    };
  }
  
  