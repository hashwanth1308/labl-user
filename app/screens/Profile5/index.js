import React, { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  Animated,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import { Images, useTheme } from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  ProfilePerformance,
  HotelItem1,
} from '@components';
import _ from 'lodash';
import * as Utils from '@utils';
import styles from './styles';
import { UserData, HotelData } from '@data';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { modelsFindOneRequest, modelsGetRequest } from '../../api/models';
import { userFollowingGetRequest, userFollowingDeleteRequest, userFollowingPostRequest } from '../../api/following';
import { usersFindOneRequest } from '../../api/users';
import { modelProductsGetRequest } from '../../api';

export default function Profile5({ navigation, route }) {
  const deltaY = new Animated.Value(0);
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [hotels] = useState(HotelData);
  const windowHeight = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const { object } = route.params;
  console.log('object----', object);
  const dispatch = useDispatch();
  // const [userData] = useState(UserData[0]);
  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const heightImageBanner = Utils.scaleWithPixel(335, 1);
  const marginTopBanner = heightImageBanner - heightHeader - 120;
  useEffect(() => {
    dispatch(modelsFindOneRequest(object?.id));
  }, [dispatch]);
  useEffect(() => {
    dispatch(usersFindOneRequest(UserData?.user?.id));
  }, [dispatch]);
  const user = useSelector(state => state.usersReducer.usersOneData);
  console.log("user", user);
  const modelsData = useSelector(state => state.modelsReducer.modelsOneData);
  console.log('models', modelsData);

  useEffect(() => {
    dispatch(userFollowingGetRequest(user?.id))
  }, [dispatch])
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log(UserData);
  const userFollowings = useSelector(state => state.followingReducer.userFollowingData);
  console.log("follow", userFollowings);
  const [follow, setFollow] = useState(false);
  const [followId, setFollowId] = useState("");
  useEffect(() => {
    const followed = _.find(userFollowings, function (o) { return o.model?.id === object?.model?.id; });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true)
    }
  }, []);

  useEffect(() => {
    const followed = _.find(userFollowings, function (o) { return o.model?.id === object?.model?.id; });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true)
    }
  }, [userFollowings]);
  const handleClick = (object) => {
    if (follow) {
      setFollow(false)
      dispatch(userFollowingDeleteRequest(followId));
    }
    else {
      setFollow(true);
      let objectValue = {};
      objectValue.user = UserData?.user?.id,
        objectValue.model = object?.id,
        objectValue.isFollowing = true
      dispatch(userFollowingPostRequest(objectValue));
    }

  }

  useEffect(() => {
    const obj ={};
    obj.model = object?.id
    dispatch(modelProductsGetRequest(obj))
  }, [dispatch])
  const modelProducts = useSelector(state => state.modelProductsReducer.modelProductsData);


  const performance = [
    { value: object?.userfollowing?.length, title: 'Followers' },
    { value: object?.modelfollowings?.length, title: 'Following' },
    { value: modelProducts?.length, title: 'Posts' },
  ];

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => setLoading1(prev => !prev), 1000);
  }, []);


  return (
    <View style={{ flex: 1 }}>
      <Header
        title=""
        renderLeft={() => {
          return (
            <View
              style={{
                height: 30,
                width: 30,
                borderRadius: 30,
                backgroundColor: '#fff',
                alignItems: 'center',
                justifyContent: 'center',
                margin: 0,
              }}>
              <Icon
                name="arrow-left"
                size={20}
                color={colors.primary}
                enableRTL={true}
              />
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />

      <SafeAreaView style={{ flex: 1 }} edges={['right', 'left', 'bottom']}>
        <View style={{ flex: 1 }}>
          <ScrollView
            onScroll={Animated.event([
              {
                nativeEvent: {
                  contentOffset: { y: deltaY },
                },
              },
            ])}
            onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
            scrollEventThrottle={8}>
            <View>
              {/* <Image
                source={Images.profile2}
                style={styles.imgUser}
                resizeMode="cover"
              /> */}
              <View style={styles.contentLeftUser}>
                <Text
                  style={{
                    fontFamily: 'Montserrat-Bold',
                    fontSize: 23,
                    color: '#000',
                    marginVertical: 20,
                  }}>
                  {object?.users?.username}
                </Text>
                <View style={{ alignSelf: "center", marginHorizontal: 30, width: 130, height: 35, backgroundColor: "#000", borderRadius: 15, alignSelf: "center", alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff" }}>Model</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text
                    footnote
                    primaryColor
                    numberOfLines={1}
                    style={{ marginHorizontal: 5 }}>
                    {/* {userData.address} */}
                  </Text>
                </View>
              </View>
              {/* <View style={{ paddingHorizontal: 20, marginVertical: 30 }}>
                <ProfilePerformance data={performance} profileData={object} />
              </View> */}
              <View
                style={{
                  width: '100%',
                  borderWidth: 0.8,
                  borderColor: '#CDCDCD',
                  marginVertical: 0,
                }}></View>
                {modelProducts?.length > 0 ?
              <View>
                <FlatList
                  contentContainerStyle={{
                    padding: 10,

                  }}
                  numColumns={2}
                  data={modelProducts}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({ item, index }) => (
                    <HotelItem1
                      style={{ padding: 5 }}
                      onPress={() => navigation.navigate('PostDetail', { object: item })}
                      image={item?.imagesOfProductByModel[0]?.url}
                    />
                  )}
                />
              </View>:
              <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
                <Text style={{fontFamily:"Montserrat-Bold",fontSize:20,alignSelf:"center",marginVertical:50}}>No Uploads Yet!!!</Text>
                </View>}
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
      {loading1 && (
        <View
          style={{
            width: width,
            height: windowHeight,
            backgroundColor: 'rgba(0,0,0,0.8)',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
          }}>
          <View
            style={{
              width: '60%',
              paddingVertical: 20,
              backgroundColor: '#fff',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <View>
              <ActivityIndicator size={'large'} color="#000" />
            </View>
            <Text
              style={{
                marginLeft: 10,
                fontSize: 30,
                color: '#000',
                fontWeight: 'bold',
              }}>
              Loading..
            </Text>
          </View>
        </View>
      )}
    </View>
  );
}
