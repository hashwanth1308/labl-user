import React from 'react';
import {StyleSheet} from 'react-native';
import * as Utils from '@utils';


export default StyleSheet.create({
  contain: {
    flexDirection: 'row',
    paddingVertical: Utils.scaleWithPixel(10),
  },
  contentLeft: {
    flex: 8,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  thumb: {
    width: Utils.scaleWithPixel(40),
    height: Utils.scaleWithPixel(40),
    borderRadius: Utils.scaleWithPixel(20),
    marginRight: Utils.scaleWithPixel(5),
    borderWidth:Utils.scaleWithPixel(0.5),
    borderColor:"#ffffff"
  },
  contentRight: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
});
