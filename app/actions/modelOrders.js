import {
  MODELORDERS_POST_CHECK_SUCCESS,
  MODELORDERS_SET_COUNT,
  MODELORDERS_COUNT_CHECK_SUCCESS,
  MODELORDERS_DELETE_CHECK_SUCCESS,
  MODELORDERS_FINDONE_CHECK_SUCCESS,
  MODELORDERS_SET_ONE_DATA,
  MODELORDERS_GET_CHECK_SUCCESS,
  MODELORDERITEMS_GET_CHECK_SUCCESS,
  MODELORDERS_SET_DATA,
  MODELORDERITEMS_SET_DATA,
  MODELORDERS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function modelOrdersPostCheckSuccess(bool = false) {
  return {
    type: MODELORDERS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelOrdersSetCount(object) {
  return {
    type: MODELORDERS_SET_COUNT,
    payload: object,
  };
}
export function modelOrdersCountCheckSuccess(bool = false) {
  return {
    type: MODELORDERS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelOrdersDeleteCheckSuccess(bool = false) {
  return {
    type: MODELORDERS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelOrdersFindOneCheckSuccess(bool = false) {
  return {
    type: MODELORDERS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelOrdersSetOneData(object) {
  return {
    type: MODELORDERS_SET_ONE_DATA,
    payload: object,
  };
}
export function modelOrdersGetCheckSuccess(bool = false) {
  return {
    type: MODELORDERS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function modelOrderItemsGetCheckSuccess(bool = false) {
  return {
    type: MODELORDERITEMS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelOrdersSetData(object) {
  return {
    type: MODELORDERS_SET_DATA,
    payload: object,
  };
}
export function modelOrderItemsSetData(object) {
  return {
    type: MODELORDERITEMS_SET_DATA,
    payload: object,
  };
}

export function modelOrdersUpdateCheckSuccess(bool = false) {
  return {
    type: MODELORDERS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

