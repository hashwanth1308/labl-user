import {
  modelsPostCheckSuccess,
  modelsSetCount,
  modelsCountCheckSuccess,
  modelsDeleteCheckSuccess,
  modelsFindOneCheckSuccess,
  modelsSetOneData,
  modelsGetCheckSuccess,
  modelsSetData,
  modelsUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

//create
export function modelsPostRequest(element) {
  return dispatch => {
    dispatch(modelsPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `models`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
      data: {
        users: element.user._id,
      },
    })
      .then(response => {
        console.log('after user created, model cretion', response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }

        return response.data;
      })
      .then(data => {
        dispatch(modelsSetData(a));
        dispatch(modelsPostCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelsPostCheckSuccess(false));
      });
  };
}

//count
export function modelsCountRequest(element) {
  return dispatch => {
    dispatch(modelsCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `models/count`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelsSetCount(data));
        dispatch(modelsCountCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelsCountCheckSuccess(false));
      });
  };
}

//delete
export function modelsDeleteRequest(id) {
  return dispatch => {
    dispatch(modelsDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `models/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelsDeleteCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelsDeleteCheckSuccess(false));
      });
  };
}

//findOne
export function modelsFindOneRequest(id) {
  console.log(id);
  return dispatch => {
    //dispatch(modelsFindOneCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `models/${id}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log("response",response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data);
        dispatch(modelsSetOneData(data));
       // dispatch(modelsFindOneCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
       // dispatch(modelsFindOneCheckSuccess(false));
      });
  };
}

//find - get
export function modelsGetRequest(element) {
  const object = {};
  object.users = element;
  return dispatch => {
    dispatch(modelsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `models`,
      params: object,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log(response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(modelsSetData(data));
        dispatch(modelsGetCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelsGetCheckSuccess(false));
      });
  };
}

//update
export function modelsUpdateRequest(id) {
  return dispatch => {
    dispatch(modelsUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `models/${id}`,
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(modelsSetData(data));
        dispatch(modelsUpdateCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(modelsUpdateCheckSuccess(false));
      });
  };
}
