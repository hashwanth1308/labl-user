import React from 'react';
import { StyleSheet } from 'react-native';
import { BaseColor } from '@config';

export default StyleSheet.create({
  contain: {
    alignItems: 'center',
    paddingHorizontal: 30,
    flex: 1,
    marginTop:50
  },
  textInput: {
    height: 46,
    backgroundColor: BaseColor.fieldColor,
    borderRadius: 5,
    marginTop: 10,
    padding: 10,
    width: '100%',
  },
  img: {
    height: 100,
    marginTop: 30,
     marginBottom: 30,
    width: 150
  },
  contain1: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 40,

    flex: 1,
  },
  Term: {
    fontSize:22,
    fontFamily:"Montserrat-Bold"
  },
  button: {
    height: 45,
    width: 220,
    borderRadius: 12,
    justifyContent: "center"
  },
  text: {
    fontFamily:"Roboto",
    fontSize:18,
    color:"white",
    alignSelf:"center",
    fontWeight:'700'
  }
});
