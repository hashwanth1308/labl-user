import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';

export default StyleSheet.create({
  inputItem: {
    flex: 6.5,
    marginLeft: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 46,
    backgroundColor: BaseColor.fieldColor,
    borderRadius: 5,
    padding: 10,
  },
  checkDefault: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopWidth: 1,
    paddingVertical: 15,
    marginTop: 10,
  },
  location: {
    flexDirection: 'row',
    marginTop: 10,
  },
  contentTag: {
    marginLeft: 20,
    marginTop: 10,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentUser: {
    // paddingHorizontal: 20,
    // flexDirection: 'row',
    marginBottom: 20,
    top: 200,
    borderTopLeftRadius: 70,
    borderTopRightRadius: 70,
    backgroundColor: "#fff"
  },
  imgBanner: {
    // height: 800,
    flex:1,
    width: '100%',
    position: 'relative',
  },
  imgUser: {
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: BaseColor.whiteColor,
  },
  contentLeftUser: {
    // flex: 1,
    justifyContent: 'flex-start',
    alignItems:"center"
    // marginLeft: 15,
  },
});
