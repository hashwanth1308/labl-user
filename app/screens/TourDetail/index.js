import React, {useState} from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from 'react-native';
import {BaseStyle, BaseColor, Images, useTheme} from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Icon,
  ProfileDescription,
  ProfilePerformance,
  Tag,
  Text,
  Card,
  TourDay,
  TourItem,
  Button,
  PackageItem,
  RateDetail,
  CommentItem,
} from '@components';
import {TabView, TabBar} from 'react-native-tab-view';
import styles from './styles';
import {UserData, ReviewData, TourData, PackageData} from '@data';
import {useTranslation} from 'react-i18next';

export default function TourDetail({navigation}) {
  const {colors} = useTheme();
  const {t} = useTranslation();

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    //  {key: 'information', title: t('information')},
    {key: 'tour', title: t('Orders')},
    {key: 'information', title: t('Status ')},
    {key: 'package', title: t('Delivered')},
    {key: 'review', title: t('Returned')},
  ]);
  const [userData] = useState(UserData[0]);

  // When tab is activated, set what's index value
  const handleIndexChange = index => setIndex(index);

  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, {backgroundColor: colors.primary}]}
      style={[styles.tabbar, {backgroundColor: colors.background}]}
      tabStyle={styles.tab}
      inactiveColor={BaseColor.grayColor}
      activeColor={colors.text}
      renderLabel={({route, focused, color}) => (
        <View style={{flex: 1, width: 130, alignItems: 'center'}}>
          <Text headline semibold={focused} style={{color}}>
            {route.title}
          </Text>
        </View>
      )}
    />
  );

  // Render correct screen container when tab is activated
  const renderScene = ({route, jumpTo}) => {
    switch (route.key) {
      case 'information':
        return <InformationTab jumpTo={jumpTo} navigation={navigation} />;
      case 'tour':
        return <TourTab jumpTo={jumpTo} navigation={navigation} />;
      case 'package':
        return <PackageTab jumpTo={jumpTo} navigation={navigation} />;
      case 'review':
        return <ReviewTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };

  return (
    <View style={{flex: 1}}>
      <Header
        title={t('Orders Status')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        {/* <ProfileDescription
          image={userData.image}
          name={userData.name}
          subName={userData.major}
          description={userData.address}
          style={{marginTop: -60, paddingHorizontal: 20}}
        /> */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 20,
          }}>
          {/* <Tag primary style={{width: 80}}>
            + {t('follow')}
          </Tag> */}
          {/* <View style={{flex: 1, paddingLeft: 10, paddingVertical: 5}}>
            <ProfilePerformance data={userData.performance} type="small" />
          </View> */}
        </View>
        <View style={{flex: 1}}>
          <TabView
            lazy
            navigationState={{index, routes}}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            onIndexChange={handleIndexChange}
          />
          <View
            style={[
              styles.contentButtonBottom,
              {borderTopColor: colors.border},
            ]}>
            <View>
              {/* <Text caption1 semibold>
                Price {t('Details')}
              </Text> */}
              {/* <Text title3 primaryColor semibold>
                $2,199.00
              </Text> */}
              {/* <Text caption1 semibold style={{marginTop: 5}}>
                3 {t('participants')}
              </Text> */}
            </View>
            {/* <Button onPress={() => navigation.navigate('PreviewBooking')}>
              {t('Order Now')}
            </Button> */}
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
}

/**
 * @description Show when tab Information activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function InformationTab({navigation}) {
  const [tours] = useState(TourData);
  const [dayTour] = useState([
    // {
    //   id: '1',
    //   image: Images.trip1,
    //   day: 'Order 1',
    //   title: 'Rain & Rainbow',
    //   description:
    //   'Blue and orange printed A-line tunic, has a round neck, three-quarter sleeves, gathers detail, button closure'

    // },
    // {
    //   id: '2',
    //   image: Images.trip2,
    //   day: 'Order 2',
    //   title: 'Biba',
    //   description:
    //     'Pink straight kurti Ethnic motifs embroidered Round neck, three-quarter, puff sleeves Mirror work detail Machine weave regular cotton viscose blend',
      
    // },
    // {
    //   id: '3',
    //   image: Images.trip3,
    //   day: 'Order 3',
    //   title: 'Vishudh',
    //   description:
    //     'Black and Off-white striped Tunic, has a round neck, and three-quarter sleeves',
    // },
    // {
    //   id: '4',
    //   image: Images.trip4,
    //   day: 'Order 4',
    //   title: 'Vishudh',
    //   description:
    //     'Yellow embroidered woven A-line top, has a tie-up neck, and three-quarter sleeves',
    // },
  ]);
  const [information] = useState([
    {title: 'Order1', detail: ''},
    {title: 'Location', detail: 'Hyderabad'},
    {title: 'Expected Delivery', detail: '2 Days'},
    {title: 'Departure', detail: '08:00'},
    {title: 'Order2', detail: ''},
    {title: 'Location', detail: 'Secunderabad'},
    {title: 'Expected Delivery', detail: '4 Days'},
    {title: 'Departure', detail: '06:00'},
    
    // {title: 'Price ', detail: '2,199.00 USD'},
    // {title: 'size ', detail: 'M'},

    // {title: 'Group size', detail: '3 - 20 people'},
    // {title: 'Transportation', detail: 'Boat, Bicycle, Car'},
  ]);
  
  const {colors} = useTheme();
  return (
    <ScrollView>
      
      <View style={{paddingHorizontal: 20}}>
        {information.map((item, index) => {
          return (
            
            <View
              style={[
                styles.lineInformation,
                {borderBottomColor: colors.border},
              ]}
              key={'information' + index}>
              <Text body2 grayColor>
                {item.title}
              </Text>
              <Text body2 semibold accentColor>
                {item.detail}
              </Text>
            </View>
          );
        })}
        
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 20,
          }}>

          
          <TouchableOpacity>
            {/* <Text headline semibold>
             Reviews
            </Text> */}
          </TouchableOpacity>
        </View>
        <View style={styles.contentImageGird}>
          {/* <View style={{flex: 4, marginRight: 10}}>
            <Card image={Images.trip7}>
              <Text headline semibold whiteColor>
                Super
              </Text>
            </Card>
          </View> */}
          <View style={{flex: 6}}>
            {/* <View style={{flex: 1}}>
              <Card image={Images.trip3}>
                <Text headline semibold whiteColor>
                  Excellent
                </Text>
              </Card>
            </View> */}
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginTop: 10,
              }}>
              {/* <View style={{flex: 6, marginRight: 10}}>
                <Card image={Images.trip4}>
                  <Text headline semibold whiteColor>
                    Nice
                  </Text>
                </Card>
              </View> */}
              <View style={{flex: 4}}>
                {/* <Card image={Images.trip6}>
                  <Text headline semibold whiteColor>
                    10+
                  </Text>
                </Card> */}
              </View>
            </View>
          </View>
        </View>
      </View>
      <View>
        <Text
          headline
          semibold
          style={{
            marginHorizontal: 20,
            marginTop: 20,
            marginBottom: 10,
          }}>
            
          
        </Text>
        <FlatList
          contentContainerStyle={{paddingLeft: 5, paddingRight: 20,}}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={dayTour}
          keyExtractor={(item, index) => item.id}
          renderItem={({item}) => (
            <TourDay
              image={item.image}
              day={item.day}
              title={item.title}
              description={item.description}
              style={{marginLeft: 15}}
              onPress={() => {}}
            />
          )}
        />
      </View>
      <View style={{paddingHorizontal: 20, marginTop: 20}}>
        {/* <Text headline semibold style={{marginBottom: 10}}>
          Includes
        </Text> */}
        {/* <Text body2>
          - Donec sollicitudin molestie malesuada. Quisque velit nisi, pretium
          ut lacinia in, elementum id enim.
        </Text> */}
        {/* <Text body2 style={{marginTop: 5}}>
          - Other hygienic practices that the new hotel — which handles, among
          other guests, patients seeking medical treatment at the Texas Medical
          Center — include removing nonessential items like decorative pillows
          and magazines
        </Text> */}
        {/* <Text body2 style={{marginTop: 5}}>
          - Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Donec
          rutrum congue leo eget malesuada.
        </Text> */}
      </View>
      <View style={{paddingHorizontal: 20, marginTop: 20}}>
        {/* <Text headline semibold style={{marginBottom: 10}}>
          Excludes
        </Text> */}
        {/* <Text body2>
          - Donec sollicitudin molestie malesuada. Quisque velit nisi, pretium
          ut lacinia in, elementum id enim.
        </Text> */}
        {/* <Text body2 style={{marginTop: 5}}>
          - Other hygienic practices that the new hotel — which handles, among
          other guests, patients seeking medical treatment at the Texas Medical
          Center — include removing nonessential items like decorative pillows
          and magazines
        </Text> */}
        {/* <Text body2 style={{marginTop: 5}}>
          - Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Donec
          rutrum congue leo eget malesuada.
        </Text> */}
      </View>
      <View>
        {/* <Text
          headline
          semibold
          style={{
            marginLeft: 20,
            marginTop: 20,
          }}>
          Openning Tours
        </Text> */}
        {/* <Text body2 style={{marginBottom: 10, marginLeft: 20}}>
          Let find out what most interesting things
        </Text> */}
        <FlatList
          // contentContainerStyle={{paddingLeft: 5, paddingRight: 20}}
          // horizontal={true}
          // showsHorizontalScrollIndicator={false}
          // data={tours}
          // keyExtractor={(item, index) => item.id}
          renderItem={({item, index}) => (
            <TourItem
              grid
              style={[styles.tourItem, {marginLeft: 15}]}
              onPress={() => {
                navigation.navigate('TourDetail');
              }}
              // image={item.image}
              // name={item.name}
              // location={item.location}
              // travelTime={item.location}
              // startTime={item.startTime}
              // price={item.price}
              rate={item.rate}
              rateCount={item.rateCount}
              numReviews={item.numReviews}
              author={item.author}
              services={item.services}
            />
          )}
        />
      </View>
    </ScrollView>
  );
}

/**
 * @description Show when tab Tour activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function TourTab({navigation}) {
  return (
    <ScrollView>
      <View style={{paddingHorizontal: 20, marginTop: 20}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          {/* <Text headline semibold>
            Gallery
          </Text> */}
          {/* <Text footnote grayColor>
            Show more
          </Text> */}
        </View>
        <View style={styles.contentImageGird}>
          <View style={{flex: 4, marginRight: 10}}>
            {/* <Card image={Images.trip7}>
              { <Text headline semibold whiteColor>
                Dallas
              </Text> }
            </Card> */}
          </View>
          <View style={{flex: 6}}>
            <View style={{flex: 1}}>
              {/* <Card image={Images.trip3}>
                <Text headline semibold whiteColor>
                  Warsaw
                </Text>
              </Card> */}
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginTop: 10,
              }}>
              <View style={{flex: 6, marginRight: 10}}>
                {/* <Card image={Images.trip4}>
                  <Text headline semibold whiteColor>
                    Yokohama
                  </Text>
                </Card> */}
              </View>
              <View style={{flex: 4}}>
                {/* <Card image={Images.trip6}>
                  <Text headline semibold whiteColor>
                    10+
                  </Text>
                </Card> */}
              </View>
            </View>
          </View>
        </View>
        <Text headline semibold style={{marginTop: -210}}>
         order 1: Fabindia
        </Text>
        <Image
          source={Images.room14}
          style={{height:300, width: '100%', marginTop: 10}}
        />
        <Text body2 semibold style={{marginTop: 10}}>
          Product Details
        </Text>
        <Text body2 style={{marginTop: 10}}>
        Blue top
        </Text>
        <Text body2 style={{marginTop: 10}}>
        Vertical striped
        
        </Text>
        <Text body2 semibold style={{marginTop: 10}}>
        Price Details        
        </Text>
        <Text headline  style={{marginTop: 10}}>
          Total MRP :$3998
        </Text>
        <Text headline  style={{marginTop: 10}}>
          Discount On MRP :-$2000
         </Text>
         <Text headline  style={{marginTop: 10}}>
          Total MRP :-$1998
         </Text>
        
        

        <Text headline semibold style={{marginTop: 20}}>
          Order 2: INDYA
        </Text>
        <Image
          source={Images.room15}
          style={{height: 300, width: '100%', marginTop: 10}}
        />
        <Text body2 semibold style={{marginTop: 10}}>
          Product Details
        </Text>
        <Text body2 style={{marginTop: 10}}>
        Gold-Toned solid woven regular top, has a round neck, sleeveless, and zip closure
</Text>
        <Text body2 style={{marginTop: 10}}>
        Sleeveless
        </Text>
        <Text body2 semibold style={{marginTop: 10}}>
        Price Details        
        </Text>
        <Text headline  style={{marginTop: 10}}>
          Total MRP :$3998
        </Text>
        <Text headline  style={{marginTop: 10}}>
          Discount On MRP :-$2000
         </Text>
         <Text headline  style={{marginTop: 10}}>
          Total MRP :-$1998
         </Text>
        <Text headline semibold style={{marginTop: 20}}>
          order 3: Tissu
        </Text>
        <Image
          source={Images.room16}
          style={{height: 300, width: '100%', marginTop: 10}}
        />
        <Text body2 style={{marginTop: 10}}>
        Navy blue and pink floral print A-line kurti, has a round neck, front placket with buttons, side slits, three-quarter sleeves and straight hem
        </Text>
        <Text body2 style={{marginTop: 10}}>
        Regular Sleeves
        </Text>
        <Text body2 style={{marginTop: 10}}>
        Round Neck
        </Text>
        <Text body2 semibold style={{marginTop: 10}}>
        Price Details        
        </Text>
        <Text headline  style={{marginTop: 10}}>
          Total MRP :$3998
        </Text>
        <Text headline  style={{marginTop: 10}}>
          Discount On MRP :-$2000
         </Text>
         <Text headline  style={{marginTop: 10}}>
          Total MRP :-$1998
         </Text>
      </View>
    </ScrollView>
  );
}

/**
 * @description Show when tab Package activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function PackageTab({navigation}) {
  const [packageItem] = useState(PackageData[0]);
  const [packageItem2] = useState(PackageData[2]);

  return (
    <ScrollView>
      <View style = {{paddingHorizontal:200,paddingVertical:20,paddingBottom:15,paddingLeft:10,height:250}}>
      <Card image={Images.trip7}>
  { <Text headline semibold whiteColor>
  
  </Text> }
</Card>
</View>
      <View style={{paddingHorizontal: 20}}>
      <Text body2 semibold style={{marginTop: 20}}>
        product Details
        </Text>
        <Text body2 semibold style={{marginTop: 20}}>
        Women Camel Brown Solid Puffer Jacket        </Text>
        <Text body2 style={{marginTop: 20}}>
          Price Details
        </Text>
        <Text body2 style={{marginTop: 20}}>
          Total MRP : $3998
        </Text>
        <Text body2 style={{marginTop: 20}}>
          Discount On MRP : -$2999
        </Text>
        <Text body2 style={{marginTop: 20}}>
         Coupon Applied : ABLABL20
        </Text>
        <Text body2 style={{marginTop: 20}}>
         Total Amount : $1998
        </Text>
        <View style = {{paddingHorizontal:200,paddingVertical:20,paddingBottom:15,paddingLeft:10,height:250}}>
      <Card image={Images.trip7}>
  { <Text headline semibold whiteColor>
  
  </Text> }
</Card>
</View>
<Text body2 style={{marginTop: 20}}>
          Price Details
        </Text>
        <Text body2 semibold style={{marginTop: 20}}>
        product Details
        </Text>
        <Text body2 semibold style={{marginTop: 20}}>
        Women Camel Brown Solid Puffer Jacket        </Text>
        <Text body2 style={{marginTop: 20}}>
          Total MRP : $3998
        </Text>
        <Text body2 style={{marginTop: 20}}>
          Discount On MRP : -$2999
        </Text>
        <Text body2 style={{marginTop: 20}}>
         Coupon Applied : ABLABL20
        </Text>
        <Text body2 style={{marginTop: 20}}>
         Total Amount : $1998
        </Text>
        <PackageItem
          // packageName={packageItem.packageName}
          // price={packageItem.price}
          // type={packageItem.type}
          // description={packageItem.description}
          // services={packageItem.services}
          // onPressIcon={() => {
          //   navigation.navigate('PricingTable');
          // }}
          onPress={() => {
            navigation.navigate('PreviewBooking');
          }}
          // style={{marginBottom: 10, marginTop: 20}}
        />
        <PackageItem
          // detail
          // packageName={packageItem2.packageName}
          // price={packageItem2.price}
          // type={packageItem2.type}
          // description={packageItem2.description}
          // services={packageItem2.services}
        />
      </View>
    </ScrollView>
  );
}

/**
 * @description Show when tab Review activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function ReviewTab({navigation}) {
  const [refreshing] = useState(false);
  // const [rateDetail] = useState({
  //   // point: 4.7,
  //   // maxPoint: 5,
  //   // totalRating: 25,
  //   data: ['80%', '10%', '10%', '0%', '0%'],
  // });
  const [reviewList] = useState(ReviewData);
  const {colors} = useTheme();

  return (
    <FlatList
      contentContainerStyle={{padding: 20}}
      refreshControl={
        <RefreshControl
          colors={[colors.primary]}
          tintColor={colors.primary}
          refreshing={refreshing}
          onRefresh={() => {}}
        />
      }
      data={reviewList}
      keyExtractor={(item, index) => item.id}
      ListHeaderComponent={() => (
        <RateDetail
          // point={rateDetail.point}
          // maxPoint={rateDetail.maxPoint}
          // totalRating={rateDetail.totalRating}
          // data={rateDetail.data}
        />
      )}
      renderItem={({item}) => (
        <CommentItem
          style={{marginTop: 10}}
          image={item.source}
          name={item.name}
          rate={item.rate}
          date={item.date}
          title={item.title}
          comment={item.comment}
        />
      )}
    />
  );
}
