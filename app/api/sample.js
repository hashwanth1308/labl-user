import{ 
    
    furnishingsPostCheckSuccess,
    furnishingsSetCount,
    furnishingsCountCheckSuccess,
    furnishingsDeleteCheckSuccess,
    furnishingsFindOneCheckSuccess,
    furnishingsSetOneData,
    furnishingsGetCheckSuccess,
    furnishingsSetData,
    furnishingsUpdateCheckSuccess,
    } from '../actions';
    import axios from 'axios';
    let a = [];

    //create
    export function furnishingsPostRequest(element) {
        return dispatch => {
          dispatch(furnishingsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `furnishings`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              dispatch(furnishingsSetData(a));
              dispatch(furnishingsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(furnishingsPostCheckSuccess(false));
             
            });
        };
    }

     //count
     export function furnishingsCountRequest(element) {
        return dispatch => {
            dispatch(furnishingsCountCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'get',
                url: `furnishings/count`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                dispatch(furnishingsSetCount(data));
                dispatch(furnishingsCountCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(furnishingsCountCheckSuccess(false));
                
            });
        };
    }

    //delete
    export function furnishingsDeleteRequest(id) {
        
        return dispatch => {
            dispatch(furnishingsDeleteCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'delete',
                url: `furnishings/${id}`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                dispatch(furnishingsDeleteCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(furnishingsDeleteCheckSuccess(false));
                
            });
        };
    }

    //findOne
    export function furnishingsFindOneRequest(id) {
        return dispatch => {
          dispatch(furnishingsFindOneCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'get',
              url: `furnishings/${id}`,
              headers:{
                //  Authorization: `Bearer ${token}`,
                "Content-Type" : "application/json"
              }
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              dispatch(furnishingsOneData(data));
              dispatch(furnishingsFindOneCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin,)
              dispatch(furnishingsFindOneCheckSuccess(false));
            });
        };
    };

    //find - get
    export function furnishingsGetRequest(element) {
    return dispatch => {
      dispatch(furnishingsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `furnishings`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json",
            data:element
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"user data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(furnishingsSetOneData(data));
          dispatch(furnishingsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin)
          dispatch(furnishingsGetCheckSuccess(false));
        });
    };
    };
    
    //update
    export function furnishingsUpdateRequest(id) {
       
        return dispatch => {
            dispatch(furnishingsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `furnishings/${id}`,
                headers:{
                "Content-Type" : "application/json"}
                
                })
            .then(response => {
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(furnishingsSetData(data));
                dispatch(furnishingsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(furnishingsUpdateCheckSuccess(false));
                
            });
        };
    }
    
   

   