import {

  ordersPostCheckSuccess,
  ordersSetCount,
  ordersCountCheckSuccess,
  ordersDeleteCheckSuccess,
  ordersFindOneCheckSuccess,
  ordersSetOneData,
  ordersGetCheckSuccess,
  ordersSetData,
  ordersUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
import { object } from 'prop-types';
let a = [];

//create
export function ordersPostRequest(element) {
  console.log("element", element);
  return dispatch => {
    dispatch(ordersPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `http://3.111.78.117:5377/orders`,
      data: element,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("orders post", data);
        dispatch(ordersSetData(data));
        dispatch(ordersPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(ordersPostCheckSuccess(false));

      });
  };
}

//count
export function ordersCountRequest(element) {
  return dispatch => {
    dispatch(ordersCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `orders/count`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(ordersSetCount(data));
        dispatch(ordersCountCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(ordersCountCheckSuccess(false));

      });
  };
}

//delete
export function ordersDeleteRequest(id) {

  return dispatch => {
    dispatch(ordersDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `orders/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },

    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(ordersDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(ordersDeleteCheckSuccess(false));

      });
  };
}

//findOne
export function ordersFindOneRequest(id) {
  return dispatch => {
    dispatch(ordersFindOneCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `orders/${id}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(ordersOneData(data));
        dispatch(ordersFindOneCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin,)
        dispatch(ordersFindOneCheckSuccess(false));
      });
  };
};

//find - get
export function ordersGetRequest(obj) {
  // const obejct = {};
  // obejct.user = element
  return dispatch => {
    dispatch(ordersGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `orderitems`,
      params: obj,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",

      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(ordersSetData(data));
        dispatch(ordersGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(ordersGetCheckSuccess(false));
      });
  };
};

//update
export function ordersUpdateRequest(element, id) {

  return dispatch => {
    dispatch(ordersUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      data: element,
      url: `orders/${id}`,
      headers: {
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(ordersSetData(data));
        dispatch(ordersUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(ordersUpdateCheckSuccess(false));

      });
  };
}


//update
export function ordersItemsUpdateRequest(element, id) {

  return dispatch => {
    dispatch(ordersUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      data: element,
      url: `orderitems/${id}`,
      headers: {
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(ordersSetData(data));
        dispatch(ordersUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(ordersUpdateCheckSuccess(false));

      });
  };
}


