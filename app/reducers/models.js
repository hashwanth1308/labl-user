import {
  MODELS_POST_CHECK_SUCCESS,
  MODELS_SET_COUNT,
  MODELS_COUNT_CHECK_SUCCESS,
  MODELS_DELETE_CHECK_SUCCESS,
  MODELS_FINDONE_CHECK_SUCCESS,
  MODELS_SET_ONE_DATA,
  MODELS_GET_CHECK_SUCCESS,
  MODELS_SET_DATA,
  MODELS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  modelsPostCheckSuccess: false,
  modelsCount: 0,
  modelsCountCheckSuccess : false,
  modelsDeleteCheckSuccess: false,
  modelsFindOneCheckSuccess: false,
  modelsOneData: [],
  modelsGetCheckSuccess: false,
  modelsData: [],
  modelsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MODELS_POST_CHECK_SUCCESS:
      return {
        ...state,
        modelsPostCheckSuccess: action.payload,
      };
      case MODELS_SET_COUNT:
      return {
        ...state,
        modelsCount: action.payload,
      };
      case MODELS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        modelsCountCheckSuccess: action.payload,
      };
      case MODELS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        modelsDeleteCheckSuccess: action.payload,
      };
      case MODELS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        modelsFindOneCheckSuccess: action.payload,
          };
      case MODELS_SET_ONE_DATA:
      return {
        ...state,
        modelsOneData: action.payload,
          };
      case MODELS_GET_CHECK_SUCCESS:
      return {
        ...state,
        modelsGetCheckSuccess: action.payload,
          };
      case MODELS_SET_DATA:
      return {
        ...state,
        modelsData: action.payload,
          };
      case MODELS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        modelsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
