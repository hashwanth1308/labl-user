import {
  CARTS_POST_CHECK_SUCCESS,
  CARTS_SET_COUNT,
  CARTS_COUNT_CHECK_SUCCESS,
  CARTS_DELETE_CHECK_SUCCESS,
  CARTS_FINDONE_CHECK_SUCCESS,
  CARTS_SET_ONE_DATA,
  CARTS_GET_CHECK_SUCCESS,
  CARTS_SET_DATA,
  CARTS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function cartsPostCheckSuccess(bool = false) {
  return {
    type: CARTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function cartsSetCount(object) {
  return {
    type: CARTS_SET_COUNT,
    payload: object,
  };
}
export function cartsCountCheckSuccess(bool = false) {
  return {
    type: CARTS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function cartsDeleteCheckSuccess(bool = false) {
  return {
    type: CARTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function cartsFindOneCheckSuccess(bool = false) {
  return {
    type: CARTS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function cartsSetOneData(object) {
  return {
    type: CARTS_SET_ONE_DATA,
    payload: object,
  };
}
export function cartsGetCheckSuccess(bool = false) {
  return {
    type: CARTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function cartsSetData(object) {
  return {
    type: CARTS_SET_DATA,
    payload: object,
  };
}

export function cartsUpdateCheckSuccess(bool = false) {
  return {
    type: CARTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

