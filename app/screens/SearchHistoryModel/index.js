import React, { useEffect, useState } from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  TouchableHighlight,
} from 'react-native';
import { BaseStyle, BaseColor, Images, useTheme } from '@config';
import { Header, SafeAreaView, TextInput, Icon, Text, Card } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { productsGetRequest } from '../../api/products';
import { Searchbar } from 'react-native-paper';
import { categoriesGetRequest } from '../../api';
import Voice from '@react-native-community/voice';
import { Image } from 'react-native';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';


export default function SearchHistoryModel({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(false);
  const [searchHistory, setSearchHistory] = useState([
    { id: '1', keyword: 'T-shirts' },
    { id: '2', keyword: 'Hoodie' },
    { id: '3', keyword: 'Shirt' },
    { id: '4', keyword: 'Sweat shirt' },
    { id: '5', keyword: 'trousers' },
  ]);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(productsGetRequest());
  }, [dispatch]);

  const productsData = useSelector(
    state => state.productsReducer.productsData,
  );
  console.log("productsData", productsData);
  useEffect(() => {
    dispatch(categoriesGetRequest());
  }, [dispatch]);

  const categoriesData = useSelector(
    state => state.categoriesReducer.categoriesData,
  );
  const subCategoriesData = useSelector(
    state => state.categoriesReducer.parentCategoriesData,
  );

  console.log("subCategoriesData", subCategoriesData);

  const parCat = [];
  categoriesData?.forEach(item => {
    item?.parentCategories?.forEach(i => {
      parCat.push(i)
    })
  });
  console.log();

  const subCat = [];
  categoriesData?.forEach(item => {
    item?.parentCategories?.forEach(i => {
      i?.subCategories?.forEach(element => {
        subCat.push(element)
      })
    })
  });

  // const prod = [];
  // productsData?.forEach(item => {
  //   prod.push(item)
  // });
  // console.log("prod", prod);

  const [data, setData] = useState([]);
  const [data1, setData1] = useState([]);
  const [data2, setData2] = useState([]);
  console.log("data", data);

  useEffect(() => {
    if (search === 'clear') {
      setSearch('')
    }
    else if (parCat) {
      const data = parCat;
      const filteredData = data.filter(element => {
        const searchtext = search.toLowerCase();
        const name = element?.name?.toLowerCase().replace(/ /g, '')
        if (name?.includes(searchtext)) {
          return true;
        } else return false;
      });
      setData(filteredData);
    }
  }, [search]);
  useEffect(() => {
    if (search === 'clear') {
      setSearch('')
    }
    else if (subCat) {
      const data = subCat;
      const filteredData = data.filter(element => {
        const searchtext = search.toLowerCase();
        const name = element?.subCategoryName?.toLowerCase().replace(/ /g, '')
        if (name?.includes(searchtext)) {
          return true;
        } else return false;
      });
      setData1(filteredData);
    }
  }, [search]);
  useEffect(() => {
    if (search === 'clear') {
      setSearch('')
    }
    else if (productsData) {
      const data = productsData;
      const filteredData = data.filter(element => {
        const searchtext = search.toLowerCase();
        const name = element?.productName?.toLowerCase().replace(/ /g, '')
        if (name?.includes(searchtext)) {
          return true;
        } else return false;
      });
      setData2(filteredData);
    }
  }, [search]);

  /**
   * call when search data
   * @param {*} keyword
   */
  const onSearch = keyword => {
    const found = searchHistory.some(item => item.keyword == keyword);
    let searchData = [];

    if (found) {
      searchData = searchHistory.map(item => {
        return {
          ...item,
          checked: item.keyword == keyword,
        };
      });
    } else {
      searchData = searchHistory.concat({
        keyword: search,
      });
    }
    setSearchHistory(searchData);
    setLoading(true);
    setTimeout(() => navigation.goBack(), 1000);
  };


  const [result, setResult] = useState('')


  useEffect(() => {
    Voice.onSpeechStart = onSpeechStartHandler;
    Voice.onSpeechEnd = onSpeechEndHandler;
    Voice.onSpeechResults = onSpeechResultsHandler;

    return () => {
      Voice.destroy().then(Voice.removeAllListeners);
    }
  }, [])

  const onSpeechStartHandler = (e) => {
    console.log("start handler==>>>", e)
  }
  const onSpeechEndHandler = (e) => {
    setLoading(false)
    console.log("stop handler", e)
  }

  const onSpeechResultsHandler = (e) => {
    let text = e.value[0]
    setResult(text)
    setSearch(text)
    console.log("speech result handler", e)
  }

  const _startRecognizing = async () => {
    setLoading(true)
    try {
      await Voice.start('en-Us');
    } catch (e) {
      console.log(e);
    }
  };

  const _stopRecognizing = async () => {
    //Stops listening for speech
    try {
      await Voice.stop();
    } catch (e) {
      console.log(e);
    }
  };


  return (
    <View style={{ flex: 1 }}>
      {/* <Header
        title={t('search')}
        style={{ height: 80, backgroundColor: '#000' }}
        renderLeft={() => {
          return (
            <View style={{ flex: 1, justifyContent: "space-around" }}>
              <View style={{ flexDirection: "row", width: 220, justifyContent: "space-between", alignSelf: "flex-start" }}>
                <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                  <Icon
                    name="arrow-left"
                    size={20}
                    color={colors.primary}
                    enableRTL={true}
                  />
                </View>
                <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", width: "100%", marginHorizontal: 20 }}>Search</Text>
              </View>
            </View>
          )
        }}
        renderRight={() => {
          if (loading) {
            return <ActivityIndicator size="small" color={colors.primary} />;
          }
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ padding: 20 }}>
            {/* <TextInput
              onChangeText={text => setSearch(text)}
              placeholder={t('Search here')}
              value={search}
              onSubmitEditing={() => {
                onSearch(search);
              }}
              style={{ backgroundColor: "#CDCDCD" }}
              icon={
                <TouchableOpacity
                  onPress={() => {
                    setSearch('');
                  }}
                  style={styles.btnClearSearch}>
                  <Icon name="times" size={18} color={BaseColor.grayColor} />
                </TouchableOpacity>
              }
            /> */}
            {/* <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}> */}
            {/* <View style={{ flex: 1, borderWidth: 1, borderColor: "#000", marginHorizontal: 10 }}>
              <Searchbar
                placeholder="Search"
                onChangeText={text => setSearch(text)}
                value={search}
                onIconPress={() => setSearchHistory([])}
              />
            </View> */}
            <View style={{ flex: 1, flexDirection: "row", width: "100%" }}>
              <TouchableOpacity style={{ alignSelf: "center" }} onPress={() => navigation.goBack()}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}

                />
              </TouchableOpacity>
              <View style={styles.textInputStyle}>
                <TextInput
                  value={search}
                  style={{ flex: 1 }}
                  placeholder="Search"
                  onChangeText={text => setSearch(text)}
                />
                {!loading ? <TouchableHighlight
                  onPress={() => _startRecognizing()}
                  style={{ marginVertical: 20 }}>
                  <Image
                    style={{ height: 30, width: 30 }}
                    source={{
                      uri:
                        'https://raw.githubusercontent.com/AboutReact/sampleresource/master/microphone.png',
                    }}
                  />
                </TouchableHighlight>
                  : <ActivityIndicator color="red" size="large" />
                }
              </View>
            </View>
            {/* <TouchableOpacity
              style={{
                alignSelf: 'center',
                marginTop: 24,
                backgroundColor: 'red',
                padding: 8,
                borderRadius: 4
              }}
              onPress={() => _stopRecognizing()}
            >
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Stop</Text>
            </TouchableOpacity> */}

            <View style={{ paddingTop: 20 }}>
              {search?.length > 0 || data?.length > 0 || data1?.length > 0 || data2?.length > 0 ?
                <View
                  style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                  }}>
                  {data?.length > 0 ? data.map((item, index) => (
                    // console.log("item", item),
                    <>
                      <TouchableOpacity
                        style={[
                          styles.itemHistory,
                          // { backgroundColor: "#CDCDCD" },
                          item.checked
                            ? {
                              backgroundColor: colors.primary,
                            }
                            : {},
                        ]}
                        onPress={() =>
                          navigation.navigate('CollectionsScreenModelSearch', {
                            prodObj: item,
                          })
                        }
                        key={'search' + index}>
                        <Text
                          style={{ fontFamily: "Montserrat-Bold", fontSize: 16 }}>
                          {item?.name}
                        </Text>
                      </TouchableOpacity>
                      <View style={{ borderWidth: 0.5, borderColor: "#000", width: "100%" }}></View>
                    </>
                  )) : null}
                  {data1?.length > 0 ? data1.map((item, index) => (
                    // console.log("item", item),
                    <>
                      <TouchableOpacity
                        style={[
                          styles.itemHistory,
                          // { backgroundColor: "#CDCDCD" },
                          item.checked
                            ? {
                              backgroundColor: colors.primary,
                            }
                            : {},
                        ]}
                        onPress={() => navigation.navigate("CollectionlistModelSearch", { productObject: item })}
                        key={'search' + index}>
                        <Text
                          style={{ fontFamily: "Montserrat-Bold", fontSize: 16 }}>
                          {item?.subCategoryName}
                        </Text>
                      </TouchableOpacity>
                      <View style={{ borderWidth: 0.5, borderColor: "#000", width: "100%", }}></View>
                    </>
                  )) : null}
                  {data2?.length > 0 ? data2.map((item, index) => (
                    // console.log("item", item),
                    <>
                      <TouchableOpacity
                        style={[
                          styles.itemHistory,
                          // { backgroundColor: "#CDCDCD" },
                          item.checked
                            ? {
                              backgroundColor: colors.primary,
                            }
                            : {},
                        ]}
                        onPress={() => navigation.navigate("PostDetailModelLABL", { object: item })}
                        key={'search' + index}>
                        <Text
                          style={{ fontFamily: "Montserrat-Bold", fontSize: 16 }}>
                          {item?.productName}
                        </Text>
                      </TouchableOpacity>
                      <View style={{ borderWidth: 0.5, borderColor: "#000", width: "100%", marginVertical: 5 }}></View>
                    </>
                  )) : null}
                </View> :
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center", alignSelf: "center" }}>
                  <Icon1 name="alert" size={40} color="#CDCDCD" />
                  <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#CDCDCD" }}>No Results Found</Text>
                </View>}
            </View>
            {/* <View style={{ paddingTop: 20 }}>
              <View style={styles.rowTitle}>
                <Text headline>{t('discover_more').toUpperCase()}</Text>
                <TouchableOpacity>
                  <Text caption1 accentColor>
                    {t('refresh')}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                {discoverMore.map((item, index) => (
                  <TouchableOpacity
                    style={[styles.itemHistory, { backgroundColor: colors.card }]}
                    key={'discover' + index}>
                    <Text caption2>{item.keyword}</Text>
                  </TouchableOpacity>
                ))}
              </View>
            </View> */}

          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
}
