import {

  addressPostCheckSuccess,
  addressSetCount,
  addressCountCheckSuccess,
  addressDeleteCheckSuccess,
  addressFindOneCheckSuccess,
  addressSetOneData,
  addressGetCheckSuccess,
  addressSetData,
  addressUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

//create
export function addressPostRequest(element) {
  console.log(element, "element")
  return dispatch => {
    dispatch(addressPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `addresses`,
      data: element,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(addressSetData(data));
        dispatch(addressPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(addressPostCheckSuccess(false));

      });
  };
}

//default address update
export function addressCountRequest(obj,accessToken) {
  return dispatch => {
    dispatch(addressCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `addressupdate`,
      data: obj,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(addressSetData(data));
        dispatch(addressCountCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(addressCountCheckSuccess(false));

      });
  };
}

//delete
export function addressDeleteRequest(id) {

  return dispatch => {
    dispatch(addressDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `address/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },

    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(addressDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(addressDeleteCheckSuccess(false));

      });
  };
}

//findOne
export function addressFindOneRequest(id) {
  return dispatch => {
    dispatch(addressFindOneCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `address/${id}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(addressOneData(data));
        dispatch(addressFindOneCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin,)
        dispatch(addressFindOneCheckSuccess(false));
      });
  };
};

//find - get
export function addressGetRequest(userid) {
  return dispatch => {
    dispatch(addressGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `addresses`,
      params: userid,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(addressSetData(data));
        dispatch(addressGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(addressGetCheckSuccess(false));
      });
  };
};

//update
export function addressUpdateRequest(id, element) {
  return dispatch => {
    dispatch(addressUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `addresses/${id}`,
      headers: {
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        // dispatch(addressSetData(data));
        dispatch(addressUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(addressUpdateCheckSuccess(false));

      });
  };
}



