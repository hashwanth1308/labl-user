import {
  PRICINGS_POST_CHECK_SUCCESS,
  PRICINGS_SET_COUNT,
  PRICINGS_COUNT_CHECK_SUCCESS,
  PRICINGS_DELETE_CHECK_SUCCESS,
  PRICINGS_FINDONE_CHECK_SUCCESS,
  PRICINGS_SET_ONE_DATA,
  PRICINGS_GET_CHECK_SUCCESS,
  PRICINGS_SET_DATA,
  PRICINGS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  pricingsPostCheckSuccess: false,
  pricingsCount: 0,
  pricingsCountCheckSuccess : false,
  pricingsDeleteCheckSuccess: false,
  pricingsFindOneCheckSuccess: false,
  pricingsOneData: [],
  pricingsGetCheckSuccess: false,
  pricingsData: [],
  pricingsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PRICINGS_POST_CHECK_SUCCESS:
      return {
        ...state,
        pricingsPostCheckSuccess: action.payload,
      };
      case PRICINGS_SET_COUNT:
      return {
        ...state,
        pricingsCount: action.payload,
      };
      case PRICINGS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        pricingsCountCheckSuccess: action.payload,
      };
      case PRICINGS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        pricingsDeleteCheckSuccess: action.payload,
      };
      case PRICINGS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        pricingsFindOneCheckSuccess: action.payload,
          };
      case PRICINGS_SET_ONE_DATA:
      return {
        ...state,
        pricingsOneData: action.payload,
          };
      case PRICINGS_GET_CHECK_SUCCESS:
      return {
        ...state,
        pricingsGetCheckSuccess: action.payload,
          };
      case PRICINGS_SET_DATA:
      return {
        ...state,
        pricingsData: action.payload,
          };
      case PRICINGS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        pricingsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
