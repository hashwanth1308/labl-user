import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils'

export default StyleSheet.create({
  navbar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  titleView: {
    paddingHorizontal: 20,
   flexDirection:"row",
   justifyContent:"space-between"
  },
  contentPage: {
    marginBottom: 0,
  },
  slide: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  img: {
    width: 392,
    height: 134,
    // borderRadius: Utils.scaleWithPixel(200) / 2,
  },
  contain: {
    paddingHorizontal: 0,
    paddingVertical: 0,
  },
  tabbar: {
    height: 53,
  },
  tab: {
    flex: 1,
    width:200
  },
  indicator: {
    height: 2,
  },
});
