import {
    LOGIN_LOADING,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
  } from '../actions/actionTypes';
  
  let initialState = {
    loginLoading: false,
    loginSuccess: false,
    loginFailure: false,
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case LOGIN_LOADING:
        return {
          ...state,
          loginLoading: action.payload,
        };
      case LOGIN_SUCCESS:
        return {
          ...state,
          loginSuccess: action.payload,
        };
      case LOGIN_FAILURE:
        return {
          ...state,
          loginFailure: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default reducer;
  