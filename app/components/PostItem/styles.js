import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  imageBanner: {width: '100%', height: Utils.scaleWithPixel(320)},
  content: {
    marginHorizontal: 20,
    paddingVertical: 10,
    borderBottomWidth: 1,
  },
});
