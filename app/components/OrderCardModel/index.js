import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity, Alert } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Image, Text } from '@components';
import { Images, useTheme, BaseColor } from '@config';
import StarRating from '../StarRating';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/Feather';
import { format, render, cancel, register } from 'timeago.js';
import { modelOrdersUpdateRequest } from '../../api/modelOrders';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Snackbar from 'react-native-snackbar';
import { productRatingGetRequest, productRatingPostRequest } from '../../api';
import _ from 'lodash';

export default function OrderCardModel(props) {
  const { colors } = useTheme();
  const dispatch = useDispatch();
  const [rate, setRate] = useState(0);
  const navigation = useNavigation();
  const { style, name, size, color, orderId, time, quantity, image, onPress, status, productName, orderItem } = props;
  console.log("orderItem", orderItem);
  const id = orderItem?.id;
  const userData = useSelector(state => state.accessTokenReducer.userData);
  const orderShipped = (orderstatus) => {
    console.log(orderstatus);
    // setLoading(true);
    const element = { status: orderstatus }
    console.log(element, "element")
    dispatch(modelOrdersUpdateRequest(id, element));
    // setLoading(false);
  }

  useEffect(() => {
    const obj = {};
    obj.model = userData?.user?.model?.id;
    dispatch(productRatingGetRequest(obj))
  }, [dispatch])

  const userRating = useSelector(state => state.productRatingReducer.productRatingData);
  console.log("userRating", userRating);

  const [rated, setRated] = useState(false);
  useEffect(() => {
    const cartitem = _.find(userRating, function (o) {
      return o?.product?.id === orderItem?.product?.id;
    });
    console.log(cartitem, 'cartitem');
    if (cartitem) {
      // setProductId(whishlistitems.id);
      setRated(true);
    }
  }, []);

  useEffect(() => {
    const cartitem = _.find(userRating, function (o) {
      return o?.product?.id === orderItem?.product?.id;
    });
    if (cartitem) {
      // setProductId(whishlistitems.id);
      setRated(true);
    }
  }, [userRating]);


  const givenRating = (rating) => {
    setRate(rating);
    const obj = {};
    obj.model = userData?.user?.model?.id;
    obj.product = orderItem?.product?.id;
    obj.rating = String(rating);
    dispatch(productRatingPostRequest(obj))
    Snackbar.show({
      text: 'Thanks for rating',
      duration: Snackbar.LENGTH_SHORT,
    });
    console.log("obj", obj);
    navigation.goBack();
  }


  const showAlert = (orderstatus) => {
    Alert.alert(
      "Wait !!!",
      "Are you sure, You want to cancel this order",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: () => {
            orderShipped(orderstatus)
          }
          // style: "cancel",
        },
      ],
      {
        cancelable: true,
      }
    );
  }
  return (
    <View style={[styles.content, { borderRadius: 20, borderColor: colors.border }, style]}>
      <TouchableOpacity onPress={() => navigation.navigate("OrdersScreenModelLABL", { object: orderItem })} style={{ flexDirection: "row", height: 100, width: "100%", backgroundColor: "#000", borderTopLeftRadius: 20, borderTopRightRadius: 20, alignItems: "center", justifyContent: "space-around" }}>
        {orderItem?.status == "Cancelled" ?
          <View style={{ marginLeft: 0, height: 40, width: 40, borderRadius: 30, backgroundColor: "#909090", justifyContent: "center", alignItems: "center" }}>
            <Icon name="close" size={25} color="#fff" />
          </View> : <View style={{ marginLeft: 20, height: 40, width: 40, borderRadius: 30, backgroundColor: "#909090", justifyContent: "center", alignItems: "center" }}>
            <Icon1 name="package" size={25} color="#fff" />
          </View>}
        <View style={{ marginLeft: 0, alignSelf: "center" }}>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22, color: "#fff", marginVertical: 5 }}>{orderId}</Text>
          <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
            {orderItem?.status === "Delivered" || "Cancelled" || "Returned" ?
              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20, color: "#fff" }}>{(new Date(orderItem?.updatedAt).toLocaleString())}</Text> :
              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20, color: "#fff" }}>{(new Date(time).toLocaleString())}</Text>}
          </View>
        </View>
        <Icon1 name='chevron-right' size={30} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity
        // style={[styles.content, { borderColor: colors.border }, style]}
        onPress={onPress}
        activeOpacity={0.9}>
        <View
          style={{
            padding: 10,
            flexDirection: 'row',
          }}>
          <Image source={{uri :image}} style={styles.imageBanner} />
          <View style={{ flex: 1, alignItems: 'flex-start', margin: 20 }}>
            <Text numberOfLines={0.5} style={{ fontFamily: "Montserrat-SemiBold", fontSize: 14,width:160 }}>
              {name}
            </Text>
            <Text blackColor style={{ fontFamily: "Montserrat-Medium", fontSize: 17, marginVertical: 5 }}>
              {productName}
            </Text>
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 12, marginBottom: 10 }}>
              Quantity : {quantity}
            </Text>
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 12 }}>
              Size : {size}
            </Text>
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 12, marginVertical: 10, width: 200 }}>
              Color : {color}
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: "row", alignSelf: "center", marginBottom: 20 }}>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000" }}>Status :- {' '}</Text>
          {status === "Cancelled" ?
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "red" }}>{status}</Text> :
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "green" }}>{status}</Text>}
        </View>
        {status !== "Delivered" ?
          status === "Cancelled" ? <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
            <TouchableOpacity onPress={() => navigation.navigate("HomeScreenModelLABL")} style={{ borderRadius: 5, borderWidth: 1.5, height: 40, width: 140, borderColor: "#000", justifyContent: "center", alignItems: "center" }} >
              <Text style={{ color: "#000", fontFamily: "Montserrat-SemiBold", fontSize: 22 }}>Re-Order</Text>
            </TouchableOpacity>
          </View> :
            <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
              <TouchableOpacity onPress={() => showAlert("Cancelled")} style={{ borderRadius: 5, borderWidth: 1.5, height: 40, width: 140, borderColor: "#000", justifyContent: "center", alignItems: "center" }} >
                <Text style={{ color: "#000", fontFamily: "Montserrat-SemiBold", fontSize: 22 }}>Cancel</Text>
              </TouchableOpacity>
            </View> :
          <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
            <TouchableOpacity style={{ borderRadius: 5, borderWidth: 1.5, height: 40, width: 140, borderColor: "#000", justifyContent: "center", alignItems: "center" }} >
              <Text style={{ color: "#000", fontFamily: "Montserrat-SemiBold", fontSize: 22 }}>Exchange</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => orderShipped("Returned")} style={{ borderRadius: 5, borderWidth: 1.5, height: 40, width: 140, borderColor: "#000", justifyContent: "center", alignItems: "center" }} >
              <Text style={{ color: "#000", fontFamily: "Montserrat-SemiBold", fontSize: 22 }}>Return</Text>
            </TouchableOpacity>
          </View>}
        {rated ? null :
          <>
            <View style={{ width: "100%", borderWidth: 0.5, borderColor: "#000", marginVertical: 20 }}></View>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20, color: "#000", alignSelf: "flex-start" }}>Rate Product : </Text>
              <View style={{ width: 130 }}>
                <StarRating starSize={20}
                  maxStars={5}
                  rating={rate}
                  selectedStar={rating => {
                    givenRating(rating);
                  }}
                  fullStarColor="#000"
                />
              </View>
            </View>
          </>}
      </TouchableOpacity>
    </View>
  );
}

OrderCardModel.propTypes = {
  image: PropTypes.node.isRequired,
  name: PropTypes.string,
  productName: PropTypes.string,
  time: PropTypes.string,
  size: PropTypes.string,
  color: PropTypes.string,
  quantity: PropTypes.string,
  status: PropTypes.string,
  orderId: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,

};

OrderCardModel.defaultProps = {
  image: '',
  name: '',
  productName: '',
  time: '',
  size: '',
  color: '',
  quantity: '',
  status: '',
  orderId: '',
  style: {},
  onPress: () => { },
};
