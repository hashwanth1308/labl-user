import React, { useEffect, useState } from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Dimensions,
  ActivityIndicator
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { BaseStyle, useTheme, Images } from '@config';
import { Header, SafeAreaView, Text, Icon, Image, TextInput, Button } from '@components';
import { useTranslation } from 'react-i18next';
import styles from './styles';
import BarChart from 'react-native-bar-chart';
import {
  earningsCountRequest, earningsGetRequest, earningsFindOneRequest, earningsPostRequest,
  earningsDeleteRequest,
  earningsUpdateRequest
} from '../../api/earnings';
import Icon4 from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/AntDesign';
import Icon3 from 'react-native-vector-icons/Fontisto';
import { TabView, TabBar } from 'react-native-tab-view';
import { modelOrdersGetRequest, modelProductsGetRequest, ordersGetRequest } from '../../api';
import axios from 'axios';

export default function EarningsScreenModelLABL({ navigation }) {

  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading(false)
    ), 1000)
  }, []);
  const { colors } = useTheme();
  const { t } = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const themeStorage = useSelector(state => state.application.theme);
  const addressData = useSelector(state => state.earningsReducer.earningsData);
  const windowHeight = Dimensions.get('window').height;
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  console.log("Token", userToken);
  const userData = useSelector(state => state.accessTokenReducer.userData)
  console.log("profileuser", userData);
  const userId = userData?.user?.id
  console.log("userId", userId);

  useEffect(() => {
    const obj = {};
    obj.model = userData?.user?.model?.id;
    dispatch(modelProductsGetRequest(obj))
  }, [])

  const modelProducts = useSelector(state => state.modelProductsReducer.modelProductsData);

  useEffect(() => {
    const obj = {};
    obj.model = userData?.user?.model?.id;
    dispatch(ordersGetRequest(obj));
  }, [dispatch])
  const ordersData = useSelector(state => state.ordersReducer.ordersData);
  console.log("userordersData", ordersData);



  const dispatch = useDispatch();
  const [card, setCard] = useState('');
  const [valid, setValid] = useState('');
  const [digit, setDigit] = useState('');
  const [name, setName] = useState('');
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });


  // useEffect(() => {
  //     dispatch(earningsCountRequest());

  // }, [dispatch])
  // useEffect(() => {
  //   dispatch(earningsGetRequest());

  // }, [dispatch])

  // const onFindOneEarnings = () => {
  //   setLoading(true);
  //   dispatch(earningsFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };


  // const onAddEarnings = () => {
  //   setLoading(true);
  //   dispatch(earningsPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteEarnings = () => {
  //   setLoading(true);
  //   dispatch(earningsDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateEarnings = () => {
  //   setLoading(true);
  //   dispatch(earningsUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: 'week', title: t('Week'), icon: "view-grid-outline" },
    { key: 'month', title: t('Month'), icon: "format-list-bulleted" },
    { key: 'year', title: t('Year'), icon: "format-list-bulleted" }
  ]);

  // When tab is activated, set what's index value
  const handleIndexChange = index => setIndex(index);

  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, { backgroundColor: "#000" }]}
      style={[styles.tabbar, { backgroundColor: "#909090" }]}
      tabStyle={styles.tab}
      inactiveColor={"#CDCDCD"}
      activeColor={"#fff"}
      renderLabel={({ route, focused, color }) => (
        <View style={{ flex: 1, alignItems: 'center', width: 130 }}>
          <Text headline style={{ color, top: 2, fontSize: 15, fontFamily: "Montserrat-Bold" }}>
            {route.title}
          </Text>
        </View>
      )
      }
    />
  );

  // Render correct screen container when tab is activated
  const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'week':
        return <WeekTab jumpTo={jumpTo} navigation={navigation} />;
      case 'month':
        return <MonthTab jumpTo={jumpTo} navigation={navigation} />;
      case 'year':
        return <YearTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };


  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('Upload Products')}
        style={{ height: 70, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flexDirection: "row", width: 200, justifyContent: "space-between", alignSelf: "flex-start" }}>
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={"#000"}
                  enableRTL={true}
                />
              </View>
              <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 18, color: "#fff", right: 70 }}>Earnings</Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ padding: 0 }}>
            <View style={{ flexDirection: "row", marginVertical: 20 }}>
              <Image source={{ uri: userData?.user?.image ? userData?.user?.image : "" }} style={{ height: 60, width: 60, borderRadius: 5, marginHorizontal: 20 }} />
              <View style={{ flexDirection: "column", justifyContent: "space-around" }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 23 }}>My Dashboard</Text>
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 17 }}>{new Date().toDateString()}</Text>
              </View>
            </View>
            <View
              style={{
                height: 120,
                width: '100%',
                padding: 20,
                backgroundColor: '#000',
                // marginVertical: 5,
                flexDirection: 'column',
              }}>
              {/* <Text
                style={{
                  fontFamily: 'Montserrat-Medium',
                  fontSize: 17,
                  color: '#fff',
                  margin: 10,
                }}>
                {new Date().toDateString()}
              </Text> */}
              <ScrollView
                horizontal
                contentContainerStyle={{ marginVertical: 10 }}>
                <View
                  style={{
                    backgroundColor: '#fff',
                    height: 60,
                    width: 160,
                    borderRadius: 10,
                    marginLeft: 5,
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Icon4 name="shirt" color="#000" size={28} />
                  <View style={{ flexDirection: 'column' }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat-Regular',
                        fontSize: 20,
                        color: '#909090',
                      }}>
                      Products
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 18,
                        color: '#000',
                      }}>
                      {modelProducts?.length ? modelProducts?.length : 0}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    backgroundColor: '#fff',
                    height: 60,
                    width: 160,
                    borderRadius: 10,
                    marginHorizontal: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Icon2 name="like1" color="#000" size={28} />
                  <View style={{ flexDirection: 'column' }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat-Regular',
                        fontSize: 20,
                        color: '#909090',
                      }}>
                      Orders
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 18,
                        color: '#000',
                      }}>
                      6 (Month)
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    backgroundColor: '#fff',
                    height: 60,
                    width: 160,
                    borderRadius: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Icon1 name="credit" color="#000" size={28} />
                  <View style={{ flexDirection: 'column' }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat-Regular',
                        fontSize: 20,
                        color: '#909090',
                      }}>
                      Earnings
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 18,
                        color: '#000',
                      }}>
                      $3125 (Month)
                    </Text>
                  </View>
                </View>
                {/* <View
                  style={{
                    backgroundColor: '#fff',
                    height: 60,
                    width: 160,
                    borderRadius: 10,
                    marginLeft: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Icon3 name="shopping-sale" color="#000" size={28} />
                  <View style={{ flexDirection: 'column' }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat-Regular',
                        fontSize: 20,
                        color: '#909090',
                      }}>
                      Sales
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 18,
                        color: '#000',
                      }}>
                      6 (Day)
                    </Text>
                  </View>
                </View> */}
              </ScrollView>
            </View>
            <View>
              <TabView
                // lazy
                navigationState={{ index, routes }}
                renderScene={renderScene}
                renderTabBar={renderTabBar}
                onIndexChange={handleIndexChange}
                style={{ height: windowHeight * 5, marginTop: 5 }}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      {loading &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }

    </View>
  )
}

/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function WeekTab({ navigation }) {
  const data = [
    [70, 30],
    [80, 20],
    [90, 60],
    [100, 70],
    [70, 40],
    [70, 40],
    [70, 40],
  ];
  // labels
  const horizontalData = ['M', 'T', 'W', 'T', 'F', 'S', 'SU'];
  return (
    <View>
      <View style={{ height: 180, width: "100%", marginVertical: 5, }}>
        <View style={{ flexDirection: "row", justifyContent: "space-around", marginVertical: 20 }}>
          <View style={{ height: 60, width: 150, backgroundColor: "#CDCDCD", borderRadius: 5, justifyContent: "center", alignItems: "center" }}>
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 24, color: "#000" }}>{0}</Text>
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#000" }}>Total Views</Text>
          </View>
          <View style={{ height: 60, width: 150, backgroundColor: "#CDCDCD", borderRadius: 5, justifyContent: "center", alignItems: "center" }}>
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 24, color: "#000" }}>{0}</Text>
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#000" }}>Orders Placed</Text>
          </View>
        </View>
        <View style={{ flexDirection: "row", width: 345, height: 60, alignSelf: "center", backgroundColor: "#CDCDCD", borderRadius: 5, justifyContent: "space-evenly", alignItems: "center" }}>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 24, color: "#000" }}>₹ {0}</Text>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#000" }}>Earned from user</Text>
        </View>
      </View>
      <View style={{ alignItems: "center" }}>
        <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, marginVertical: 10 }}>Earnings Growth</Text>
        <View style={{ borderWidth: 1, borderColor: "#000", width: 180 }}></View>
      </View>
      <View style={{ alignItems: "center", alignSelf: "center", flexDirection: "row", marginVertical: 20 }}>
        <View style={{ flexDirection: "row", marginHorizontal: 10 }}>
          <View style={{ height: 13, width: 13, borderRadius: 20, backgroundColor: "#000", marginHorizontal: 10, alignSelf: "center" }}></View>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18 }}>This Week</Text>
        </View>
        <View style={{ flexDirection: "row", marginHorizontal: 10 }}>
          <View style={{ height: 13, width: 13, borderRadius: 20, backgroundColor: "#CDCDCD", marginHorizontal: 10, alignSelf: "center" }}></View>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18 }}>Last Week</Text>
        </View>
      </View>
      <View style={{ padding: 0 }}>
        <BarChart data={data} horizontalData={horizontalData} barColor="#CDCDCD" labelColor='#000' secondBarColor='#000' />
      </View>
    </View>
  )
}

/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function MonthTab({ navigation }) {

  return (
    <View>

    </View>
  )
}

/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function YearTab({ navigation }) {

  return (
    <View>

    </View>
  )
}