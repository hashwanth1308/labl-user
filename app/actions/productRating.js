import {
  PRODUCTRATING_POST_CHECK_SUCCESS,
  PRODUCTRATING_SET_COUNT,
  PRODUCTRATING_COUNT_CHECK_SUCCESS,
  PRODUCTRATING_DELETE_CHECK_SUCCESS,
  PRODUCTRATING_FINDONE_CHECK_SUCCESS,
  PRODUCTRATING_SET_ONE_DATA,
  PRODUCTRATING_GET_CHECK_SUCCESS,
  PRODUCTRATING_SET_DATA,
  PRODUCTRATING_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function productRatingPostCheckSuccess(bool = false) {
  return {
    type: PRODUCTRATING_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productRatingSetCount(object) {
  return {
    type: PRODUCTRATING_SET_COUNT,
    payload: object,
  };
}
export function productRatingCountCheckSuccess(bool = false) {
  return {
    type: PRODUCTRATING_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productRatingDeleteCheckSuccess(bool = false) {
  return {
    type: PRODUCTRATING_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productRatingFindOneCheckSuccess(bool = false) {
  return {
    type: PRODUCTRATING_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productRatingSetOneData(object) {
  return {
    type: PRODUCTRATING_SET_ONE_DATA,
    payload: object,
  };
}
export function productRatingGetCheckSuccess(bool = false) {
  return {
    type: PRODUCTRATING_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productRatingSetData(object) {
  return {
    type: PRODUCTRATING_SET_DATA,
    payload: object,
  };
}

export function productRatingUpdateCheckSuccess(bool = false) {
  return {
    type: PRODUCTRATING_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

