import{ 
    
  modelPayoutsPostCheckSuccess,
  modelPayoutsSetCount,
  modelPayoutsCountCheckSuccess,
  modelPayoutsDeleteCheckSuccess,
  modelPayoutsFindOneCheckSuccess,
  modelPayoutsSetOneData,
  modelPayoutsGetCheckSuccess,
  modelPayoutsSetData,
  modelPayoutsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function modelPayoutsPostRequest(element) {
      return dispatch => {
        dispatch(modelPayoutsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `model-payouts`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(modelPayoutsSetData(a));
            dispatch(modelPayoutsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(modelPayoutsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function modelPayoutsCountRequest(element) {
      return dispatch => {
          dispatch(modelPayoutsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `model-payouts/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(modelPayoutsSetCount(data));
              dispatch(modelPayoutsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(modelPayoutsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function modelPayoutsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(modelPayoutsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `model-payouts/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(modelPayoutsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(modelPayoutsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function modelPayoutsFindOneRequest(id) {
      return dispatch => {
        dispatch(modelPayoutsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `model-payouts/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(modelPayoutsOneData(data));
            dispatch(modelPayoutsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(modelPayoutsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function modelPayoutsGetRequest(element) {
  return dispatch => {
    dispatch(modelPayoutsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `model-payouts`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(modelPayoutsSetOneData(data));
        dispatch(modelPayoutsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(modelPayoutsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function modelPayoutsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(modelPayoutsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `model-payouts/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(modelPayoutsSetData(data));
              dispatch(modelPayoutsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(modelPayoutsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 