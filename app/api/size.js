import{ 
    
  sizePostCheckSuccess,
  sizeSetCount,
  sizeCountCheckSuccess,
  sizeDeleteCheckSuccess,
  sizeGetCheckSuccess,
  colorGetCheckSuccess,
  sizeSetData,
  colorSetData,
  sizeUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function sizePostRequest(pName,pDesc,price,quantity,selectedValue,selectedCat, profileImage,id) {
    const objectValue ={};
    objectValue.productName = pName;
    objectValue.productDescription = pDesc;
    objectValue.productPrice = price;
    objectValue.productAvailableQuantityALL = quantity;
    // objectValue.collectionName = selectedValue;
    objectValue.productAvailableSizes = selectedCat?.value;
    objectValue.productImages = profileImage;
    objectValue.label = id;
      return dispatch => {
        dispatch(sizePostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `sizes`,
            data:objectValue,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(sizeSetData(data));
            dispatch(sizePostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(sizePostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function sizeCountRequest(element) {
      return dispatch => {
          dispatch(sizeCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `sizes/count`,
              headers:{
              // Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(sizeSetCount(data));
              dispatch(sizeCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(sizeCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function sizeDeleteRequest(id) {
      
      return dispatch => {
          dispatch(sizeDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `sizes/${id}`,
              headers:{
              // Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(sizeDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(sizeDeleteCheckSuccess(false));
              
          });
      };
  }

  //find - get
  export function sizeGetRequest(element) {
  return dispatch => {
    dispatch(sizeGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `productsizes`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("data",data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(sizeSetData(data));
        dispatch(sizeGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(sizeGetCheckSuccess(false));
      });
  };
  };


  export function colorGetRequest(element) {
  return dispatch => {
    dispatch(colorGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `product-colors`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(colorSetData(data));
        dispatch(colorGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(colorGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function sizeUpdateRequest(id) {
     
      return dispatch => {
          dispatch(sizeUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `sizes/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(sizeSetData(data));
              dispatch(sizeUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(sizeUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 