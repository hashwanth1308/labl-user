import {
  MODELPAYOUTS_POST_CHECK_SUCCESS,
  MODELPAYOUTS_SET_COUNT,
  MODELPAYOUTS_COUNT_CHECK_SUCCESS,
  MODELPAYOUTS_DELETE_CHECK_SUCCESS,
  MODELPAYOUTS_FINDONE_CHECK_SUCCESS,
  MODELPAYOUTS_SET_ONE_DATA,
  MODELPAYOUTS_GET_CHECK_SUCCESS,
  MODELPAYOUTS_SET_DATA,
  MODELPAYOUTS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  modelPayoutsPostCheckSuccess: false,
  modelPayoutsCount: 0,
  modelPayoutsCountCheckSuccess : false,
  modelPayoutsDeleteCheckSuccess: false,
  modelPayoutsFindOneCheckSuccess: false,
  modelPayoutsOneData: [],
  modelPayoutsGetCheckSuccess: false,
  modelPayoutsData: [],
  modelPayoutsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MODELPAYOUTS_POST_CHECK_SUCCESS:
      return {
        ...state,
        modelPayoutsPostCheckSuccess: action.payload,
      };
      case MODELPAYOUTS_SET_COUNT:
      return {
        ...state,
        modelPayoutsCount: action.payload,
      };
      case MODELPAYOUTS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        modelPayoutsCountCheckSuccess: action.payload,
      };
      case MODELPAYOUTS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        modelPayoutsDeleteCheckSuccess: action.payload,
      };
      case MODELPAYOUTS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        modelPayoutsFindOneCheckSuccess: action.payload,
          };
      case MODELPAYOUTS_SET_ONE_DATA:
      return {
        ...state,
        modelPayoutsOneData: action.payload,
          };
      case MODELPAYOUTS_GET_CHECK_SUCCESS:
      return {
        ...state,
        modelPayoutsGetCheckSuccess: action.payload,
          };
      case MODELPAYOUTS_SET_DATA:
      return {
        ...state,
        modelPayoutsData: action.payload,
          };
      case MODELPAYOUTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        modelPayoutsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
