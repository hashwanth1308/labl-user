import { StyleSheet } from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({

    iconContent: {

        // alignItems: 'center',
        width: 150,
        height: 190,
        // marginVertical:10

    },
    item: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 5,
        paddingBottom: 5,
      },
      contain: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        paddingTop: 5,
        paddingBottom: 5,
      },
      thumb: {width: 55, height: 100, marginRight: 10},
      content: {
        flex: 1,
        // flexDirection: 'row',
        margin:10
      },
      left: {
        // flex: 7,
        alignItems: 'flex-start',
        justifyContent: 'center',
      },
      right: {
        flex: 2.5,
        alignItems: 'flex-start',
        justifyContent: 'center',
        
      },

});
