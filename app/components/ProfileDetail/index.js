import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Image, Icon, Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { BaseColor, useTheme,Images } from '@config';
import { useDispatch, useSelector } from 'react-redux';

export default function ProfileDetail(props) {
  const { colors } = useTheme();
  const {
    style,
    image,
    styleLeft,
    styleThumb,
    styleRight,
    onPress,
    textFirst,
    point,
    textSecond,
    textThird,
    icon,
  } = props;
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  return (
    <TouchableOpacity
      style={[styles.contain, style]}
      onPress={onPress}
      activeOpacity={0.9}>
      <View style={[styles.contentLeft, styleLeft]}>
        <View>
          <View style={{ height: 128, width: 105, borderWidth: 1.5, borderColor: "#fff", borderRadius: 15, marginLeft: 20, alignItems: "center", justifyContent: "center" }}>
            <Image source={{uri : UserData?.user?.image ? UserData?.user?.image : "https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png"}} style={[styles.thumb, styleThumb]} />
          </View>
          {/* <View style={[styles.point, {backgroundColor: colors.primaryLight}]}>
            <Text overline whiteColor semibold>
              {point}
            </Text>
          </View> */}
        </View>
        <View style={{ flex: 1, alignItems: 'center', top: 40 }}>
          <Text numberOfLines={1} style={{ fontFamily: "Montserrat-SemiBold", color: "#fff", fontSize: 22 }}>
            {textFirst}
          </Text>
          <Text
            body2
            style={{
              marginTop: 3,
              // paddingRight: 10,
              fontFamily: "Montserrat-SemiBold", color: "#fff", fontSize: 16
            }}
            numberOfLines={1}>
            {textSecond}
          </Text>
          <Text footnote grayColor numberOfLines={1}>
            {textThird}
          </Text>
          <View style={{ backgroundColor: "#fff", height: 16, width: 45, borderRadius: 10, justifyContent: "center", alignItems: "center" }}>
            {UserData?.user?.isModel ?
              <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000" }}>
                Model
              </Text> :
              <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 13, color: "#000" }}>
                User
              </Text>}
            {/* <Icon name="chevron-down" size={15} color="#000" /> */}
          </View>
        </View>
      </View>
      {/* {icon && (
        <View style={[styles.contentRight, styleRight]}>
          <Icon
            name="angle-right"
            size={18}
            color={BaseColor.grayColor}
            enableRTL={true}
          />
        </View>
      )} */}
    </TouchableOpacity>
  );
}

ProfileDetail.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  image: PropTypes.node.isRequired,
  textFirst: PropTypes.string,
  // point: PropTypes.string,
  textSecond: PropTypes.string,
  textThird: PropTypes.string,
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  icon: PropTypes.bool,
  onPress: PropTypes.func,
};

ProfileDetail.defaultProps = {
  image: '',
  textFirst: '',
  textSecond: '',
  icon: true,
  // point: '',
  style: {},
  styleLeft: {},
  styleThumb: {},
  styleRight: {},
  onPress: () => { },
};
