import{ 
    
  vendorsPostCheckSuccess,
  vendorsSetCount,
  vendorsCountCheckSuccess,
  vendorsDeleteCheckSuccess,
  vendorsFindOneCheckSuccess,
  vendorsSetOneData,
  vendorsGetCheckSuccess,
  vendorsSetData,
  vendorsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function vendorsPostRequest(element) {
      return dispatch => {
        dispatch(vendorsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `vendors`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(vendorsSetData(data));
            dispatch(vendorsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(vendorsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function vendorsCountRequest(element) {
      return dispatch => {
          dispatch(vendorsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `vendors/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(vendorsSetCount(data));
              dispatch(vendorsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(vendorsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function vendorsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(vendorsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `vendors/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(vendorsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(vendorsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function vendorsFindOneRequest(id) {
      return dispatch => {
        dispatch(vendorsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `vendors/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(vendorsOneData(data));
            dispatch(vendorsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(vendorsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function vendorsGetRequest(element) {
  return dispatch => {
    dispatch(vendorsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `vendors`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(vendorsSetOneData(data));
        dispatch(vendorsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(vendorsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function vendorsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(vendorsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `vendors/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(vendorsSetData(data));
              dispatch(vendorsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(vendorsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 