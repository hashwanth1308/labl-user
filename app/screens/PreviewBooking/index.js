import React from 'react';
import { View, ScrollView, } from 'react-native';
import { BaseStyle, BaseColor, useTheme, Images } from '@config';
import { Header, SafeAreaView, Icon, Text, Button, OrderDetails } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import * as Progress from 'react-native-progress';

export default function PreviewBooking({ navigation,route }) {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const { orderObject } = route.params;
  console.log("orderObject",orderObject);
  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('Order Details')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

        // renderRight={() => {
        //   return <Icon name="shopping-cart" size={24} color={colors.primary} />;
        // }}
        // onPressRight={() => {
        //   navigation.navigate('Cart');
        // }}
      />

      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <ScrollView>
          <View style={{ paddingHorizontal: 20 }}>
            <View
              style={[styles.blockView, { borderBottomColor: colors.border }]}>
              <Text body2 semibold style={{ marginBottom: 10 }}>
                {t('Order ID-0D223435788#123')}
              </Text>

            </View>
            <View>
              <OrderDetails
                title="women jacket"
                description="New stylish jacket"
                style={{ marginTop: 15, width: '100%', bottom: 8 }}
                image={Images.trip8}
                onPress={() => {
                  navigation.navigate('Home');
                }}
              />

            </View>
            <Progress.Bar progress={0.3} width={300} height={10} color={colors.primary} />
            <Text body2 style={{ marginBottom: 10 }}>

              {t('Ordered')}
            </Text>
            <View>
              <Text body2 style={{ left: 130, bottom: 25 }}>

                {t('Shipped')}
              </Text>
              <Text body2 style={{ left: 250, bottom: 40 }}>

                {t('Delivered')}
              </Text>
            </View>

            <View>
              <Text body2 style={{ marginBottom: 10 }}>

                {t('Shipping Details')}
              </Text>
              <Text body2 semibold style={{ marginBottom: 10 }}>

                {t('WEBSOC')}
              </Text>
              <Text body2 semibold style={{ marginBottom: 10 }}>

                {t('H:NO-141/23')}
              </Text>
              <Text body2 semibold style={{ marginBottom: 10 }}>

                {t('Gandhi Road')}
              </Text>
              <Text body2 semibold style={{ marginBottom: 10 }}>

                {t('kukatpally')}
              </Text>
              <Text body2 semibold style={{ marginBottom: 10 }}>

                {t('Telangana-509878')}
              </Text>
              <Text body2 semibold style={{ marginBottom: 10 }}>

                {t('Phonenumber-9838831811')}
              </Text>
            </View>
            <View style={[styles.line, { backgroundColor: colors.border }]} />

            <View
              style={[styles.blockView, { borderBottomColor: colors.border }]}>
              <Text body2 semibold style={{ marginBottom: 10 }}>

                {t('Price Details')}
              </Text>


              <Text body2 style={{ marginBottom: 5 }}>
                Price Of Product             </Text>

              <View style={{ flex: 1, alignItems: 'flex-end', bottom: 25 }}>
                <Text body2 semibold>
                  {t('₹ 599')}
                </Text>
              </View>
              <Text body2 style={{ marginBottom: 5 }}>
                Total Discount On Product
              </Text>
              <View style={{ flex: 1, alignItems: 'flex-end', bottom: 22 }}>
                <Text body2 semibold>
                  {t('₹ 199')}
                </Text>
              </View>
              <Text body2 style={{ marginBottom: 5 }}>
                Shipping Fee
              </Text>
              <View style={{ flex: 1, alignItems: 'flex-end', bottom: 22 }}>
                <Text body2 semibold>
                  {t('₹ 49')}
                </Text>
              </View>
              <Text body2 style={{ marginBottom: 5 }}>
                Shipping Discount
              </Text>
              <View style={{ flex: 1, alignItems: 'flex-end', bottom: 22 }}>
                <Text body2 semibold>
                  {t('-₹ 49')}
                </Text>
              </View>

              <Text body2 style={{ marginBottom: 5 }}>
                Total Amount
              </Text>
              <View style={{ flex: 1, alignItems: 'flex-end', bottom: 20 }}>
                <Text body2 semibold>
                  {t('₹ 399')}
                </Text>
              </View>

              <View style={[styles.line, { backgroundColor: colors.border }]} />

            </View>
          </View>


        </ScrollView>

      </SafeAreaView>
    </View>
  );
}
