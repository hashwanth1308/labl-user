import{ 
    
  shippingsPostCheckSuccess,
  shippingsSetCount,
  shippingsCountCheckSuccess,
  shippingsDeleteCheckSuccess,
  shippingsFindOneCheckSuccess,
  shippingsSetOneData,
  shippingsGetCheckSuccess,
  shippingsSetData,
  shippingsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function shippingsPostRequest(element) {
      return dispatch => {
        dispatch(shippingsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `shippings`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(shippingsSetData(a));
            dispatch(shippingsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(shippingsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function shippingsCountRequest(element) {
      return dispatch => {
          dispatch(shippingsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `shippings/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(shippingsSetCount(data));
              dispatch(shippingsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(shippingsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function shippingsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(shippingsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `shippings/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(shippingsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(shippingsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function shippingsFindOneRequest(id) {
      return dispatch => {
        dispatch(shippingsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `shippings/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(shippingsOneData(data));
            dispatch(shippingsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(shippingsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function shippingsGetRequest(element) {
  return dispatch => {
    dispatch(shippingsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `shippings`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(shippingsSetOneData(data));
        dispatch(shippingsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(shippingsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function shippingsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(shippingsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `shippings/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(shippingsSetData(data));
              dispatch(shippingsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(shippingsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 