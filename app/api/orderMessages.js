import{ 
    
  orderMessagesPostCheckSuccess,
  orderMessagesSetCount,
  orderMessagesCountCheckSuccess,
  orderMessagesDeleteCheckSuccess,
  orderMessagesFindOneCheckSuccess,
  orderMessagesSetOneData,
  orderMessagesGetCheckSuccess,
  orderMessagesSetData,
  orderMessagesUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function orderMessagesPostRequest(element) {
      return dispatch => {
        dispatch(orderMessagesPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `order-messages`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(orderMessagesSetData(a));
            dispatch(orderMessagesPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(orderMessagesPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function orderMessagesCountRequest(element) {
      return dispatch => {
          dispatch(orderMessagesCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `order-messages/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(orderMessagesSetCount(data));
              dispatch(orderMessagesCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(orderMessagesCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function orderMessagesDeleteRequest(id) {
      
      return dispatch => {
          dispatch(orderMessagesDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `order-messages/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(orderMessagesDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(orderMessagesDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function orderMessagesFindOneRequest(id) {
      return dispatch => {
        dispatch(orderMessagesFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `order-messages/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(orderMessagesOneData(data));
            dispatch(orderMessagesFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(orderMessagesFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function orderMessagesGetRequest(element) {
  return dispatch => {
    dispatch(orderMessagesGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `order-messages`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(orderMessagesSetOneData(data));
        dispatch(orderMessagesGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(orderMessagesGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function orderMessagesUpdateRequest(id) {
     
      return dispatch => {
          dispatch(orderMessagesUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `order-messages/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(orderMessagesSetData(data));
              dispatch(orderMessagesUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(orderMessagesUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 