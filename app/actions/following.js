import {
  MODEL_FOLLOWING_POST_CHECK_SUCCESS,
  USER_FOLLOWING_POST_CHECK_SUCCESS,
  MODEL_FOLLOWING_SET_COUNT,
  MODEL_FOLLOWING_COUNT_CHECK_SUCCESS,
  MODEL_FOLLOWING_DELETE_CHECK_SUCCESS,
  USER_FOLLOWING_DELETE_CHECK_SUCCESS,
  MODEL_FOLLOWING_GET_CHECK_SUCCESS,
  USER_FOLLOWING_GET_CHECK_SUCCESS,
  MODEL_FOLLOWING_SET_DATA,
  USER_FOLLOWING_SET_DATA,
  MODEL_FOLLOWING_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function modelFollowingPostCheckSuccess(bool = false) {
  return {
    type: MODEL_FOLLOWING_POST_CHECK_SUCCESS,
    payload: bool,
  };
}
export function userFollowingPostCheckSuccess(bool = false) {
  return {
    type: USER_FOLLOWING_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelFollowingSetCount(object) {
  return {
    type: MODEL_FOLLOWING_SET_COUNT,
    payload: object,
  };
}
export function modelFollowingCountCheckSuccess(bool = false) {
  return {
    type: MODEL_FOLLOWING_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelFollowingDeleteCheckSuccess(bool = false) {
  return {
    type: MODEL_FOLLOWING_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}
export function userFollowingDeleteCheckSuccess(bool = false) {
  return {
    type: USER_FOLLOWING_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function modelFollowingGetCheckSuccess(bool = false) {
  return {
    type: MODEL_FOLLOWING_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function userFollowingGetCheckSuccess(bool = false) {
  return {
    type: USER_FOLLOWING_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelFollowingSetData(object) {
  return {
    type: MODEL_FOLLOWING_SET_DATA,
    payload: object,
  };
}
export function userFollowingSetData(object) {
  return {
    type: USER_FOLLOWING_SET_DATA,
    payload: object,
  };
}

export function modelFollowingUpdateCheckSuccess(bool = false) {
  return {
    type: MODEL_FOLLOWING_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

