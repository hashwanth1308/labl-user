import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import {Image, Text} from '@components';
import {Images, useTheme} from '@config';

export default function EventCard(props) {
  const {colors} = useTheme();
  const {style, title, location, time, image, onPress} = props;
  return (
    <TouchableOpacity
      style={[styles.content, {borderColor: colors.border}, style]}
      onPress={onPress}
      activeOpacity={0.9}>
         <Image source={{uri : image ? image : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsQ-YHX2i3RvTDDmpfnde4qyb2P8up7Wi3Ww&usqp=CAU'}} style={styles.imageBanner}></Image>
      <View
        style={{
          padding: 10,
          flexDirection: 'row',
        }}>
        <View style={{alignItems: 'center', marginRight: 60}}>
          {/* <Text body2 primaryColor semibold>
            OCT
          </Text> */}
          {/* <Text body1 grayColor semibold>
            31
          </Text> */}
        </View>
        <View style={{flex: 1, alignItems: 'flex-start',}}>
          <Text body2  numberOfLines={1} style={{flex: 1,marginRight: 30,color: 'black',fontWeight: 'bold',}}>
            {title}
          </Text>
          <View style={{alignItems: 'center', marginRight:50}} >
          <Text   style={{marginVertical: 5,color: 'green',fontWeight: 'bold',}}>
            {time}
          </Text>
          <Text numberOfLines={1}  style={{marginVertical: 5,color: 'black',fontWeight: 'bold',}}>
            {location}
          </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

EventCard.propTypes = {
  image: PropTypes.node.isRequired,
  title: PropTypes.string,
  time: PropTypes.string,
  location: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

EventCard.defaultProps = {
  image: Images.profile2,
  title: 'The Roadster',
  time: 'Winter Wear',
  location: '50-70% off',
  style: {},
  onPress: () => {},
};
