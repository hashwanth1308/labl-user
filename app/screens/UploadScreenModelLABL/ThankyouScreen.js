import * as React from 'react';
import { View,Dimensions } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Text, Button } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';

export default function ThankyouScreen({ navigation }) {
    const { t } = useTranslation();
    const { colors } = useTheme();
    const height = Dimensions.get('window').height;
    // const [loading1, setLoading1] = useState(true);
    React.useEffect(() => {
        setTimeout(() => {
          navigation.replace('Profile5ModelLABL');
        }, 1000);
      }, [navigation]);

    return (
        <View style={{ flex: 1 }}>
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
                <View style={{ flex: 1, height: height, width: "100%", alignItems: "center", justifyContent: "center",flexDirection:"column" }}>
                    <View style={{height:200,width:200,borderRadius:10,justifyContent:"center",alignItems:"center"}}>
                        <Icon name="check-circle" size={70} />
                    </View>
                    <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000" }}>Product Uploaded Successfully</Text>
                </View>
            </SafeAreaView>
        </View>
    );
}
