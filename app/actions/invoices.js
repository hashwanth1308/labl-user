import {
  INVOICES_POST_CHECK_SUCCESS,
  INVOICES_SET_COUNT,
  INVOICES_COUNT_CHECK_SUCCESS,
  INVOICES_DELETE_CHECK_SUCCESS,
  INVOICES_FINDONE_CHECK_SUCCESS,
  INVOICES_SET_ONE_DATA,
  INVOICES_GET_CHECK_SUCCESS,
  INVOICES_SET_DATA,
  INVOICES_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function invoicesPostCheckSuccess(bool = false) {
  return {
    type: INVOICES_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function invoicesSetCount(object) {
  return {
    type: INVOICES_SET_COUNT,
    payload: object,
  };
}
export function invoicesCountCheckSuccess(bool = false) {
  return {
    type: INVOICES_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function invoicesDeleteCheckSuccess(bool = false) {
  return {
    type: INVOICES_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function invoicesFindOneCheckSuccess(bool = false) {
  return {
    type: INVOICES_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function invoicesSetOneData(object) {
  return {
    type: INVOICES_SET_ONE_DATA,
    payload: object,
  };
}
export function invoicesGetCheckSuccess(bool = false) {
  return {
    type: INVOICES_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function invoicesSetData(object) {
  return {
    type: INVOICES_SET_DATA,
    payload: object,
  };
}

export function invoicesUpdateCheckSuccess(bool = false) {
  return {
    type: INVOICES_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

