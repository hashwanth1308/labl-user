import {
  USER_CARTS_POST_CHECK_SUCCESS,
  USER_CARTS_SET_COUNT,
  USER_CARTS_COUNT_CHECK_SUCCESS,
  USER_CARTS_DELETE_CHECK_SUCCESS,
  USER_CARTS_FINDONE_CHECK_SUCCESS,
  USER_CARTS_SET_ONE_DATA,
  USER_CARTS_GET_CHECK_SUCCESS,
  USER_CARTS_SET_DATA,
  USER_CARTS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  userCartsPostCheckSuccess: false,
  userCartsCount: 0,
  userCartsCountCheckSuccess : false,
  userCartsDeleteCheckSuccess: false,
  userCartsFindOneCheckSuccess: false,
  userCartsOneData: [],
  userCartsGetCheckSuccess: false,
  userCartsData: [],
  userCartsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case  USER_CARTS_POST_CHECK_SUCCESS:
      return {
        ...state,
        userCartsPostCheckSuccess: action.payload,
      };
      case USER_CARTS_SET_COUNT:
      return {
        ...state,
        userCartsCount: action.payload,
      };
      case USER_CARTS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        userCartsCountCheckSuccess: action.payload,
      };
      case  USER_CARTS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        userCartsDeleteCheckSuccess: action.payload,
      };
      case USER_CARTS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        userCartsFindOneCheckSuccess: action.payload,
          };
      case USER_CARTS_SET_ONE_DATA:
      return {
        ...state,
        userCartsOneData: action.payload,
          };
      case USER_CARTS_GET_CHECK_SUCCESS:
      return {
        ...state,
        userCartsGetCheckSuccess: action.payload,
          };
      case USER_CARTS_SET_DATA:
      return {
        ...state,
        userCartsData: action.payload,
          };
      case USER_CARTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        userCartsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
