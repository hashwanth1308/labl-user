import {
  SALESORDERS_POST_CHECK_SUCCESS,
  SALESORDERS_SET_COUNT,
  SALESORDERS_COUNT_CHECK_SUCCESS,
  SALESORDERS_DELETE_CHECK_SUCCESS,
  SALESORDERS_FINDONE_CHECK_SUCCESS,
  SALESORDERS_SET_ONE_DATA,
  SALESORDERS_GET_CHECK_SUCCESS,
  SALESORDERS_SET_DATA,
  SALESORDERS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function salesOrdersPostCheckSuccess(bool = false) {
  return {
    type: SALESORDERS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function salesOrdersSetCount(object) {
  return {
    type: SALESORDERS_SET_COUNT,
    payload: object,
  };
}
export function salesOrdersCountCheckSuccess(bool = false) {
  return {
    type: SALESORDERS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function salesOrdersDeleteCheckSuccess(bool = false) {
  return {
    type: SALESORDERS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function salesOrdersFindOneCheckSuccess(bool = false) {
  return {
    type: SALESORDERS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function salesOrdersSetOneData(object) {
  return {
    type: SALESORDERS_SET_ONE_DATA,
    payload: object,
  };
}
export function salesOrdersGetCheckSuccess(bool = false) {
  return {
    type: SALESORDERS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function salesOrdersSetData(object) {
  return {
    type: SALESORDERS_SET_DATA,
    payload: object,
  };
}

export function salesOrdersUpdateCheckSuccess(bool = false) {
  return {
    type: SALESORDERS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

