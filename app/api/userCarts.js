import {

  userCartsPostCheckSuccess,
  userCartsSetCount,
  userCartsCountCheckSuccess,
  userCartsDeleteCheckSuccess,
  userCartsFindOneCheckSuccess,
  userCartsSetOneData,
  userCartsGetCheckSuccess,
  userCartsSetData,
  userCartsUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

//create
export function userCartsPostRequest(element) {
  console.log(element, "usercartdata");
  return dispatch => {
    dispatch(userCartsPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `carts`,
      data: element,
      headers: {
        //  Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        console.log("cartpost", response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("cartpost", data)
        dispatch(userCartsSetData(data));
        dispatch(userCartsPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(userCartsPostCheckSuccess(false));

      });
  };
}

//count
export function cartsCountRequest(element) {
  return dispatch => {
    dispatch(userCartsCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `carts/count`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(userCartsSetCount(data));
        dispatch(userCartsCountCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(userCartsCountCheckSuccess(false));

      });
  };
}

//delete
export function userCartsDeleteRequest(id) {
  console.log("cart id for del", id);
  return dispatch => {
    dispatch(userCartsDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `carts/${id}`,
      headers: {
        // Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },

    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data)
        dispatch(userCartsDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(userCartsDeleteCheckSuccess(false));

      });
  };
}

//findOne
export function cartsFindOneRequest(id) {
  return dispatch => {
    dispatch(userCartsFindOneCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `carts/${id}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(cartsOneData(data));
        dispatch(userCartsFindOneCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin,)
        dispatch(userCartsFindOneCheckSuccess(false));
      });
  };
};

//find - get
export function userCartsGetRequest(obj) {
  // const object = {};
  // object.user = id
  // console.log(object)
  return dispatch => {
    dispatch(userCartsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `carts`,
      params: obj,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",

      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("cartdata", data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(userCartsSetData(data));
        dispatch(userCartsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(userCartsGetCheckSuccess(false));
      });
  };
};

//update
export function userCartsUpdateRequest(element, id) {

  return dispatch => {
    dispatch(userCartsUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      data: element,
      url: `carts/${id}`,
      headers: {
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("CartDataresponse", data)
        dispatch(userCartsSetOneData(data));
        dispatch(userCartsUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(userCartsUpdateCheckSuccess(false));

      });
  };
}



