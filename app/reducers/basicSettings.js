import {
  BASICSETTINGS_POST_CHECK_SUCCESS,
  BASICSETTINGS_SET_COUNT,
  BASICSETTINGS_COUNT_CHECK_SUCCESS,
  BASICSETTINGS_DELETE_CHECK_SUCCESS,
  BASICSETTINGS_FINDONE_CHECK_SUCCESS,
  BASICSETTINGS_SET_ONE_DATA,
  BASICSETTINGS_GET_CHECK_SUCCESS,
  BASICSETTINGS_SET_DATA,
  BASICSETTINGS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  basicSettingsPostCheckSuccess: false,
  basicSettingsCount: 0,
  basicSettingsCountCheckSuccess : false,
  basicSettingsDeleteCheckSuccess: false,
  basicSettingsFindOneCheckSuccess: false,
  basicSettingsOneData: [],
  basicSettingsGetCheckSuccess: false,
  basicSettingsData: [],
  basicSettingsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case BASICSETTINGS_POST_CHECK_SUCCESS:
      return {
        ...state,
        basicSettingsPostCheckSuccess: action.payload,
      };
      case BASICSETTINGS_SET_COUNT:
      return {
        ...state,
        basicSettingsCount: action.payload,
      };
      case BASICSETTINGS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        basicSettingsCountCheckSuccess: action.payload,
      };
      case BASICSETTINGS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        basicSettingsDeleteCheckSuccess: action.payload,
      };
      case BASICSETTINGS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        basicSettingsFindOneCheckSuccess: action.payload,
          };
      case BASICSETTINGS_SET_ONE_DATA:
      return {
        ...state,
        basicSettingsOneData: action.payload,
          };
      case BASICSETTINGS_GET_CHECK_SUCCESS:
      return {
        ...state,
        basicSettingsGetCheckSuccess: action.payload,
          };
      case BASICSETTINGS_SET_DATA:
      return {
        ...state,
        basicSettingsData: action.payload,
          };
      case BASICSETTINGS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        basicSettingsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
