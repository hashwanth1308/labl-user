import {
  PAYMENTS_POST_CHECK_SUCCESS,
  PAYMENTS_SET_COUNT,
  PAYMENTS_COUNT_CHECK_SUCCESS,
  PAYMENTS_DELETE_CHECK_SUCCESS,
  PAYMENTS_FINDONE_CHECK_SUCCESS,
  PAYMENTS_SET_ONE_DATA,
  PAYMENTS_GET_CHECK_SUCCESS,
  PAYMENTS_SET_DATA,
  PAYMENTS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function paymentsPostCheckSuccess(bool = false) {
  return {
    type: PAYMENTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function paymentsSetCount(object) {
  return {
    type: PAYMENTS_SET_COUNT,
    payload: object,
  };
}
export function paymentsCountCheckSuccess(bool = false) {
  return {
    type: PAYMENTS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function paymentsDeleteCheckSuccess(bool = false) {
  return {
    type: PAYMENTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function paymentsFindOneCheckSuccess(bool = false) {
  return {
    type: PAYMENTS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function paymentsSetOneData(object) {
  return {
    type: PAYMENTS_SET_ONE_DATA,
    payload: object,
  };
}
export function paymentsGetCheckSuccess(bool = false) {
  return {
    type: PAYMENTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function paymentsSetData(object) {
  return {
    type: PAYMENTS_SET_DATA,
    payload: object,
  };
}

export function paymentsUpdateCheckSuccess(bool = false) {
  return {
    type: PAYMENTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

