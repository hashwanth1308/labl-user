import React, { useEffect, useState } from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
import { BaseStyle, useTheme, Images } from '@config';
import { Header, SafeAreaView, Icon, Text, TextInput, Button, Image } from '@components';
import { useTranslation } from 'react-i18next';
import styles from './styles';
import {
  vendorsCountRequest, vendorsGetRequest, vendorsFindOneRequest,
  vendorsPostRequest, vendorsDeleteRequest,
  vendorsUpdateRequest
} from '../../api/vendors';
import axios from 'axios';
import { modelFollowingPostRequest, modelFollowingGetRequest, modelFollowingDeleteRequest } from '../../api/following';
import { useNavigation } from '@react-navigation/native';

export default function VendorsScreenLABL({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const themeStorage = useSelector(state => state.application.theme);

  const addressData = useSelector(state => state.vendorsReducer.vendorsData);

  const dispatch = useDispatch();
  const [card, setCard] = useState('');
  const [valid, setValid] = useState('');
  const [digit, setDigit] = useState('');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log(UserData);

  const followcheck = useSelector(state => state.followingReducer.modelFollowingPostCheckSuccess);
  const followdelete = useSelector(state => state.followingReducer.modelFollowingDeleteCheckSuccess);
  // useEffect(() => {
  //     dispatch(vendorsCountRequest());

  // }, [dispatch])

  useEffect(() => {
    dispatch(vendorsGetRequest());

  }, [dispatch])
  useEffect(() => {
    dispatch(modelFollowingGetRequest(UserData?.user?.model?.id));

  }, [dispatch])


  // const onFindOneVendors = () => {
  //   setLoading(true);
  //   dispatch(vendorsFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };


  // const onAddVendors = () => {
  //   setLoading(true);
  //   dispatch(vendorsPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteVendors = () => {
  //   setLoading(true);
  //   dispatch(vendorsDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateVendors = () => {
  //   setLoading(true);
  //   dispatch( vendorsUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };


  // const [vendorId, setVendorId] = useState('');
  // console.log(vendorId);
  const vendors = useSelector(state => state.vendorsReducer.vendorsOneData);
  console.log("vendors",vendors);

  const data = [
    {
      id: 1,
      name: "ABCDEF",
      image: Images.profile1
    },
    {
      id: 1,
      name: "ABCDEF",
      image: Images.profile1
    },
    {
      id: 1,
      name: "ABCDEF",
      image: Images.profile1
    },
    {
      id: 1,
      name: "ABCDEF",
      image: Images.profile1
    },
    {
      id: 1,
      name: "ABCDEF",
      image: Images.profile1
    },
    {
      id: 1,
      name: "ABCDEF",
      image: Images.profile1
    },
  ]

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev)=>(!prev))
    ), 1000)
  }, [followcheck,followdelete]);
 
  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ padding: 20 }}>
            <View style={{ alignItems: "center" }}>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, marginVertical: 10 }}>{vendors.length}  Vendors</Text>
              <View style={{ borderWidth: 1, borderColor: "#000", width: 180 }}></View>
            </View>
            <FlatList
              vertical
              data={vendors}
              // style={{ margin: 10 }}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => {
                return (
                  <Component name={item?.user?.username} object={item} />
                )
              }}
            />
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height:height,backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor:"#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  )
}

function Component(props) {
  const { name, object } = props;
  console.log("object",object);
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log(UserData);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [follow, setFollow] = useState(false);
  const modelfollowings = useSelector(state => state.followingReducer.modelFollowingData);
  console.log(modelfollowings);
  const [followId, setFollowId] = useState("")
  useEffect(() => {
    const followed = _.find(modelfollowings, function (o) { return o.vendor?.id === object.id; });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true)
    }
  }, []);

  useEffect(() => {
    const followed = _.find(modelfollowings, function (o) { return o.vendor?.id === object.id; });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true)
    }
  }, [modelfollowings]);
  const handleClick = (object) => {
    if (follow) {
      setFollow(false)
      dispatch(modelFollowingDeleteRequest(followId));
    }
    else {
      setFollow(true);
      let objectValue = {};
      objectValue.model = UserData?.user?.model?.id,
        objectValue.vendor = object?.id,
        objectValue.isFollowing = true
      dispatch(modelFollowingPostRequest(objectValue));
    }

  }
  return (
    <View style={{ flex: 1, flexDirection: "row", justifyContent: "space-between", marginVertical: 20 }}>
      {/* <View style={{ flex: 0.8, flexDirection: "row",justifyContent:"space-around" }}> */}
      <Image source={{uri : object?.user?.image ? object?.user?.image : "https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png"}} style={{ height: 40, width: 40, borderRadius: 30, alignSelf: "center" }} />
      <TouchableOpacity onPress={() => navigation.navigate("VendorProfile", { object })} style={{ flexDirection: "column", justifyContent: "center", alignItems: "center", marginLeft: -50 }}>
        <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, textAlign: "left", width: 100 }}>{name}</Text>
        <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 14, textAlign: "left", width: 100 }}>Vendor</Text>
      </TouchableOpacity>
      {/* </View> */}
      {follow ?
        <TouchableOpacity onPress={() => handleClick(object)} style={{ width: 120, height: 30, borderRadius: 10, borderWidth: 1, alignSelf: "center", alignItems: "center", justifyContent: "center" }}>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000" }}>Following</Text>
        </TouchableOpacity> :
        <TouchableOpacity onPress={() => handleClick(object)} style={{ width: 120, height: 30, backgroundColor: "#000", borderRadius: 10, alignSelf: "center", alignItems: "center", justifyContent: "center" }}>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff" }}>Follow</Text>
        </TouchableOpacity>}
    </View>
  )
}