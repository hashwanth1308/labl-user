import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  contain: {
    flexDirection: 'column',

    // paddingTop: 5,
    // paddingBottom: 5,
  },
  thumb: {width: 64, height: 64, marginRight: 15,margin:20, borderRadius: 34},
  content: {
    flex: 1,
    flexDirection: 'row',
  },
  left: {
    flex: 7.5,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  right: {
    flex: 2.5,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    // height:300,
    // backgroundColor:"#000",
    margin:0,
  },
  contain1: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    height:620,
    backgroundColor:"#CDCDCD",
    padding: 0,
    // flex: 1,
  },
  contentSwipeDown: {
    paddingTop: 20,
    height:100,
    alignItems: 'center',
  },
  lineSwipeDown: {
    width: 40,
    height: 3.5,
    borderRadius:20,
    backgroundColor: "#CDCDCD",
    bottom:10
  },
});
