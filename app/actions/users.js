import {
  USERS_POST_CHECK_SUCCESS,
  FP_POST_CHECK_SUCCESS,
  USERS_SET_COUNT,
  USERS_COUNT_CHECK_SUCCESS,
  USERS_DELETE_CHECK_SUCCESS,
  USERS_FINDONE_CHECK_SUCCESS,
  USERS_SET_ONE_DATA,
  USERS_GET_CHECK_SUCCESS,
  USERS_SET_DATA,
  FP_SET_DATA,
  USERS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function usersPostCheckSuccess(bool = false) {
  return {
    type: USERS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}
export function fpPostCheckSuccess(bool = false) {
  return {
    type: FP_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function usersSetCount(object) {
  return {
    type: USERS_SET_COUNT,
    payload: object,
  };
}
export function usersCountCheckSuccess(bool = false) {
  return {
    type: USERS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function usersDeleteCheckSuccess(bool = false) {
  return {
    type: USERS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function usersFindOneCheckSuccess(bool = false) {
  return {
    type: USERS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function usersSetOneData(object) {
  return {
    type: USERS_SET_ONE_DATA,
    payload: object,
  };
}
export function usersGetCheckSuccess(bool = false) {
  return {
    type: USERS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function usersSetData(object) {
  return {
    type: USERS_SET_DATA,
    payload: object,
  };
}
export function fpSetData(object) {
  return {
    type: FP_SET_DATA,
    payload: object,
  };
}

export function usersUpdateCheckSuccess(bool = false) {
  return {
    type: USERS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

