import {
  SUB_CATEGORIES_POST_CHECK_SUCCESS,
  SUB_CATEGORIES_SET_COUNT,
  SUB_CATEGORIES_COUNT_CHECK_SUCCESS,
  SUB_CATEGORIES_DELETE_CHECK_SUCCESS,
  SUB_CATEGORIES_FINDONE_CHECK_SUCCESS,
  SUB_CATEGORIES_SET_ONE_DATA,
  SUB_CATEGORIES_GET_CHECK_SUCCESS,
  SUB_CATEGORIES_SET_DATA,
  SUB_CATEGORIES_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  subCategoriesPostCheckSuccess: false,
  subCategoriesCount: 0,
  subCategoriesCountCheckSuccess : false,
  subCategoriesDeleteCheckSuccess: false,
  subCategoriesFindOneCheckSuccess: false,
  subCategoriesOneData: [],
  subCategoriesGetCheckSuccess: false,
  subCategoriesData: [],
  subCategoriesUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUB_CATEGORIES_POST_CHECK_SUCCESS:
      return {
        ...state,
        subCategoriesPostCheckSuccess: action.payload,
      };
      case SUB_CATEGORIES_SET_COUNT:
      return {
        ...state,
        subCategoriesCount: action.payload,
      };
      case SUB_CATEGORIES_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        subCategoriesCountCheckSuccess: action.payload,
      };
      case SUB_CATEGORIES_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        subCategoriesDeleteCheckSuccess: action.payload,
      };
      case SUB_CATEGORIES_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        subCategoriesFindOneCheckSuccess: action.payload,
          };
      case SUB_CATEGORIES_SET_ONE_DATA:
      return {
        ...state,
        subCategoriesOneData: action.payload,
          };
      case SUB_CATEGORIES_GET_CHECK_SUCCESS:
      return {
        ...state,
        subCategoriesGetCheckSuccess: action.payload,
          };
      case SUB_CATEGORIES_SET_DATA:
      return {
        ...state,
        subCategoriesData: action.payload,
          };
      case SUB_CATEGORIES_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        subCategoriesUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
