import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import {
  Image,
  Icon,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  EventCard,
  Header,
  CartCard,
  TextInput,
  RangeSlider
} from '@components';
import _ from 'lodash';
import Slider from '@react-native-community/slider';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import Icon1 from 'react-native-vector-icons/Fontisto';
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { productsGetRequestUsingCollectionId } from '../../api/products';
import { subCategoriesFindOneRequest, subCategoriesGetRequest } from '../../api/subCategories';
import { useSelector, useDispatch } from 'react-redux';
import Modal from 'react-native-modal';
import { colorGetRequest, modelProductsGetRequest, sizeGetRequest } from '../../api';

export default function CollectionlistUserSearch({ navigation, route }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { colors } = useTheme();
  const [imageData] = useState([]);
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const [adults, setAdults] = useState(2);
  const { productObject } = route.params;
  console.log("collection", productObject);
  const id = productObject?.id;

  useEffect(() => {
    dispatch(modelProductsGetRequest());
  }, [dispatch]);

  useEffect(() => {
    dispatch(subCategoriesFindOneRequest(id))
  }, [dispatch])
  useEffect(() => {
    dispatch(subCategoriesGetRequest())
  }, [dispatch])
  useEffect(() => {
    dispatch(sizeGetRequest())
  }, [dispatch])
  useEffect(() => {
    dispatch(colorGetRequest())
  }, [dispatch])
  const color = useSelector(state => state.sizeReducer.colorData);
  const sizes = useSelector(state => state.sizeReducer.sizeData);
  console.log("color", color);
  console.log("sizes", sizes);

  const productsData = useSelector(
    state => state.modelProductsReducer.modelProductsData,
  );

  const subCategories = useSelector(state => state.subCategoriesReducer.subCategoriesOneData);
  console.log("subCategories", subCategories);

  const [modalVisible, setModalVisible] = useState(false);
  const toggleModal = () => {
    setModalVisible(!modalVisible);
    // setCheckList([]);
  };

  // const searchitems = productsData?.filter(item => )

  const [menuItems, setMenuItems] = useState([
    { id: '1', name: 'Size', },
    { id: '2', name: 'Color', },
    { id: '3', name: 'Price', },
    { id: '4', name: 'Brand', },
    { id: '5', name: 'Category', },
  ])
  const [selectedsize, setSelectedsize] = useState([]);
  console.log("selected", selectedsize);

  const handlePressSize = (id) => {
    const selectedIndex = selectedsize.indexOf(id);
    if (selectedIndex === -1) {
      setSelectedsize([...selectedsize, id]);
    } else {
      selectedsize.splice(selectedIndex, 1);
      setSelectedsize([...selectedsize]);
    }
  };

  const [selectedcolor, setSelectedcolor] = useState([]);
  const handlePressColor = (id) => {
    const selectedIndex = selectedcolor.indexOf(id);
    if (selectedIndex === -1) {
      setSelectedcolor([...selectedcolor, id]);
    } else {
      selectedcolor.splice(selectedIndex, 1);
      setSelectedcolor([...selectedcolor]);
    }
  };
  const [selectedBrand, setSelectedBrand] = useState([]);
  const handlePressBrand = (id) => {
    const selectedIndex = selectedBrand.indexOf(id);
    if (selectedIndex === -1) {
      setSelectedBrand([...selectedBrand, id]);
    } else {
      selectedBrand.splice(selectedIndex, 1);
      setSelectedBrand([...selectedBrand]);
    }
  };

  const [selectedItem, setSelectedItem] = useState('1')


  const [priceBegin, setPriceBegin] = useState(0);
  const [priceEnd, setPriceEnd] = useState(1000);

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const [loading, setLoading] = useState(false);
  const deltaY = new Animated.Value(0);
  // useEffect(() => {
  //   dispatch(productsGetRequestUsingCollectionId(item.id));

  // }, [dispatch])
  const productsDataFromSelectedCollection = useSelector(state => state.productsReducer.productsDataFromSelectedCollection);
  console.log("productsDataFromSelectedCollection", productsDataFromSelectedCollection);
  const selectedProducts = async (object) => {
    setLoading(true);
    // await dispatch(collectionsPostRequest(cName, profileImage,labelId));
    navigation.navigate('PostDetail', { object })
    setLoading(false);
  };
  const renderIconService = () => {
    return (
      <FlatList
        numColumns={2}
        data={subCategories?.modelProducts}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              style={{ margin: 8, borderColor: "#CDCDCD", height: 310, width: 180, padding: 5, borderWidth: 1 }}
              activeOpacity={0.9}
              onPress={() => selectedProducts(item)}
            // onPress={() => {
            //   navigation.navigate('HotelDetail');
            // }}
            >
              <View
                style={[styles.iconContent]}>

                <Image
                  source={{
                    uri:
                      item?.imagesOfProductByModel ? item?.imagesOfProductByModel[0]?.url : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsQ-YHX2i3RvTDDmpfnde4qyb2P8up7Wi3Ww&usqp=CAU'
                  }}
                  //source={item?.productImages ? `https://staging-backend.labl.store` + item?.productImages[0]?.url : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsQ-YHX2i3RvTDDmpfnde4qyb2P8up7Wi3Ww&usqp=CAU'} 
                  style={{ height: 180, width: 170, borderRadius: 0 }}
                  onPress={() => selectedProducts(item)} />

              </View>
              <View style={styles.content}>
                <View style={styles.left}>
                  <Text numberOfLines={1} style={{ fontFamily: "Montserrat-Bold", fontSize: 16, width: 150 }}>
                    {item?.product?.productName}
                  </Text>
                  <Text
                    style={{ fontFamily: "Montserrat-Regular", fontSize: 18, width: 150, marginVertical: 10 }}>
                    ₹ {item?.product?.productPrice}
                  </Text>

                  {/* <Text
                    note
                    numberOfLines={1}
                    footnote
                    grayColor
                  >
                    {item.productPrice}
                  </Text> */}
                </View>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", padding: 10 }}>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#000" }}>4.6</Text>
                <Icon1 name="star" size={12} color="#000" style={{ marginHorizontal: 5 }} />
                <View style={{ borderWidth: 0.6, borderColor: "#000", height: 20, marginHorizontal: 10 }}></View>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#000" }}>82</Text>
              </View>
            </TouchableOpacity>
          )
        }}
      />
    );
  };

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);


  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('My Orders')} 
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flex: 1, width: 300, justifyContent: "space-between", flexDirection: "row" }}>
              {/* <View style={{ flexDirection: "row", width: 300, justifyContent: "space-evenly", }}> */}
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center", alignSelf: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
              <Text style={{ width: 220, alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", right: 40 }}>{productObject?.subCategoryName}</Text>
              {/* </View> */}

            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        <ScrollView
          onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
          scrollEventThrottle={8}>
          <View style={{}}>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
              <TouchableOpacity onPress={toggleModal} style={{ flexDirection: "row", marginVertical: 10, flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Icon2 name="filter" size={20} color="#000" />
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20, color: "#000", marginHorizontal: 10 }}>Filter</Text>
              </TouchableOpacity>
              <View style={{ height: 30, width: 1, borderWidth: 1, borderColor: "#CDCDCD" }} />
              <TouchableOpacity style={{ flexDirection: "row", marginVertical: 10, flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Icon name="sort" size={20} color="#000" />
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20, color: "#000", marginHorizontal: 10 }}>Sort</Text>
              </TouchableOpacity>
            </View>
            <View style={{ width: "100%", borderWidth: 1, borderColor: "#CDCDCD" }} />
            {renderIconService()}
          </View>
          <View>
            <Modal
              isVisible={modalVisible}
              onBackdropPress={() => setModalVisible(false)}
              onSwipeComplete={() => setModalVisible(false)}
              swipeDirection={['down']}
              style={styles.bottomModal}>
              <View style={styles.contain}>
                <TouchableOpacity
                  onPress={toggleModal}
                  style={{
                    height: 50,
                    width: '100%',
                    backgroundColor: '#fff',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    padding: 10,
                  }}>
                  <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 18 }}>
                    Filters
                  </Text>
                  <Icon2 name="close" size={28} />
                </TouchableOpacity>
                <View style={{width:"100%",borderWidth:0.5,borderColor:"#CDCDCD"}} />
                <View style={styles.content1}>
                  <View style={styles.menuColumn}>
                    {menuItems.map(
                      (item, index) => {
                        return (
                          <TouchableOpacity key={item.id} onPress={() => setSelectedItem(item.id)} style={[styles.menuItem, item.id === selectedItem ? styles.selectedMenuItem : null]}>
                            <Text style={{
                              marginLeft: 10,
                              alignSelf: 'flex-start',
                              color: item.id === selectedItem ? "#000": "#fff",
                              fontSize: 16,
                              fontWeight: 'bold',
                            }}>{item.name}</Text>
                          </TouchableOpacity>
                        )
                      }
                    )
                    }
                  </View>
                  <View style={styles.settingsColumn}>
                    {
                      selectedItem === '1' &&
                      // <View style={styles.settingsView} >
                      <FlatList
                        data={sizes}
                        extraData={selectedsize}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => (
                          <View style={{ marginVertical: 15 }}>
                            <TouchableOpacity
                              onPress={() => handlePressSize(item?.id)}
                              key={item.id} style={{ flexDirection: "row", justifyContent: "space-around", width: "60%", alignItems: "center" }}>
                              <Icon3 name="check" size={20} color={selectedsize?.includes(item.id) ? "green" : "#CDCDCD"} />
                              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 15, textAlign: "left", width: "50%", fontWeight: selectedsize?.includes(item.id) ? "bold" : "normal" }}>{item.type}</Text>
                            </TouchableOpacity>
                            <View style={{ width: "100%", borderWidth: 0.5, borderColor: "#CDCDCD" }}></View>
                          </View>
                        )}
                      />
                      // </View>
                    }
                    {
                      selectedItem === '2' &&
                      // <View style={styles.settingsView} >
                      <FlatList
                        data={color}
                        extraData={selectedcolor}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => (
                          <View style={{ marginVertical: 15 }}>
                            <TouchableOpacity
                              onPress={() => handlePressColor(item.id)}
                              key={item.id} style={{ flexDirection: "row", justifyContent: "space-around", width: "60%", alignItems: "center" }}>
                              <Icon3 name="check" size={20} color={selectedcolor?.includes(item.id) ? "green" : "#CDCDCD"} />
                              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 15, textAlign: "left", width: "50%", fontWeight: selectedcolor?.includes(item.id) ? "bold" : "normal" }}>{item.color}</Text>
                            </TouchableOpacity>
                            <View style={{ width: "100%", borderWidth: 0.5, borderColor: "#CDCDCD" }}></View>
                          </View>
                        )}
                      />
                      // </View>
                    }
                    {
                      selectedItem === '3' &&
                      <View>
                        <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, alignSelf: "flex-start", marginVertical: 10 }}>
                          Selected price range
                        </Text>
                        <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, alignSelf: "flex-start", marginVertical: 10 }}>
                          ₹{priceBegin} - ₹{priceEnd}
                        </Text>
                        <RangeSlider
                          min={100}
                          max={1000}
                          style={{ marginVertical: 10 }}
                          color={colors.border}
                          selectionColor={colors.primary}
                          onValueChanged={(low, high) => {
                            setPriceBegin(low);
                            setPriceEnd(high);
                          }}
                        />

                      </View>
                    }
                    {
                      selectedItem === '4' &&
                      <View>
                        <FlatList
                          data={productsData}
                          extraData={selectedBrand}
                          keyExtractor={item => item.id}
                          renderItem={({ item }) => (
                            <View style={{ marginVertical: 15 }}>
                              <TouchableOpacity
                                onPress={() => handlePressBrand(item.id)}
                                key={item.id} style={{ flexDirection: "row", justifyContent: "flex-start", width: "100%", alignItems: "center" }}>
                                <Icon3 name="check" size={20} color={selectedBrand?.includes(item.id) ? "green" : "#CDCDCD"} />
                                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 15, marginHorizontal: 20, fontWeight: selectedBrand?.includes(item.id) ? "bold" : "normal" }}>{item?.product?.productBrandName}</Text>
                              </TouchableOpacity>
                              <View style={{ width: "100%", borderWidth: 0.5, borderColor: "#CDCDCD" }}></View>
                            </View>
                          )}
                        />
                      </View>
                    }
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: "flex-end",
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    // height: 80,
                    flex: 0,
                    width: '100%',
                    padding: 10,
                    backgroundColor: '#000',
                  }}>
                  <TouchableOpacity
                    style={{
                      // backgroundColor: '#D9D9D9',
                      flex: 0.5,
                      padding: 10,
                      // borderRadius: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 17, color: "#fff" }}>
                      Clear filters
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      // backgroundColor: '#000',
                      flex: 0.5,
                      padding: 10,
                      // borderRadius: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 17,
                        color: '#fff',
                      }}>
                      Apply
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </View>
        </ScrollView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}

