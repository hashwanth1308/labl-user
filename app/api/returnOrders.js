import{ 
    
  returnOrdersPostCheckSuccess,
  returnOrdersSetCount,
  returnOrdersCountCheckSuccess,
  returnOrdersDeleteCheckSuccess,
  returnOrdersFindOneCheckSuccess,
  returnOrdersSetOneData,
  returnOrdersGetCheckSuccess,
  returnOrdersSetData,
  returnOrdersUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function returnOrdersPostRequest(element) {
      return dispatch => {
        dispatch(returnOrdersPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `return-orders`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(returnOrdersSetData(a));
            dispatch(returnOrdersPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(returnOrdersPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function returnOrdersCountRequest(element) {
      return dispatch => {
          dispatch(returnOrdersCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `return-orders/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(returnOrdersSetCount(data));
              dispatch(returnOrdersCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(returnOrdersCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function returnOrdersDeleteRequest(id) {
      
      return dispatch => {
          dispatch(returnOrdersDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `return-orders/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(returnOrdersDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(returnOrdersDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function returnOrdersFindOneRequest(id) {
      return dispatch => {
        dispatch(returnOrdersFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `return-orders/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(returnOrdersOneData(data));
            dispatch(returnOrdersFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(returnOrdersFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function returnOrdersGetRequest(element) {
  return dispatch => {
    dispatch(returnOrdersGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `return-orders`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(returnOrdersSetOneData(data));
        dispatch(returnOrdersGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(returnOrdersGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function returnOrdersUpdateRequest(id) {
     
      return dispatch => {
          dispatch(returnOrdersUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `return-orders/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(returnOrdersSetData(data));
              dispatch(returnOrdersUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(returnOrdersUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 