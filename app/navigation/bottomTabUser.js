import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useSelector } from 'react-redux';
import { BaseColor, useTheme, useFont } from '@config';
import { useTranslation } from 'react-i18next';
import { Icon } from '@components';
import Icon1 from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import Icon3 from 'react-native-vector-icons/Feather';
import Cart from '@screens/Cart';
import Wishlist from '@screens/Wishlist';
import HomeScreenUserLABL from '@screens/HomeScreenUserLABL';
import SignIn from '@screens/SignIn';
import FollowersScreen from '@screens/FollowersScreenUserLABL';
import CategoriesScreenUserLABL from '@screens/CategoriesScreenUserLABL';

const BottomTab = createBottomTabNavigator();

export default function BottomTabNavigator() {
    const { colors } = useTheme();
    const font = useFont();
    const { t } = useTranslation()
    const auth = useSelector(state => state.auth);
    const login = auth.login.success;
    const accessToken = useSelector(
        state => state.accessTokenReducer.accessToken,
    );
    return (
        <BottomTab.Navigator
            initialRouteName={"HomeScreenUserLABL"}
            screenOptions={{
                headerShown: false,
                tabBarActiveTintColor: colors.primary,
                inactiveTintColor: BaseColor.grayColor,
                tabBarShowLabel: true,
                tabBarShowIcon: true,
                tabBarLabelStyle: {
                    fontSize: 12,
                    fontFamily: font,
                },
                tabBarStyle: { borderTopWidth: 1 },
            }}>
            <BottomTab.Screen
                name="HomeScreenUserLABL"
                component={accessToken ? HomeScreenUserLABL : SignIn}
                // component={HomeScreenUserLABL}
                //   options={{
                //     tabBarStyle: { display: "none" },
                //  }}

                options={{
                    tabBarStyle: accessToken ? { display: 'flex' } : { display: 'none' },
                    // tabBarStyle: { display: "flex" },
                    // tabBarVisible: false,
                    // tabBarVisible: login ? false : true,
                    title: t('Home'),
                    tabBarIcon: ({ color }) => {
                        return <Icon2 name="home" color={color} size={25} solid />;
                    },
                }}
            />
            <BottomTab.Screen
                name="fav"
                component={CategoriesScreenUserLABL}
                options={{
                    tabBarStyle: accessToken ? { display: 'flex' } : { display: 'none' },

                    title: t('Categories'),
                    tabBarIcon: ({ color }) => {
                        return <Icon1 solid color={color} name="grid" size={20} />;
                    },
                }}
            />
            {/* <BottomTab.Screen
                name="fav1"
                component={FollowersScreen}
                options={{
                    tabBarStyle: accessToken ? { display: 'flex' } : { display: 'none' },
                    title: t('Following'),
                    tabBarIcon: ({ color }) => {
                        return (
                            <Icon1 solid color={color} name="people-sharp" size={26} />
                        );
                    },
                }}
            /> */}
            <BottomTab.Screen
                name="Orders"
                component={Wishlist}
                options={{
                    tabBarStyle: accessToken ? { display: 'flex' } : { display: 'none' },

                    title: t('Wishlist'),
                    tabBarIcon: ({ color }) => {
                        return <Icon1 solid color={color} name="heart" size={24} />;
                    },
                }}
            />
            {/* <BottomTab.Screen
    name="Post"
    component={Post}
    options={{
      title: t('Bag'),
      tabBarIcon: ({color}) => {
        return <Icon color={color} name="shopping-bag" size={20} solid />;
      },
    }}
  /> */}
            <BottomTab.Screen
                name="Cart"
                component={Cart}
                options={{
                    tabBarStyle: accessToken ? { display: 'flex' } : {display:none},
                    title: t('Cart'),
                    tabBarIcon: ({ color }) => {
                        return <Icon solid color={color} name="shopping-bag" size={20} />;
                    },
                }}
            />
        </BottomTab.Navigator>
    );
}
