import { StyleSheet,Dimensions } from 'react-native';
import * as Utils from '@utils';
import {BaseColor} from '@config';

const height = Dimensions.get("window").height;

export default StyleSheet.create({
  btnPromotion: {
    height: 30,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    top: 5,

  },
  btnPromotion1: {
    height: 30,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    top: 10
  },
  line: {
    height: 1,
    bottom: 10
  },
  bottomModal: {
    justifyContent: 'flex-end',
    // height:300,
    // backgroundColor:"#000",
    margin:0,
  },
  contain: {
    alignItems: 'center',
    justifyContent: 'flex-start',
  //  flex:1,
    backgroundColor:"#CDCDCD",
    // padding: 0,
  },
  contentSwipeDown: {
    paddingTop: 20,
    height:100,
    alignItems: 'center',
  },
  lineSwipeDown: {
    width: 40,
    height: 3.5,
    borderRadius:20,
    backgroundColor: "#CDCDCD",
    bottom:10
  },
  tabbar: {
    height: 53,
  },
  tab: {
    flex: 1,
    width:200
  },
  indicator: {
    height: 2,
  },
  imageBanner: {
    height: Utils.scaleWithPixel(130),
    width: Utils.scaleWithPixel(120),
    borderRadius: 10,
    margin:10
  },
  content: {
    // borderRadius: 35,
    borderWidth: 0.5,
    height:Utils.scaleWithPixel(height/2.7),
    width: Utils.scaleWithPixel(290),
    marginBottom:20
  },
});
