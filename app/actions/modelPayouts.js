import {
  MODELPAYOUTS_POST_CHECK_SUCCESS,
  MODELPAYOUTS_SET_COUNT,
  MODELPAYOUTS_COUNT_CHECK_SUCCESS,
  MODELPAYOUTS_DELETE_CHECK_SUCCESS,
  MODELPAYOUTS_FINDONE_CHECK_SUCCESS,
  MODELPAYOUTS_SET_ONE_DATA,
  MODELPAYOUTS_GET_CHECK_SUCCESS,
  MODELPAYOUTS_SET_DATA,
  MODELPAYOUTS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function modelPayoutsPostCheckSuccess(bool = false) {
  return {
    type: MODELPAYOUTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelPayoutsSetCount(object) {
  return {
    type: MODELPAYOUTS_SET_COUNT,
    payload: object,
  };
}
export function modelPayoutsCountCheckSuccess(bool = false) {
  return {
    type: MODELPAYOUTS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelPayoutsDeleteCheckSuccess(bool = false) {
  return {
    type: MODELPAYOUTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelPayoutsFindOneCheckSuccess(bool = false) {
  return {
    type: MODELPAYOUTS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelPayoutsSetOneData(object) {
  return {
    type: MODELPAYOUTS_SET_ONE_DATA,
    payload: object,
  };
}
export function modelPayoutsGetCheckSuccess(bool = false) {
  return {
    type: MODELPAYOUTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelPayoutsSetData(object) {
  return {
    type: MODELPAYOUTS_SET_DATA,
    payload: object,
  };
}

export function modelPayoutsUpdateCheckSuccess(bool = false) {
  return {
    type: MODELPAYOUTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

