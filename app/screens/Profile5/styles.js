import { StyleSheet } from 'react-native';
import { BaseColor } from '@config';
import * as Utils from '@utils';

export default StyleSheet.create({
  location: {
    flexDirection: 'row',
    marginTop: 10,
  },
  contentTag: {
    marginLeft: 20,
    marginTop: 10,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentUser: {
    // paddingHorizontal: 20,
    // flexDirection: 'row',
    marginBottom: 20,
    top: 200,
    borderTopLeftRadius: 70,
    borderTopRightRadius: 70,
    backgroundColor: "#fff"
  },
  imgBanner: {
    height: 800,
    width: '100%',
    position: 'relative',
  },
  imgUser: {
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: BaseColor.whiteColor,
  },
  contentLeftUser: {
    // flex: 1,
    justifyContent: 'center',
    alignItems:"center"
    // marginLeft: 15,
  },
});
