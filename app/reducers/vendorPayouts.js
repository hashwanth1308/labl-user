import {
  VENDORPAYOUTS_POST_CHECK_SUCCESS,
  VENDORPAYOUTS_SET_COUNT,
  VENDORPAYOUTS_COUNT_CHECK_SUCCESS,
  VENDORPAYOUTS_DELETE_CHECK_SUCCESS,
  VENDORPAYOUTS_FINDONE_CHECK_SUCCESS,
  VENDORPAYOUTS_SET_ONE_DATA,
  VENDORPAYOUTS_GET_CHECK_SUCCESS,
  VENDORPAYOUTS_SET_DATA,
  VENDORPAYOUTS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  vendorPayoutsPostCheckSuccess: false,
  vendorPayoutsCount: 0,
  vendorPayoutsCountCheckSuccess : false,
  vendorPayoutsDeleteCheckSuccess: false,
  vendorPayoutsFindOneCheckSuccess: false,
  vendorPayoutsOneData: [],
  vendorPayoutsGetCheckSuccess: false,
  vendorPayoutsData: [],
  vendorPayoutsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case VENDORPAYOUTS_POST_CHECK_SUCCESS:
      return {
        ...state,
        vendorPayoutsPostCheckSuccess: action.payload,
      };
      case VENDORPAYOUTS_SET_COUNT:
      return {
        ...state,
        vendorPayoutsCount: action.payload,
      };
      case VENDORPAYOUTS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        vendorPayoutsCountCheckSuccess: action.payload,
      };
      case VENDORPAYOUTS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        vendorPayoutsDeleteCheckSuccess: action.payload,
      };
      case VENDORPAYOUTS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        vendorPayoutsFindOneCheckSuccess: action.payload,
          };
      case VENDORPAYOUTS_SET_ONE_DATA:
      return {
        ...state,
        vendorPayoutsOneData: action.payload,
          };
      case VENDORPAYOUTS_GET_CHECK_SUCCESS:
      return {
        ...state,
        vendorPayoutsGetCheckSuccess: action.payload,
          };
      case VENDORPAYOUTS_SET_DATA:
      return {
        ...state,
        vendorPayoutsData: action.payload,
          };
      case VENDORPAYOUTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        vendorPayoutsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
