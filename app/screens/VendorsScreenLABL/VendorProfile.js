import React, { useEffect, useState } from 'react';
import { View, ScrollView, Animated, ImageBackground, TouchableOpacity, FlatList, Dimensions } from 'react-native';
import { Images, useTheme } from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  ProfilePerformanceVendor,
  HotelItem1
} from '@components';
import * as Utils from '@utils';
import styles from './styles';
import { UserData, HotelData } from '@data';
import { useTranslation } from 'react-i18next';
import { labelsGetRequest } from '../../api';
import { useDispatch, useSelector } from 'react-redux';


export default function VendorProfile({ navigation, route }) {
  const deltaY = new Animated.Value(0);
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [hotels] = useState(HotelData);
  const dispatch = useDispatch();
  const { object } = route.params;
  console.log("object--->", object);
  const height = Dimensions.get('window');
  const [userData] = useState(UserData[0]);
  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const heightImageBanner = Utils.scaleWithPixel(335, 1);
  const marginTopBanner = heightImageBanner - heightHeader - 120;

  useEffect(() => {
    const obj = {};
    obj.id = object?.user?.label;
    dispatch(labelsGetRequest(obj));
  }, [dispatch]);
  const label = useSelector(
    state => state.labelsReducer.labelsData,
  );
  console.log("label", label);

  const performance = [
    { value: object?.modelfollowings?.length ? object?.modelfollowings?.length : "0", title: object?.modelfollowings?.length >= 2 || object?.modelfollowings?.length === 0 ? 'Followers' : 'Follower' },
    { value: label[0]?.products?.length, title: label[0]?.products?.length > 1 ? 'Posts' : 'Post' },
  ];

  return (
    // <ImageBackground
    //   source={{uri : object?.user?.image}}
    //   style={[
    //     styles.imgBanner,
    //   ]}
    // >
    <View style={{ flex: 1 }}>
      <Header
      title=""
      renderLeft={() => {
        return (
          <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center", margin:0 }}>
            <Icon
              name="arrow-left"
              size={20}
              color={"#000"}
              enableRTL={true}
            />
          </View>
        );
      }}
      onPressLeft={() => {
        navigation.goBack();
      }}
    />

      <SafeAreaView style={{ flex: 1 }} edges={['right', 'left', 'bottom']}>
        <View style={{ flex: 1 }}>
          <ScrollView
            onScroll={Animated.event([
              {
                nativeEvent: {
                  contentOffset: { y: deltaY },
                },
              },
            ])}
            onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
            scrollEventThrottle={8}>

            <View>
              <View>
                <Text style={{fontFamily:"Montserrat-Bold",fontSize:20,alignSelf:"center"}}>{object?.user?.username}</Text>
                <View style={{backgroundColor:"#000",height:30,width:120,borderRadius:20,alignItems:"center",marginVertical:20, justifyContent:"center",alignSelf:"center"}}>
                  <Text style={{fontFamily:"Montserrat-Bold",fontSize:16,color:"#fff"}}>Vendor</Text>
                </View>
              </View>

              <View style={{ marginVertical: 0, flex: 1, padding: 10, margin: 10 }}>
                <ProfilePerformanceVendor data={performance} profileData={performance} />
              </View>
              <View style={{ width: "100%", borderWidth: 0.8, borderColor: "#CDCDCD", marginVertical: 0 }}></View>
              <FlatList
                // contentContainerStyle={{
                //  padding:10
                // }}
                // refreshControl={
                //   <RefreshControl refreshing={refreshing} onRefresh={loadUserData} />
                // }
                numColumns={2}
                data={label[0]?.products}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <HotelItem1
                    
                    style={{ padding: 10,left:10 }}
                    image={item?.productImages[0]?.url}
                  />
                )}
              />
            </View>
          </ScrollView>

        </View>
      </SafeAreaView>

    </View>
  );
}
