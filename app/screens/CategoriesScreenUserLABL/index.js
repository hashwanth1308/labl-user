import React, { useEffect, useState } from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  FlatList,
  TouchableHighlight,
  RefreshControl,
  Animated,
  Dimensions,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { BaseStyle, useTheme } from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  Image,
  TextInput,
  Button,
} from '@components';
import { useTranslation } from 'react-i18next';
import styles from './styles';
import {
  categoriesGetRequest,
  parentCategoriesGetRequest,
} from '../../api/categories';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';

export default function CategoriesScreenUserLABL({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const themeStorage = useSelector(state => state.application.theme);
  useEffect(() => {
    dispatch(categoriesGetRequest());
  }, [dispatch]);
  const addressData = useSelector(
    state => state.collectionsReducer.collectionsData,
  );
  const categoriesData = useSelector(
    state => state.categoriesReducer.categoriesData,
  );

  console.log('categoriesData', categoriesData);
  const dispatch = useDispatch();
  const [refreshing, setRefreshing] = useState(false);
  const [card, setCard] = useState('');
  const [valid, setValid] = useState('');
  const [digit, setDigit] = useState('');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });


  const [expanded, setExpanded] = useState(null);
  const handleClick = i => {
    if (expanded == i) {
      setExpanded(null);
    } else {
      setExpanded(i);
    }
  };
  // useEffect(() => {
  //   dispatch(collectionsCountRequest());

  // }, [dispatch])
  // useEffect(() => {
  //   dispatch(collectionsGetRequest());

  // }, [dispatch])

  // const onFindOneCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onAddCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => setLoading1(prev => !prev), 1000);
  }, []);

  return (
    <View style={{ flex: 1 }}>
      {/* <Header
        title={t('add_payment_method')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ padding: 0 }}>
            {/* <View style={{ flexDirection: "row", marginVertical: 15 }}>
              <Text style={{ fontFamily: "Roboto-Bold", fontSize: 16, marginHorizontal: 10 }}>Categories</Text>
            </View> */}
            {/* <View style={styles.line} /> */}
            <FlatList
              // contentContainerStyle={{
              //   paddingLeft: 5,
              //   paddingRight: 20,
              //   paddingTop: 10,
              // }}
              // numColumns={3}
              refreshControl={
                <RefreshControl
                  colors={[colors.primary]}
                  tintColor={colors.primary}
                  refreshing={refreshing}
                  onRefresh={() => {
                    setRefreshing(true);
                  }}
                />
              }
              horizontal={false}
              data={categoriesData}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <>
                  {expanded == index ? (
                    <TouchableOpacity
                      onPress={() => setExpanded(null)}
                      style={{ flexDirection: 'row' }}>
                      <ImageBackground
                        // onCl={() => handleClick(index)}
                        source={{
                          uri:
                            item?.mobileImage
                              ? item?.mobileImage
                              : '',
                        }}
                        style={{
                          backgroundColor: '#000',
                          height: 100,
                          width: 400,
                          flexDirection: 'column',
                          padding: 30,
                          // justifyContent: 'space-around',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            marginHorizontal: 30,
                            // justifyContent: 'space-evenly',
                          }}>
                          <Text
                            style={{
                              fontFamily: 'Roboto-Bold',
                              fontSize: 18,
                              // textAlign:"center",
                              // alignSelf: 'center',
                              color: '#000',
                            }}>
                            {item?.name}
                          </Text>
                          <Icon1
                            name={'chevron-down'}
                            size={20}
                            color="#000"
                            style={{ alignSelf: 'center' }}
                          />
                        </View>
                        <Text
                          style={{
                            fontFamily: 'Roboto-Regular',
                            fontSize: 16,
                            color: '#000',
                            textAlign: "left",
                            marginHorizontal: 30,
                          }}>
                          {item?.description}
                        </Text>
                      </ImageBackground>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      onPress={() => handleClick(index)}
                      style={{ flexDirection: 'row', marginHorizontal: 40, justifyContent: "center" }}>
                      <ImageBackground
                        // onCl={() => handleClick(index)}
                        source={{
                          uri:
                            item?.mobileImage
                              ? item?.mobileImage
                              : '',
                        }}
                        style={{
                          backgroundColor: '#000',
                          height: 100,
                          width: 400,
                          flexDirection: 'column',
                          padding: 30,
                          // justifyContent: 'space-around',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            marginHorizontal: 30,
                            // justifyContent: 'space-evenly',
                          }}>
                          <Text
                            style={{
                              fontFamily: 'Roboto-Bold',
                              fontSize: 18,
                              // textAlign:"center",
                              // alignSelf: 'center',
                              color: '#000',
                            }}>
                            {item?.name}
                          </Text>
                          <Icon1
                            name={'chevron-down'}
                            size={20}
                            color="#000"
                            style={{ alignSelf: 'center' }}
                          />
                        </View>
                        <Text
                          style={{
                            fontFamily: 'Roboto-Regular',
                            fontSize: 16,
                            color: '#000',
                            textAlign: "left",
                            marginHorizontal: 30,
                          }}>
                          {item?.description}
                        </Text>
                      </ImageBackground>
                    </TouchableOpacity>
                  )}
                  {expanded == index ? (
                    <View
                      style={{
                        flex: 1,
                        width: '100%',
                        // marginHorizontal: 20,
                      }}>
                      {item?.parentCategories.map(item => (
                        <>
                          <TouchableOpacity
                            style={{ flexDirection: "row", margin: 7 }}
                            onPress={() =>
                              navigation.navigate('CollectionsScreenUserLABL', {
                                prodObj: item,
                              })
                            }>
                            {item?.image ?
                              <Image source={{ uri: item?.image }} style={{ height: 25, width: 25, borderRadius: 30 }} /> :
                              <Image source={require('../../assets/images/category.png')} style={{ height: 25, width: 25, borderRadius: 30 }} />}
                            <Text
                              style={{
                                color: '#000',
                                fontFamily: 'Roboto-Medium',
                                fontSize: 16,
                                alignSelf: "center",
                                marginHorizontal: 10,
                              }}>
                              {item?.name}
                            </Text>
                          </TouchableOpacity>
                          <View style={{ height: 1, width: "100%", backgroundColor: "#CDCDCD" }} />
                        </>
                      ))}
                    </View>
                  ) : null}
                </>
              )}
            />
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      {loading1 && (
        <View
          style={{
            width: width,
            height: height,
            backgroundColor: 'rgba(0,0,0,0.8)',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
          }}>
          <View
            style={{
              width: '60%',
              paddingVertical: 20,
              backgroundColor: '#fff',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <View>
              <ActivityIndicator size={'large'} color="#000" />
            </View>
            <Text
              style={{
                marginLeft: 10,
                fontSize: 30,
                color: '#000',
                fontWeight: 'bold',
              }}>
              Loading..
            </Text>
          </View>
        </View>
      )}
    </View>
  );
}
