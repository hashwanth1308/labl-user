import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Image, Text} from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import {useTheme} from '@config';
export default function ListThumbCircle(props) {
  const {colors} = useTheme();
  const {
    style,
    imageStyle,
    image,
    txtLeftTitle,
    txtContent,
    txtRight,
    onPress,
  } = props;
  return (
    <TouchableOpacity
      style={[
        styles.contain,
        { borderBottomColor: colors.border},
        style,
      ]}
      onPress={onPress}
      activeOpacity={0.9}>
      <Image source={{uri : image ? image : "https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png"}} style={[styles.thumb, imageStyle]} />
          <Text
            numberOfLines={1}
            style={{
             fontFamily:"Montserrat-SemiBold",
             fontSize:14,
             textAlign:"center"
            }}>
            {txtContent}
          </Text>
    </TouchableOpacity>
  );
}

ListThumbCircle.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  imageStyle: PropTypes.object,
  image: PropTypes.node.isRequired,
  txtLeftTitle: PropTypes.string,
  txtContent: PropTypes.string,
  txtRight: PropTypes.string,
  onPress: PropTypes.func,
};

ListThumbCircle.defaultProps = {
  style: {},
  imageStyle: {},
  image: '',
  txtLeftTitle: '',
  txtContent: '',
  txtRight: '',
  onPress: () => {},
};
