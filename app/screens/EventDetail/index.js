import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
} from 'react-native';
import {
  Image,
  Icon,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  EventCard,
  Header,
  CartCard,
  TextInput
} from '@components';
import CheckBox from '@react-native-community/checkbox';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { PromotionData, TourData, HotelData, BestData } from '@data';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { cartsGetRequest } from '../../api/carts';
import { productsGetRequest, productsPostRequest } from '../../api/products';
import { color } from 'react-native-reanimated';

export default function EventDetail({ navigation }) {
  const { t } = useTranslation();

  const { colors } = useTheme();
  const [imageData] = useState([]);
  const [adults, setAdults] = useState(2);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(productsGetRequest());

  }, [dispatch])

  useEffect(() => {
    dispatch(cartsGetRequest());

  }, [dispatch])
  const cartsData = useSelector(state => state.cartsReducer.cartsData)
  const productsData = useSelector(state => state.productsReducer.productsData)


  // console.log("cartsData", cartsData);
  // console.log("UserToken", userToken);



  // const onFindOneCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };


  // const onAddCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  //   console.log(cartsData[0], cartsData);

  const [Wishlist] = useState([
    {
      image: Images.event4,
      name: 'MEN',
      description: 'kurta',
      price: '$399',
      route: 'Hotel',
    },
    {
      image: Images.event8,
      name: 'WOMEN',
      description: 'kurta',
      price: '$399',
      route: 'Tour',
    },
    {
      image: Images.event9,
      name: 'KIDS',
      description: 'kurta',
      price: '$399',
      route: 'OverViewCar',
    },
    {
      image: Images.event10,
      name: 'JACKETS',
      description: 'kurta',
      price: '$399',
      route: 'FlightSearch',
    },
    

  ]);

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const deltaY = new Animated.Value(0);
  const App = () => { }
  const [isSelected, setSelection] = useState(false);

  let ImageUrl = imageData ? { imageData } : require('../../assets/images/avata-01.jpeg')

  const renderIconService = () => {
    return (
      <FlatList
        columnWrapperStyle={{ paddingRight: 20, }}
        numColumns={2}
        data={Wishlist}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              style={styles.itemService}
              activeOpacity={0.9}
              onPress={() => {
                navigation.navigate('HotelDetail');
              }}>
              <View
                style={[styles.iconContent,]}>
                <Image source={item.image} style={{ height: 180, width: 140, borderRadius: 8, left: 15,top:10 }} />
              </View>
              <View style={{ flex: 1, alignSelf: "center", }}>
                <Text style={{}}>
                  {t(item.name)}
                </Text>
                <Text style={{}}>
                  {t(item.description)}
                </Text>
                <Text style={{}}>
                  {t(item.price)}
                </Text>
              </View>
            </TouchableOpacity>

          )
        }}
      />
    );
  };
  return (
    <View style={{ flex: 1, top: 30 }}>
      <Header
        title={t('Wishlist')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

        renderRight={() => {
          return <Icon name="shopping-cart" size={24} color={colors.primary} />;
        }}
        onPressRight={() => {
          navigation.navigate('Cart');
        }}
      />

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        <ScrollView
          onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
          scrollEventThrottle={8}>
          <View style={{ paddingHorizontal: 20 }}>

            {renderIconService()}
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

