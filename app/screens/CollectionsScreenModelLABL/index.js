import React, { useEffect, useState } from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Text, Image, TextInput, Button } from '@components';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';
import {
  collectionsCountRequest, collectionsGetRequest, collectionsFindOneRequest,
  collectionsPostRequest,
  collectionsDeleteRequest,
  collectionsUpdateRequest
} from '../../api/collections';

export default function CollectionsScreenModelLABL({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  const { prodObj } = route.params;
  console.log("prodObj", prodObj);
  const themeStorage = useSelector(state => state.application.theme);

  const addressData = useSelector(state => state.collectionsReducer.collectionsData);

  const dispatch = useDispatch();
  const [card, setCard] = useState('');
  const [valid, setValid] = useState('');
  const [digit, setDigit] = useState('');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });

  // useEffect(() => {
  //     dispatch(collectionsCountRequest());

  // }, [dispatch])
  // useEffect(() => {
  //   dispatch(collectionsGetRequest());

  // }, [dispatch])

  // const onFindOneCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };


  // const onAddCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateCollections = () => {
  //   setLoading(true);
  //   dispatch(collectionsUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  const [loading1, setLoading1] = useState(true);
    useEffect(() => {
      setTimeout(() => (
        setLoading1((prev)=>(!prev))
      ), 1000)
    }, []);
  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('My Orders')} 
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flex: 1,width:300, justifyContent: "space-between",flexDirection:"row" }}>
              {/* <View style={{ flexDirection: "row", width: 300, justifyContent: "space-evenly", }}> */}
                <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center",alignSelf:"center" }}>
                  <Icon
                    name="arrow-left"
                    size={20}
                    color={colors.primary}
                    enableRTL={true}
                  />
                </View>
                <Text style={{width:220, alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", right: 40 }}>{prodObj?.name}</Text>
              {/* </View> */}

            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

      // renderRight={() => {
      //   return <Icon name="shopping-cart" size={24} color={colors.primary} />;
      // }}
      // onPressRight={() => {
      //   navigation.navigate('Cart');
      // }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ padding: 20 }}>
            {/* {prodObj?.subCategories?.map((item) => ( */}
            <Component item={prodObj?.subCategories} />
            {/* ))} */}
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height:height,backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor:"#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  )
}

function Component(props) {
  const { item } = props;
  console.log(item);
  const navigation = useNavigation();
  return (
    <FlatList
      // contentContainerStyle={{
      //   paddingLeft: 5,
      //   paddingRight: 20,
      //   paddingTop: 20,
      // }}
      numColumns={3}
      data={item}
      keyExtractor={(item, index) => item.id}
      renderItem={({ item, index }) => (
        <TouchableOpacity onPress={() => navigation.navigate("CollectionlistModelLABL", {productObject:item})} style={{ flexDirection: "column", alignItems: "center",marginHorizontal:20,marginVertical:10 }}>
          <Image source={{uri : item?.image ? item?.image : ""}} style={{ height: 80, width: 80, borderRadius: 50, borderWidth: 1 }} />
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 15,marginVertical:10 }}>{item?.subCategoryName}</Text>
        </TouchableOpacity>
      )}
    />

  )
}