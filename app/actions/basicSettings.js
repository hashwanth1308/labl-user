import {
  BASICSETTINGS_POST_CHECK_SUCCESS,
  BASICSETTINGS_SET_COUNT,
  BASICSETTINGS_COUNT_CHECK_SUCCESS,
  BASICSETTINGS_DELETE_CHECK_SUCCESS,
  BASICSETTINGS_FINDONE_CHECK_SUCCESS,
  BASICSETTINGS_SET_ONE_DATA,
  BASICSETTINGS_GET_CHECK_SUCCESS,
  BASICSETTINGS_SET_DATA,
  BASICSETTINGS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function basicSettingsPostCheckSuccess(bool = false) {
  return {
    type: BASICSETTINGS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function basicSettingsSetCount(object) {
  return {
    type: BASICSETTINGS_SET_COUNT,
    payload: object,
  };
}
export function basicSettingsCountCheckSuccess(bool = false) {
  return {
    type: BASICSETTINGS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function basicSettingsDeleteCheckSuccess(bool = false) {
  return {
    type: BASICSETTINGS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function basicSettingsFindOneCheckSuccess(bool = false) {
  return {
    type: BASICSETTINGS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function basicSettingsSetOneData(object) {
  return {
    type: BASICSETTINGS_SET_ONE_DATA,
    payload: object,
  };
}
export function basicSettingsGetCheckSuccess(bool = false) {
  return {
    type: BASICSETTINGS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function basicSettingsSetData(object) {
  return {
    type: BASICSETTINGS_SET_DATA,
    payload: object,
  };
}

export function basicSettingsUpdateCheckSuccess(bool = false) {
  return {
    type: BASICSETTINGS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

