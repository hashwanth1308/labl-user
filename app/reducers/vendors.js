import {
  VENDORS_POST_CHECK_SUCCESS,
  VENDORS_SET_COUNT,
  VENDORS_COUNT_CHECK_SUCCESS,
  VENDORS_DELETE_CHECK_SUCCESS,
  VENDORS_FINDONE_CHECK_SUCCESS,
  VENDORS_SET_ONE_DATA,
  VENDORS_GET_CHECK_SUCCESS,
  VENDORS_SET_DATA,
  VENDORS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  vendorsPostCheckSuccess: false,
  vendorsCount: 0,
  vendorsCountCheckSuccess: false,
  vendorsDeleteCheckSuccess: false,
  vendorsFindOneCheckSuccess: false,
  vendorsOneData: [],
  vendorsGetCheckSuccess: false,
  vendorsData: [],
  vendorsUpdateCheckSuccess: false,


};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case VENDORS_POST_CHECK_SUCCESS:
      return {
        ...state,
        vendorsPostCheckSuccess: action.payload,
      };
    case VENDORS_SET_COUNT:
      return {
        ...state,
        vendorsCount: action.payload,
      };
    case VENDORS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        vendorsCountCheckSuccess: action.payload,
      };
    case VENDORS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        vendorsDeleteCheckSuccess: action.payload,
      };
    case VENDORS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        vendorsFindOneCheckSuccess: action.payload,
      };
    case VENDORS_SET_ONE_DATA:
      return {
        ...state,
        vendorsOneData: action.payload,
      };
    case VENDORS_GET_CHECK_SUCCESS:
      return {
        ...state,
        vendorsGetCheckSuccess: action.payload,
      };
    case VENDORS_SET_DATA:
      return {
        ...state,
        vendorsData: action.payload,
      };
    case VENDORS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        vendorsUpdateCheckSuccess: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
