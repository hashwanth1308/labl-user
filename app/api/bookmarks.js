import {
  bookmarksGetCheckSuccess,
  bookmarksGetCheckSuccessByUser,
  bookmarksSetData,
  bookmarksSetDataByUser,
  bookmarksPostCheckSuccess,
  bookmarksUpdateCheckSuccess,
  bookmarksDeleteCheckSuccess,
  bookmarksGetCheckSuccessForFeed,
  bookmarksSetDataForFeed,
} from '../actions';
import axios from 'axios';
let a = [];

export function bookmarksGetRequest(element, token) {
  console.log('bookmarks get request');
  return dispatch => {
    dispatch(bookmarksGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `bookmarks`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        // console.log('get bookmarks from api', response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(bookmarksSetData(data));
        dispatch(bookmarksGetCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings');
        dispatch(bookmarksGetCheckSuccess(false));
      });
  };
}
export function bookmarksGetRequestByUser(userId) {
  console.log(userId, "bookmarksuserid");
  return dispatch => {
    dispatch(bookmarksGetCheckSuccessByUser(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `wishlists?user=${userId}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        // console.log('get bookmarks from api', response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data, "response")
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(bookmarksSetDataByUser(data));
        dispatch(bookmarksGetCheckSuccessByUser(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings');
        dispatch(bookmarksGetCheckSuccessByUser(false));
      });
  };
}
export function bookmarksGetRequestByModelUser(userId) {
  console.log(userId, "bookmarksuserid");
  return dispatch => {
    dispatch(bookmarksGetCheckSuccessByUser(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `wishlists?model=${userId}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        // console.log('get bookmarks from api', response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data, "response")
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(bookmarksSetDataByUser(data));
        dispatch(bookmarksGetCheckSuccessByUser(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings');
        dispatch(bookmarksGetCheckSuccessByUser(false));
      });
  };
}
export function bookmarksGetRequestForFeed(userId) {
  const objectvalue = {};
  objectvalue.bookmarkedBy = userId;
  objectvalue.postType = 'feed';

  console.log('bookmarks get request', objectvalue);
  return dispatch => {
    dispatch(bookmarksGetCheckSuccessForFeed(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `book-marks`,
      params: objectvalue,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        // console.log('get bookmarks from api', response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(bookmarksSetDataForFeed(data));
        dispatch(bookmarksGetCheckSuccessForFeed(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings');
        dispatch(bookmarksGetCheckSuccessForFeed(false));
      });
  };
}

export function bookmarksPostRequest(objectValue) {
  console.log('objectValue', objectValue);
  // const bookmarkObject={};
  // // bookmarkObject.addToBookmark = objectValue.addToBookmark;
  // bookmarkObject.bookmarkedBy = objectValue.bookmarkedBy;

  // if(objectValue.postType=="feed"){
  //   bookmarkObject.savedFeed=objectValue.savedFeed;
  //   bookmarkObject.postType=objectValue.postType;
  // }
  // if(objectValue.postType=="video"){
  //   bookmarkObject.savedVideo=objectValue.savedVideo;
  //   bookmarkObject.postType=objectValue.postType;
  // }
  // console.log("bookmarkObject",bookmarkObject);

  return dispatch => {
    // dispatch(bookmarksPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: 'wishlists',
      data: objectValue,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log('response', response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(bookmarksSetData(data));
        console.log(data, "successdata");
        dispatch(bookmarksPostCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(bookmarksPostCheckSuccess(false));
      });
  };
}

export function bookmarksUpdateRequest(element) {
  console.log(element.accessToken);
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(bookmarksSetData(element));
    dispatch(bookmarksUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `book-marks/611baca84161ced881fda003`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(bookmarksSetData(a));
        dispatch(bookmarksUpdateCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(bookmarksUpdateCheckSuccess(false));
      });
  };
}

export function bookmarksDeleteRequest(bookmarkId) {
  return dispatch => {
    dispatch(bookmarksDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `wishlists/${bookmarkId}`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(bookmarksSetData(data));
        dispatch(bookmarksDeleteCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(bookmarksDeleteCheckSuccess(false));
      });
  };
}
export function bookmarksDeleteRequestByUser(bookmarkId) {
  return dispatch => {
    dispatch(bookmarksDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `wishlists/${bookmarkId}`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(bookmarksSetDataByUser(data));
        dispatch(bookmarksDeleteCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(bookmarksDeleteCheckSuccess(false));
      });
  };
}
