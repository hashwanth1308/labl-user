import {
  ADDRESS_POST_CHECK_SUCCESS,
  ADDRESS_SET_COUNT,
  ADDRESS_COUNT_CHECK_SUCCESS,
  ADDRESS_DELETE_CHECK_SUCCESS,
  ADDRESS_FINDONE_CHECK_SUCCESS,
  ADDRESS_SET_ONE_DATA,
  ADDRESS_GET_CHECK_SUCCESS,
  ADDRESS_SET_DATA,
  ADDRESS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {
  addressPostCheckSuccess: false,
  addressCount: 0,
  addressCountCheckSuccess: false,
  addressDeleteCheckSuccess: false,
  addressFindOneCheckSuccess: false,
  addressOneData: [],
  addressGetCheckSuccess: false,
  addressData: [],
  addressUpdateCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADDRESS_POST_CHECK_SUCCESS:
      return {
        ...state,
        addressPostCheckSuccess: action.payload,
      };
    case ADDRESS_SET_COUNT:
      return {
        ...state,
        addressCount: action.payload,
      };
    case ADDRESS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        addressCountCheckSuccess: action.payload,
      };
    case ADDRESS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        addressDeleteCheckSuccess: action.payload,
      };
    case ADDRESS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        addressFindOneCheckSuccess: action.payload,
      };
    case ADDRESS_SET_ONE_DATA:
      return {
        ...state,
        addressOneData: action.payload,
      };
    case ADDRESS_GET_CHECK_SUCCESS:
      return {
        ...state,
        addressGetCheckSuccess: action.payload,
      };
    case ADDRESS_SET_DATA:
      return {
        ...state,
        addressData: action.payload,
      };
    case ADDRESS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        addressUpdateCheckSuccess: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
