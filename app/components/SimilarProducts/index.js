import React from 'react';
import { View, TouchableOpacity, FlatList } from 'react-native';
import { Image, Text, Icon, StarRating, Tag } from '@components';
import { BaseColor, useTheme } from '@config';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import styles from './styles';
export default function SimilarProducts(props) {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const {
    list,
    block,
    grid,
    style,
    image,
    name,
    location,
    price,
    available,
    rate,
    rateStatus,
    onPress,
    onPressTag,
    services,
    rateCount,
    numReviews,
    children,
    styleContent
  } = props;
  return (

    <View style={[styles.listContent, style]}>
    <TouchableOpacity onPress={onPress} activeOpacity={0.9}>
    <Image source={{uri:image?image:'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg'}} style={styles.listImage} />
    </TouchableOpacity>
         <View style={[styles.content, styleContent]}>{children}</View>

  </View>
);
};
SimilarProducts.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  image: PropTypes.node.isRequired,
  list: PropTypes.bool,
  block: PropTypes.bool,
  grid: PropTypes.bool,
  name: PropTypes.string,
  location: PropTypes.string,
  price: PropTypes.string,
  available: PropTypes.string,
  rate: PropTypes.number,
  rateCount: PropTypes.string,
  rateStatus: PropTypes.string,
  numReviews: PropTypes.number,
  services: PropTypes.array,
  onPress: PropTypes.func,
  onPressTag: PropTypes.func,
};

SimilarProducts.defaultProps = {
  style: {},
  image: '',
  list: true,
  block: false,
  grid: false,
  name: '',
  location: '',
  price: '',
  available: '',
  rate: 0,
  rateCount: '',
  rateStatus: '',
  numReviews: 0,
  services: [],
  onPress: () => { },
  onPressTag: () => { },
};
