import {SET_ACCESS_TOKEN} from '../actions/actionTypes';
let initialState = {
  accessToken: null,
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ACCESS_TOKEN:
      return {
        accessToken:null
      };
    default:
      return state;
  }
};

export default reducer;