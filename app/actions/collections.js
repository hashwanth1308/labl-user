import {
  COLLECTIONS_POST_CHECK_SUCCESS,
  COLLECTIONS_SET_COUNT,
  COLLECTIONS_COUNT_CHECK_SUCCESS,
  COLLECTIONS_DELETE_CHECK_SUCCESS,
  COLLECTIONS_FINDONE_CHECK_SUCCESS,
  COLLECTIONS_SET_ONE_DATA,
  COLLECTIONS_GET_CHECK_SUCCESS,
  COLLECTIONS_SET_DATA,
  COLLECTIONS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function collectionsPostCheckSuccess(bool = false) {
  return {
    type: COLLECTIONS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function collectionsSetCount(object) {
  return {
    type: COLLECTIONS_SET_COUNT,
    payload: object,
  };
}
export function collectionsCountCheckSuccess(bool = false) {
  return {
    type: COLLECTIONS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function collectionsDeleteCheckSuccess(bool = false) {
  return {
    type: COLLECTIONS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function collectionsFindOneCheckSuccess(bool = false) {
  return {
    type: COLLECTIONS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function collectionsSetOneData(object) {
  return {
    type: COLLECTIONS_SET_ONE_DATA,
    payload: object,
  };
}
export function collectionsGetCheckSuccess(bool = false) {
  return {
    type: COLLECTIONS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function collectionsSetData(object) {
  return {
    type: COLLECTIONS_SET_DATA,
    payload: object,
  };
}

export function collectionsUpdateCheckSuccess(bool = false) {
  return {
    type: COLLECTIONS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

