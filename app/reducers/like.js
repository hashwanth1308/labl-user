import {
    LIKE_GET_CHECK_SUCCESS,
    LIKE_POST_CHECK_SUCCESS,
    LIKE_UPDATE_CHECK_SUCCESS,
    LIKE_DELETE_CHECK_SUCCESS,
    LIKE_SET_DATA,
    LIKE_SET_DATA_BY_USER,
    LIKE_GET_CHECK_SUCCESS_BY_USER,
    LIKE_GET_CHECK_SUCCESS_FOR_FEED,
    LIKE_SET_DATA_FOR_FEED
} from '../actions/actionTypes';

const initialState = {
    likeGetCheckSuccess: false,
    likeData: [],
    likePostCheckSuccess: false,
    likeUpdateCheckSuccess: false,
    likeDeleteCheckSuccess: false,
    likeGetCheckSuccessByUser: false,
    likeDataByUser: [],
    likeDataForFeed: [],
    likeGetCheckSuccessForFeed: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LIKE_GET_CHECK_SUCCESS:
            return {
                ...state,
                likeGetCheckSuccess: action.payload,
            };
        case LIKE_GET_CHECK_SUCCESS_BY_USER:
            return {
                ...state,
                likeGetCheckSuccessByUser: action.payload,
            };
        case LIKE_GET_CHECK_SUCCESS_FOR_FEED:
            return {
                ...state,
                likeGetCheckSuccessForFeed: action.payload,
            };
        case LIKE_POST_CHECK_SUCCESS:
            return {
                ...state,
                likePostCheckSuccess: action.payload,
            };
        case LIKE_UPDATE_CHECK_SUCCESS:
            return {
                ...state,
                likeUpdateCheckSuccess: action.payload,
            };
        case LIKE_DELETE_CHECK_SUCCESS:
            return {
                ...state,
                likeDeleteCheckSuccess: action.payload,
            };

        case LIKE_SET_DATA:
            return {
                ...state,
                likeData: action.payload,
            };
        case LIKE_SET_DATA_BY_USER:
            return {
                ...state,
                likeDataByUser: action.payload,
            };
        case LIKE_SET_DATA_FOR_FEED:
            return {
                ...state,
                likeDataForFeed: action.payload,
            };

        default:
            return state;
    }
};

export default reducer;
