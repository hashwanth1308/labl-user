import {
  STATUS_POST_CHECK_SUCCESS,
  STATUS_SET_COUNT,
  STATUS_COUNT_CHECK_SUCCESS,
  STATUS_DELETE_CHECK_SUCCESS,
  STATUS_FINDONE_CHECK_SUCCESS,
  STATUS_SET_ONE_DATA,
  STATUS_GET_CHECK_SUCCESS,
  STATUS_SET_DATA,
  STATUS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  statusPostCheckSuccess: false,
  statusCount: 0,
  statusCountCheckSuccess : false,
  statusDeleteCheckSuccess: false,
  statusFindOneCheckSuccess: false,
  statusOneData: [],
  statusGetCheckSuccess: false,
  statusData: [],
  statusUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case STATUS_POST_CHECK_SUCCESS:
      return {
        ...state,
        statusPostCheckSuccess: action.payload,
      };
      case STATUS_SET_COUNT:
      return {
        ...state,
        statusCount: action.payload,
      };
      case STATUS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        statusCountCheckSuccess: action.payload,
      };
      case STATUS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        statusDeleteCheckSuccess: action.payload,
      };
      case STATUS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        statusFindOneCheckSuccess: action.payload,
          };
      case STATUS_SET_ONE_DATA:
      return {
        ...state,
        statusOneData: action.payload,
          };
      case STATUS_GET_CHECK_SUCCESS:
      return {
        ...state,
        statusGetCheckSuccess: action.payload,
          };
      case STATUS_SET_DATA:
      return {
        ...state,
        statusData: action.payload,
          };
      case STATUS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        statusUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
