import{ 
    
  earningsPostCheckSuccess,
  earningsSetCount,
  earningsCountCheckSuccess,
  earningsDeleteCheckSuccess,
  earningsFindOneCheckSuccess,
  earningsSetOneData,
  earningsGetCheckSuccess,
  earningsSetData,
  earningsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function earningsPostRequest(element) {
      return dispatch => {
        dispatch(earningsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `earnings`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(earningsSetData(a));
            dispatch(earningsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(earningsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function earningsCountRequest(element) {
      return dispatch => {
          dispatch(earningsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `earnings/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(earningsSetCount(data));
              dispatch(earningsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(earningsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function earningsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(earningsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `earnings/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(earningsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(earningsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function earningsFindOneRequest(id) {
      return dispatch => {
        dispatch(earningsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `earnings/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(earningsOneData(data));
            dispatch(earningsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(earningsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function earningsGetRequest(element) {
  return dispatch => {
    dispatch(earningsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `earnings`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(earningsSetOneData(data));
        dispatch(earningsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(earningsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function earningsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(earningsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `earnings/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(earningsSetData(data));
              dispatch(earningsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(earningsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 