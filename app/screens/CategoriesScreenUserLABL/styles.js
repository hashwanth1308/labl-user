import { StyleSheet } from 'react-native';
import { BaseColor } from '@config';

export default StyleSheet.create({
  inputItem: {
    flex: 6.5,
    marginLeft: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 46,
    backgroundColor: BaseColor.fieldColor,
    borderRadius: 5,
    padding: 10,
  },
  checkDefault: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopWidth: 1,
    paddingVertical: 15,
    marginTop: 10,
  },
  horizontalLine: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    position: 'relative',
  },
  line: {
    // flex: 1,
    // alignSelf:"center",
    width: "100%",
    height: 1,
    marginBottom:10,
    backgroundColor: '#000',
    // marginRight: 10,
  },
  container: {
    // position: 'absolute',
    // top: -12,
    // left: 0,
    backgroundColor: 'green'
  },
  talkBubble: {
    backgroundColor: "transparent",
    alignSelf: "center",
  },
  talkBubbleSquare: {
    // width: 350,
    height: 1,
    backgroundColor: "red",
    borderRadius: 10,
  },
  triangle: {
    width: 0,
    height: 0,
    // alignSelf:"flex-start",
    backgroundColor: "transparent",
    // borderStyle: "solid",
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderBottomWidth: 15,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#FD1C03',
    borderLeftColor: 'transparent',
  },
});
