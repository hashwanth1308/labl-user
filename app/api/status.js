import {

  statusPostCheckSuccess,
  statusSetCount,
  statusCountCheckSuccess,
  statusDeleteCheckSuccess,
  statusFindOneCheckSuccess,
  statusSetOneData,
  statusGetCheckSuccess,
  statusSetData,
  statusUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

//create
export function statusPostRequest(element) {
  console.log(element);
  return dispatch => {
    dispatch(statusPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `statuses`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      },
      data: element
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(statusSetData(a));
        dispatch(statusPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(statusPostCheckSuccess(false));

      });
  };
}

//count
export function statusCountRequest(element) {
  return dispatch => {
    dispatch(statusCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `statuses/count`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(statusSetCount(data));
        dispatch(statusCountCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(statusCountCheckSuccess(false));

      });
  };
}

//delete
export function statusDeleteRequest(id) {

  return dispatch => {
    dispatch(statusDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `statuses/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },

    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(statusDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(statusDeleteCheckSuccess(false));

      });
  };
}

//findOne
export function statusFindOneRequest(id) {
  return dispatch => {
    dispatch(statusFindOneCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `statuses/${id}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(statusOneData(data));
        dispatch(statusFindOneCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin,)
        dispatch(statusFindOneCheckSuccess(false));
      });
  };
};

//find - get
export function statusGetRequest(element) {
  return dispatch => {
    dispatch(statusGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `statuses`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
        // data: element
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(statusSetOneData(data));
        dispatch(statusGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(statusGetCheckSuccess(false));
      });
  };
};

//update
export function statusUpdateRequest(id) {

  return dispatch => {
    dispatch(statusUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `statuses/${id}`,
      headers: {
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(statusSetData(data));
        dispatch(statusUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(statusUpdateCheckSuccess(false));

      });
  };
}



