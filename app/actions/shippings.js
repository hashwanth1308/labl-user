import {
  SHIPPINGS_POST_CHECK_SUCCESS,
  SHIPPINGS_SET_COUNT,
  SHIPPINGS_COUNT_CHECK_SUCCESS,
  SHIPPINGS_DELETE_CHECK_SUCCESS,
  SHIPPINGS_FINDONE_CHECK_SUCCESS,
  SHIPPINGS_SET_ONE_DATA,
  SHIPPINGS_GET_CHECK_SUCCESS,
  SHIPPINGS_SET_DATA,
  SHIPPINGS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function shippingsPostCheckSuccess(bool = false) {
  return {
    type: SHIPPINGS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function shippingsSetCount(object) {
  return {
    type: SHIPPINGS_SET_COUNT,
    payload: object,
  };
}
export function shippingsCountCheckSuccess(bool = false) {
  return {
    type: SHIPPINGS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function shippingsDeleteCheckSuccess(bool = false) {
  return {
    type: SHIPPINGS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function shippingsFindOneCheckSuccess(bool = false) {
  return {
    type: SHIPPINGS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function shippingsSetOneData(object) {
  return {
    type: SHIPPINGS_SET_ONE_DATA,
    payload: object,
  };
}
export function shippingsGetCheckSuccess(bool = false) {
  return {
    type: SHIPPINGS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function shippingsSetData(object) {
  return {
    type: SHIPPINGS_SET_DATA,
    payload: object,
  };
}

export function shippingsUpdateCheckSuccess(bool = false) {
  return {
    type: SHIPPINGS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

