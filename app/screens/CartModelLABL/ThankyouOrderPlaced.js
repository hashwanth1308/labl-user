import * as React from 'react';
import { View,Dimensions } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Text, Button } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import Lottie from 'lottie-react-native';

export default function ThankyouOrderPlaced({ navigation }) {
    const { t } = useTranslation();
    const { colors } = useTheme();
    const height = Dimensions.get('window').height;
    const animationRef = React.useRef(null)
    // const [loading1, setLoading1] = useState(true);
    React.useEffect(() => {
        animationRef.current?.play()
    
        // Or set a specific startFrame and endFrame with:
        animationRef.current?.play(30, 150);
      }, [])
    React.useEffect(() => {
        setTimeout(() => {
          navigation.replace('BottomTabNavigator1');
        }, 1000);
      }, [navigation]);

    return (
        <View style={{ flex: 1 }}>
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                edges={['right', 'left', 'bottom']}>
               <View style={{ flex: 1, width: "100%", alignItems: "center",justifyContent:"center" }}>
                    <View style={{ borderRadius: 10, justifyContent: "flex-start", alignItems: "center" }}>
                        {/* <Image source={require('../../assets/images/done.gif')} style={{height:150,width:150,alignSelf:"center"}} /> */}
                        <Lottie ref={animationRef} source={require('../../assets/images/done1.json')} style={{height:400,width:"100%"}} autoPlay />
                    </View>
                    <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000" }}>Order Placed Successfully</Text>
                </View>
            </SafeAreaView>
        </View>
    );
}
