import {
  MODEL_FOLLOWING_POST_CHECK_SUCCESS,
  USER_FOLLOWING_POST_CHECK_SUCCESS,
  MODEL_FOLLOWING_SET_COUNT,
  MODEL_FOLLOWING_COUNT_CHECK_SUCCESS,
  MODEL_FOLLOWING_DELETE_CHECK_SUCCESS,
  USER_FOLLOWING_DELETE_CHECK_SUCCESS,
  MODEL_FOLLOWING_GET_CHECK_SUCCESS,
  USER_FOLLOWING_GET_CHECK_SUCCESS,
  MODEL_FOLLOWING_SET_DATA,
  USER_FOLLOWING_SET_DATA,
  MODEL_FOLLOWING_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  modelFollowingPostCheckSuccess: false,
  userFollowingPostCheckSuccess: false,
  modelFollowingCount: 0,
  modelFollowingCountCheckSuccess: false,
  modelFollowingDeleteCheckSuccess: false,
  userFollowingDeleteCheckSuccess: false,
  modelFollowingGetCheckSuccess: false,
  userFollowingGetCheckSuccess: false,
  modelFollowingData: [],
  userFollowingData: [],
  modelFollowingUpdateCheckSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MODEL_FOLLOWING_POST_CHECK_SUCCESS:
      return {
        ...state,
        modelFollowingPostCheckSuccess: action.payload,
      };
    case USER_FOLLOWING_POST_CHECK_SUCCESS:
      return {
        ...state,
        userFollowingPostCheckSuccess: action.payload,
      };
    case MODEL_FOLLOWING_SET_COUNT:
      return {
        ...state,
        modelFollowingCount: action.payload,
      };
    case MODEL_FOLLOWING_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        modelFollowingCountCheckSuccess: action.payload,
      };
    case MODEL_FOLLOWING_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        modelFollowingDeleteCheckSuccess: action.payload,
      };
    case USER_FOLLOWING_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        userFollowingDeleteCheckSuccess: action.payload,
      };
    case MODEL_FOLLOWING_GET_CHECK_SUCCESS:
      return {
        ...state,
        modelFollowingGetCheckSuccess: action.payload,
      };
    case USER_FOLLOWING_GET_CHECK_SUCCESS:
      return {
        ...state,
        userFollowingGetCheckSuccess: action.payload,
      };
    case MODEL_FOLLOWING_SET_DATA:
      return {
        ...state,
        modelFollowingData: action.payload,
      };
    case USER_FOLLOWING_SET_DATA:
      return {
        ...state,
        userFollowingData: action.payload,
      };
    case MODEL_FOLLOWING_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        modelFollowingUpdateCheckSuccess: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
