import {StyleSheet,Dimensions} from 'react-native';
import * as Utils from '@utils';

const height = Dimensions.get("window").height;
export default StyleSheet.create({
  imageBanner: {
    height: Utils.scaleWithPixel(130),
    width: Utils.scaleWithPixel(120),
    borderRadius: 10,
    margin:10
  },
  content: {
    borderWidth: 0.5,
    flex:1,
    paddingBottom:10,
    marginHorizontal:20,
    right:5,
    marginBottom:20

  },
});
