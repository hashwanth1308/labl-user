import {StyleSheet} from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  imageBanner: {flex: 1, borderRadius: 8},
  
  content: {
    position: 'absolute',
    alignItems: 'flex-start',
    bottom: 0,
    padding: 10,
  },
  blockImage: {
    height: Utils.scaleWithPixel(200),
    borderRadius: 10,

  },
});
