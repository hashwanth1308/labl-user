import {
  ORDERS_POST_CHECK_SUCCESS,
  ORDERS_SET_COUNT,
  ORDERS_COUNT_CHECK_SUCCESS,
  ORDERS_DELETE_CHECK_SUCCESS,
  ORDERS_FINDONE_CHECK_SUCCESS,
  ORDERS_SET_ONE_DATA,
  ORDERS_GET_CHECK_SUCCESS,
  ORDERS_SET_DATA,
  ORDERS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  ordersPostCheckSuccess: false,
  ordersCount: 0,
  ordersCountCheckSuccess : false,
  ordersDeleteCheckSuccess: false,
  ordersFindOneCheckSuccess: false,
  ordersOneData: [],
  ordersGetCheckSuccess: false,
  ordersData: [],
  ordersUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ORDERS_POST_CHECK_SUCCESS:
      return {
        ...state,
        ordersPostCheckSuccess: action.payload,
      };
      case ORDERS_SET_COUNT:
      return {
        ...state,
        ordersCount: action.payload,
      };
      case ORDERS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        ordersCountCheckSuccess: action.payload,
      };
      case ORDERS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        ordersDeleteCheckSuccess: action.payload,
      };
      case ORDERS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        ordersFindOneCheckSuccess: action.payload,
          };
      case ORDERS_SET_ONE_DATA:
      return {
        ...state,
        ordersOneData: action.payload,
          };
      case ORDERS_GET_CHECK_SUCCESS:
      return {
        ...state,
        ordersGetCheckSuccess: action.payload,
          };
      case ORDERS_SET_DATA:
      return {
        ...state,
        ordersData: action.payload,
          };
      case ORDERS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        ordersUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
