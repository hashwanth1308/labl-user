import {

  modelOrdersPostCheckSuccess,
  modelOrdersSetCount,
  modelOrdersCountCheckSuccess,
  modelOrdersDeleteCheckSuccess,
  modelOrdersFindOneCheckSuccess,
  modelOrdersSetOneData,
  modelOrdersGetCheckSuccess,
  modelOrderItemsGetCheckSuccess,
  modelOrdersSetData,
  modelOrderItemsSetData,
  modelOrdersUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

//create
export function modelOrdersPostRequest(element) {
  console.log(element);
  return dispatch => {
    dispatch(modelOrdersPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `model-orders`,
      data: element,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("modelordr,", data)
        dispatch(modelOrdersSetData(data));
        dispatch(modelOrdersPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(modelOrdersPostCheckSuccess(false));

      });
  };
}

//count
export function modelOrdersCountRequest(element) {
  return dispatch => {
    dispatch(modelOrdersCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `model-orders/count`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelOrdersSetCount(data));
        dispatch(modelOrdersCountCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(modelOrdersCountCheckSuccess(false));

      });
  };
}

//delete
export function modelOrdersDeleteRequest(id) {

  return dispatch => {
    dispatch(modelOrdersDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `model-orders/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },

    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelOrdersDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(modelOrdersDeleteCheckSuccess(false));

      });
  };
}

//findOne
export function modelOrdersFindOneRequest(id) {
  return dispatch => {
    dispatch(modelOrdersFindOneCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `model-orders/${id}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelOrdersSetOneData(data));
        dispatch(modelOrdersFindOneCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin,)
        dispatch(modelOrdersFindOneCheckSuccess(false));
      });
  };
};

//find - get
export function modelOrdersGetRequest(obj) {
  return dispatch => {
    dispatch(modelOrdersGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `modelorderitems`,
      params: obj,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data, "modelordered-data")
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(modelOrdersSetData(data));
        dispatch(modelOrdersGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(modelOrdersGetCheckSuccess(false));
      });
  };
};
export function modelOrderItemsGetRequest(element) {
  const object = {}
  object.modelOrder = element;
  console.log("object", object);
  return dispatch => {
    // dispatch(modelOrderItemsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `modelorderitems`,
      params: object,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(modelOrderItemsSetData(data));
        // dispatch(modelOrderItemsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        // dispatch(modelOrderItemsGetCheckSuccess(false));
      });
  };
};

//update
export function modelOrdersUpdateRequest(id, element) {
  console.log( "modelOrders",id, element)
  return dispatch => {
    // dispatch(modelOrdersUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      data: element,
      url: `modelorderitems/${id}`,
      headers: {
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log("datamodal", data)
        dispatch(modelOrdersSetData(data));
        // dispatch(modelOrdersUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        // dispatch(modelOrdersUpdateCheckSuccess(false));

      });
  };
}



