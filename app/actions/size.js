import {
  SIZE_POST_CHECK_SUCCESS,
  SIZE_SET_COUNT,
  SIZE_COUNT_CHECK_SUCCESS,
  SIZE_DELETE_CHECK_SUCCESS,
  SIZE_GET_CHECK_SUCCESS,
  COLOR_GET_CHECK_SUCCESS,
  SIZE_SET_DATA,
  COLOR_SET_DATA,
  SIZE_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function sizePostCheckSuccess(bool = false) {
  return {
    type: SIZE_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function sizeSetCount(object) {
  return {
    type: SIZE_SET_COUNT,
    payload: object,
  };
}
export function sizeCountCheckSuccess(bool = false) {
  return {
    type: SIZE_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function sizeDeleteCheckSuccess(bool = false) {
  return {
    type: SIZE_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}


export function sizeGetCheckSuccess(bool = false) {
  return {
    type: SIZE_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function colorGetCheckSuccess(bool = false) {
  return {
    type: COLOR_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function sizeSetData(object) {
  return {
    type: SIZE_SET_DATA,
    payload: object,
  };
}
export function colorSetData(object) {
  return {
    type: COLOR_SET_DATA,
    payload: object,
  };
}

export function sizeUpdateCheckSuccess(bool = false) {
  return {
    type: SIZE_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

