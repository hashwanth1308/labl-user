import React, { useState, useEffect } from 'react';
import { View, ScrollView, KeyboardAvoidingView, Platform, TouchableOpacity, PermissionsAndroid, ImageBackground, ActivityIndicator, Dimensions } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { useDispatch, useSelector } from 'react-redux';
import {
  Image,
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
} from '@components';
import { TextInput } from 'react-native-paper';
import styles from './styles';
import { UserData } from '@data';
import { useTranslation } from 'react-i18next';
import { signupRequestPut } from '../../api/signup';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons'

import { uploadFilesPostRequest } from '../../api/uploadApi';

export default function ProfileEdit({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  useEffect(() => {
    setMale(false);
    setFemale(false);
  }, []);
  const dispatch = useDispatch();
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);
  // const userData = useSelector(state => state.signupReducer.signupMessageData)
  // const userData = useSelector(state => state.signupReducer.signupMessageData);
  const userData = useSelector(state => state.accessTokenReducer.userData);
  const userId = userData?.user?.id
  console.log("user", userData);
  const [firstName, setFirstName] = useState(userData?.user?.firstname);
  const [lastName, setLastName] = useState(userData?.user?.lastname);
  const [gender, setGender] = useState(userData?.user?.gender);
  console.log(gender);
  const [mobile, setMobile] = useState(userData?.user?.mobile);
  const [email, setEmail] = useState(userData?.user?.email)
  const [location, setLocation] = useState('')
  const [GSTIN, setGSTIN] = useState('')
  const [image] = useState(UserData[0].image);
  const [loading, setLoading] = useState(false);
  const [profileImage, setProfileImage] = useState('');

  const success = async () => {
    setLoading1(true);
    await dispatch(signupRequestPut(firstName, lastName, gender, mobile, email, userId, selectedImage));
    navigation.navigate('Profile')
    setLoading1(false);
  };

  const selectImage = () => {
    const options = {
      noData: true
    }
    ImagePicker.launchImageLibrary(options, response => {
      console.log(response.assets, "all response", response.assets.uri)
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        const source = { uri: response.uri }
        console.log(source)
        setImage(source)
      }
    })
  }
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write camera err', err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };


  const [selectedImage, setSelectedImage] = useState();
  console.log("selectedImage", selectedImage);
  const captureImage = async (type) => {
    // let options = {
    //   mediaType: type,
    //   maxWidth: 300,
    //   maxHeight: 550,
    //   quality: 1,
    //   videoQuality: 'low',
    //   durationLimit: 30, //Video max duration in seconds
    //   saveToPhotos: true,
    // };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {

      ImagePicker.openPicker({
        multiple: true
      }).then(images => {
        // let imgd = image.concat(images)
        // setImage(imgd);
        editImage(images);
      });
    }
  };
  const editImage = (images) => {
    ImagePicker.openCropper({
      path: images[0]?.path,
      height: 400,
      width: 400
    }).then(images => {
      console.log(images);
      toNextPage(images)
    })
  }
  const toNextPage = async (images) => {
    console.log('imgApi2', images);
    let formData = new FormData();
    setLoading1(true);

    console.log('lopala', images);

    formData.append('files', {
      name: images?.modificationDate,
      type: images?.mime,
      uri: images?.path,
    });
    console.log(formData);

    let imgApi = await uploadFilesPostRequest(formData);
    console.log('imgApi', imgApi);
    setSelectedImage(imgApi[0]?.url);
    // setProfileImage(imgApi[0]?.id)
    setLoading1(false);
    // success(imgApi[0]);
  };
  console.log("image", selectedImage);

  const [male, setMale] = useState(false);
  const [female, setFemale] = useState(false);
  const handleClick = () => {
    setMale(true);
    setFemale(false);
    setGender('Male');
  }
  const handleClick1 = () => {
    setFemale(true);
    setMale(false);
    setGender('Female');
  }
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);




  return (
    <View style={{ flex: 1 }}>
      <ImageBackground source={require('../../assets/images/background.png')} style={{ height: 150, width: "100%", marginBottom: 100 }}>
        <Header
          title={t('edit_profile')}
          renderLeft={() => {
            return (
              <View style={{ height: 35, width: 35, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
            );
          }}
          onPressLeft={() => {
            navigation.goBack();
          }}
          onPressRight={() => { }}
        />
        <TouchableOpacity onPress={() => captureImage()}>
          <View style={{ alignSelf: "center", borderWidth: 2, borderColor: "#fff", borderRadius: 13, height: 161, width: 135, alignItems: "center", }}>
            {selectedImage ?
              <Image source={{ uri: selectedImage ? selectedImage : userData?.user?.image }} style={styles.thumb} /> :
              <Image source={require('../../assets/images/profile-1.jpg')} style={styles.thumb} />
            }
          </View>
          <Image source={require('../../assets/images/edit.png')} style={{ height: 20, width: 20, borderRadius: 30, alignSelf: "center", top: -155, left: 55 }} />
        </TouchableOpacity>
        <View style={styles.contentTitle}>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18 }}>
            {userData?.user?.username}
          </Text>
        </View>
      </ImageBackground>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <ScrollView contentContainerStyle={styles.contain}>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'android' ? 'height' : 'padding'}
            keyboardVerticalOffset={offsetKeyboard}
            style={{ flex: 1 }}>
            <TextInput
              label="First Name"
              style={{ width: 300, backgroundColor: "#fff" }}
              activeOutlineColor="#CDCDCD"
              mode='outlined'
              onChangeText={text => setFirstName(text)}
              // placeholder={t('First Name')}
              value={firstName}
            />
            <TextInput
              label="Last Name"
              style={{ marginTop: 10, width: 300, backgroundColor: "#fff" }}
              activeOutlineColor="#CDCDCD"
              mode='outlined'
              onChangeText={text => setLastName(text)}
              // placeholder={t('First Name')}
              value={lastName}
            />
            <TextInput
              label="Mobile Number"
              style={{ marginTop: 10, width: 300, backgroundColor: "#fff" }}
              activeOutlineColor="#CDCDCD"
              mode='outlined'
              onChangeText={text => setMobile(text)}
              value={mobile}
            />
            <TextInput
              label="Email"
              style={{ marginTop: 10, width: 300, backgroundColor: "#fff" }}
              onChangeText={text => setEmail(text)}
              activeOutlineColor="#CDCDCD"
              mode='outlined'
              value={email}
            />
            <View style={styles.gender}>
              <TouchableOpacity onPress={() => handleClick()} style={{ alignSelf: "center" }}>
                <Text style={{ fontSize: 18, fontFamily: "Montserrat-SemiBold", alignSelf: "center" }}>Male</Text>
              </TouchableOpacity>
              {male || userData?.user?.gender === 'Male' ?
                <Icon1 name="check" size={25} style={{ color: "#909090", marginLeft: -30, alignSelf: "center" }} /> : null}
              <View style={{ width: 1, height: 60, borderWidth: 0.8, borderColor: "#909090", alignSelf: "center" }}></View>
              <TouchableOpacity onPress={() => handleClick1()} style={{ alignSelf: "center" }}>
                <Text style={{ fontSize: 18, fontFamily: "Montserrat-SemiBold", alignSelf: "center" }}>Female</Text>
              </TouchableOpacity>
              {female || userData?.user?.gender === 'Female' ?
                <Icon1 name="check" size={25} style={{ color: "#909090", alignSelf: "center", marginLeft: -30 }} /> : null}
            </View>
            <TextInput
              label="Location"
              style={{ marginTop: 10, width: 300, backgroundColor: "#fff" }}
              onChangeText={text => setLocation(text)}
              activeOutlineColor="#CDCDCD"
              mode='outlined'
              value={location}
            />

          </KeyboardAvoidingView>
        </ScrollView>
        <View style={{ paddingVertical: 15, paddingHorizontal: 20 }}>
          <Button
            // loading={loading}
            full
            onPress={() => {
              success()
            }}>
            {t('Save details')}
          </Button>
        </View>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}
