import React, {useState,useEffect} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import {BaseStyle, useTheme} from '@config';
import {Header, SafeAreaView, Icon, Text} from '@components';
import styles from './styles';
import {useTranslation} from 'react-i18next';
import {CurrencyData} from '@data';

export default function Currency({navigation}) {
  const {colors} = useTheme();
  const {t} = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;

  const [loading, setLoading] = useState(false);
  const [currency, setCurrency] = useState(CurrencyData);

  /**
   * @description Called when setting currency is selected
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   * @param {object} select
   */
  const onSelect = select => {
    setCurrency(
      currency.map(item => {
        if (item.currency == select.currency) {
          return {
            ...item,
            checked: true,
          };
        } else {
          return {
            ...item,
            checked: false,
          };
        }
      }),
    );
  };

  /**
   * @description Load item one by one
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   * @param {*} item
   * @param {*} index
   * @returns
   */
  const renderItem = (item, index) => {
    return (
      <TouchableOpacity
        style={[styles.item, {borderBottomColor: colors.border}]}
        onPress={() => onSelect(item)}>
        <Text
          body1
          style={
            item.checked
              ? {
                  color: colors.primary,
                }
              : {}
          }>
          {item.currency}
        </Text>
        {item.checked ? (
          <Icon name="check" size={14} color={colors.primary} />
        ) : null}
      </TouchableOpacity>
    );
  };
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev)=>(!prev))
    ), 1000)
  }, []);


  return (
    <View style={{flex: 1}}>
      <Header
        title={t('currency')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        renderRight={() => {
          if (loading) {
            return <ActivityIndicator size="small" color={colors.primary} />;
          } else {
            return (
              <Text headline primaryColor numberOfLines={1}>
                {t('Save')}
              </Text>
            );
          }
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
        onPressRight={() => {
          setLoading(true);
          setTimeout(() => {
            navigation.goBack();
          }, 500);
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <View style={styles.contain}>
          <View style={{width: '100%', height: '100%'}}>
            <FlatList
              data={currency}
              keyExtractor={(item, index) => item.id}
              renderItem={({item, index}) => renderItem(item, index)}
            />
          </View>
        </View>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height:height,backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor:"#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}
