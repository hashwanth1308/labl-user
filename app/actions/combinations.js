import {
  COMBINATIONS_POST_CHECK_SUCCESS,
  COMBINATIONS_SET_COUNT,
  COMBINATIONS_COUNT_CHECK_SUCCESS,
  COMBINATIONS_DELETE_CHECK_SUCCESS,
  COMBINATIONS_FINDONE_CHECK_SUCCESS,
  COMBINATIONS_SET_ONE_DATA,
  COMBINATIONS_GET_CHECK_SUCCESS,
  COMBINATIONS_SET_DATA,
  COMBINATIONS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function combinationsPostCheckSuccess(bool = false) {
  return {
    type: COMBINATIONS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function combinationsSetCount(object) {
  return {
    type: COMBINATIONS_SET_COUNT,
    payload: object,
  };
}
export function combinationsCountCheckSuccess(bool = false) {
  return {
    type: COMBINATIONS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function combinationsDeleteCheckSuccess(bool = false) {
  return {
    type: COMBINATIONS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function combinationsFindOneCheckSuccess(bool = false) {
  return {
    type: COMBINATIONS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function combinationsSetOneData(object) {
  return {
    type: COMBINATIONS_SET_ONE_DATA,
    payload: object,
  };
}
export function combinationsGetCheckSuccess(bool = false) {
  return {
    type: COMBINATIONS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function combinationsSetData(object) {
  return {
    type: COMBINATIONS_SET_DATA,
    payload: object,
  };
}

export function combinationsUpdateCheckSuccess(bool = false) {
  return {
    type: COMBINATIONS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

