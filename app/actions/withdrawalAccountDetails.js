import {
  WITHDRAWALACCOUNTDETAILS_POST_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_SET_COUNT,
  WITHDRAWALACCOUNTDETAILS_COUNT_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_DELETE_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_FINDONE_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_SET_ONE_DATA,
  WITHDRAWALACCOUNTDETAILS_GET_CHECK_SUCCESS,
  WITHDRAWALACCOUNTDETAILS_SET_DATA,
  WITHDRAWALACCOUNTDETAILS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function withdrawalAccountDetailsPostCheckSuccess(bool = false) {
  return {
    type: WITHDRAWALACCOUNTDETAILS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function withdrawalAccountDetailsSetCount(object) {
  return {
    type: WITHDRAWALACCOUNTDETAILS_SET_COUNT,
    payload: object,
  };
}
export function withdrawalAccountDetailsCountCheckSuccess(bool = false) {
  return {
    type: WITHDRAWALACCOUNTDETAILS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function withdrawalAccountDetailsDeleteCheckSuccess(bool = false) {
  return {
    type: WITHDRAWALACCOUNTDETAILS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function withdrawalAccountDetailsFindOneCheckSuccess(bool = false) {
  return {
    type: WITHDRAWALACCOUNTDETAILS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function withdrawalAccountDetailsSetOneData(object) {
  return {
    type: WITHDRAWALACCOUNTDETAILS_SET_ONE_DATA,
    payload: object,
  };
}
export function withdrawalAccountDetailsGetCheckSuccess(bool = false) {
  return {
    type: WITHDRAWALACCOUNTDETAILS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function withdrawalAccountDetailsSetData(object) {
  return {
    type: WITHDRAWALACCOUNTDETAILS_SET_DATA,
    payload: object,
  };
}

export function withdrawalAccountDetailsUpdateCheckSuccess(bool = false) {
  return {
    type: WITHDRAWALACCOUNTDETAILS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

