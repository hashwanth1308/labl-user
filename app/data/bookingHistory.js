import {Images} from '@config';

const BookingHistoryData = [
  {
    id: '1',
    name: 'Roadster',
    checkIn: 'M',
    checkOut: '2',
    total: '$3999',
    price: '50%OFF',
  },
  {
    id: '2',
    name: 'BIBA',
    checkIn: 'L',
    checkOut: '4',
    total: '$2999',
    price: '20%OFF',
  },
  {
    id: '3',
    name: 'PUMA',
    checkIn: 'S',
    checkOut: '8',
    total: '$6999',
    price: '40%OFF',
  },
  {
    id: '4',
    name: 'H&M',
    checkIn: 'XL',
    checkOut: '6',
    total: '$22999',
    price: '20%OFF',
  },
];

export {BookingHistoryData};
