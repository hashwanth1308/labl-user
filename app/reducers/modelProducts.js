import {
  MODELPRODUCTS_POST_CHECK_SUCCESS,
  MODELPRODUCTS_SET_COUNT,
  MODELPRODUCTS_COUNT_CHECK_SUCCESS,
  MODELPRODUCTS_DELETE_CHECK_SUCCESS,
  MODELPRODUCTS_FINDONE_CHECK_SUCCESS,
  MODELPRODUCTS_SET_ONE_DATA,
  MODELPRODUCTS_GET_CHECK_SUCCESS,
  MODELPRODUCTS_SET_DATA,
  MODELPRODUCTS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  modelProductsPostCheckSuccess: false,
  modelProductsCount: 0,
  modelProductsCountCheckSuccess : false,
  modelProductsDeleteCheckSuccess: false,
  modelProductsFindOneCheckSuccess: false,
  modelProductsOneData: [],
  modelProductsGetCheckSuccess: false,
  modelProductsData: [],
  modelProductsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MODELPRODUCTS_POST_CHECK_SUCCESS:
      return {
        ...state,
        modelProductsPostCheckSuccess: action.payload,
      };
      case MODELPRODUCTS_SET_COUNT:
      return {
        ...state,
        modelProductsCount: action.payload,
      };
      case MODELPRODUCTS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        modelProductsCountCheckSuccess: action.payload,
      };
      case MODELPRODUCTS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        modelProductsDeleteCheckSuccess: action.payload,
      };
      case MODELPRODUCTS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        modelProductsFindOneCheckSuccess: action.payload,
          };
      case MODELPRODUCTS_SET_ONE_DATA:
      return {
        ...state,
        modelProductsOneData: action.payload,
          };
      case MODELPRODUCTS_GET_CHECK_SUCCESS:
      return {
        ...state,
        modelProductsGetCheckSuccess: action.payload,
          };
      case MODELPRODUCTS_SET_DATA:
      return {
        ...state,
        modelProductsData: action.payload,
          };
      case MODELPRODUCTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        modelProductsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
