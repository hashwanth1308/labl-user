import {
  LABELS_POST_CHECK_SUCCESS,
  LABELS_SET_COUNT,
  LABELS_COUNT_CHECK_SUCCESS,
  LABELS_DELETE_CHECK_SUCCESS,
  LABELS_FINDONE_CHECK_SUCCESS,
  LABELS_SET_ONE_DATA,
  LABELS_GET_CHECK_SUCCESS,
  LABELS_SET_DATA,
  LABELS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function labelsPostCheckSuccess(bool = false) {
  return {
    type: LABELS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function labelsSetCount(object) {
  return {
    type: LABELS_SET_COUNT,
    payload: object,
  };
}
export function labelsCountCheckSuccess(bool = false) {
  return {
    type: LABELS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function labelsDeleteCheckSuccess(bool = false) {
  return {
    type: LABELS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function labelsFindOneCheckSuccess(bool = false) {
  return {
    type: LABELS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function labelsSetOneData(object) {
  return {
    type: LABELS_SET_ONE_DATA,
    payload: object,
  };
}
export function labelsGetCheckSuccess(bool = false) {
  return {
    type: LABELS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function labelsSetData(object) {
  return {
    type: LABELS_SET_DATA,
    payload: object,
  };
}

export function labelsUpdateCheckSuccess(bool = false) {
  return {
    type: LABELS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

