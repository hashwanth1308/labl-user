import{ 
    
  salesOrdersPostCheckSuccess,
  salesOrdersSetCount,
  salesOrdersCountCheckSuccess,
  salesOrdersDeleteCheckSuccess,
  salesOrdersFindOneCheckSuccess,
  salesOrdersSetOneData,
  salesOrdersGetCheckSuccess,
  salesOrdersSetData,
  salesOrdersUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function salesOrdersPostRequest(element) {
      return dispatch => {
        dispatch(salesOrdersPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `sales-orders`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(salesOrdersSetData(a));
            dispatch(salesOrdersPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(salesOrdersPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function salesOrdersCountRequest(element) {
      return dispatch => {
          dispatch(salesOrdersCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `sales-orders/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(salesOrdersSetCount(data));
              dispatch(salesOrdersCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(salesOrdersCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function salesOrdersDeleteRequest(id) {
      
      return dispatch => {
          dispatch(salesOrdersDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `sales-orders/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(salesOrdersDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(salesOrdersDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function salesOrdersFindOneRequest(id) {
      return dispatch => {
        dispatch(salesOrdersFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `sales-orders/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(salesOrdersOneData(data));
            dispatch(salesOrdersFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(salesOrdersFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function salesOrdersGetRequest(element) {
  return dispatch => {
    dispatch(salesOrdersGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `sales-orders`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(salesOrdersSetOneData(data));
        dispatch(salesOrdersGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(salesOrdersGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function salesOrdersUpdateRequest(id) {
     
      return dispatch => {
          dispatch(salesOrdersUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `sales-orders/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(salesOrdersSetData(data));
              dispatch(salesOrdersUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(salesOrdersUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 