import {
  STATUS_POST_CHECK_SUCCESS,
  STATUS_SET_COUNT,
  STATUS_COUNT_CHECK_SUCCESS,
  STATUS_DELETE_CHECK_SUCCESS,
  STATUS_FINDONE_CHECK_SUCCESS,
  STATUS_SET_ONE_DATA,
  STATUS_GET_CHECK_SUCCESS,
  STATUS_SET_DATA,
  STATUS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function statusPostCheckSuccess(bool = false) {
  return {
    type: STATUS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function statusSetCount(object) {
  return {
    type: STATUS_SET_COUNT,
    payload: object,
  };
}
export function statusCountCheckSuccess(bool = false) {
  return {
    type: STATUS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function statusDeleteCheckSuccess(bool = false) {
  return {
    type: STATUS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function statusFindOneCheckSuccess(bool = false) {
  return {
    type: STATUS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function statusSetOneData(object) {
  return {
    type: STATUS_SET_ONE_DATA,
    payload: object,
  };
}
export function statusGetCheckSuccess(bool = false) {
  return {
    type: STATUS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function statusSetData(object) {
  return {
    type: STATUS_SET_DATA,
    payload: object,
  };
}

export function statusUpdateCheckSuccess(bool = false) {
  return {
    type: STATUS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

