import React, { useState, useEffect } from 'react';
import { View, KeyboardAvoidingView, Platform, ActivityIndicator, Dimensions, ScrollView, Keyboard } from 'react-native';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Button, Text, TextInput } from '@components';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { signupRequest } from '../../api/signup';
import { usersGetRequest } from '../../api';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import { Image } from 'react-native';
import { Images } from '../../config/images';
import * as Utils from '@utils';
import CheckBox from '@react-native-community/checkbox';

export default function SignUpScreenUserLABL({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const width = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(usersGetRequest());
  // }, [dispatch]);
  // const userArrayData = useSelector(state => state.usersReducer.usersOneData)
  // console.log("User", userData);
  const [secure, setSecure] = useState(true);
  const [toggleCheckBox, setToggleCheckBox] = useState(false)
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [mobile, setMobile] = useState('');
  const [success, setSuccess] = useState({
    name: true,
    email: true,
    password: true,
    mobile: true
  });
  const signupSuccess = useSelector(state => state.signupReducer.signupSuccess)

  /**
   * call when action signup
   *
   */


  const onSignUp = async () => {
    if (name == '' || email == '' || mobile == '' || password == '') {
      setSuccess({
        ...success,
        name: name != '' ? true : false,
        email: email != '' ? true : false,
        mobile: mobile != '' ? true : false,
        password: password != '' ? true : false,
      });
    } else {
      setLoading(true);
      const isModel = true;
      const isVendor = false;
      dispatch(signupRequest(name, email, mobile, password, isModel, isVendor));
    }

  };
  if (signupSuccess) {
    console.log("signupSuccesssignupSuccess 123----------------------")
    navigation.navigate('HomeScreenModelLABL')
    // navigation.navigate('HomeScreenModelLABL')
  }

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);


  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('Model sign_up')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <ScrollView contentContainerStyle={{ marginVertical: 0 }}>
          <Image source={Images.logo} style={{ height: Utils.scaleWithPixel(100), width: Utils.scaleWithPixel(150), alignSelf: "center", marginVertical: 0, }} />
          <KeyboardAvoidingView
            behavior={Platform.OS === 'android' ? 'height' : 'padding'}
            keyboardVerticalOffset={offsetKeyboard}
            style={{ flex: 1 }}>

            <View style={styles.contain}>
              <Text style={{ fontSize: 25, color: "#000", fontFamily: "Montserrat-Bold", marginVertical: 40, position: "relative" }}>Create Account</Text>
              <TextInput
                onChangeText={text => setName(text)}
                placeholder={t('User Name')}
                success={success.name}
                value={name}
                style={{ width: 322, height: 52, elevation: 5, borderRadius: 30 }}
              />
              {/* {userArrayData.map((item, index) => {
              return (
                name == item?.username ?
                  <View style={{ alignItems: "center", left: 10, marginTop: 10, alignSelf: "flex-start", flexDirection: "row" }}>
                    <Icon1 name="alert-circle" size={15} color="red" />
                    <Text style={{ fontFamily: "Roboto", fontSize: 18, color: "red" }}>Username already exists</Text>
                  </View> : null
              )
            })} */}
              <TextInput
                style={{ marginTop: 10, width: 322, height: 52, elevation: 5, borderRadius: 30 }}
                onChangeText={text => setMobile(text)}
                placeholder={t('Mobile Number')}
                success={success.mobile}
                value={mobile}
              />
              {/* {userArrayData.map((item, index) => {
              return (
                mobile == item?.mobile ?
                  <View style={{ alignItems: "center", left: 10, marginTop: 10, alignSelf: "flex-start", flexDirection: "row" }}>
                    <Icon1 name="alert-circle" size={15} color="red" />
                    <Text style={{ fontFamily: "Roboto", fontSize: 18, color: "red" }}>Mobile Number already exists</Text>
                  </View> : null
              )
            })} */}

              <TextInput
                style={{ marginTop: 10, width: 322, height: 52, elevation: 5, borderRadius: 30 }}
                onChangeText={text => setEmail(text)}
                placeholder={t('email')}
                keyboardType="email-address"
                success={success.email}
                value={email}
              />
              {/* {userArrayData.map((item, index) => {
              return (
                email == item?.email ?
                  <View style={{ alignItems: "center", left: 10, marginTop: 10, alignSelf: "flex-start", flexDirection: "row" }}>
                    <Icon1 name="alert-circle" size={15} color="red" />
                    <Text style={{ fontFamily: "Roboto", fontSize: 18, color: "red" }}>Email already exists</Text>
                  </View> : null
              )
            })} */}
              <TextInput
                style={{ marginTop: 10, width: 322, height: 52, elevation: 5, borderRadius: 30 }}
                onChangeText={text => setPassword(text)}
                placeholder={t('Password')}
                success={success.password}
                value={password}
                secureTextEntry={secure}
                onSubmitEditing={Keyboard.dismiss}
              >
                <Icon1
                  name={secure ? 'eye-off-outline' : 'eye-outline'}
                  onPress={() => setSecure(!secure)}
                  solid
                  color="black"
                  size={24}
                // style={{ elevation: 5 }}
                />
              </TextInput>
              <View style={{ flexDirection: "row", alignItems: "center", top: 30 }}>
                <CheckBox value={toggleCheckBox}
                  onValueChange={(newValue) => setToggleCheckBox(newValue)} />
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#000" }}>I agree the privacy policy</Text>
              </View>
              {toggleCheckBox ?
                <Button
                  // full
                  style={{ marginTop: 50, width: 172, height: 39, borderRadius: 30 }}
                  loading={loading}
                  onPress={() =>
                    onSignUp()
                    // navigation.navigate('Otp')
                  }>
                  {t('Register')}
                </Button> :
                <Button
                  // full
                  disabled
                  style={{ marginTop: 50, width: 172, height: 39, borderRadius: 30 }}
                  loading={loading}
                  onPress={() =>
                    onSignUp()
                    // navigation.navigate('Otp')
                  }>
                  {t('Register')}
                </Button>
              }
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: windowHeight, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}
