import {
  EARNINGS_POST_CHECK_SUCCESS,
  EARNINGS_SET_COUNT,
  EARNINGS_COUNT_CHECK_SUCCESS,
  EARNINGS_DELETE_CHECK_SUCCESS,
  EARNINGS_FINDONE_CHECK_SUCCESS,
  EARNINGS_SET_ONE_DATA,
  EARNINGS_GET_CHECK_SUCCESS,
  EARNINGS_SET_DATA,
  EARNINGS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function earningsPostCheckSuccess(bool = false) {
  return {
    type: EARNINGS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function earningsSetCount(object) {
  return {
    type: EARNINGS_SET_COUNT,
    payload: object,
  };
}
export function earningsCountCheckSuccess(bool = false) {
  return {
    type: EARNINGS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function earningsDeleteCheckSuccess(bool = false) {
  return {
    type: EARNINGS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function earningsFindOneCheckSuccess(bool = false) {
  return {
    type: EARNINGS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function earningsSetOneData(object) {
  return {
    type: EARNINGS_SET_ONE_DATA,
    payload: object,
  };
}
export function earningsGetCheckSuccess(bool = false) {
  return {
    type: EARNINGS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function earningsSetData(object) {
  return {
    type: EARNINGS_SET_DATA,
    payload: object,
  };
}

export function earningsUpdateCheckSuccess(bool = false) {
  return {
    type: EARNINGS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

