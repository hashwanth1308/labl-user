import{ 
    
  combinationsPostCheckSuccess,
  combinationsSetCount,
  combinationsCountCheckSuccess,
  combinationsDeleteCheckSuccess,
  combinationsFindOneCheckSuccess,
  combinationsSetOneData,
  combinationsGetCheckSuccess,
  combinationsSetData,
  combinationsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function combinationsPostRequest(element) {
      return dispatch => {
        dispatch(combinationsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `combinations`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(combinationsSetData(a));
            dispatch(combinationsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(combinationsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function combinationsCountRequest(element) {
      return dispatch => {
          dispatch(combinationsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `combinations/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(combinationsSetCount(data));
              dispatch(combinationsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(combinationsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function combinationsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(combinationsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `combinations/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(combinationsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(combinationsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function combinationsFindOneRequest(id) {
      return dispatch => {
        dispatch(combinationsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `combinations/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(combinationsOneData(data));
            dispatch(combinationsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(combinationsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function combinationsGetRequest(element) {
  return dispatch => {
    dispatch(combinationsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `combinations`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(combinationsSetOneData(data));
        dispatch(combinationsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(combinationsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function combinationsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(combinationsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `combinations/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(combinationsSetData(data));
              dispatch(combinationsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(combinationsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 