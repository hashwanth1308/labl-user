import React, { useEffect, useRef, useState } from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  Animated
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Text, Image, TextInput, Button } from '@components';
import { useTranslation } from 'react-i18next';
import styles from './styles';

export default function OrdersScreenModelLABL({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const themeStorage = useSelector(state => state.application.theme);

  const addressData = useSelector(state => state.ordersReducer.ordersData);

  const dispatch = useDispatch();
  const [card, setCard] = useState('');
  const [valid, setValid] = useState('');
  const [digit, setDigit] = useState('');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });

  const { object } = route.params;
  console.log("obj", object);

  const [selectedStep, setSelectedStep] = useState(0);
  const progress1 = useRef(new Animated.Value(0)).current;
  const progress2 = useRef(new Animated.Value(0)).current;
  const progress3 = useRef(new Animated.Value(0)).current;
  const progress4 = useRef(new Animated.Value(0)).current;
  const progress5 = useRef(new Animated.Value(0)).current;
  const start1 = () => {
    Animated.timing(progress1, {
      toValue: 70,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  };
  const start2 = () => {
    Animated.timing(progress2, {
      toValue: 140,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  };
  const start3 = () => {
    Animated.timing(progress3, {
      toValue: 210,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  };
  const start4 = () => {
    Animated.timing(progress4, {
      toValue: 280,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  };
  const start5 = () => {
    Animated.timing(progress5, {
      toValue: 100,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  };
  useEffect(() => {
    setTimeout(() => {
      if (object?.status === "ordered") {
        setSelectedStep(0)
        // start1()
      }
      else if (object?.status === "ReadyToShip") {
        setSelectedStep(0.3)
        start2()
      }
      else if (object?.status === "Shipped") {
        setSelectedStep(2)
        start2()
      }else if(object?.status === "Delivered"){
        setSelectedStep(4)
        start4();
      }
    }, 1000);
  }, [object?.status])



  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('My Orders')} 
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flex: 1, justifyContent: "space-around" }}>
              <View style={{ flexDirection: "row", width: 200, justifyContent: "space-between", alignSelf: "flex-start" }}>
                <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                  <Icon
                    name="arrow-left"
                    size={20}
                    color={colors.primary}
                    enableRTL={true}
                  />
                </View>
                <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", right: 40 }}>Order Details</Text>
              </View>

            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

      // renderRight={() => {
      //   return <Icon name="shopping-cart" size={24} color={colors.primary} />;
      // }}
      // onPressRight={() => {
      //   navigation.navigate('Cart');
      // }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ padding: 20 }}>
            <View style={{ justifyContent: "center", alignItems: "center", marginBottom: 10 }}>
              <Image source={{uri : object?.product?.productImages[0]?.url}} style={{ height: 200, width: 150, borderRadius: 10, marginBottom: 20 }} />
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 17, marginVertical: 10 }}>{object?.product?.productName}</Text>
              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 22, marginVertical: 10 }}>{object?.product?.productBrandName}</Text>
              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 22, marginVertical: 10 }}>Size : {object?.productVarient?.size?.type}</Text>
            </View>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1, marginVertical: 10 }}>
                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 15,
                      backgroundColor: selectedStep >= 0 ? 'green' : '#CDCDCD',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{ color: '#fff' }}>1</Text>
                  </View>
                  <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, color: selectedStep >= 0 ? 'green' : "#000", marginHorizontal: 10 }}>Order Confirmed on {new Date(object?.createdAt).toDateString()}</Text>
                </View>
                <View
                  style={{
                    width: 6,
                    height: 70,
                    marginHorizontal: 7,
                    backgroundColor: '#f2f2f2',
                  }}></View>
                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 15,
                      backgroundColor: selectedStep > 1 ? 'green' : '#CDCDCD',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{ color: '#fff' }}>2</Text>
                  </View>
                  <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, color: selectedStep > 1 ? 'green' : "#000", marginHorizontal: 10 }}>Shipped</Text>
                </View>
                <View
                  style={{
                    width: 6,
                    height: 70,
                    marginHorizontal: 7,
                    backgroundColor: '#f2f2f2',
                  }}></View>
                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 15,
                      backgroundColor: selectedStep > 2 ? 'green' : '#CDCDCD',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{ color: '#fff' }}>3</Text>
                  </View>
                  <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, color: selectedStep > 2 ? 'green' : "#000", marginHorizontal: 10 }}>Out for Delivery</Text>
                </View>
                <View
                  style={{
                    width: 6,
                    height: 70,
                    marginHorizontal: 7,
                    backgroundColor: '#f2f2f2',
                  }}></View>
                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 15,
                      backgroundColor: selectedStep > 3 ? 'green' : '#CDCDCD',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{ color: '#fff' }}>4</Text>
                  </View>
                  <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, color: selectedStep > 3 ? 'green' : "#000", marginHorizontal: 10 }}>Delivered</Text>
                </View>
              </View>
              <View
                style={{
                  width: '100%',
                  alignItems: 'flex-start',
                  // padding: 50,
                  position: 'absolute',
                  top: 0,
                  marginVertical: 10
                }}>
                <Animated.View
                  style={{
                    width: 6,
                    height: progress1,
                    marginTop: 0,
                    marginHorizontal: 7,
                    backgroundColor: 'green',
                  }}></Animated.View>

                <Animated.View
                  style={{
                    width: 6,
                    height: progress2,
                    marginTop: 5,
                    marginHorizontal: 7,
                    backgroundColor: 'green',
                  }}></Animated.View>
                <Animated.View
                  style={{
                    width: 6,
                    height: progress3,
                    marginTop: 0,
                    marginHorizontal: 7,
                    backgroundColor: 'green',
                  }}></Animated.View>
                <Animated.View
                  style={{
                    width: 6,
                    height: progress4,
                    marginTop: 0,
                    marginHorizontal: 7,
                    backgroundColor: 'green',
                  }}></Animated.View>
                {/* <Animated.View
                  style={{
                    width: 6,
                    height: progress5,
                    marginTop: 20,
                    marginHorizontal: 7,
                    backgroundColor: 'green',
                  }}></Animated.View> */}
              </View>
              <View style={{ flex: 1, marginVertical: 10, padding: 20, borderColor: "#CDCDCD", borderWidth: 1 }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000" }}>Delivery Address</Text>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#000", marginVertical: 10 }}>{object?.modelOrder?.address?.fullName} | {object?.modelOrder?.address?.mobileNumber}</Text>
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20, color: "#000" }}>{object?.modelOrder?.address?.houseNo}, {object?.modelOrder?.address?.areaColony}, {object?.modelOrder?.address?.city}, {object?.modelOrder?.address?.state}, {object?.modelOrder?.address?.country}, {object?.modelOrder?.address?.pincode}</Text>
              </View>
              <View style={{ flex: 1, borderWidth: 1, borderColor: "#CDCDCD", marginVertical: 10, flexDirection: "row", justifyContent: "space-between", padding: 10, alignItems: "center" }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#000" }}>Total Order Price</Text>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#000" }}>₹ {object?.product?.productPrice}</Text>
              </View>

            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  )
}