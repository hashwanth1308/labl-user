import {
    COMMENT_GET_CHECK_SUCCESS,
    COMMENT_POST_CHECK_SUCCESS,
    COMMENT_UPDATE_CHECK_SUCCESS,
    COMMENT_DELETE_CHECK_SUCCESS,
    COMMENT_SET_DATA,
    COMMENT_SET_DATA_BY_USER,
    COMMENT_GET_CHECK_SUCCESS_BY_USER,
    COMMENT_GET_CHECK_SUCCESS_FOR_FEED,
    COMMENT_SET_DATA_FOR_FEED
} from '../actions/actionTypes';

const initialState = {
    commentGetCheckSuccess: false,
    commentData: [],
    commentPostCheckSuccess: false,
    commentUpdateCheckSuccess: false,
    commentDeleteCheckSuccess: false,
    commentGetCheckSuccessByUser: false,
    commentDataByUser: [],
    commentDataForFeed: [],
    commentGetCheckSuccessForFeed: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case COMMENT_GET_CHECK_SUCCESS:
            return {
                ...state,
                commentGetCheckSuccess: action.payload,
            };
        case COMMENT_GET_CHECK_SUCCESS_BY_USER:
            return {
                ...state,
                commentGetCheckSuccessByUser: action.payload,
            };
        case COMMENT_GET_CHECK_SUCCESS_FOR_FEED:
            return {
                ...state,
                commentGetCheckSuccessForFeed: action.payload,
            };
        case COMMENT_POST_CHECK_SUCCESS:
            return {
                ...state,
                commentPostCheckSuccess: action.payload,
            };
        case COMMENT_UPDATE_CHECK_SUCCESS:
            return {
                ...state,
                commentUpdateCheckSuccess: action.payload,
            };
        case COMMENT_DELETE_CHECK_SUCCESS:
            return {
                ...state,
                commentDeleteCheckSuccess: action.payload,
            };

        case COMMENT_SET_DATA:
            return {
                ...state,
                commentData: action.payload,
            };
        case COMMENT_SET_DATA_BY_USER:
            return {
                ...state,
                commentDataByUser: action.payload,
            };
        case COMMENT_SET_DATA_FOR_FEED:
            return {
                ...state,
                commentDataForFeed: action.payload,
            };

        default:
            return state;
    }
};

export default reducer;
