import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,

} from 'react-native';
import {
  Image,
  Icon,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  Product,
  Header,
  TextInput,
  SimilarProducts,
  Browse,

} from '@components';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { PromotionData, TourData1, HotelData, BestData } from '@data';
import { useTranslation } from 'react-i18next';
import { LinearProgress, Avatar } from 'react-native-elements';
import {
  labelsGetRequest, labelsFindOneRequest

} from '../../api/labels';
import { useSelector, useDispatch } from 'react-redux';
export default function UserSelectedProductScreenLABL({ navigation, route }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [icons] = useState([
    {
      image: Images.event4,
      name: 'MEN',
      route: 'Hotel',
    },
    {
      image: Images.event8,
      name: 'WOMEN',
      route: 'Tour',
    },
    {
      image: Images.event9,
      name: 'KIDS',
      route: 'OverViewCar',
    },
    {
      image: Images.event10,
      name: 'JACKETS',
      route: 'FlightSearch',
    },
    {
      image: Images.event8,
      name: 'WINTER',
      route: 'CruiseSearch',
    },
    {
      image: Images.event9,
      name: 'SUMMER',
      route: 'BusSearch',
    },
    {
      image: Images.event10,
      name: 'FESTIVE',
      route: 'DashboardEvent',
    },
    {
      image: Images.event10,
      name: 'more',
      route: 'More',
    },
  ]);
  const [relate] = useState([
    {
      id: '0',
      image: Images.event5,
      title: 'The Roadster',
      time: 'Winter Wear',
      location: '50-70% OFF',
    },
    {
      id: '1',
      image: Images.event6,
      title: 'H&M',
      time: 'Sweat-Shirts',
      location: 'Starting At $899',
    },
  ]);
  const onFilter = () => {
    navigation.navigate('Filter');
  };
  const onChangeSort = () => { };
  const [modeView, setModeView] = useState('');

  const [promotion] = useState(PromotionData);
  const [tour1] = useState(TourData1);
  const [Best] = useState(BestData);
  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(false);
  const { productsObject } = route.params;
  console.log("productsObject",productsObject);
  const products = productsObject?.imagesOfProductByModel;
  const modelId = productsObject?.model?.id;

  // const labelsData = useSelector(state => state.labelsReducer.labelsData);
  // console.log("labelsData", labelsData);
  // const selectedLabelCollecion = labelsData?.collections;
  // useEffect(() => {
  //   dispatch(labelsGetRequest(labelId));

  // }, [dispatch]);
  const selectedProducts = async (data) => {
    setLoading(true);
    // await dispatch(collectionsPostRequest(cName, profileImage,labelId));
    navigation.navigate('UserSelectedProductDetails', { productsObject })
    setLoading(false);
  };
  const onSearch = keyword => {
    const found = searchHistory.some(item => item.keyword == keyword);
    let searchData = [];

    if (found) {
      searchData = searchHistory.map(item => {
        return {
          ...item,
          checked: item.keyword == keyword,
        };
      });
    } else {
      searchData = searchHistory.concat({
        keyword: search,
      });
    }
    setSearchHistory(searchData);
    setLoading(true);
    setTimeout(() => navigation.goBack(), 1000);
  };
  return (
    <View style={{ flex: 1, top: 10 }}>
      {/* <Header
        title={t(productsObject*.label.labelName)}
        subTitle={t(productsObject.collectionName.collectionName)}
        renderLeft={() => {

          return <Icon name="arrow-left" size={24} color={colors.primary} />;
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}
      <View style={{ marginLeft: 55, bottom: 35 }}>
        {/* <TouchableOpacity>
          <Avatar
            rounded
            onPress={() => {
              navigation.navigate('Profile');
            }}
            source={{
              uri: `https://staging-backend.labl.store` + productsObject.collectionName.image.url
            }}
          />
        </TouchableOpacity> */}
        <View style={styles.fixToText}>
          {/* <Button
            style={styles.btnPromotion}
            onPress={() => {
              navigation.navigate('PreviewBooking');
            }}> */}

          <Browse
            modeView={modeView}
            onChangeSort={onChangeSort}
            onFilter={onFilter}
          />
          <Button
            style={styles.btnPromotion}
            onPress={() => {
              navigation.navigate('SearchHistory');
            }}>
            <Text body2 semibold whiteColor>
              {t('Search')}
            </Text>
          </Button>

        </View>
      </View>
      <SafeAreaView >
        <ScrollView
          scrollEventThrottle={8}>
          <View>
            <Text title2 semibold style={styles.titleView}>
              {/* {t('Deals Today')} */}
            </Text>
            <FlatList
              contentContainerStyle={{ paddingLeft: 18, }}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={products}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <Product
                  style={[styles.promotionItem]}
                  image={`https://staging-backend.labl.store` + item.url}
                  onPress={() => selectedProducts(item)}>
                  <Text>
                    {item.name}
                  </Text>
                  <Text >
                    {item.name}
                  </Text>
                  <View style={styles.contentCartPromotion}>
                    <Button
                      style={styles.btnPromotion1}
                      onPress={() => selectedProducts(item)}>
                      <Text body2 semibold whiteColor>
                        {t('View Product')}
                      </Text>
                    </Button>
                    <TouchableOpacity>
                      <View style={{ marginLeft: 140 }}>
                        <Icon name="bookmark" size={24} color={colors.primary}
                          onPress={() => navigation.navigate('EventDetail',)} />
                      </View>
                    </TouchableOpacity>
                  </View>
                </Product>
              )}
            />
          </View>
          <View style={styles.titleView}>
            <Text title3 bold style={{ top: 40, marginBottom: 15, }}>
              {t('RECOMMENDED PRODUCTS')}
            </Text>
            <Text body2 grayColor style={{ top: 30, marginBottom: 5 }}>
              {t('Eligible for Commission')}
            </Text>
            <Text body2 grayColor style={{ marginLeft: 265, bottom: 15, }}>
              {t('See all')}
            </Text>
          </View>
          {/* <FlatList
            columnWrapperStyle={{ paddingRight: 20, }}
            numColumns={2}
            vertical={true}
            showsHorizontalScrollIndicator={false}
            data={selectedLabelCollecion}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item, index }) => (
              <SimilarProducts
                style={[styles.tourItem, { marginLeft: 30 }]}
                image={`https://staging-backend.labl.store`+item?.image?.url}
                onPress={() => navigation.navigate('Collectionlist', { item })}>
                <Text style={{ color: 'white' }}>
                  {item?.collectionName}
                </Text>
              </SimilarProducts>
            )}
          /> */}
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
