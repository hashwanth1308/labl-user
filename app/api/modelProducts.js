import {

  modelProductsPostCheckSuccess,
  modelProductsSetCount,
  modelProductsCountCheckSuccess,
  modelProductsDeleteCheckSuccess,
  modelProductsFindOneCheckSuccess,
  modelProductsSetOneData,
  modelProductsGetCheckSuccess,
  modelProductsSetData,
  modelProductsUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

//create
export function modelProductsPostRequest(obj) {
  // console.log("elemnt",element);
  return dispatch => {
    dispatch(modelProductsPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `model-products`,
      data:obj,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      },


    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelProductsSetData(data));
        dispatch(modelProductsPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(modelProductsPostCheckSuccess(false));

      });
  };
}

//count
export function modelProductsCountRequest(element) {
  return dispatch => {
    dispatch(modelProductsCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `model-products/count`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelProductsSetCount(data));
        dispatch(modelProductsCountCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(modelProductsCountCheckSuccess(false));

      });
  };
}

//delete
export function modelProductsDeleteRequest(id) {

  return dispatch => {
    dispatch(modelProductsDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `model-products/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },

    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelProductsDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(modelProductsDeleteCheckSuccess(false));

      });
  };
}

//findOne
export function modelProductsFindOneRequest(id) {
  return dispatch => {
    dispatch(modelProductsFindOneCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `model-products/${id}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(modelProductsOneData(data));
        dispatch(modelProductsFindOneCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin,)
        dispatch(modelProductsFindOneCheckSuccess(false));
      });
  };
};

//find - get
export function modelProductsGetRequest(obj) {
  return dispatch => {
    dispatch(modelProductsGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `model-products`,
      params:obj,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",

      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(modelProductsSetData(data));
        dispatch(modelProductsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(modelProductsGetCheckSuccess(false));
      });
  };
};

//update
export function modelProductsUpdateRequest(id) {

  return dispatch => {
    dispatch(modelProductsUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `model-products/${id}`,
      headers: {
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(modelProductsSetOneData(data));
        dispatch(modelProductsUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(modelProductsUpdateCheckSuccess(false));

      });
  };
}



