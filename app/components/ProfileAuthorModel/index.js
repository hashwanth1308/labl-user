import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity, Share } from 'react-native';
import { Image, Text } from '@components';
import styles from './styles';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { modelFollowingGetRequest, userFollowingDeleteRequest, modelFollowingPostRequest } from '../../api/following';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import _ from 'lodash';
import { usersFindOneRequest } from '../../api/users';
import { format, render, cancel, register } from 'timeago.js';

export default function ProfileAuthorModel(props) {
  const {
    style,
    image,
    styleLeft,
    styleThumb,
    styleRight,
    onPress,
    name,
    description,
    textRight,
    object
  } = props;
  console.log("objectPost", object);
  const dispatch = useDispatch();
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log(UserData);

const share = async() => {
  const result = await Share.share({
    message:
      'React Native | A framework for building native apps using React',
  });
  if (result.action === Share.sharedAction) {
    if (result.activityType) {
      // shared with activity type of result.activityType
    } else {
      // shared
    }
  } else if (result.action === Share.dismissedAction) {
    // dismissed
  }
}

  // useEffect(() => {
  //   dispatch(usersFindOneRequest(UserData?.user?.id));
  // }, [dispatch]);
  // const user = useSelector(state => state.usersReducer.usersOneData);
  // console.log("user", user);
  useEffect(() => {
    dispatch(modelFollowingGetRequest(UserData?.user?.id))
  }, [dispatch])
  const modelFollowings = useSelector(state => state.followingReducer.modelFollowingData);
  console.log("follow", modelFollowings);
  const [follow, setFollow] = useState(false);
  const [followId, setFollowId] = useState("");
  useEffect(() => {
    const followed = _.find(modelFollowings, function (o) { return o.vendor?.id === object?.vendor?.id; });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true)
    }
  }, []);

  useEffect(() => {
    const followed = _.find(modelFollowings, function (o) { return o.vendor?.id === object?.vendor?.id; });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true)
    }
  }, [modelFollowings]);
  const handleClick = (object) => {
    if (follow) {
      setFollow(false)
      dispatch(userFollowingDeleteRequest(followId));
    }
    else {
      setFollow(true);
      let objectValue = {};
      objectValue.model = UserData?.user?.id,
        objectValue.vendor = object?.id,
        objectValue.isFollowing = true
      dispatch(modelFollowingPostRequest(objectValue));
    }

  }
  return (
    <TouchableOpacity
      style={[styles.contain, style]}
      onPress={onPress}
      activeOpacity={0.9}>
      <View style={[styles.contentLeft, styleLeft]}>
        <View>
          <View style={{ flexDirection: "row",alignItems:"stretch" }}>
            <Image source={{ uri: image ? image : "https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png" }} style={[styles.thumb, styleThumb]} />
            <View style={{ flexDirection: "column" }}>
              <Text numberOfLines={1} style={{ fontFamily: "Montserrat-Medium", fontSize: 20,marginRight:10, alignSelf: "flex-start" }}>
                {name}
              </Text>
              <Text numberOfLines={1} style={{ fontFamily: "Montserrat-Medium", fontSize: 12 }}>
                {format(new Date(description))}
              </Text>
            </View>
            <Text style={{ fontSize: 30, alignSelf: "flex-start",marginTop:-10,marginHorizontal: -5 }}>.</Text>
            {follow ?
              <TouchableOpacity onPress={() => handleClick(object)} style={{ alignSelf: "flex-start", marginVertical: 3,marginHorizontal:8 }}>
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 16, color: "#000" }}>Following</Text>
              </TouchableOpacity> :
              <TouchableOpacity onPress={() => handleClick(object)} style={{ alignSelf: "flex-start", marginVertical: 3,marginHorizontal:8 }}>
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 16, color: "#000" }}>Follow</Text>
              </TouchableOpacity>}
          </View>

        </View>
      </View>
      <TouchableOpacity onPress={() => share()} style={[styles.contentRight, styleRight]}>
        <Icon name="share" size={30} />
      </TouchableOpacity>
    </TouchableOpacity>
  );
}

ProfileAuthorModel.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  image: PropTypes.node.isRequired,
  name: PropTypes.string,
  description: PropTypes.string,
  textRight: PropTypes.string,
  styleLeft: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleThumb: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleRight: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onPress: PropTypes.func,
};

ProfileAuthorModel.defaultProps = {
  image: 'https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png',
  name: '',
  description: '',
  textRight: '',
  styleLeft: {},
  styleThumb: {},
  styleRight: {},
  style: {},
  onPress: () => { },
};
