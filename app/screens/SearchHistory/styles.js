import {StyleSheet} from 'react-native';
import {BaseColor, BaseStyle} from '@config';

export default StyleSheet.create({
  btnClearSearch: {
    position: 'absolute',
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: '100%',
  },
  rowTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemHistory: {
    marginTop: 5,
    padding: 10,
    marginRight: 10,
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    // position: 'absolute',
    backgroundColor:"#000",
    right: 0,
    top: 0,
    zIndex: 1
  },
  textInputStyle: {
    flexDirection: "row",
    alignItems: 'center',
    width:"95%",
    justifyContent: 'space-between',
    height: 48,
    backgroundColor: 'white',
    borderRadius: 20,
    marginHorizontal:10,
    paddingHorizontal: 16,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 2,
  },
});
