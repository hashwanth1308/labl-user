import{ 
    
  invoicesPostCheckSuccess,
  invoicesSetCount,
  invoicesCountCheckSuccess,
  invoicesDeleteCheckSuccess,
  invoicesFindOneCheckSuccess,
  invoicesSetOneData,
  invoicesGetCheckSuccess,
  invoicesSetData,
  invoicesUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function invoicesPostRequest(element) {
      return dispatch => {
        dispatch(invoicesPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `invoices`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(invoicesSetData(a));
            dispatch(invoicesPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(invoicesPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function invoicesCountRequest(element) {
      return dispatch => {
          dispatch(invoicesCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `invoices/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(invoicesSetCount(data));
              dispatch(invoicesCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(invoicesCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function invoicesDeleteRequest(id) {
      
      return dispatch => {
          dispatch(invoicesDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `invoices/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(invoicesDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(invoicesDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function invoicesFindOneRequest(id) {
      return dispatch => {
        dispatch(invoicesFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `invoices/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(invoicesOneData(data));
            dispatch(invoicesFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(invoicesFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function invoicesGetRequest(element) {
  return dispatch => {
    dispatch(invoicesGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `invoices`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(invoicesSetOneData(data));
        dispatch(invoicesGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(invoicesGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function invoicesUpdateRequest(id) {
     
      return dispatch => {
          dispatch(invoicesUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `invoices/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(invoicesSetData(data));
              dispatch(invoicesUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(invoicesUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 