import React, { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  FlatList,
  Animated,
  TouchableOpacity,
  RefreshControl
} from 'react-native';
import { BaseColor, Images, useTheme } from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  StarRating,
  PostListItem,
  HelpBlock,
  Button,
  RoomType,
  CommentItem,
  RateDetail,
  Tag,
  Image,
  ProductDetail,
  Details
} from '@components';
import { productsGetRequest, productsPostRequest } from '../../api/products';
import props from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import * as Utils from '@utils';
import { InteractionManager } from 'react-native';
import { LinearProgress, Avatar } from 'react-native-elements';
// import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import styles from './styles';
import { HelpBlockData, ReviewData, PromotionData1, } from '@data';
import { useTranslation } from 'react-i18next';
import { cartsPostRequest } from '../../api/carts';
import { modelsGetRequest } from '../../api/models';
export default function HotelDetail({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const [reviewList] = useState(ReviewData);
  const [loading, setLoading] = useState(false);

  const [rateDetail] = useState({
    point: 4.7,
    maxPoint: 5,
    totalRating: 25,
    data: ['80%', '10%', '10%', '0%', '0%'],
  });
  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const [renderMapView, setRenderMapView] = useState(false);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(productsGetRequest());

  }, [dispatch])

  // useEffect(() => {
  //   dispatch(cartsPostRequest());

  // }, [dispatch])
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  // console.log("UserData", UserData)
  const loginModelData = useSelector(state => state.modelsReducer.modelsData);
  useEffect(() => {
    dispatch(modelsGetRequest(modelId));
  }, [dispatch])
  const productsObject = route.params;
  console.log(productsObject, "productsObject");
  console.log("loginModelData", loginModelData);
  const productImage = "https://staging-backend.labl.store" + productsObject?.product?.productImages[0]?.url;
  const modelId = loginModelData[0]?.id;
  // console.log("cartsData",cartsData)
  const onSelectFacilities = select => {
    setFacilities(
      facilities.map(item => {
        if (item.name == select.name) {
          return {
            ...item,
            checked: true,
          };
        } else {
          return {
            ...item,
            checked: false,
          };
        }
      }),
    );
  };

  const [promotion1] = useState(PromotionData1);
  const [todo] = useState([
    {
      id: '1',
      title: 'South Travon',
      image: Images.trip1,
    },
    {
      id: '2',
      title: 'South Travon',
      image: Images.trip2,
    },
    {
      id: '3',
      title: 'South Travon',
      image: Images.trip3,
    },
    {
      id: '4',
      title: 'South Travon',
      image: Images.trip4,
    },
    {
      id: '5',
      title: 'South Travon',
      image: Images.trip5,
    },
  ]);
  const [facilities, setFacilities] = useState([
    { id: '1', name: 'S', checked: true },
    { id: '2', name: 'M' },
    { id: '3', name: 'L' },
    { id: '4', name: 'XL' },
  ]);

  const [helpBlock] = useState(HelpBlockData);
  const deltaY = new Animated.Value(0);

  useEffect(() => {
    InteractionManager.runAfterInteractions(() => {
      setRenderMapView(true);
    });
  }, []);

  const heightImageBanner = Utils.scaleWithPixel(250, 1);
  const marginTopBanner = heightImageBanner - heightHeader - 40;
  const { image } = props;


  const saveCart = async (productsObject) => {

    setLoading(true);
    // console.log("button pressed")
    const cartObject = {};
    cartObject.product = productsObject?.id;
    cartObject.model = modelId;
    await dispatch(cartsPostRequest(cartObject));
    navigation.navigate('Cart')
    setLoading(false);
  };

  return (
    <View style={{ flex: 1, top: 10 }}>


      <Header
        title={t(productsObject?.label?.labelName)}
        subTitle={t(productsObject?.collectionName?.collectionName)}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />

          );
        }}

        // renderRight={() => {
        //   // return (
        //   //   <Icon name="images" size={20} color={BaseColor.whiteColor} />
        //   // );
        // }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      // onPressRight={() => {
      //   navigation.navigate('PreviewImage');
      // }}
      />
      <View style={{ marginLeft: 55, bottom: 40 }}>
        <TouchableOpacity>
          <Avatar
            rounded
            onPress={() => {
              navigation.navigate('Profile');
            }}
            source={{
              uri:
                `https://staging-backend.labl.store` + productsObject?.collectionName?.image?.url
            }}
          />
        </TouchableOpacity>
      </View>
      <SafeAreaView >
        <ScrollView
          scrollEventThrottle={8}>
          <View>
            <FlatList
              contentContainerStyle={{ paddingLeft: 5, paddingRight: 20, }}
              horizontal={true}
              showsHorizontalScrollIndicator={true}
              data={productImage}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => (
                <ProductDetail
                  style={[styles.promotionItem]}
                  image={`https://staging-backend.labl.store` + item?.url}
                  onPress={() => navigation.navigate('PreviewImage')}>
                </ProductDetail>
              )}
            />
          </View>
          <View style={{ paddingHorizontal: 20, }}>

            <View>

              <Text headline semibold style={{ marginTop: 25 }}>
                {t(productsObject.productName)}
              </Text>
              <View style={{ alignItems: 'flex-end', bottom: 18 }}>
                <Icon name="bookmark" size={24} color={colors.primary} />
              </View>
              <View style={{ alignItems: 'flex-end', right: 40, bottom: 43 }}>
                <Icon name="telegram-plane" size={28} color={colors.primary} />
              </View>

              <Text body2 style={{ bottom: 45 }}>
                ${productsObject.productPrice}
              </Text>
              <View style={styles.contentList}>
                <FlatList
                  contentContainerStyle={{ paddingLeft: 5, paddingRight: 20 }}
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={facilities}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({ item, index }) => (
                    <Tag
                      primary={item.checked}
                      style={{ marginLeft: 10, width: 60 }}
                      outline={!item.checked}
                      onPress={() => onSelectFacilities(item)}>
                      {item.name}
                    </Tag>
                  )}
                />
              </View>
              <Button
                style={{ top: 10 }}
                // onPress={() => {
                //   navigation.navigate('Cart');
                // }}
                onPress={() => saveCart(productsObject)}
              >
                <Text body2 semibold whiteColor>
                  {t('ADD TO CART')}
                </Text>
              </Button>

            </View>
            <View style={[styles.line, { backgroundColor: colors.primary }]} />

            <View>
              <Text headline semibold style={{ marginTop: 25 }}>
                {t('DESCRIPTION')}
              </Text>
              <Text body2 style={{ marginTop: 15 }}>
                {productsObject?.productDescription}
              </Text>
            </View>
            <View>
              <Text headline semibold style={{ marginTop: 25 }}>
                {t('Product Details')}
              </Text>

            </View>
            <View>
              <Text headline semibold style={{ marginTop: 25 }}>
                {t('Product Availability')}
              </Text>
              <Text body2 style={{ marginTop: 15 }}>
                {productsObject?.productAvailableQuantityALL}
              </Text>
            </View>
            {/* <View>
              <Text headline semibold style={{ marginTop: 25 }}>
                {t('Material & Care')}
              </Text>
              <Text body2 style={{ marginTop: 15 }}>
                {productsObject?.productAvailableQuantityALL}
              </Text>
            </View>
            <View
              style={[styles.blockView, { borderBottomColor: colors.border }]}>
              <Text headline semibold>
                {t('Specifications')}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 5,
                }}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                  }}>
                  <Text body2 grayColor>
                    {t('Sleeve Length')}
                  </Text>
                  <Text body2 accentColor semibold>
                    Long Sleeves
                  </Text>

                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                  }}>
                  <Text body2 grayColor>
                    {t('Neck')}
                  </Text>
                  <Text body2 accentColor semibold>
                    Round Neck
                  </Text>

                </View>
              </View>
            </View> */}

            <View
              style={[styles.blockView, { borderBottomColor: colors.border }]}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 10,
                  alignItems: 'flex-end',
                }}>
                <Text headline semibold>
                  {t('More From This Vendor')}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('Post');
                  }}>
                  <Text caption1 grayColor>
                    {t('show_more')}
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <FlatList
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={todo}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({ item }) => (
                    <Details
                      style={{ marginRight: 15 }}
                      title="HIGHLANDER"
                      date="Only 6 Left"
                      description="Men Black Solid Hooded Sweatshirt"
                      image={item.image}
                      onPress={() => {
                        navigation.navigate('Home');
                      }}
                    />
                  )}
                />
                <View style={[styles.blockView, { borderBottomColor: colors.border }]}></View>
                <View style={[styles.blockView, { borderBottomColor: colors.border }]}></View>
                <View style={[styles.blockView, { borderBottomColor: colors.border }]}></View>
                <View style={[styles.blockView, { borderBottomColor: colors.border }]}></View>
                <View style={[styles.blockView, { borderBottomColor: colors.border }]}></View>

              </View>

            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
