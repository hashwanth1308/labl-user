import React, { useEffect, useMemo, useState } from 'react';
import {
  View,
  Animated,
  RefreshControl,
  FlatList,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  useWindowDimensions,
  Alert,
  BackHandler,
  TouchableHighlight
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
  HotelItem,
  SafeAreaView,
  HomeHeader,
  Text,
  Image,
  ListThumbCircle,
  PostItemModel,
  ProfileAuthor,
  Button,
  ProfileAuthorModel
} from '@components';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon1 from 'react-native-vector-icons/Feather';
import { Avatar } from 'react-native-elements';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { categoriesGetRequest } from '../../api/categories';
import SearchBar from 'react-native-dynamic-search-bar';
import { UserData } from '../../data/user';
import { HotelData } from '../../data/hotel';
import { TabView, TabBar } from 'react-native-tab-view';
import { productsGetRequest } from '../../api/products';
import { labelsFindOneRequest, labelsGetRequest } from '../../api/labels';
import SegmentedControlTab from 'react-native-segmented-control-tab'
import _ from 'lodash';
import { logout } from '../../actions';
import axios from 'axios';

export default function HomeScreenModelLABL({ navigation }) {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [users] = useState(UserData);
  const width = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const height = windowHeight - 220;
  const userData = useSelector(state => state.accessTokenReducer.userData);
  // console.log(userData, 'userdata');
  const [refreshing] = useState(false);
  const [banners, setBanners] = useState([]);
  // console.log("banners", banners);
  const onChangeSort = () => { };


  const [showItemCount, setShowItemCount] = useState(9);
  const [showItemCount1, setShowItemCount1] = useState(9);
  const onLogOut = () => {
    setLoading1(true);
    // dispatch(AuthActions.authentication(false, response => {}));
    // navigation.navigate('SignIn')
    setTimeout(() => {
      dispatch(logout())
      navigation.replace('SignIn');
    }, 1000);
  };

  useEffect(async () => {
    await axios.get("https://staging-backend.labl.store/banners?type=Mobile", {
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${userData?.jwt}`
      }
    }).then((res) => {
      setBanners(res?.data)
    }).catch((err) => console.log(err))
  }, [])


  const layout = useWindowDimensions();

  const [customStyleIndex, setCustomStyleIndex] = useState(0);

  const handleCustomIndexSelect = (index) => {
    setCustomStyleIndex(index);
  }

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(productsGetRequest());
  }, [dispatch]);
  useEffect(() => {
    dispatch(categoriesGetRequest());
  }, [dispatch]);
  const productData = useSelector(
    state => state?.productsReducer?.productsData,
  );
  const productsToShow = useMemo(() => productData.slice(0, showItemCount), [productData, showItemCount]);
  const productsToShow1 = useMemo(() => productData.slice(0, showItemCount1), [productData, showItemCount1]);


  const [scrollEnabled, setScrollEnabled] = useState(true);
  const [slide] = useState([
    { key: 1, image: Images.scroll1, screen: "Cart" },
    { key: 2, image: Images.scroll2 },
    { key: 3, image: Images.scroll3 },
    { key: 4, image: Images.scroll4 },
  ]);
  const [modeView, setModeView] = useState('grid');

  const scrollAnim = new Animated.Value(0);
  const offsetAnim = new Animated.Value(0);
  const clampedScroll = Animated.diffClamp(
    Animated.add(
      scrollAnim.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
        extrapolateLeft: 'clamp',
      }),
      offsetAnim,
    ),
    0,
    40,
  );
  const onFilter = () => {
    navigation.navigate('Filter');
  };
  const onChangeView = () => {
    Utils.enableExperimental();
    switch (modeView) {
      case 'block':
        setModeView('grid');

        break;
      case 'grid':
        setModeView('list');

        break;
      case 'list':
        setModeView('block');

        break;
      default:
        setModeView('block');
        break;
    }
  };
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  return (
    <View style={{ flex: 1, backgroundColor: 'transparent' }}>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <ScrollView>
          <View
            style={{
              flex: 1,
              backgroundColor: '#000',
              height: 120,
              // position: 'absolute',
              width: '100%',
              marginBottom: 10,
            }}>
            <HomeHeader
              // title={userData?.user?.username}
              // subTitle="24 Dec 2018, 2 Nights, 1 Room"
              renderLeft={() => {
                return (
                  <TouchableOpacity onPress={() => {
                    navigation.navigate('ProfileModelLABL');
                  }}
                    style={{ flexDirection: "row" }}
                  >
                    <Avatar
                      rounded
                      style={{ height: 37, width: 37 }}
                      onPress={() => {
                        // navigation.navigate('ProfileModelLABL');
                      }}
                      source={{
                        uri: userData?.user?.image ? userData?.user?.image : "https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png"
                      }}
                    />
                    <Text style={{ marginHorizontal: 10, alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff" }}>{userData?.user?.username}</Text>
                  </TouchableOpacity>
                );
              }}
              // renderRight={() => {
              //   return (
              //     <View>
              //       <TouchableOpacity
              //         onPress={() => onLogOut()}
              //         style={{
              //           flexDirection: 'row',
              //           backgroundColor: '#fff',
              //           height: 22,
              //           width: 65,
              //           borderRadius: 10,
              //           marginBottom: -10,
              //           justifyContent: 'center',
              //         }}>
              //         <Text
              //           style={{
              //             fontFamily: 'Montserrat-SemiBold',
              //             fontSize: 16,
              //             color: '#000',
              //             alignSelf: 'center',
              //           }}>
              //           Model
              //         </Text>
              //         <Icon
              //           name="chevron-down"
              //           size={15}
              //           color="#000"
              //           style={{ alignSelf: 'center' }}
              //         />
              //       </TouchableOpacity>
              //     </View>
              //   );
              // }}
              renderRight={() => {
                return <Icon name="bell" color="#CDCDCD" size={28} />;
              }}
              // onPressLeft={() => {
              //   navigation.goBack();
              // }}
              onPressRightSecond={() => {
                navigation.navigate('Notification');
              }}
            />
            <TouchableOpacity onPress={() => navigation.navigate("SearchHistoryModel")} style={{ padding: 10 }}>
              <Image source={require('../../assets/images/search.png')} style={{ width: 360, height: 35, borderRadius: 20, alignSelf: "center", marginTop: 20 }} />
            </TouchableOpacity>
            {/* <SearchBar
              placeholder="Search here"
              onPress={() => alert('onPress')}
              onChangeText={text => console.log(text)}
              style={{ alignSelf: 'center', top: 35 }}
            /> */}
          </View>


          <Swiper
            // dotStyle={{
            //   backgroundColor: BaseColor.dividerColor,
            //   // top:-100
            // }}
            height={140}
            autoplay={true}
            showsPagination={false}
            // activeDotColor={colors.primary}
            // paginationStyle={styles.contentPage}
            removeClippedSubviews={false}>
            {banners?.map((item, index) => {
              return (
                <TouchableHighlight style={styles.slide} key={item.key}>
                  <Image source={{ uri: item?.image }} style={styles.img} />
                </TouchableHighlight>
              );
            })}
          </Swiper>
          <View style={{ padding: 5 }}>
            <SegmentedControlTab
              values={['GRID', 'LIST']}
              selectedIndex={customStyleIndex}
              onTabPress={(index) => handleCustomIndexSelect(index)}
              borderRadius={0}
              tabsContainerStyle={{ height: 50, backgroundColor: '#000' }}
              tabStyle={{ backgroundColor: '#CDCDCD', borderWidth: 0, borderColor: 'transparent' }}
              activeTabStyle={{ backgroundColor: '#000', marginTop: 2 }}
              tabTextStyle={{ color: '#000', fontWeight: 'bold', fontSize: 20 }}
              activeTabTextStyle={{ color: '#fff' }}
            />
            {customStyleIndex === 0
              && <View>
                <FlatList
                  contentContainerStyle={{
                    paddingLeft: 5,
                    paddingRight: 20,
                    paddingTop: 10,
                  }}
                  numColumns={3}
                  // initialNumToRender={6}
                  data={productsToShow}
                  // onEndReached={() => setShowItemCount(showItemCount + 9)}
                  // onEndReachedThreshold={0.5}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({ item, index }) => (
                    // console.log("https://staging-backend.labl.store" + item?.product?.productImages[0]?.url, "imagesss")
                    <HotelItem
                      // grid
                      image={
                        item?.productImages[0]?.url
                      }
                      name={item?.name}
                      location={item.location}
                      price={item.price}
                      available={item.available}
                      rate={item.rate}
                      rateStatus={item.rateStatus}
                      numReviews={item.numReviews}
                      services={item.services}
                      // style={{ marginLeft: 15, marginBottom: 20 }}
                      onPress={() =>
                        navigation.navigate('PostDetailModelLABL', { object: item })
                      }
                    />
                  )}
                />
                {showItemCount < productData.length && (
                  <TouchableOpacity style={{ marginVertical: 10, backgroundColor: "#000", alignSelf: "center", justifyContent: "center", alignItems: "center", height: 40, width: 100 }} onPress={() => setShowItemCount(showItemCount + 9)} >
                    <Text style={{ color: "#fff", fontFamily: "Roboto-Bold", fontSize: 16 }}>Load More</Text>
                  </TouchableOpacity>
                )}
              </View>}
            {customStyleIndex === 1
              && <View style={{ flex: 1 }}>
                <FlatList
                  contentContainerStyle={{
                    flex: 1,
                    // paddingLeft: 5,
                    // paddingRight: 20,
                    padding: 10,
                  }}
                  // numColumns={3}
                  data={productsToShow1}
                  // onEndReached={() => setShowItemCount(showItemCount + 9)}
                  // onEndReachedThreshold={0.5}
                  keyExtractor={(item, index) => item.id}
                  renderItem={({ item, index }) => (
                    <View
                      style={{
                        flex: 1,
                        borderColor: '#CDCDCD',
                        borderRadius: 10,
                        borderWidth: 1,
                        marginBottom: 30,
                      }}>
                      <PostItemModel
                        image={item?.productImages[0]?.url}
                        title={item.name}
                        description={item.name}
                        likes={item?.likes?.length}
                        commentsLength={item?.comments}
                        style={{ margin: 0 }}
                        onPress={() => navigation.navigate('PostDetailModelLABL', { object: item })}
                        object={item}
                        commentNavigate={() => navigation.navigate('CommentsModel', { object: item })}>
                        <ProfileAuthorModel
                          image={item?.label?.user?.image}
                          name={item?.label?.labelName}
                          textRight={item.name}
                          object={item}
                          description={item?.createdAt}
                          style={{ paddingHorizontal: 0 }}
                          onPress={() => navigation.navigate('Profile5ModelLABL')}
                        />
                      </PostItemModel>
                    </View>
                  )}
                />
                {showItemCount1 < productData.length && (
                  <TouchableOpacity style={{ marginVertical: 10, backgroundColor: "#000", alignSelf: "center", justifyContent: "center", alignItems: "center", height: 40, width: 100 }} onPress={() => setShowItemCount1(showItemCount1 + 9)} >
                    <Text style={{ color: "#fff", fontFamily: "Roboto-Bold", fontSize: 16 }}>Load More</Text>
                  </TouchableOpacity>
                )}
              </View>}
          </View>
        </ScrollView>
      </SafeAreaView>
      {/* <View style={{flex:0.1, backgroundColor:'transparent'}}>
        <TouchableOpacity style={{ alignSelf: "center", flexDirection: "row", height: 60, width: 200, backgroundColor: "#000", borderRadius: 30,}}>
        </TouchableOpacity>
      </View> */}
      {/* <View
        style={{
          borderWidth: 1,
          borderColor: 'rgba(0,0,0,0.2)',
          alignItems: 'center',
          justifyContent: 'center',
          width: 140,
          position: 'absolute',
          bottom: 10,
          right: 120,
          height: 50,
          // zIndex:13,
          // elevation: 100,
          backgroundColor: '#000',
          borderRadius: 100,
        }}>
        {val ? renderItem() : null}
        {val ?
          <TouchableOpacity
            style={{
              flexDirection: "row",
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => setVal(false)}
          >
            <Icon name='plus-circle' size={30} color='#fff' />
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22, color: "#fff", marginHorizontal: 10 }}>Upload</Text>
          </TouchableOpacity> :
          <TouchableOpacity
            style={{
              flexDirection: "row",
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => handleChange()}
          >
            <Icon name='plus-circle' size={30} color='#fff' />
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22, color: "#fff", marginHorizontal: 10 }}>Upload</Text>
          </TouchableOpacity>}
      </View> */}
      {loading1 &&
        <View style={{ width: width, height: windowHeight, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}


