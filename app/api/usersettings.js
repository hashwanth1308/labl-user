import{ 
    usersettingsGetCheckSuccess,
    usersettingsSetData,
    usersettingsPostCheckSuccess,
    usersettingsUpdateCheckSuccess,
    usersettingsDeleteCheckSuccess,

    } from '../actions';
    import axios from 'axios';
    let a = [];
    
    export function usersettingsGetRequest(element,token) {
    return dispatch => {
      dispatch(usersettingsGetCheckSuccess(false));
      const axiosLogin = axios.create();
        return axiosLogin({
          method: 'get',
          url: `user-settings`,
          headers:{
            //  Authorization: `Bearer ${token}`,
            "Content-Type" : "application/json"
          }
          })
        .then(response => {
          if (response.status !== 200) {
            throw Error(response.statusText);
          }
          return response.data;
        })
        .then(data => {
          // console.log(data)
          // console.log(element,"user data",data.data[0])
          // const propertyValues = Object.values(data.data);
          // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
          // a.push(obj)
          dispatch(usersettingsSetData(data));
          dispatch(usersettingsGetCheckSuccess(true));
        })
        .catch((error) => {
          console.log(error,axiosLogin,'furnishings')
          dispatch(usersettingsGetCheckSuccess(false));
        });
    };
    };
    
    export function usersettingsPostRequest(element) {
        return dispatch => {
          dispatch(usersettingsPostCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'post',
              url: `user-settings`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(usersettingsSetData(a));
              dispatch(usersettingsPostCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(usersettingsPostCheckSuccess(false));
             
            });
        };
    }
    
    export function usersettingsUpdateRequest(element) {
        console.log(element.accessToken)
        const accessToken = element.accessToken;
        delete element.accessToken;
        return dispatch => {
            dispatch(usersettingsSetData(element));
            dispatch(usersettingsUpdateCheckSuccess(false));
            const axiosLogin = axios.create();
            return axiosLogin({
                method: 'put',
                url: `user-settings/610cd05b3be536107bcb5bff`,
                headers:{
                Authorization: `Bearer ${accessToken}`,
                "Content-Type" : "application/json"},
                data:element,
                })
            .then(response => {
                // console.log(response)
                if (response.status !== 200) {
                throw Error(response.statusText);
                }
                return response.data;
            })
            .then(data => {
                // console.log("data",data)
                dispatch(usersettingsSetData(a));
                dispatch(usersettingsUpdateCheckSuccess(true));
            })
            .catch((error) => {
                console.log(error,axiosLogin)
                dispatch(usersettingsUpdateCheckSuccess(false));
                
            });
        };
    }

    export function usersettingsDeleteRequest(element) {
        return dispatch => {
          dispatch(usersettingsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
            return axiosLogin({
              method: 'delete',
              url: `user-settings/610cd05b3be536107bcb5bff`,
              headers:{
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type" : "application/json"}
             
              })
            .then(response => {
              if (response.status !== 200) {
                throw Error(response.statusText);
              }
              return response.data;
            })
            .then(data => {
              const propertyValues = Object.values(data.data);
              const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
              a.push(obj)
              dispatch(usersettingsSetData(a));
              dispatch(usersettingsDeleteCheckSuccess(true));
            })
            .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(usersettingsDeleteCheckSuccess(false));
             
            });
        };
    }