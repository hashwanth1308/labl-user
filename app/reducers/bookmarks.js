import {
    BOOKMARKS_GET_CHECK_SUCCESS,
    BOOKMARKS_POST_CHECK_SUCCESS,
    BOOKMARKS_UPDATE_CHECK_SUCCESS,
    BOOKMARKS_DELETE_CHECK_SUCCESS,
    BOOKMARKS_SET_DATA,
    BOOKMARKS_SET_DATA_BY_USER,
    BOOKMARKS_GET_CHECK_SUCCESS_BY_USER,
    BOOKMARKS_GET_CHECK_SUCCESS_FOR_FEED,
    BOOKMARKS_SET_DATA_FOR_FEED
  } from '../actions/actionTypes';
  
  const initialState = {
    bookmarksGetCheckSuccess: false,
    bookmarksData: [],
    bookmarksPostCheckSuccess: false,
    bookmarksUpdateCheckSuccess: false,
    bookmarksDeleteCheckSuccess: false,
    bookmarksGetCheckSuccessByUser: false,
    bookmarksDataByUser: [],
    bookmarksDataForFeed: [],
    bookmarksGetCheckSuccessForFeed: false
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case BOOKMARKS_GET_CHECK_SUCCESS:
        return {
          ...state,
          bookmarksGetCheckSuccess: action.payload,
        };
      case BOOKMARKS_GET_CHECK_SUCCESS_BY_USER:
        return {
          ...state,
          bookmarksGetCheckSuccessByUser: action.payload,
        };
      case BOOKMARKS_GET_CHECK_SUCCESS_FOR_FEED:
        return {
          ...state,
          bookmarksGetCheckSuccessForFeed: action.payload,
        };
      case BOOKMARKS_POST_CHECK_SUCCESS:
        return {
          ...state,
          bookmarksPostCheckSuccess: action.payload,
        };
      case BOOKMARKS_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
          bookmarksUpdateCheckSuccess: action.payload,
        };
      case BOOKMARKS_DELETE_CHECK_SUCCESS:
        return {
          ...state,
          bookmarksDeleteCheckSuccess: action.payload,
        };
  
      case BOOKMARKS_SET_DATA:
        return {
          ...state,
          bookmarksData: action.payload,
        };
      case BOOKMARKS_SET_DATA_BY_USER:
        return {
          ...state,
          bookmarksDataByUser: action.payload,
        };
      case BOOKMARKS_SET_DATA_FOR_FEED:
        return {
          ...state,
          bookmarksDataForFeed: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  