import {
  FAVDESIGNS_POST_CHECK_SUCCESS,
  FAVDESIGNS_SET_COUNT,
  FAVDESIGNS_COUNT_CHECK_SUCCESS,
  FAVDESIGNS_DELETE_CHECK_SUCCESS,
  FAVDESIGNS_FINDONE_CHECK_SUCCESS,
  FAVDESIGNS_SET_ONE_DATA,
  FAVDESIGNS_GET_CHECK_SUCCESS,
  FAVDESIGNS_SET_DATA,
  FAVDESIGNS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  favDesignsPostCheckSuccess: false,
  favDesignsCount: 0,
  favDesignsCountCheckSuccess : false,
  favDesignsDeleteCheckSuccess: false,
  favDesignsFindOneCheckSuccess: false,
  favDesignsOneData: [],
  favDesignsGetCheckSuccess: false,
  favDesignsData: [],
  favDesignsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FAVDESIGNS_POST_CHECK_SUCCESS:
      return {
        ...state,
        favDesignsPostCheckSuccess: action.payload,
      };
      case FAVDESIGNS_SET_COUNT:
      return {
        ...state,
        favDesignsCount: action.payload,
      };
      case FAVDESIGNS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        favDesignsCountCheckSuccess: action.payload,
      };
      case FAVDESIGNS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        favDesignsDeleteCheckSuccess: action.payload,
      };
      case FAVDESIGNS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        favDesignsFindOneCheckSuccess: action.payload,
          };
      case FAVDESIGNS_SET_ONE_DATA:
      return {
        ...state,
        favDesignsOneData: action.payload,
          };
      case FAVDESIGNS_GET_CHECK_SUCCESS:
      return {
        ...state,
        favDesignsGetCheckSuccess: action.payload,
          };
      case FAVDESIGNS_SET_DATA:
      return {
        ...state,
        favDesignsData: action.payload,
          };
      case FAVDESIGNS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        favDesignsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
