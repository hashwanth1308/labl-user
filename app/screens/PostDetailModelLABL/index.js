import React, { useEffect, useRef, useState } from 'react';
import { View, ScrollView, Animated, TouchableOpacity, FlatList, ActivityIndicator, Dimensions, ToastAndroid, RefreshControl } from 'react-native';
import { BaseColor, Images, useTheme } from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  ProfileAuthor,
  ProfileGroup,
  Card,
  PostListItem,
  Image
} from '@components';
import * as Utils from '@utils';
import styles from './styles';
import Modal from 'react-native-modal';
import { useTranslation } from 'react-i18next';
import Icon1 from 'react-native-vector-icons/Fontisto';
import { useDispatch, useSelector } from 'react-redux';
import Swiper from 'react-native-swiper';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon3 from 'react-native-vector-icons/Ionicons';
import { bookmarksDeleteRequest, bookmarksGetRequestByModelUser, bookmarksGetRequestByUser, bookmarksPostRequest } from '../../api/bookmarks';
import _, { uniq } from 'lodash';
import { cartsPostRequest } from '../../api/carts';
import { labelsFindOneRequest, labelsGetRequest } from '../../api/labels';
import { productVarientGetRequest, productsGetRequestUsingCollectionId, productsGetRequest } from '../../api/products';
import { addressGetRequest } from '../../api/address';
import Snackbar from 'react-native-snackbar';
import { cartsGetRequest } from '../../api/carts';
import { modelProductsPostRequest, productRatingGetRequest, modelProductsGetRequest } from '../../api';
import { productSizeGetRequest } from '../../api/products';
import axios from 'axios';

export default function PostDetailModelLABL({ navigation, route }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const { colors } = useTheme();
  const [loading, setLoading] = useState(false);
  const [atpbutton, setAtpbutton] = useState();
  console.log("atpbutton",atpbutton);
  const [atp, setAtp] = useState(false);
  // console.log("atp", atp)
  const userData = useSelector(state => state.accessTokenReducer.userData);
  console.log(userData, "userData");
  const [refreshing, setRefreshing] = useState(false);
  const [dummy, setDummy] = useState(false);



  //to check whether the product is in model's profile or not

  // console.log('modelProductsData', modelProductsData);

  useEffect(async () => {
    const obj = {};
    obj.labelId = object?.label?.id;
    obj.modelId = userData?.user?.model?.id;
    obj.productId = object?.id;
    await axios
      .post('https://staging-backend.labl.store/getModels', obj, {
        headers: {
          'content-type': 'application/json',
        },
      })
      .then(({ data }) => {
        // console.log("----->>>>>>orderuser", data);
        setAtpbutton(data)

      })
      .catch((err) => { console.log(err); });



  }, [loading1, dummy])

  useEffect(async () => {
    await axios.get(`https://staging-backend.labl.store/model-products?model=${userData?.user?.model?.id}`)
      .then((res) => {
        // console.log("res", res.data);
        const proditem = _.find(res?.data, function (o) {
          return o?.product?.id === object?.id;
        });
        console.log(proditem, 'proditem');
        if (proditem) {
          // setProductId(whishlistitems.id);
          setAtp(true);
        } else setAtp(false)
      }).catch((err) => console.log(err))
    console.log("atp", atp);
  }, [dummy])

  // const modelProductsData = useSelector(
  //   state => state.modelProductsReducer.modelProductsData,
  // );

  // useEffect(() => {
  //   const proditem = _.find(modelProductsData, function (o) {
  //     return o?.product?.id === object?.id;
  //   });
  //   // console.log(proditem, 'proditem');
  //   if (proditem) {
  //     // setProductId(whishlistitems.id);
  //     setAtp(true);
  //   }
  // }, [dummy])



  const addToProfile = async () => {
    setLoading1(true);
    const obj = {};
    obj.product = object?.id;
    obj.model = userData?.user?.model?.id;
    obj.category = object?.subCategory?.parentCategory?.category?.id;
    obj.parentCategory = object?.subCategory?.parentCategory?.id;
    obj.subCategory = object?.subCategory?.id;
    obj.priceOfProductWithModelCommission = Math.round(Number(object?.productPrice) + ((0.17) * Number(object?.productPrice)));
    obj.imagesOfProductByModel = object?.productImages;
    await dispatch(modelProductsPostRequest(obj));
    setAtp(true);
    setLoading1(false);
  }


  // useEffect(() => {
  //   const proditem = _.find(modelProductsData, function (o) {
  //     return o?.product?.id === object?.id;
  //   });
  //   if (proditem) {
  //     // setProductId(whishlistitems.id);
  //     setAtp(true);
  //   }
  // }, [modelProductsData, loading1]);


  const { object } = route.params;
  console.log(object, "postDetails");
  const deltaY = new Animated.Value(0);
  const heightHeader = Utils.heightHeader();
  const heightImageBanner = Utils.scaleWithPixel(300);
  const marginTopBanner = heightImageBanner - heightHeader - 30;

  useEffect(() => {
    dispatch(productSizeGetRequest());
  }, [dispatch]);
  const size = useSelector(state => state.productsReducer.productSizeData);
  // console.log("size", size);
  useEffect(() => {
    const obj = {};
    obj.id = object?.label?.id;
    dispatch(labelsGetRequest(obj));
  }, [dispatch]);
  const labels = useSelector(
    state => state.labelsReducer.labelsData,
  );
  const prods = labels?.map((item) => item?.products);
  const otherProducts = prods.flat()?.filter((item) => item?.id !== object?.id)

  // console.log("prods", prods.flat());

  const viewRef = useRef();
  const _scrollViewBottom = height;
  const [sel, setSel] = useState(false);
  const scrollToBottom = () => {
    setTimeout(() => {
      if (viewRef.current) {
        viewRef.current.scrollTo({
          animated: true,
          y: height + 100,
          x: 0,
        });
      }
    }, 500);
  }

  const scrollToTop = (item) => {

    setLoading1(true);
    setRefreshing(true)
    setTimeout(() => {
      if (viewRef.current) {
        viewRef.current.scrollTo({
          animated: true,
          y: height - 1000,
          x: 0,
        });
      }
      navigation.navigate('PostDetailModelLABL', { object: item })
      setRefreshing(false);
      setLoading1(false);
      setAtp(false);
      setDummy(!dummy)
    }, 1000);
    // setLoading1(false);
  }


  useEffect(() => {
    dispatch(productsGetRequest());
  }, []);
  const productsData = useSelector(state => state.productsReducer.productsData);

  useEffect(() => {
    const obj = {};
    obj.user = userData?.user?.id;
    dispatch(addressGetRequest(obj));

  }, []);
  const UserAddress = useSelector(state => state.addressReducer.addressData);
  // console.log("address", UserAddress);

  const [modalVisible, setModalVisible] = useState(false);
  const toggleModal = () => {
    setModalVisible(!modalVisible);
  };

  useEffect(() => {
    const obj = {};
    obj.product = object?.id;
    dispatch(cartsGetRequest(obj));

  }, []);
  const cartData = useSelector(
    state => state.cartsReducer.cartsData,
  );
  // console.log(cartData, 'cartData');

  useEffect(() => {
    const cartitem = _.find(cartData, function (o) {
      return o?.product?.id === object?.id;
    });
    // console.log(cartitem, 'cartitem');
    if (cartitem) {
      // setProductId(whishlistitems.id);
      setAddedCart(true);
    }
  }, []);

  useEffect(() => {
    const cartitem = _.find(cartData, function (o) {
      return o?.product?.id === object?.id;
    });
    if (cartitem) {
      // setProductId(whishlistitems.id);
      setAddedCart(true);
    }
  }, [cartData]);

  useEffect(() => {
    loadUserData();
  }, [refreshing]);

  const loadUserData = () => {
    // setRefreshing(true)
    dispatch(productVarientGetRequest(object?.id));
  };



  useEffect(() => {
    dispatch(bookmarksGetRequestByModelUser(userData?.user?.model?.id));
  }, []);
  useEffect(() => {
    dispatch(labelsFindOneRequest(object?.label?.id));
  }, []);
  useEffect(() => {
    dispatch(productVarientGetRequest(object?.id));
  }, []);
  // useEffect(() => {
  //   const obj = {};
  //   obj.product = object?.id;
  //   obj.size = id;
  //   dispatch(productsGetRequestUsingCollectionId(obj))
  // }, []);
  const productVarient = useSelector(state => state.productsReducer.productVarientData);
  // console.log("productVarient", productVarient);


  const [varientSelected, setVarientSelected] = useState(null);
  // console.log("varientSelected", varientSelected);
  const uniqueIds = [];


  const sizes = ['S', 'M', 'L', 'XL', 'XXL','XXXL','Free Size'];

  const sortByReference = (a, b) => {
    const aIndex = sizes.findIndex(reference => reference === a.type);
    const bIndex = sizes.findIndex(reference => reference === b.type);
    return aIndex - bIndex;
  };

  const uniqueProduct = [...new Set(productVarient.map(item => item.size))];
  // console.log("uniqueProduct", uniqueProduct);

  const uniquePr = uniqueProduct.filter(element => {
    const isDuplicate = uniqueIds.includes(element?.type);

    if (!isDuplicate) {
      uniqueIds?.push(element?.type)
      return true;
    }

    return false;
  });
  const unique = uniquePr?.sort(sortByReference);
  // console.log("unique", unique);
  const sizeOrder = size?.sort(sortByReference);
  // console.log("sizeOrder", sizeOrder);



  const productColor = useSelector(state => state.productsReducer.productsDataFromSelectedCollection);
  // console.log("productColor", productColor);

  const [selectAddress, setSelectedAddress] = useState(null);
  // console.log("selectAddress", selectAddress);

  const vendor = useSelector(state => state.labelsReducer.labelsData);
  // console.log("vendor", vendor);
  const WhistList = useSelector((state) => state.bookMarksReducer.bookmarksDataByUser);
  // console.log(WhistList, "whishlist");
  const [productId, setProductId] = useState("");
  const [productmarked, setProductmarked] = useState(false);


  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) { return o.product?.id === object?.id });
    // console.log(whishlistitems, "whishlistitems");
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true)
    }
  }, []);

  useEffect(() => {
    const whishlistitems = _.find(WhistList, function (o) { return o.product.id === object?.id });
    if (whishlistitems) {
      setProductId(whishlistitems.id);
      setProductmarked(true)
    }
  }, [WhistList]);
  const addWhishlist = async () => {
    if (productmarked) {
      setProductmarked(false);
      await dispatch(bookmarksDeleteRequest(productId));
    }
    else {
      setProductmarked(true);
      const objectValue = {};
      objectValue.model = userData?.user?.model?.id;
      objectValue.product = object?.id;
      // console.log("bookmarkpost", objectValue);
      // setLoading(true);

      await dispatch(bookmarksPostRequest(objectValue));
    }
  }
  const showToastWithGravityAndOffset = () => {
    ToastAndroid.showWithGravityAndOffset(
      "Product is added to your Cart ",
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
    );
  };
  useEffect(() => {
    const obj = {};
    obj.product = object?.id;
    dispatch(productRatingGetRequest(obj));

  }, []);
  const rating = useSelector(state => state.productRatingReducer.productRatingData);
  // console.log("rating", rating);

  const usersRated = rating?.filter((item) => item?.model)

  const totalRating = rating?.map((item) => item?.rating);
  // console.log("totalRating", totalRating);
  const totalSum =
    totalRating?.length > 0 &&
    totalRating?.reduce((partialSum, a) => Number(partialSum) + Number(a), 0) / totalRating?.length;


  const [addedCart, setAddedCart] = useState(false);
  const handleAddToCart = () => {
    if (varientSelected === null || selectAddress === null) {
      alert("Please select Size,Color and Address before adding to cart")
    } else {
      const cartData = {}
      cartData.model = userData?.user?.model?.id;
      cartData.product = object?.id;
      cartData.productVarient = varientSelected;
      cartData.address = selectAddress?.id;
      dispatch(cartsPostRequest(cartData));
      // console.log(cartData, "cartData");
      setAddedCart(true);
      Snackbar.show({
        text: 'Product is added your Cart',
        duration: Snackbar.LENGTH_SHORT,
      });
    }
  }

  useEffect(() => {
    setChecked(null);
    setChecked1(null);
  }, [loading1, refreshing]);

  const [checked, setChecked] = useState(null);
  const [checked1, setChecked1] = useState(null);
  const onSelectSizes = select => {
    setLoading1(true);
    const obj = {};
    obj.product = object?.id;
    obj.size = select?.id;
    dispatch(productsGetRequestUsingCollectionId(obj))
    setChecked(select?.id);
    setLoading1(false);
  };

  const handleChange = (item) => {
    setChecked1(item?.id);
    setVarientSelected(item)
  }


  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('Shopping Bag')}
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flexDirection: "row", width: 200, justifyContent: "space-between", alignSelf: "flex-start", top: -10 }}>
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
              <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", width: 150 }}>{object?.productBrandName}</Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView style={{ flex: 1 }} edges={['right', 'left', 'bottom']}>
        <ScrollView
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: { y: deltaY },
              },
            },
          ])}
          ref={viewRef}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={loadUserData} />
          }
          scrollEventThrottle={8}>
          <Profile
            image={userData?.image}
            name={object?.label?.labelName}
            // description="5 hours ago | 100k views"
            // textRight="Jun 2018"
            style={{
              marginTop: 20,
            }}
          />
          <View
            style={{
              paddingHorizontal: 20,
              marginBottom: 20,
              // marginTop: marginTopBanner,
            }}>
            <View style={{ marginVertical: 0 }}>
              <Swiper
                autoplay={true}
                height={450}
                showsPagination={false}
                removeClippedSubviews={false}
              >
                {object?.productImages?.length > 0 && object?.productImages?.map((item, index) => {
                  return (
                    <TouchableOpacity onPress={() => navigation.navigate("PreviewImage", { obj: object?.productImages })} style={styles.slide} key={item.key}>
                      <Image source={{ uri: item?.url }} style={styles.imgBanner} />
                    </TouchableOpacity>
                  );
                })}
              </Swiper>
            </View>
            <View style={{ flex: 1, height: 70, backgroundColor: "#909090", opacity: 1, padding: 15, flexDirection: "row", justifyContent: "space-between" }}>
              <TouchableOpacity onPress={() => scrollToBottom()} style={{ flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
                <Icon1 name="shopping-bag" size={17} color="#fff" style={{ marginVertical: 5 }} />
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#fff" }}>Similar Products</Text>
              </TouchableOpacity>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#fff" }}>{Number(totalSum)}</Text>
                <Icon1 name="star" size={12} color="#fff" style={{ marginHorizontal: 5 }} />
                <View style={{ borderWidth: 0.6, borderColor: "#fff", height: 20, marginHorizontal: 10 }}></View>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#fff" }}>{usersRated?.length}</Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-around", height: 120, width: "100%", backgroundColor: "#fff" }}>
              {/* <TouchableOpacity onPress={() => addWhishlist()} style={{ flexDirection: "row", borderColor: "#D9D9D9", borderWidth: 1, height: 50, width: 150, borderRadius: 10, justifyContent: "space-evenly", alignItems: "center" }}>
                {productmarked ? (
                  <>
                    <Icon1 name="heart" size={20} color="black" />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "black" }}>Wishlist</Text>
                  </>
                ) : (
                  <>
                    <Icon1 name="heart" size={20} color="#CDCDCD" />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#CDCDCD" }}>Wishlist</Text>
                  </>
                )}
              </TouchableOpacity> */}
              {atpbutton?.result === true ?
                (atpbutton?.product === object?.id || atp === true ?
                  <View style={{ flexDirection: "row", borderColor: "#000", borderWidth: 1, height: 50, width: 280, borderRadius: 10, justifyContent: "space-evenly", alignItems: "center" }}>
                    <Icon3 name="checkmark-circle-outline" size={24} color="green" />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#000" }}>Added to your Portfolio</Text>
                  </View> :
                  <TouchableOpacity onPress={() => addToProfile()} style={{ flexDirection: "row", backgroundColor: "#000", height: 50, width: 250, borderRadius: 10, justifyContent: "space-evenly", alignItems: "center" }}>
                    {loading ? <ActivityIndicator size={"large"} color="#000" /> :
                      <>
                        <Icon3 name="ios-grid" size={20} color="#fff" />
                        <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#fff" }}>Add to your Portfolio</Text>
                      </>}
                  </TouchableOpacity>) :
                addedCart ?
                  (<TouchableOpacity onPress={() => navigation.navigate("CartModelLABL")} style={{ flexDirection: "row", backgroundColor: "#000", height: 50, width: 150, borderRadius: 10, justifyContent: "space-evenly", alignItems: "center" }}>
                    <Icon1 name="shopping-bag" size={17} color="#fff" />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#fff" }}>View Cart</Text>
                  </TouchableOpacity>) :
                  (<TouchableOpacity onPress={() => handleAddToCart()} style={{ flexDirection: "row", backgroundColor: "#000", height: 50, width: 150, borderRadius: 10, justifyContent: "space-evenly", alignItems: "center" }}>
                    <Icon1 name="shopping-bag" size={17} color="#fff" />
                    <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#fff" }}>Add to Bag</Text>
                  </TouchableOpacity>)}
            </View>
            <Text
              style={{
                fontFamily: "Montserrat-Bold",
                fontSize: 16
              }}>
              Brand - {object?.productBrandName}
            </Text>
            <Text
              style={{
                fontFamily: "Montserrat-Medium",
                fontSize: 16,
                marginVertical: 10

              }}>
              {object?.productName}
            </Text>
            <Text
              style={{
                fontFamily: "Montserrat-SemiBold",
                fontSize: 16,
                marginVertical: 10

              }}>
              ₹ {object?.productPrice}
            </Text>
            {atpbutton?.product === object?.id || atp === true ? null :
            <View style={{ flexDirection: "column", marginVertical: 10, marginBottom: 0 }}>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16 }}>Available Size : </Text>

              {unique != "" ?
                // <Component unique={unique} object={object} productColor={productColor} onChange={setVarientSelected} />
                <View>
                  <FlatList
                    horizontal
                    data={unique}
                    keyExtractor={(item, index) => item.id}
                    renderItem={({ item, index }) => (
                      (checked === item?.id ?
                        <TouchableOpacity style={{ margin: 10, backgroundColor: "#000", borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
                          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, color: "#fff" }}>{item?.type}</Text>
                        </TouchableOpacity> :
                        <TouchableOpacity onPress={() => onSelectSizes(item)} style={{ margin: 10, borderColor: "#000", borderWidth: 1, borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
                          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16 }}>{item?.type}</Text>
                        </TouchableOpacity>
                      )
                    )}
                  />
                  {checked ?
                    <View style={{ flexDirection: "column", marginVertical: 15 }}>
                      <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>Available Colors : </Text>
                      {/* <ColorComponent color={productColor} onChange={onChange} /> */}
                      <View>
                        <FlatList
                          horizontal
                          data={productColor}
                          keyExtractor={(item, index) => item.id}
                          renderItem={({ item, index }) => (
                            (checked1 === item?.id ?
                              <TouchableOpacity style={{ margin: 10, backgroundColor: "#000", borderRadius: 20, width: 75, height: 35, justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#fff" }}>{item?.color?.color}</Text>
                              </TouchableOpacity> :
                              <TouchableOpacity onPress={() => handleChange(item)} style={{ margin: 10, borderWidth: 1, borderColor: "#000", borderRadius: 20, width: 75, height: 35, justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18 }}>{item?.color?.color}</Text>
                              </TouchableOpacity>)
                          )}

                        />
                      </View>
                      {/* <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20 }}>{object?.productAvailableSizes}</Text> */}
                    </View> : null}
                </View>
                :
                <View style={{ marginVertical: 15 }}>
                  <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 16, color: "#000" }}>No Sizes available!!!</Text>
                </View>}
            </View>}
            {/* <View style={{ height: 70, width: "100%", backgroundColor: "#000", borderRadius: 10, justifyContent: "space-around", alignItems: "center", flexDirection: "row" }}>
              <View style={{ height: 50, width: 50, borderRadius: 10, backgroundColor: "#fff", justifyContent: "center" }}>
                <Icon2 name="message-processing" size={35} style={{ alignSelf: "center" }} />
              </View>
              <View style={{ flexDirection: "column" }}>
                <Text style={{ color: "#fff", fontFamily: "Montserrat-Bold", fontSize: 23, marginVertical: 0 }}>Chat with vendor</Text>
                <Text style={{ color: "#fff", fontFamily: "Montserrat-SemiBold", fontSize: 20, marginVertical: 5 }}>Tap to chat</Text>
              </View>
              <TouchableOpacity style={{ height: 40, width: 40, backgroundColor: "#909090", justifyContent: "center", borderRadius: 20 }}>
                <Icon2 name="chevron-right" size={40} style={{ color: "#fff", alignSelf: "center" }} />
              </TouchableOpacity>
            </View> */}
           {atpbutton?.product === object?.id || atp === true ? null :
            <View style={{ marginVertical: 10 }}>
              {UserAddress?.length > 0 ?
                <View>
                  <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 16, marginVertical: 10 }}>Delivery Details :-</Text>
                  <View style={{ borderWidth: 1, height: 60, width: "100%", borderRadius: 10, flexDirection: "row", justifyContent: "space-between", padding: 10, alignItems: "center" }}>
                    {selectAddress ?
                      <Text style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 13, width: 190 }}>
                        {selectAddress?.houseNo}, {selectAddress?.areaColony}, {selectAddress?.pincode}
                      </Text> :
                      <Text style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 14 }}>
                        please add address
                      </Text>}
                    <TouchableOpacity onPress={toggleModal}>
                      <Text
                        style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 14,textDecorationLine:"underline" }}>
                        Click here
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View> :
                <TouchableOpacity onPress={() => navigation.navigate("AddAddressScreenLABL")} style={{ width: "100%", height: 60, backgroundColor: "#000", alignSelf: "center", justifyContent: "center", alignItems: "center" }}>
                  <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#fff" }}>+ Add New Address</Text>
                </TouchableOpacity>}
              <View>
                <Modal
                  isVisible={modalVisible}
                  onBackdropPress={() => setModalVisible(false)}
                  onSwipeComplete={() => setModalVisible(false)}
                  swipeDirection={['down']}
                  style={styles.bottomModal}>
                  <View style={styles.contain}>
                    {/* <Image source={image} style={{ height: 150, width: 120, borderRadius: 10 }} /> */}
                    <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, alignSelf: "center", marginVertical: 10 }}>Choose your Delivery Address</Text>
                    <AddressComponent address={UserAddress} onChange={setSelectedAddress} modal={setModalVisible} />

                  </View>
                </Modal>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20 }}>
                <Icon1 name="shopping-bag" size={17} color="#000" style={{ marginHorizontal: 10 }} />
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 18 }}>Expected Delivery by 12 Jun</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20 }}>
                <Icon2 name="cash" size={20} color="#000" style={{ marginHorizontal: 10 }} />
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 18 }}>Cash on Delivery Available</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20 }}>
                <Icon3 name="swap-horizontal" size={20} color="#000" style={{ marginHorizontal: 10 }} />
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 18 }}>Easy 30 days return & exchange available</Text>
              </View>
            </View>}
            <View>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, marginVertical: 10 }}>Product Details :-</Text>
              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 15, marginVertical: 10, marginHorizontal: 10 }}>{object?.productDescription}</Text>
            </View>
            <View style={{ marginVertical: 10 }}>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, marginVertical: 10 }}>Easy 30 days returns and exchanges</Text>
              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 15, marginVertical: 10 }}>Choose to return or exchange for a different size (If available) within 30 days.</Text>
            </View>
            <View>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, marginVertical: 10 }}>
                You may also like
              </Text>
              <FlatList
                horizontal
                data={otherProducts}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => (
                  <TouchableOpacity onPress={() => scrollToTop(item)} style={{ margin: 10 }}>
                    <Image source={{ uri: item?.productImages?.length > 0 ? item?.productImages[0]?.url : "" }} style={{ height: 105, width: 105, borderRadius: 0 }} />
                    <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18 }}>{item?.productBrandName}</Text>
                    <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 15 }}>₹ {item?.productPrice}</Text>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }

    </View>
  );
}

function Profile(props) {
  const { image, name } = props;
  return (
    <View style={{ flexDirection: "row", justifyContent: "center", alignSelf: "flex-start", margin: 20 }}>
      <Image source={{ uri: image ? image : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png' }} style={{ height: 45, width: 45, borderRadius: 30 }} />
      <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, alignSelf: "center", color: "#000", marginHorizontal: 20 }}>{name}</Text>
    </View>
  )
}

// function Component(props) {
//   const { unique, object, productColor, onChange } = props;
//   const dispatch = useDispatch();
//   const [loading1, setLoading1] = useState(false);
//   const [checked, setChecked] = useState(null);
//   const onSelectSizes = select => {
//     setLoading1(true);
//     const obj = {};
//     obj.product = object?.id;
//     obj.size = select?.id;
//     dispatch(productsGetRequestUsingCollectionId(obj))
//     setChecked(select?.id);
//     setLoading1(false);
//   };
//   return (
//     <View>
//       <FlatList
//         horizontal
//         data={unique}
//         keyExtractor={(item, index) => item.id}
//         renderItem={({ item, index }) => (
//           (checked === item?.id ?
//             <TouchableOpacity style={{ margin: 10, backgroundColor: "#000", borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
//               <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff" }}>{item?.type}</Text>
//             </TouchableOpacity> :
//             <TouchableOpacity onPress={() => onSelectSizes(item)} style={{ margin: 10, borderWidth: 1, borderColor: "#000", borderRadius: 20, width: 35, height: 35, justifyContent: "center", alignItems: "center" }}>
//               <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>{item?.type}</Text>
//             </TouchableOpacity>)
//         )}
//       />
//       {unique != "" ?
//         <View style={{ flexDirection: "column", marginVertical: 15 }}>
//           <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>Available Colors : </Text>
//           <ColorComponent color={productColor} onChange={onChange} />
//           {/* <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 20 }}>{object?.productAvailableSizes}</Text> */}
//         </View> : null}
//     </View>
//   )
// }

// function ColorComponent(props) {
//   const { color, onChange } = props;
//   const [checked, setChecked] = useState(null);
//   console.log("checked", checked);
//   const handleChange = (item) => {
//     setChecked(item?.id);
//     onChange(item)
//   }
//   return (
//     <View>
//       <FlatList
//         horizontal
//         data={color}
//         keyExtractor={(item, index) => item.id}
//         renderItem={({ item, index }) => (
//           (checked === item?.id ?
//             <TouchableOpacity style={{ margin: 10, backgroundColor: "#000", borderRadius: 20, width: 75, height: 35, justifyContent: "center", alignItems: "center" }}>
//               <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#fff" }}>{item?.color?.color}</Text>
//             </TouchableOpacity> :
//             <TouchableOpacity onPress={() => handleChange(item)} style={{ margin: 10, borderWidth: 1, borderColor: "#000", borderRadius: 20, width: 75, height: 35, justifyContent: "center", alignItems: "center" }}>
//               <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18 }}>{item?.color?.color}</Text>
//             </TouchableOpacity>)
//         )}

//       />
//     </View>
//   )
// }

function AddressComponent(props) {
  const { address, onChange, modal } = props;
  const [checked, setChecked] = React.useState(null);
  const handleClick = (item) => {
    setChecked(item?.id);
    onChange(item);
    modal(false);
  }
  return (
    <ScrollView horizontal>
      <View style={{ flexDirection: "row", marginVertical: 30 }}>
        {address.map((item) => (
          <TouchableOpacity onPress={() => handleClick(item)} style={{ height: 150, borderWidth: 1, borderColor: "#000", width: 200, marginHorizontal: 10, marginBottom: 20, padding: 10 }}>
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, marginVertical: 10, }}>{item?.fullName}</Text>
              {checked === item?.id ?
                <View style={{ alignSelf: "center" }}>
                  <Icon2
                    name="check-circle"
                    size={20}
                  />
                </View> : null}
            </View>
            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 15 }}>{item?.houseNo},{'\n'}{item?.areaColony},{'\n'}{item?.city},{'\n'}{item?.state},{item?.pincode}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </ScrollView>
  )
}