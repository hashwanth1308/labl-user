import{ 
    
  subCategoriesPostCheckSuccess,
  subCategoriesSetCount,
  subCategoriesCountCheckSuccess,
  subCategoriesDeleteCheckSuccess,
  subCategoriesFindOneCheckSuccess,
  subCategoriesSetOneData,
  subCategoriesGetCheckSuccess,
  subCategoriesSetData,
  subCategoriesUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function subCategoriesPostRequest(element) {
      return dispatch => {
        dispatch(subCategoriesPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `sub-categories`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(subCategoriesSetData(a));
            dispatch(subCategoriesPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(subCategoriesPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function subCategoriesCountRequest(element) {
      return dispatch => {
          dispatch(subCategoriesCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `sub-categories/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(subCategoriesSetCount(data));
              dispatch(subCategoriesCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(subCategoriesCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function subCategoriesDeleteRequest(id) {
      
      return dispatch => {
          dispatch(subCategoriesDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `sub-categories/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(subCategoriesDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(subCategoriesDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function subCategoriesFindOneRequest(id) {
      return dispatch => {
        dispatch(subCategoriesFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `sub-categories/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(subCategoriesSetOneData(data));
            dispatch(subCategoriesFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(subCategoriesFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function subCategoriesGetRequest(element) {
    const obj={};
    obj.category=element;
    console.log("category id",obj);
  return dispatch => {
    dispatch(subCategoriesGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `sub-categories`,
        // params:obj,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
         
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(subCategoriesSetData(data));
        dispatch(subCategoriesGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(subCategoriesGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function subCategoriesUpdateRequest(id) {
     
      return dispatch => {
          dispatch(subCategoriesUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `sub-categories/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(subCategoriesSetData(data));
              dispatch(subCategoriesUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(subCategoriesUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 