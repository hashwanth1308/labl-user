import {
  CARTS_POST_CHECK_SUCCESS,
  CARTS_SET_COUNT,
  CARTS_COUNT_CHECK_SUCCESS,
  CARTS_DELETE_CHECK_SUCCESS,
  CARTS_FINDONE_CHECK_SUCCESS,
  CARTS_SET_ONE_DATA,
  CARTS_GET_CHECK_SUCCESS,
  CARTS_SET_DATA,
  CARTS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  cartsPostCheckSuccess: false,
  cartsCount: 0,
  cartsCountCheckSuccess : false,
  cartsDeleteCheckSuccess: false,
  cartsFindOneCheckSuccess: false,
  cartsOneData: [],
  cartsGetCheckSuccess: false,
  cartsData: [],
  cartsUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CARTS_POST_CHECK_SUCCESS:
      return {
        ...state,
        cartsPostCheckSuccess: action.payload,
      };
      case CARTS_SET_COUNT:
      return {
        ...state,
        cartsCount: action.payload,
      };
      case CARTS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        cartsCountCheckSuccess: action.payload,
      };
      case CARTS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        cartsDeleteCheckSuccess: action.payload,
      };
      case CARTS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        cartsFindOneCheckSuccess: action.payload,
          };
      case CARTS_SET_ONE_DATA:
      return {
        ...state,
        cartsOneData: action.payload,
          };
      case CARTS_GET_CHECK_SUCCESS:
      return {
        ...state,
        cartsGetCheckSuccess: action.payload,
          };
      case CARTS_SET_DATA:
      return {
        ...state,
        cartsData: action.payload,
          };
      case CARTS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        cartsUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
