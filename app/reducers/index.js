import { combineReducers } from 'redux';
import AuthReducer from './auth';
import ApplicationReducer from './application';
import sampleReducer from './sample';
import addressReducer from './address';
import cartsReducer from './carts';
import categoriesReducer from './categories';
import subCategoriesReducer from './subCategories';
import collectionsReducer from './collections';
import combinationsReducer from './combinations';
import earningsReducer from './earnings';
import invoicesReducer from './invoices';
import labelsReducer from './labels';
import ordersReducer from './orders';
import paymentsReducer from './payments';
import productsReducer from './products';
import returnOrdersReducer from './returnOrders';
import favDesignsReducer from './favDesigns';
import vendorsReducer from './vendors';
import UsersettingsReducer from "./usersettings";
import withdrawalAccountDetailsReducer from './withdrawalAccountDetails';
import usersReducer from './users';
import basicSettingsReducer from './basicSettings';
import orderMessagesReducer from './orderMessages';
import salesOrdersReducer from './salesOrders';
import shippingsReducer from './shippings';
import productRatingReducer from './productRating';
import modelEarningsReducer from './modelEarnings';
import modelOrdersReducer from './modelOrders';
import modelPayoutsReducer from './modelPayouts';
import modelProductsReducer from './modelProducts';
import modelsReducer from './models';
import pricingsReducer from './pricings';
import vendorPayoutsReducer from './vendorPayouts';
import LoginReducer from "./login";
import signupReducer from "./signup";
import accessTokenReducer from "./accessToken"
import sizeReducer from './size';
import userCartsReducer from './userCarts';
import logoutReducer from './logout';
import bookMarksReducer from './bookmarks';
import followingReducer from './following';
import statusReducer from './status';
import commentReducer from './comment';
import likeReducer from './like';

export default combineReducers({
  accessTokenReducer: accessTokenReducer,
  auth: AuthReducer,
  application: ApplicationReducer,
  sizeReducer: sizeReducer,
  sampleReducer: sampleReducer,
  signupReducer: signupReducer,
  addressReducer: addressReducer,
  cartsReducer: cartsReducer,
  categoriesReducer: categoriesReducer,
  subCategoriesReducer: subCategoriesReducer,
  collectionsReducer: collectionsReducer,
  combinationsReducer: combinationsReducer,
  earningsReducer: earningsReducer,
  invoicesReducer: invoicesReducer,
  labelsReducer: labelsReducer,
  loginReducer: LoginReducer,
  ordersReducer: ordersReducer,
  paymentsReducer: paymentsReducer,
  productsReducer: productsReducer,
  returnOrdersReducer: returnOrdersReducer,
  favDesignsReducer: favDesignsReducer,
  vendorsReducer: vendorsReducer,
  withdrawalAccountDetailsReducer: withdrawalAccountDetailsReducer,
  usersReducer: usersReducer,
  basicSettingsReducer: basicSettingsReducer,
  orderMessagesReducer: orderMessagesReducer,
  salesOrdersReducer: salesOrdersReducer,
  shippingsReducer: shippingsReducer,
  productRatingReducer: productRatingReducer,
  modelEarningsCartsReducer: modelEarningsReducer,
  modelOrdersReducer: modelOrdersReducer,
  modelPayoutsReducer: modelPayoutsReducer,
  modelProductsReducer: modelProductsReducer,
  modelsReducer: modelsReducer,
  pricingsReducer: pricingsReducer,
  UsersettingsReducer: UsersettingsReducer,
  vendorPayoutsReducer: vendorPayoutsReducer,
  userCartsReducer: userCartsReducer,
  logoutReducer: logoutReducer,
  bookMarksReducer: bookMarksReducer,
  followingReducer: followingReducer,
  statusReducer: statusReducer,
  commentReducer: commentReducer,
  likeReducer: likeReducer,
});

