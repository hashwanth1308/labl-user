import { StyleSheet } from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
    imageBackground: {
        height: 140,
        width: '100%',
        position: 'absolute',
    },
    searchForm: {
        padding: 10,
        // borderRadius: 10,
        // borderWidth: 0.5,
        width: '100%',
        shadowColor: 'black',
        shadowOffset: { width: 1.5, height: 1.5 },
        shadowOpacity: 0.3,
        shadowRadius: 1,
        // elevation: 1,
    },
    contentServiceIcon: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    contentCartPromotion: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    btnPromotion: {
        // paddingVertical: 5,
        paddingHorizontal: 15,
        // flexDirection: 'row',
        justifyContent: 'space-between',
        // left:10

    },
    contentHiking: {
        marginTop: 20,
        marginLeft: 20,
        marginBottom: 10,
    },
    promotionBanner: {
        height: Utils.scaleWithPixel(150),
        width: '100%',
        marginTop: 10,
    },
    line: {
        height: 1,
        marginTop: 10,
        marginBottom: 15,
    },
    iconContent: {
        marginLeft: 20,
        // alignItems: 'center',
        width: 100,
        height: 120,
    },

    content: {
        right: 20
    },
    lineRow: {
        justifyContent: 'space-between',
    },
    iconRight: {
        width: 100,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        left: 145,
        bottom: 85
    },
    couponSection: {
        height: 52,
        borderRadius: 50,
        borderColor: 'rgba(0,0,0,0.15)',
        borderStyle: 'solid',
        borderWidth: 1,
        paddingHorizontal: 29,
        marginTop: 32,
    },
    placeholder: {
        opacity: 0.5,
        color: '#707070',
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 32,
        flex: 1,
    },
    titleView: {
        paddingHorizontal: 25,
        // paddingTop: 5,
        // paddingBottom: 5,
        fontSize: 20,
    },
    bottomModal: {
        justifyContent: 'flex-end',
        // height:300,
        // backgroundColor:"#000",
        margin: 0,
    },
    contain: {
        // alignItems: 'center',
        // height: 300,
        backgroundColor: "#fff",
        padding: 0,
        // flex: 1,
    },
    deleteBox: {
        // backgroundColor: '#000',
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
        height: 60,
        // borderRadius: 10
    },
});

