import React, {useState,useEffect} from 'react';
import {View, TouchableOpacity,ActivityIndicator,Dimensions} from 'react-native';
import {BaseStyle, BaseColor, useTheme} from '@config';
import {Header, SafeAreaView, Icon, Text, Button} from '@components';
import styles from './styles';
import {useTranslation} from 'react-i18next';

export default function MyPaymentMethod({navigation}) {
  const {colors} = useTheme();
  const {t} = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;

  const [loading] = useState(false);
  const [card] = useState([
    {
      id: 1,
      icon: 'cc-visa',
      title: '**** **** **** 1989',
      subtitle: 'Expiries 02/2020',
      primary: true,
    },
    {
      id: 2,
      icon: 'paypal',
      title: 'steve.garrett@passionui.com',
      subtitle: 'Added 01/2019',
    },
    {
      id: 3,
      icon: 'cc-mastercard',
      title: '**** **** ****2091',
      subtitle: 'Expiries 10/2021',
    },
    {
      id: 4,
      icon: 'apple-pay',
      title: 'steve.garrett@icloud.com',
      subtitle: 'Added 01/2019',
    },
  ]);

  /**
   * call when select card
   * @param {*} item
   */
  const onSelectMethod = item => {
    navigation.navigate('PaymentMethodDetail');
  };
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev)=>(!prev))
    ), 1000)
  }, []);




  return (
    <View style={{flex: 1}}>
      <Header
        title={t('my_cards')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <View style={styles.contain}>
          {card.map((item, index) => {
            return (
              <TouchableOpacity
                key={item.id}
                style={[styles.paymentItem, {borderBottomColor: colors.border}]}
                onPress={() => onSelectMethod(item)}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <View style={styles.iconContent}>
                    <Icon name={item.icon} size={48} color={colors.text} />
                  </View>
                  <View>
                    <Text body1>{item.title}</Text>
                    <Text footnote grayColor style={{marginTop: 5}}>
                      {item.subtitle}
                    </Text>
                  </View>
                </View>
                {item.primary ? (
                  <Text footnote primaryColor>
                    {t('primary')}
                  </Text>
                ) : null}
              </TouchableOpacity>
            );
          })}
        </View>
        <View style={{padding: 20}}>
          <Button
            loading={loading}
            full
            onPress={() => navigation.navigate('AddPayment')}>
            {t('add_payment_method')}
          </Button>
        </View>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height:height,backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor:"#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}
