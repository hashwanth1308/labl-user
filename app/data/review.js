import {Images} from '@config';

const ReviewData = [
  {
    id: '1',
    source: Images.profile3,
    name: 'Grigoriy Kozhukhov',
    rate: 4,
    date: 'Jun 2018',
    title: 'Better than what I have expected',
    comment:
      'The material is good but the print does not look that great. To be precise after few wash the print will get washed. That is when you will not able to wear it often. That is the only drawback. Buy a size up from your regular size. It will be a perfect fit.',
  },
  {
    id: '2',
    source: Images.profile4,
    name: 'Ea Tipene',
    rate: 4,
    date: 'Jun 2018',
    title: 'One more in winter collection! Worth it',
    comment:
      'Tooo good',
  },
];

export {ReviewData};
