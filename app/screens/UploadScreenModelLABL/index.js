import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  FlatList,
  PermissionsAndroid,
  ActivityIndicator,
  Dimensions,
  TextInput
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { useSelector, useDispatch } from 'react-redux';
import { BaseStyle, useTheme } from '@config';
import { Header, SafeAreaView, Icon, Text, Button, Image } from '@components';
import { useTranslation } from 'react-i18next';
import styles from './styles';
import {
  productsCountRequest, productsGetRequest, productsFindOneRequest,
  productsPostRequest, productsDeleteRequest,
  productsUpdateRequest
} from '../../api/products';
import { Picker } from '@react-native-picker/picker';
import { collectionsGetRequest } from '../../api/collections';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons'
import { TouchableOpacity } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-crop-picker';
import { uploadFilesPostRequest, uploadFilesDeleteRequest } from '../../api/uploadApi';
import { sizeGetRequest } from '../../api/size';
import {
  RoundedCheckbox,
  PureRoundedCheckbox,
} from "react-native-rounded-checkbox";
import _ from 'lodash';
import { modelProductsPostRequest } from '../../api/modelProducts';
import { modelOrdersGetRequest, modelOrdersPostRequest, modelOrderItemsGetRequest } from '../../api/modelOrders';

export default function ProductsCreateScreenVendorLABL({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log(UserData);

  const dispatch = useDispatch();
  const [selectedValue, setSelectedValue] = useState('');
  console.log("selected", selectedValue);
  const id = selectedValue?.id;
  const [selectedValue1, setSelectedValue1] = useState('');
  console.log("selected1", selectedValue1);

  // const handleChange = () => {
  //   setSelectedValue(se)
  // }
  useEffect(() => {
    dispatch(sizeGetRequest());

  }, [dispatch])
  useEffect(() => {
    const obj = {};
    obj.model = UserData?.user?.model?.id;
    dispatch(modelOrdersGetRequest(obj));
  }, [dispatch])
  useEffect(() => {
    dispatch(modelOrderItemsGetRequest(selectedValue?.id));
  }, [selectedValue])
  const sizes = useSelector(state => state.sizeReducer.sizeData);
  console.log("size", sizes);

  const modelOrders = useSelector(state => state.modelOrdersReducer.modelOrdersData);
  console.log("model", modelOrders);
  const orders = modelOrders?.filter((item) => item?.status === "Delivered")
  console.log("orders", orders);

  const productFromModelOrder = useSelector(state => state.modelOrdersReducer.modelOrderItemsData);
  console.log("productFromModelOrder", productFromModelOrder);

  const productCreation = async () => {
    if (!review) {
      alert('please fill the review')
    }

    else {
      setLoading(true);
      const obj = {};
      obj.product = selectedValue1?.id;
      obj.imagesOfProductByModel = selectedImage;
      obj.priceOfProductWithModelCommission = Math.round(Number(selectedValue1?.product?.productPrice) + ((0.17) * Number(selectedValue1?.product?.productPrice)));
      obj.ReviewByModelAboutProduct = review;
      obj.model = UserData?.user?.model?.id;
      obj.modelorderitem = selectedValue1?.id;
      obj.category = selectedValue1?.subCategory?.parentCategory?.category;
      obj.parentCategory = selectedValue1?.subCategory?.parentCategory;
      obj.subCategory = selectedValue1?.subCategory;
      await dispatch(modelProductsPostRequest(obj));
      navigation.navigate('ThankyouScreen');
      setLoading(false);
      setSelectedImage([]);
      setSelectedValue('');
      setSelectedValue1('');
      setReview('')
    }

  };

  const renderOrderList = () => {
    return orders?.map(collections => {
      // console.log("collections", collections)
      return <Picker.Item label={collections?.product?.productName} value={collections} />;

    });
  };
  const renderProductList = () => {
    return <Picker.Item label={selectedValue?.product?.productName} value={selectedValue?.product} />;
  };

  const [review, setReview] = useState('');
  const [loading, setLoading] = useState(false);
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });

  useEffect(() => {
    dispatch(collectionsGetRequest(id));

  }, [dispatch])
  // useEffect(() => {
  //   dispatch(productsCountRequest());

  // }, [dispatch])

  useEffect(() => {
    dispatch(productsGetRequest());

  }, [dispatch])



  // const onSelectionsChange = (selectedCat) => {
  //   // selectedFruits is array of { label, value }
  //   setSelectedCat(selectedCat)
  // }


  // const [profileImage, setProfileImage] = useState();
  const selectImage = () => {
    const options = {
      noData: true
    }
    ImagePicker.launchImageLibrary(options, response => {
      console.log(response.assets, "all response", response.assets.uri)
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        const source = { uri: response.uri }
        console.log(source)
        setImage(source)
      }
    })
  }
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write camera err', err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };


  const [selectedImage, setSelectedImage] = useState([]);
  console.log("selectedImage", selectedImage);

  const profileImages = [];
  console.log(profileImages);
  selectedImage?.forEach((item) => (
    profileImages.push({ url: item?.url })
  ))
  const captureImage = async (type) => {
    // let options = {
    //   mediaType: type,
    //   maxWidth: 300,
    //   maxHeight: 550,
    //   quality: 1,
    //   videoQuality: 'low',
    //   durationLimit: 30, //Video max duration in seconds
    //   saveToPhotos: true,
    // };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {

      ImagePicker.openPicker({
        multiple: true,
        width: 500,
        height: 500,
        cropping: true,
      }).then(image => {
        console.log("images", image);
        // let imgd = image.concat(images)
        // setImage(imgd);
        toNextPage(image);
        // setSelectedImage(images?.path)
      });
    }
  };

  // const cropper = (image) => {
  //   ImagePicker.openCropper({
  //     height: 500, width: 500, cropping: true, path: image[0]?.path,
  //   }).then(images => {
  //     toNextPage(images);
  //   })
  // }
  const toNextPage = async (images) => {
    console.log('imgApi2', images);
    let formData = new FormData();
    setLoading1(true);

    // console.log('lopala', images);
    images.map(async (item) => {
      formData.append('files', {
        name: item?.modificationDate,
        type: item?.mime,
        uri: item?.path,
      });
      console.log(formData);

      let imgApi = await uploadFilesPostRequest(formData);
      console.log('imgApi', imgApi);
      setSelectedImage([...selectedImage, ...imgApi])
      // var newArr = imgApi.concat(Arr);
      // console.log("newArr", newArr);
      // setProfileImage(imgApi[0]?.id)
      setLoading1(false);
    })
  };
  const [checked, setChecked] = React.useState(false);
  const [checked2, setChecked2] = React.useState(true);
  const [checked3, setChecked3] = React.useState(true);
  const [checked4, setChecked4] = React.useState(true);
  const [checked5, setChecked5] = React.useState(false);
  const [checked6, setChecked6] = React.useState(false);

  function deleteFile(urlitem) {
    // const s = img.filter((item, index) => 
    // index !== e);
    uploadFilesDeleteRequest(urlitem?.id)
    const s = _.filter(selectedImage, function (o) { return o.id != urlitem.id });
    setSelectedImage(s);
    console.log(s);
  }

  const renderIconCheckbox = (checkedColor, uncheckedColor, isChecked, checkedValue, onPress) => (
    <View style={{ marginLeft: 10 }}>
      <PureRoundedCheckbox
        text="L"
        isChecked={isChecked}
        checkedColor={checkedColor}
        uncheckedColor={uncheckedColor}
        onPress={onPress}
      >
        <Icon
          size={16}
          name="check"
          type="Entypo"
          color={checkedValue ? "#fdfdfd" : "transparent"}
        />
      </PureRoundedCheckbox>
    </View>
  )

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);
  return (
    <View style={{ flex: 1 }}>
      <Header
        title={t('Upload Products')}
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flexDirection: "row", width: 200, justifyContent: "space-between", alignSelf: "flex-start" }}>
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={"#000"}
                  enableRTL={true}
                />
              </View>
              <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 18, color: "#fff", right: 80 }}>Upload</Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ padding: 20 }}>
            <View style={{ flex: 2, flexDirection: 'row', alignItems: 'center', marginBottom: 20 }}>
              <Text bold style={{ fontFamily: 'Roboto', fontSize: 16 }}>
                Select from recent orders  :
              </Text>
              <View style={{ justifyContent: "center", height: 40, width: 170, borderColor: "#000", borderWidth: 1, marginLeft: 10 }}>
                <Picker
                  mode="dialog"
                  dropdownIconColor="#0000"
                  prompt='Collections'
                  selectedValue={selectedValue}
                  style={{ height: 40, width: 160, margin: 10, color: '#000' }}
                  onValueChange={setSelectedValue}
                  Icon={
                    <Icon
                      name="arrow-dropdown-circle"
                      style={{ color: 'white', fontSize: 20 }}
                    />
                  }>
                  <Picker.Item key={'unselectable'} label='--Please Select--' />
                  {renderOrderList()}
                </Picker>
              </View>
            </View>
            {selectedValue ?
              <View style={{ flex: 2, flexDirection: 'row', alignItems: 'center', marginBottom: 20 }}>
                <Text bold style={{ fontFamily: 'Roboto', fontSize: 16 }}>
                  Select the product :
                </Text>
                <View style={{ justifyContent: "center", height: 40, width: 170, borderColor: "#000", borderWidth: 1, marginLeft: 10 }}>
                  <Picker
                    mode="dialog"
                    dropdownIconColor="#0000"
                    prompt='Collections'
                    selectedValue={selectedValue1}
                    style={{ height: 40, width: 160, margin: 10, color: '#000' }}
                    onValueChange={setSelectedValue1}
                    Icon={
                      <Icon
                        name="arrow-dropdown-circle"
                        style={{ color: 'white', fontSize: 20 }}
                      />
                    }>
                    <Picker.Item key={'unselectable'} label='--Please Select--' />
                    {renderProductList()}
                  </Picker>
                </View>
              </View> : null}
            <View>
              <View style={{ flexDirection: "row", marginVertical: 10 }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#000" }}>Product name : </Text>
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 16, color: "#000", alignSelf: "center" }}>{selectedValue1?.productName}</Text>
              </View>
              <View style={{ flexDirection: "row", marginVertical: 10 }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#000" }}>Product description : </Text>
                <Text numberOfLines={1} style={{ fontFamily: "Montserrat-Medium", width: 200, fontSize: 16, color: "#000", alignSelf: "center" }}>{selectedValue1?.productDescription}</Text>
              </View>
              <View style={{ flexDirection: "row", marginVertical: 10 }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#000" }}>Product price : </Text>
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 16, color: "#000", alignSelf: "center" }}>{Math.round(Number(selectedValue1?.productPrice) + ((0.17) * Number(selectedValue1?.productPrice))) ? Math.round(Number(selectedValue1?.productPrice) + ((0.17) * Number(selectedValue1?.productPrice))) : ""}</Text>
              </View>
            </View>
            <View>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 18, color: "#000", marginVertical: 10 }}>Review for this product</Text>
              <TextInput
                onChangeText={text => setReview(text)}
                placeholder={null}
                multiline={true}
                success={success.review}
                value={review}
                // numberOfLines={10}
                // textAlignVertical='top'
                style={{ flex: 1, width: "80%", borderRadius: 10, borderWidth: 1, alignSelf: "flex-start" }}
              />
            </View>
            <View style={{ flexDirection: "column", alignItems: "center", marginTop: 20 }}>
              {selectedImage?.map((item) => (
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <TouchableOpacity onPress={() => deleteFile(item)}>
                    <Icon1 name="close" color="black" size={15} style={{ marginLeft: 5 }} />
                  </TouchableOpacity>
                  <Image source={{ uri: item?.formats?.small?.url ? item?.formats?.small?.url : "" }} style={styles.thumb} />
                </View>
              ))
              }
              <TouchableOpacity onPress={() => captureImage()} style={{ flexDirection: "row" }}>
                <Icon name="plus" color="black" size={13} style={{ marginLeft: 0, alignSelf: "center" }} />
                <Text style={{ fontSize: 16, color: "black", fontFamily: "Montserrat-Bold", textDecorationLine: "underline", marginHorizontal: 10, }}>Upload Images</Text>
              </TouchableOpacity>
            </View>
            <View style={{ paddingVertical: 15, paddingHorizontal: 20 }}>
              <Button
                // loading={loading}

                onPress={() => {
                  productCreation()
                }}>
                {t('Add Product')}
              </Button>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }

    </View>
  )
}