import React, {useEffect, useState} from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import _ from 'lodash';
import {useSelector, useDispatch} from 'react-redux';
import {BaseStyle, useTheme, Images} from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  TextInput,
  Button,
  Image,
} from '@components';
import {useTranslation} from 'react-i18next';
import styles from './styles';
import {
  vendorsCountRequest,
  vendorsGetRequest,
  vendorsFindOneRequest,
  vendorsPostRequest,
  vendorsDeleteRequest,
  vendorsUpdateRequest,
} from '../../api/vendors';
import axios from 'axios';
import {
  modelFollowingPostRequest,
  modelFollowingGetRequest,
  modelFollowingDeleteRequest,
} from '../../api/following';

export default function VendorCommonProfile({navigation, route}) {
  const {colors} = useTheme();
  const {t} = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const themeStorage = useSelector(state => state.application.theme);

  const addressData = useSelector(state => state.vendorsReducer.vendorsData);

  const dispatch = useDispatch();
  const [card, setCard] = useState('');
  const [valid, setValid] = useState('');
  const [digit, setDigit] = useState('');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });

  const {profileData} = route.params;
  console.log('profile', profileData);
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log(UserData);
  // useEffect(() => {
  //     dispatch(vendorsCountRequest());

  // }, [dispatch])

  useEffect(() => {
    dispatch(vendorsGetRequest());
  }, [dispatch]);
  useEffect(() => {
    dispatch(modelFollowingGetRequest(profileData?.model?.id));
  }, [dispatch]);

  // const onFindOneVendors = () => {
  //   setLoading(true);
  //   dispatch(vendorsFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onAddVendors = () => {
  //   setLoading(true);
  //   dispatch(vendorsPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteVendors = () => {
  //   setLoading(true);
  //   dispatch(vendorsDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateVendors = () => {
  //   setLoading(true);
  //   dispatch( vendorsUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const [vendorId, setVendorId] = useState('');
  // console.log(vendorId);
  const vendors = useSelector(state => state.vendorsReducer.vendorsOneData);
  console.log(vendors);

  const modelfollowings = useSelector(
    state => state.followingReducer.modelFollowingData,
  );
  console.log('model', modelfollowings);

  const data = [
    {
      id: 1,
      name: 'ABCDEF',
      image: Images.profile1,
    },
    {
      id: 1,
      name: 'ABCDEF',
      image: Images.profile1,
    },
    {
      id: 1,
      name: 'ABCDEF',
      image: Images.profile1,
    },
    {
      id: 1,
      name: 'ABCDEF',
      image: Images.profile1,
    },
    {
      id: 1,
      name: 'ABCDEF',
      image: Images.profile1,
    },
    {
      id: 1,
      name: 'ABCDEF',
      image: Images.profile1,
    },
  ];

  return (
    <View style={{flex: 1}}>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{flex: 1}}>
          <ScrollView contentContainerStyle={{padding: 20}}>
            {/* <View style={{alignItems: 'center'}}>
              <Text
                style={{
                  fontFamily: 'Montserrat-Bold',
                  fontSize: 20,
                  marginVertical: 10,
                }}>
                {vendors.length} Vendors
              </Text>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: '#000',
                  width: 180,
                }}></View>
            </View> */}
            <FlatList
              vertical
              data={modelfollowings}
              // style={{ margin: 10 }}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item}) => {
                return (
                  <Component
                    name={item?.vendor?.user?.username}
                    object={item}
                  />
                );
              }}
            />
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
}

function Component(props) {
  const {name, object} = props;
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log(UserData);
  const dispatch = useDispatch();
  const [follow, setFollow] = useState(false);
  const modelfollowings = useSelector(
    state => state.followingReducer.modelFollowingData,
  );
  console.log('model', modelfollowings);
  const [followId, setFollowId] = useState('');
  console.log('followid', followId);
  useEffect(() => {
    const followed = _.find(modelfollowings, function (o) {
      return o.vendor?.id === object.id;
    });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true);
    }
  }, []);

  useEffect(() => {
    const followed = _.find(modelfollowings, function (o) {
      return o.vendor?.id === object.id;
    });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true);
    }
  }, [modelfollowings]);
  const handleClick = object => {
    if (follow) {
      setFollow(false);
      dispatch(modelFollowingDeleteRequest(followId));
    } else {
      setFollow(true);
      let objectValue = {};
      (objectValue.model = UserData?.user?.model?.id),
        (objectValue.vendor = object?.id),
        (objectValue.isFollowing = true);
      dispatch(modelFollowingPostRequest(objectValue));
    }
  };
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        // justifyContent: 'space-around',
        marginVertical: 15,
      }}>
      {/* <View style={{ flex: 0.8, flexDirection: "row",justifyContent:"space-around" }}> */}
      <Image
        source={Images.profile1}
        style={{
          height: 50,
          width: 50,
          borderRadius: 30,
          alignSelf: 'center',
        }}
      />
      <View
        style={{
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          marginHorizontal: 20,
          // marginLeft: -50,
        }}>
        <Text
          style={{
            fontFamily: 'Montserrat-Bold',
            fontSize: 20,
            textAlign: 'left',
            width: 100,
          }}>
          {name}
        </Text>
        <Text
          style={{
            fontFamily: 'Montserrat-Medium',
            fontSize: 14,
            textAlign: 'left',
            width: 100,
          }}>
          Vendor
        </Text>
      </View>
      {/* </View> */}
      {/* {follow ? (
        <TouchableOpacity
          onPress={() => handleClick(object)}
          style={{
            width: 120,
            height: 30,
            borderRadius: 10,
            borderWidth: 1,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat-Bold',
              fontSize: 20,
              color: '#000',
            }}>
            Following
          </Text>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          onPress={() => handleClick(object)}
          style={{
            width: 120,
            height: 30,
            backgroundColor: '#000',
            borderRadius: 10,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat-Bold',
              fontSize: 20,
              color: '#fff',
            }}>
            Follow
          </Text>
        </TouchableOpacity>
      )} */}
    </View>
  );
}
