import {
  MODELPRODUCTS_POST_CHECK_SUCCESS,
  MODELPRODUCTS_SET_COUNT,
  MODELPRODUCTS_COUNT_CHECK_SUCCESS,
  MODELPRODUCTS_DELETE_CHECK_SUCCESS,
  MODELPRODUCTS_FINDONE_CHECK_SUCCESS,
  MODELPRODUCTS_SET_ONE_DATA,
  MODELPRODUCTS_GET_CHECK_SUCCESS,
  MODELPRODUCTS_SET_DATA,
  MODELPRODUCTS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function modelProductsPostCheckSuccess(bool = false) {
  return {
    type: MODELPRODUCTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelProductsSetCount(object) {
  return {
    type: MODELPRODUCTS_SET_COUNT,
    payload: object,
  };
}
export function modelProductsCountCheckSuccess(bool = false) {
  return {
    type: MODELPRODUCTS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelProductsDeleteCheckSuccess(bool = false) {
  return {
    type: MODELPRODUCTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelProductsFindOneCheckSuccess(bool = false) {
  return {
    type: MODELPRODUCTS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelProductsSetOneData(object) {
  return {
    type: MODELPRODUCTS_SET_ONE_DATA,
    payload: object,
  };
}
export function modelProductsGetCheckSuccess(bool = false) {
  return {
    type: MODELPRODUCTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function modelProductsSetData(object) {
  return {
    type: MODELPRODUCTS_SET_DATA,
    payload: object,
  };
}

export function modelProductsUpdateCheckSuccess(bool = false) {
  return {
    type: MODELPRODUCTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

