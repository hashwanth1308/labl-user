import {
  PRICINGS_POST_CHECK_SUCCESS,
  PRICINGS_SET_COUNT,
  PRICINGS_COUNT_CHECK_SUCCESS,
  PRICINGS_DELETE_CHECK_SUCCESS,
  PRICINGS_FINDONE_CHECK_SUCCESS,
  PRICINGS_SET_ONE_DATA,
  PRICINGS_GET_CHECK_SUCCESS,
  PRICINGS_SET_DATA,
  PRICINGS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function pricingsPostCheckSuccess(bool = false) {
  return {
    type: PRICINGS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function pricingsSetCount(object) {
  return {
    type: PRICINGS_SET_COUNT,
    payload: object,
  };
}
export function pricingsCountCheckSuccess(bool = false) {
  return {
    type: PRICINGS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function pricingsDeleteCheckSuccess(bool = false) {
  return {
    type: PRICINGS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function pricingsFindOneCheckSuccess(bool = false) {
  return {
    type: PRICINGS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function pricingsSetOneData(object) {
  return {
    type: PRICINGS_SET_ONE_DATA,
    payload: object,
  };
}
export function pricingsGetCheckSuccess(bool = false) {
  return {
    type: PRICINGS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function pricingsSetData(object) {
  return {
    type: PRICINGS_SET_DATA,
    payload: object,
  };
}

export function pricingsUpdateCheckSuccess(bool = false) {
  return {
    type: PRICINGS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

