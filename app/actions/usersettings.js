import {
    UPLOADSETTINGS_GET_CHECK_SUCCESS,
    UPLOADSETTINGS_POST_CHECK_SUCCESS,
    UPLOADSETTINGS_UPDATE_CHECK_SUCCESS,
    UPLOADSETTINGS_DELETE_CHECK_SUCCESS,
    UPLOADSETTINGS_SET_DATA,
  } from './actionTypes';

  
  
  export function uploadsettingsGetCheckSuccess(bool = false) {
    return {
      type: UPLOADSETTINGS_GET_CHECK_SUCCESS,
      payload: bool,
    };
  }
  export function uploadsettingsPostCheckSuccess(bool = false) {
    return {
      type: UPLOADSETTINGS_POST_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
  export function uploadsettingsUpdateCheckSuccess(bool = false) {
    return {
      type: UPLOADSETTINGS_UPDATE_CHECK_SUCCESS,
      payload: bool,
    };
  }

  export function uploadsettingsDeleteCheckSuccess(bool = false) {
    return {
      type: UPLOADSETTINGS_DELETE_CHECK_SUCCESS,
      payload: bool,
    };
  }
 
 
  export function uploadsettingsSetData(object) {
    return {
      type: UPLOADSETTINGS_SET_DATA,
      payload: object,
    };
  }
  