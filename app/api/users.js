import {

  usersPostCheckSuccess,
  fpPostCheckSuccess,
  usersSetCount,
  usersCountCheckSuccess,
  usersDeleteCheckSuccess,
  usersFindOneCheckSuccess,
  usersSetOneData,
  usersGetCheckSuccess,
  usersSetData,
  fpSetData,
  usersUpdateCheckSuccess,
} from '../actions';
import axios from 'axios';
import { object } from 'prop-types';
let a = [];

//create
export function usersPostRequest(element) {
  return dispatch => {
    dispatch(usersPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `users`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      }

    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(usersSetData(a));
        dispatch(usersPostCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(usersPostCheckSuccess(false));

      });
  };
}

//count
export function usersCountRequest(element) {
  return dispatch => {
    dispatch(usersCountCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `users/count`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(usersSetCount(data));
        dispatch(usersCountCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(usersCountCheckSuccess(false));

      });
  };
}

//delete
export function usersDeleteRequest(id) {

  return dispatch => {
    dispatch(usersDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `users/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json"
      },

    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(usersDeleteCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(usersDeleteCheckSuccess(false));

      });
  };
}

//findOne
export function usersFindOneRequest(id) {
  return dispatch => {
    dispatch(usersFindOneCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `users/${id}`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        dispatch(usersSetOneData(data));
        dispatch(usersFindOneCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin,)
        dispatch(usersFindOneCheckSuccess(false));
      });
  };
};

//find - get
export function usersGetRequest(element) {
  const Object = {};
  Object.userType = "Model";
  return dispatch => {
    dispatch(usersGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `users`,
      params: Object,
      headers: {
        Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZDViMTIyMWI2ZGJkNDE1NmU2YzRjYyIsImlhdCI6MTY0NTU5MzQ5NywiZXhwIjoxNjQ4MTg1NDk3fQ.UfAF8Almq4lhUqTmRk89D1NvvnrS1lsYFeXrAMwr7ds`,
        "Content-Type": "application/json",
        // data:element
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(usersSetOneData(data));
        dispatch(usersGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(usersGetCheckSuccess(false));
      });
  };
};

//update
export function usersUpdateRequest(id, password) {
  const obj = {};
  obj.password = password;
  return dispatch => {
    dispatch(usersUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `users/${id}`,
      headers: {
        "Content-Type": "application/json"
      },
      data:obj
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(usersSetData(data));
        dispatch(usersUpdateCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error, axiosLogin)
        dispatch(usersUpdateCheckSuccess(false));

      });
  };
}


export function fpPostRequest(element) {
  console.log(element);
  const obj = {};
  obj.mobile = element;
  return dispatch => {
    dispatch(fpPostCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'post',
      url: `users/`,
      data: obj,
      headers: {
        // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        console.log(data);
        dispatch(fpSetData(data));
        dispatch(fpPostCheckSuccess(true));

      })
      .catch(err => {
        dispatch(fpPostCheckSuccess(false));
        console.log("error", err.response);
        if (err.response?.status === 400) {
          alert(err.response.data.message[0].messages[0].message)
        }
      }
      )
  };
}
