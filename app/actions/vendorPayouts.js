import {
  VENDORPAYOUTS_POST_CHECK_SUCCESS,
  VENDORPAYOUTS_SET_COUNT,
  VENDORPAYOUTS_COUNT_CHECK_SUCCESS,
  VENDORPAYOUTS_DELETE_CHECK_SUCCESS,
  VENDORPAYOUTS_FINDONE_CHECK_SUCCESS,
  VENDORPAYOUTS_SET_ONE_DATA,
  VENDORPAYOUTS_GET_CHECK_SUCCESS,
  VENDORPAYOUTS_SET_DATA,
  VENDORPAYOUTS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function vendorPayoutsPostCheckSuccess(bool = false) {
  return {
    type: VENDORPAYOUTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorPayoutsSetCount(object) {
  return {
    type: VENDORPAYOUTS_SET_COUNT,
    payload: object,
  };
}
export function vendorPayoutsCountCheckSuccess(bool = false) {
  return {
    type: VENDORPAYOUTS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorPayoutsDeleteCheckSuccess(bool = false) {
  return {
    type: VENDORPAYOUTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorPayoutsFindOneCheckSuccess(bool = false) {
  return {
    type: VENDORPAYOUTS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorPayoutsSetOneData(object) {
  return {
    type: VENDORPAYOUTS_SET_ONE_DATA,
    payload: object,
  };
}
export function vendorPayoutsGetCheckSuccess(bool = false) {
  return {
    type: VENDORPAYOUTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function vendorPayoutsSetData(object) {
  return {
    type: VENDORPAYOUTS_SET_DATA,
    payload: object,
  };
}

export function vendorPayoutsUpdateCheckSuccess(bool = false) {
  return {
    type: VENDORPAYOUTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

