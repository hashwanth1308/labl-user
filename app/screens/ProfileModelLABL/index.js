import React, { useState,useEffect } from 'react';
import { View, ScrollView, TouchableOpacity, ImageBackground ,ActivityIndicator,Dimensions} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { AuthActions } from '@actions';
import { BaseStyle, useTheme, Images } from '@config';
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  ProfileDetailModel,
  ProfilePerformance,
} from '@components';
import styles from './styles';
import { UserData } from '@data';
import { useTranslation } from 'react-i18next';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import { logout } from '../../actions';

export default function ProfileModelLABL({ navigation, route }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;
  // const { productsObject } = route.params;
  const [loading, setLoading] = useState(false);
  const [userData] = useState(UserData);
  const dispatch = useDispatch();
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  const userToken = useSelector(state => state.accessTokenReducer.accessToken);

  const [performance, setPerformance] = useState([
    { value: UserData?.user?.followingProfiles?.length ? UserData?.user?.followingProfiles?.length : '53', title: 'Orders' },
    { value: UserData?.user?.followersProfiles?.length ? UserData?.user?.followersProfiles?.length : '53', title: 'favourites' },
    { value: UserData?.user?.rewardedPoints ? UserData?.user?.rewardedPoints : '53', title: 'Rewards' },
  ])// const { userObjectUrl } = route.params
  console.log("UserData in profile", UserData);


  /**
   * @description Simple logout with Redux
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   */
  const onLogOut = () => {
    // setLoading(true);
    // dispatch(AuthActions.authentication(false, response => {}));
    // navigation.navigate('SignIn')
    setTimeout(() => {
      dispatch(logout())
      navigation.replace('SignInModelLABL');
    }, 200);
  };

  const onLogOutUser = () => {
    setLoading1(true);
    // dispatch(AuthActions.authentication(false, response => {}));
    // navigation.navigate('SignIn')
    setTimeout(() => {
      dispatch(logout())
      navigation.replace('SignIn');
    }, 1000);
  };

  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev)=>(!prev))
    ), 1000)
  }, []);
 
  return (
    <View style={{ flex: 1 }}>
      <ImageBackground source={require('../../assets/images/Group_1104.jpg')} style={{ height: 200, width: "100%" }}>
        <Header
          title={t('profile')}
          renderRight={() => {
            return <View style={{ height: 35, width: 35, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
              <Icon name="arrow-left" size={20} color={colors.primary} />
            </View>
          }}
          // renderLeft={() => {
          //   return <Icon name="arrow-left" size={24} color={colors.primary} />;
          // }}
          onPressRight={() => {
            navigation.goBack();
          }}
        // onPressLeft={() => {
        //   navigation.goBack();
        // }}

        />
        <ProfileDetailModel
          image={UserData?.user?.image}
          textFirst={UserData?.user?.username}
          textSecond={UserData?.user?.mobile}
        // textThird={UserData.user.email}
        // onPress={() => navigation.navigate('ProfileExample')}
        />
      </ImageBackground>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <ScrollView>
          <View style={styles.contain}>

            {/* <ProfilePerformance
              data={performance}
              style={{ marginTop: 20, marginBottom: 20, }}
              profileDataObject={performance}

            /> */}
            <TouchableOpacity
              style={[
                styles.profileItem,
                { borderBottomColor: colors.border, borderBottomWidth: 1, width: 300 },
              ]}
              onPress={() => {
                // navigation.navigate('OrdersListModelLABL');
                onLogOutUser()
              }}>
              <View style={{ flexDirection: "column" }}>
                <Text body1 style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>{t('User Login')}</Text>
                <Text style={{ fontFamily: "Montserrat-Regular", top: 5 }}>{t('Login to your User account')}</Text>
              </View>
              <Icon1
                name="chevron-right"
                size={28}
                color={colors.primary}
                style={{ marginLeft: 5 }}
                enableRTL={true}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.profileItem,
                { borderBottomColor: colors.border, borderBottomWidth: 1, width: 300 },
              ]}
              onPress={() => {
                navigation.navigate('OrdersListModelLABL');
              }}>
              <View style={{ flexDirection: "column" }}>
                <Text body1 style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>{t('My Orders')}</Text>
                <Text style={{ fontFamily: "Montserrat-Regular", top: 5 }}>{t('Check your order status')}</Text>
              </View>
              <Icon1
                name="chevron-right"
                size={28}
                color={colors.primary}
                style={{ marginLeft: 5 }}
                enableRTL={true}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.profileItem,
                { borderBottomColor: colors.border, borderBottomWidth: 1, width: 300 },
              ]}
              onPress={() => {
                navigation.navigate('CartModelLABL');
              }}>
              <View style={{ flexDirection: "column" }}>
                <Text body1 style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>{t('Cart')}</Text>
                <Text style={{ fontFamily: "Montserrat-Regular", top: 5 }}>{t('Check your Cart items')}</Text>
              </View>
              <Icon1
                name="chevron-right"
                size={28}
                color={colors.primary}
                style={{ marginLeft: 5 }}
                enableRTL={true}
              />
            </TouchableOpacity>
            {/* <TouchableOpacity
              style={[
                styles.profileItem,
                { borderBottomColor: colors.border, borderBottomWidth: 1, width: 300 },
              ]}
              onPress={() => {
                // navigation.navigate('CartModelLABL');
              }}>
              <View style={{ flexDirection: "column" }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>{t('Help Center')}</Text>
                <Text style={{ fontFamily: "Montserrat-Regular", top: 5 }}>{t('Help regarding your recent purchases ')}</Text>
              </View>
              <Icon1
                name="chevron-right"
                size={28}
                color={colors.primary}
                style={{ marginLeft: 5 }}
                enableRTL={true}
              />
            </TouchableOpacity> */}
            <TouchableOpacity
              style={[
                styles.profileItem,
                { borderBottomColor: colors.border, borderBottomWidth: 1, width: 300 },
              ]}
              onPress={() => {
                navigation.navigate('WishlistModelLABL');
              }}>
              <View style={{ flexDirection: "column" }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>{t('Wishlist')}</Text>
                <Text style={{ fontFamily: "Montserrat-Regular", top: 5 }}>{t('Your most loved styles')}</Text>
              </View>
              <Icon1
                name="chevron-right"
                size={28}
                color={colors.primary}
                style={{ marginLeft: 5 }}
                enableRTL={true}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.profileItem, { borderBottomColor: colors.border, borderBottomWidth: 1, width: 300 }]}
              onPress={() => {
                navigation.navigate('ProfileEdit');
              }}>
              <View style={{ flexDirection: "column" }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>{t('Profile Update')}</Text>
                <Text style={{ fontFamily: "Montserrat-Regular", top: 5 }}>{t('Edit your stylish profile & cover images')}</Text>
              </View>
              <Icon1
                name="chevron-right"
                size={28}
                color={colors.primary}
                style={{ marginLeft: 5 }}
                enableRTL={true}
              />
            </TouchableOpacity>
            {/* <TouchableOpacity
              style={[styles.profileItem, { borderBottomColor: colors.border, borderBottomWidth: 1, width: 300 }]}
              onPress={() => {
                navigation.navigate('Setting');
              }}>
              <View style={{ flexDirection: "column" }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>{t('Account Settings')}</Text>
                <Text style={{ fontFamily: "Montserrat-Regular", top: 5 }}>{t('Manage notifications & app settings')}</Text>
              </View>
              <Icon1
                name="chevron-right"
                size={28}
                color={colors.primary}
                style={{ marginLeft: 5 }}
                enableRTL={true}
              />
            </TouchableOpacity> */}
            <TouchableOpacity
              style={[styles.profileItem, { borderBottomColor: colors.border, borderBottomWidth: 1, width: 300 }]}
              onPress={() => {
                navigation.navigate('AddressScreenModelLABL');
              }}>
              <View style={{ flexDirection: "column" }}>
                <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 20 }}>{t('Address')}</Text>
                <Text style={{ fontFamily: "Montserrat-Regular", top: 5 }}>{t('Save address or add more')}</Text>
              </View>
              <Icon1
                name="chevron-right"
                size={28}
                color={colors.primary}
                style={{ marginLeft: 5 }}
                enableRTL={true}
              />
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "column", alignItems: "flex-start", marginLeft: 50, marginVertical: 20 }}>
            <TouchableOpacity>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22, marginBottom: 15 }}>
                FAQS
              </Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22, marginBottom: 15 }}>
                ABOUT US
              </Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22, marginBottom: 15 }}>
                TERMS OF USE
              </Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 22 }}>
                PRIVACY POLICY
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ paddingHorizontal: 20, paddingVertical: 15 }}>
            <Button full loading={loading} onPress={() => onLogOut()}>
              {t('sign_out')}
            </Button>
          </View>
        </ScrollView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height:height,backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor:"#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  );
}
