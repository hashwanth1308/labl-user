import{ 
    
  collectionsPostCheckSuccess,
  collectionsSetCount,
  collectionsCountCheckSuccess,
  collectionsDeleteCheckSuccess,
  collectionsFindOneCheckSuccess,
  collectionsSetOneData,
  collectionsGetCheckSuccess,
  collectionsSetData,
  collectionsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function collectionsPostRequest(element) {
      return dispatch => {
        dispatch(collectionsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `collections`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(collectionsSetData(a));
            dispatch(collectionsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(collectionsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function collectionsCountRequest(element) {
      return dispatch => {
          dispatch(collectionsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `collections/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(collectionsSetCount(data));
              dispatch(collectionsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(collectionsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function collectionsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(collectionsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `collections/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(collectionsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(collectionsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function collectionsFindOneRequest(id) {
      return dispatch => {
        dispatch(collectionsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `collections/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(collectionsOneData(data));
            dispatch(collectionsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(collectionsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function collectionsGetRequest(element) {
  return dispatch => {
    dispatch(collectionsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `collections`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(collectionsSetOneData(data));
        dispatch(collectionsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(collectionsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function collectionsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(collectionsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `collections/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(collectionsSetData(data));
              dispatch(collectionsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(collectionsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 