import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
} from 'react-native';
import {
  Image,
  HotelItem,
  Card,
  Button,
  SafeAreaView,
  EventCard,
  Header,
  CartCard,
  TextInput
} from '@components';
import _ from "lodash";
import RazorpayCheckout from 'react-native-razorpay';
import CheckBox from '@react-native-community/checkbox';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { PromotionData, TourData, HotelData, BestData } from '@data';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { userCartsDeleteRequest, userCartsGetRequest } from '../../api/userCarts';
import { productsGetRequest, productsPostRequest } from '../../api/products';
import { color } from 'react-native-reanimated';
import { ordersPostRequest } from '../../api/orders';
import Icon from "react-native-vector-icons/Entypo";
import { modelsGetRequest } from '../../api/models';
import axios from "axios";
export default function UserCart({ navigation }) {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const { colors } = useTheme();
  const [imageData] = useState([]);
  const [adults, setAdults] = useState(2);
  const [orderId, setOrderId] = useState('');
  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const dispatch = useDispatch();
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  // const loginModelData = useSelector(state => state.modelsReducer.modelsData);
  // useEffect(() => {
  //   dispatch(modelsGetRequest(UserData?.user.id));
  // }, [dispatch])

  // console.log("loginModelData", loginModelData);

  // const modelId = loginModelData[0]?.id;
  // const loginModelData = useSelector(state => state.modelsReducer.modelsData);
  // const modelId=loginModelData[0]?.id;
  // useEffect(() => {
  //   dispatch(modelsGetRequest(modelId));
  // }, [dispatch])
  useEffect(() => {
    dispatch(productsGetRequest());

  }, [dispatch])

  useEffect(() => {
    dispatch(userCartsGetRequest(UserData?.user?.id));

  }, [dispatch])
  const cartsData = useSelector(state => state.userCartsReducer.userCartsData)
  const productsData = useSelector(state => state.productsReducer.productsData)


  console.log("cartsData", cartsData);
  // console.log("UserToken", userToken);
  const products = [];
  const sumValue = _.sumBy(cartsData, item => item?.modelProduct?.priceOfProductWithModelCommission);
  const productArray = _.forEach(cartsData, i => products.push({ _id: i?.modelProduct?.id }));
  console.log(sumValue, products);


  // cartsData?.forEach((i) => {
  //   products.push({ _id: i?.product?.id });
  // })
  const deleteProduct = (id) => {
    console.log("del button pressed",id);
    dispatch(userCartsDeleteRequest(id));
  };
  const orderPage = async () => {
    console.log("order button clicked")
    let objectValue = {};
    // objectValue.orderId = orderId;
    objectValue.model_products = products;
    objectValue.model_earning = sumValue;
    objectValue.users_permissions_user = UserData?.user?.id;
    console.log("order button clicked", objectValue)
    setLoading(true);
    axios
      .post(`https://staging-backend.labl.store/orders/`, objectValue)

      .then(({ data }) => {
        console.log(data);
        setOrderId(data.id);
        console.log(data);
         payment();
      })
      .catch(() => { });
  
    // await dispatch(signupRequest( email, MobileNumber, password , username, firstName, lastName));
    //await dispatch(ordersPostRequest(objectValue));

    // openModal('orderPlaced');



  };

  const payment = () => {
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: 'INR',
      key: 'rzp_test_s2Vw6rrT8MlDYg',
      amount: sumValue,
      name: 'LABL',
      order_id:orderId,
      prefill: {
        email: 'ab@websoc.co.in',
        contact: '9191919191',
        name: 'Ashok'
      },
      theme: { color: '#53a20e' }
    }
    console.log("razorpayoptions",options)
    RazorpayCheckout.open(options).then((data) => {
      // handle success
      console.og("data razorpay", data)
      alert(`Success: ${data.razorpay_payment_id}`);

    }).catch((error) => {
      // handle failure
      alert(`Error: ${error.code} | ${error.description}`);
    });
    // orderPage();
    cartsData.map((item)=>{
      deleteProduct(item.id)
    
     });
  }

  // const onFindOneCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };


  // const onAddCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateCarts = () => {
  //   setLoading(true);
  //   dispatch(cartsUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  //   console.log(cartsData[0], cartsData);

  const [cart] = useState([
    {
      image: Images.event4,
      name: 'MEN',
      description: 'kurta',
      price: '$399',
      route: 'Hotel',
    },
    {
      image: Images.event8,
      name: 'WOMEN',
      description: 'kurta',
      price: '$399',
      route: 'Tour',
    },
    {
      image: Images.event9,
      name: 'KIDS',
      description: 'kurta',
      price: '$399',
      route: 'OverViewCar',
    },
    {
      image: Images.event10,
      name: 'JACKETS',
      description: 'kurta',
      price: '$399',
      route: 'FlightSearch',
    },

  ]);

  const [heightHeader, setHeightHeader] = useState(Utils.heightHeader());
  const deltaY = new Animated.Value(0);
  const App = () => { }
  const [isSelected, setSelection] = useState(false);

  let ImageUrl = imageData ? { imageData } : require('../../assets/images/avata-01.jpeg')

  const renderIconService = () => {
    return (
      <FlatList
        vertical
        data={cartsData}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => {
          return (
            <View
              style={styles.itemService}
             >
              <View
                style={[styles.iconContent,]}>
                <TouchableOpacity onPress={() => {
                  navigation.navigate('CheckOut');
                }}>
                  <Image source={{ uri: item?.modelProduct?.imagesOfProductByModel ? `https://staging-backend.labl.store` + item?.modelProduct?.imagesOfProductByModel[0]?.url : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsQ-YHX2i3RvTDDmpfnde4qyb2P8up7Wi3Ww&usqp=CAU' }} style={{ height: 110, width: 90, borderRadius: 18, marginbottom: 10 }} />

                </TouchableOpacity>
              </View>

              <View style={styles.content}>
                <View style={styles.left}>
                  <Text
                    note
                    numberOfLines={3}
                    footnote
                    grayColor
                    style={{

                    }}>
                    {/* {item.modelProduct?.productName} */}
                  </Text>
                  <Text
                    note
                    numberOfLines={2}
                    footnote
                    grayColor
                    style={{

                    }}>
                    {/* {item.product?.productDescription} */}
                  </Text>
                  <Text
                    note
                    numberOfLines={1}
                    footnote
                    grayColor
                  >
                    {item.modelProduct?.priceOfProductWithModelCommission}
                  </Text>
                </View>

              </View>

              {/* <View style={styles.iconRight}>
                <TouchableOpacity
                  onPress={() => {
                    setAdults(adults - 1 > 0 ? adults - 1 : 0);
                  }}>
                  <Icon
                    name="minus"
                    size={24}
                    color={colors.primary}
                  />
                </TouchableOpacity>
                <Text title1>{adults}</Text>
                <TouchableOpacity
                  onPress={() => {
                    setAdults(adults + 1);
                  }}>
                  <Icon name="plus" size={24} color={colors.primary} />

                </TouchableOpacity>


              </View> */}
              {/* <CheckBox
                value={isSelected}
                onValueChange={setSelection}
                style={styles.checkbox}
              /> */}
              <View  style={{alignSelf:'flex-end',bottom:160}}>
               <TouchableOpacity
                  onPress={() => deleteProduct(item.id)}>
                  <Icon name="cross" size={24}  />

                </TouchableOpacity>
                </View>
            </View>

          )
        }}
      />
    );
  };

  const heightImageBanner = Utils.scaleWithPixel(60);
  const marginTopBanner = heightImageBanner - heightHeader;


  return (
    <View style={{ flex: 1, top: 10 }}>
      <Header
        title={t('Shopping Cart')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}

        renderRight={() => {
          return <Icon name="bell" size={24} color={colors.primary} />;
        }}
        onPressRight={() => {
          navigation.navigate('Notification');
        }}
      />

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
        <ScrollView
          onContentSizeChange={() => setHeightHeader(Utils.heightHeader())}
          scrollEventThrottle={8}>
          <View style={{ paddingHorizontal: 20 }}>
            <View
              style={[
                styles.searchForm,
                {
                  marginTop: marginTopBanner,
                  backgroundColor: colors.background,
                  borderColor: colors.border,
                  shadowColor: colors.border,
                },
              ]}>
              {renderIconService()}
            </View>

          </View>
          <View style={{ flexDirection: 'row', marginLeft: 10, marginBottom: 15 }}>
            <TextInput
              onChangeText={text => setSearch(text)}
              placeholder={t('PromoCode')}

              style={{ marginLeft: Utils.scaleWithPixel(5), borderRadius: 20, width: 330, }}
              // value={search}
              onSubmitEditing={() => {
                onSearch(search);
              }}
            />
          </View>
          <Button
            style={{ bottom: 55, width: 100, height: 30, alignSelf: 'flex-end', right: 25 }}
            onPress={() => {
              navigation.navigate('CheckOut');
            }}>
            <Text whiteColor >
              {t('Apply')}
            </Text>
          </Button>
          <Text title3 style={styles.titleView}>
            {t('Subtotal')}
          </Text>
          <View style={{ alignSelf: 'flex-end', marginRight: 20, bottom: 20 }}>
            <Text body2 semibold>
              {t(sumValue)}
            </Text>
          </View>
          {/* <Text title3 style={styles.titleView}>
            {t('Shipping')}
          </Text>
          <View style={{alignSelf:'flex-end',marginRight:20, bottom: 20 }}>
            <Text body2 semibold>
              {t('$20.00')}
            </Text>
          </View>
          <Text title3 style={styles.titleView}>
            {t('BagTotal')}
          </Text>
          <View style={{ alignSelf:'flex-end',marginRight:20, bottom: 20 }}>
            <Text body2 semibold>
              {t('$20.00')}
            </Text>
          </View> */}
          <Button
            style={{ top: 5, alignSelf: "center", borderRadius: 20, }}
            // onPress={() => {
            //   navigation.navigate('CheckOut');
            // }}
            onPress={() => orderPage()}
          >
            <Text body2 semibold whiteColor>
              {t('Proceed To Checkout')}
            </Text>
          </Button>
          <View style={[styles.line, { backgroundColor: colors.border }]} />

        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

