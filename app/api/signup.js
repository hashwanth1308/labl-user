
import axios from 'axios';
import { Alert } from "react-native";
import { applyMiddleware } from 'redux';
import { modelsPostRequest } from './models';
import { signupFailure, signupData, signupLoading, signupSuccess, verifyOtpSucess, verifyOtpFailure } from '../actions';
import { setAccessToken, setUserData } from '../actions';

export const signupRequest = (name, email, mobile, password, isModel,isVendor) => {
    const dataObject = {}
    dataObject.username = name;
    dataObject.email = email;
    dataObject.mobile = mobile;
    dataObject.password = password;
    dataObject.isModel = isModel;
    dataObject.isVendor = isVendor;
    // dataObject.userType = 'Model'

    console.log("signup post", dataObject);
    return dispatch => {
        dispatch(signupLoading(true));
        dispatch(signupSuccess(false));
        dispatch(signupFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'post',
            url: 'auth/local/register',
            data: dataObject,
            headers: {
                // Authorization: `Bearer ${userToken}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                console.log("response from signup", response)
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                console.log("response data", data)
                dispatch(setUserData(data));
                dispatch(setAccessToken(data.jwt));
                dispatch(signupData(data));
                dispatch(signupLoading(false));
                dispatch(signupSuccess(true));
                dispatch(signupFailure(false));
                dispatch(sendOtpRequest(dataObject.mobile));
                if (isModel === true) {
                    dispatch(modelsPostRequest(data));
                }


            })
            .catch((err) => {
                console.log("error", err.response);
                dispatch(signupLoading(false));
                dispatch(signupFailure(true));
                dispatch(signupSuccess(false));

                if (err.response.status === 400) {
                    alert(err.response.data.message[0].messages[0].message)
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};
//update profile
export const signupRequestPut = (firstName, lastName, gender, mobile, email, userId, selectedImage) => {
    console.log("userId", userId)
    const dataObject = {}

    dataObject.firstname = firstName;
    dataObject.lastname = lastName;
    dataObject.gender = gender;
    dataObject.mobile = mobile;
    dataObject.email = email;
    dataObject.image = selectedImage;

    console.log("signup put", dataObject);
    return dispatch => {
        dispatch(signupLoading(true));
        dispatch(signupSuccess(false));
        dispatch(signupFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'put',
            url: `users/${userId}`,
            data: dataObject,
            headers: {
                // Authorization : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZDViMTIyMWI2ZGJkNDE1NmU2YzRjYyIsImlhdCI6MTY0NTQ2MTY0MiwiZXhwIjoxNjQ4MDUzNjQyfQ.autfCsU_9EwhquhkPmBovYeJnsJmVYloUQwnOVZjQJc",         
                "Content-Type": "application/json",

            },

        })
            .then(response => {
                console.log("response from profile update", response)
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                console.log("response data", data)

                dispatch(signupData(data));
                dispatch(setUserData(data));
                dispatch(signupLoading(false));
                dispatch(signupSuccess(true));
                dispatch(signupFailure(false));



            })
            .catch((err) => {
                console.log("error", err.response);
                dispatch(signupLoading(false));
                dispatch(signupFailure(true));
                dispatch(signupSuccess(false));

                if (err.response.status === 400) {
                    alert("Please Fill Valid Data")
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};
//update api for chaneel,categories,creaters in user
export const signupRequestPutForChannel = (selectedCat, selectedFallowing, selectedChannel, userToken, userId) => {
    console.log("userId", userId)
    const dataObject = {}

    dataObject.followingCategories = selectedCat;
    dataObject.followingChannels = selectedChannel;
    dataObject.followingProfiles = selectedFallowing;

    console.log("signup put", dataObject, 'http://79.133.41.207:1337/users/' + userId);
    return dispatch => {
        dispatch(signupLoading(true));
        dispatch(signupSuccess(false));
        dispatch(signupFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'put',
            url: `http://79.133.41.207:1337/users/${userId}`,
            data: dataObject,
            headers: {
                Authorization: `Bearer ${userToken}`,
                "Content-Type": "application/json",

            },

        })
            .then(response => {
                console.log("response from profile update", response)
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                console.log("response data", data)

                dispatch(signupData(data));
                dispatch(signupLoading(false));
                dispatch(signupSuccess(true));
                dispatch(signupFailure(false));



            })
            .catch((err) => {
                console.log("error", err.response);
                dispatch(signupLoading(false));
                dispatch(signupFailure(true));
                dispatch(signupSuccess(false));

                if (err.response.status === 400) {
                    alert("Please Fill Valid Data")
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};
export const sendOtpRequest = (MobileNumber) => {
    console.log("inside send otp api", MobileNumber)
    return dispatch => {
        dispatch(signupLoading(true));
        // dispatch(signupSuccess(false));
        // dispatch(signupFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'GET',
            url: `https://api.msg91.com/api/v5/otp?template_id=6130a7bb390c095dbd546af6&mobile=91${MobileNumber}&authkey=366475AiwGawQc612c9aa8P1`,
            // data: data,
            headers: {
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                // dispatch(signupData(data));
                console.log("++++++++inside sent otp api", data);
                dispatch(signupLoading(false));
                // dispatch(signupSuccess(true));
                // dispatch(signupFailure(false));
            })
            .catch((err) => {
                console.log("error", err.response);
                dispatch(signupLoading(false));
                s
                if (err.response.status === 400) {
                    alert("Please Fill Valid Data")
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};

export const sendVerifyOtpRequest = (MobileNumber, enterOtp) => {


    return dispatch => {
        dispatch(signupLoading(true));
        // dispatch(signupSuccess(false));
        // dispatch(signupFailure(false));
        const axiosLogin = axios.create();
        delete axiosLogin.defaults.headers;
        return axiosLogin({
            method: 'post',
            url: `https://api.msg91.com/api/v5/otp/verify?authkey=366475AiwGawQc612c9aa8P1&mobile=91${MobileNumber}&otp=${enterOtp}`,


            // data: data,
            headers: {
                // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response.data;
            }).then(data => {
                dispatch(signupData(data));
                console.log("+++++verifyotp+++", data);
                console.log("dnfkjdsgfuidsgfkj1===========", data.message);
                console.log("dnfkjdsgfuidsgfkj1-------------------", data.type);
                if (data.message == "OTP verified success") {

                    alert('OTP');
                    // navigation.navigate('ProfileEdit')
                    dispatch(verifyOtpSucess(true))
                }
                else {
                    alert('OTP invaild');
                    dispatch(verifyOtpFailure(true))

                }
                // dispatch(signupLoading(false));
                // dispatch(signupSuccess(true));
                // dispatch(signupFailure(false));
            })
            .catch((err) => {
                console.log("error", err.response);
                dispatch(signupLoading(false));
                // dispatch(signupFailure(true));
                // dispatch(signupSuccess(false));
                if (err.response.status === 400) {
                    alert("Please Fill Valid Data")
                } else {
                    alert("Something Went Wrong")
                }
            }).catch(err => console.log("ll", err))
    };
};
