import React, { useEffect, useState } from 'react';
import {
  View,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import { RadioButton } from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { BaseStyle, useTheme, Images } from '@config';
import PropTypes from 'prop-types';
import { Header, SafeAreaView, Icon, Text, Image, TextInput, Button } from '@components';
import { useTranslation } from 'react-i18next';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons'
import styles from './styles';
import {
  addressCountRequest, addressGetRequest, addressFindOneRequest, addressPostRequest, addressDeleteRequest,
  addressUpdateRequest
} from '../../api/address';

export default function AddressScreenLABL({ navigation }) {
  const { colors } = useTheme();
  const { t } = useTranslation();
  const height = Dimensions.get('window').height;
  const width = Dimensions.get('window').width;

  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const themeStorage = useSelector(state => state.application.theme);

  const addressData = useSelector(state => state.addressReducer.addressData);
  console.log("address", addressData);
  const userData = useSelector(state => state.accessTokenReducer.userData);
  const userid = userData?.user?.id
  const dispatch = useDispatch();
  const [card, setCard] = useState('');
  const [valid, setValid] = useState('');
  const [digit, setDigit] = useState('');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const [primary, setPrimary] = useState(true);
  const [success] = useState({
    card: true,
    valid: true,
    digit: true,
    name: true,
  });
  const addresspost = useSelector(state => state.addressReducer.addressPostCheckSuccess);
  const addressUpdate = useSelector(state => state.addressReducer.addressUpdateCheckSuccess);
  // useEffect(() => {
  //     dispatch(addressCountRequest());

  // }, [dispatch])

  const addressCountCheckSuccess = useSelector(state => state.addressReducer.addressCountCheckSuccess)
  useEffect(() => {
    const obj = {};
    obj.user = userid;
    dispatch(addressGetRequest(obj));
  }, [addresspost, addressUpdate,addressCountCheckSuccess]);

  // const onFindOneAddress = () => {
  //   setLoading(true);
  //   dispatch(addressFindOneRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };


  // const onAddAddress = () => {
  //   setLoading(true);
  //   dispatch(addressPostRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onDeleteAddress = () => {
  //   setLoading(true);
  //   dispatch(addressDeleteRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  // const onUpdateAddress = () => {
  //   setLoading(true);
  //   dispatch(addressUpdateRequest());
  //   // setTimeout(() => {
  //   //   navigation.goBack();
  //   // }, 1000);
  // };

  const [cart] = useState(
    [
      {
        id: 1,
        name: 'Ramesh',
        description: 'flat.no-302,suvarna habitat,jai hind enclave,madhapur',
        phone: "888888888",
        route: 'Hotel',
      },
      {
        id: 2,
        name: 'Ramesh',
        description: 'flat.no-302,suvarna habitat,jai hind enclave,madhapur',
        phone: "888888888",
        route: 'Hotel',
      },
      // {
      //   image: Images.event9,
      //   name: 'Sweat-shirt',
      //   description: 'kurta',
      //   price: "399",
      //   route: 'OverViewCar',
      // },
      // {
      //   image: Images.event10,
      //   name: 'JACKETS',
      //   description: 'kurta',
      //   price: "399",
      //   route: 'FlightSearch',
      // },
      // {
      //   image: Images.event10,
      //   name: 'JACKETS',
      //   description: 'kurta',
      //   price: "399",
      //   route: 'FlightSearch',
      // },
      // {
      //   image: Images.event10,
      //   name: 'JACKETS',
      //   description: 'kurta',
      //   price: "399",
      //   route: 'FlightSearch',
      // },

    ]
  );
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <Header
        // title={t('Shopping Bag')}
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flexDirection: "row", width: 200, alignSelf: "flex-start", top: -10 }}>
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
              <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", marginHorizontal:20 }}>Address</Text>
              {/* <Text style={{ fontSize: 25, color: "#fff" }}>4 items</Text> */}
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ padding: 20 }}>
            <TouchableOpacity onPress={() => navigation.navigate("AddAddressScreenLABL")} style={{ width: "100%", height: 60, backgroundColor: "#000", justifyContent: "center", alignItems: "center" }}>
              <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "#fff" }}>+ Add New Address</Text>
            </TouchableOpacity>
            {addressData?.length > 0 ? 
            <AddressComp address={addressData} /> :null}
            
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      {loading1 &&
        <View style={{ width: width, height: height, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  )
}

function AddressComp(props) {
  const navigation = useNavigation();
  const [checked, setChecked] = React.useState(false);
  // const handleClick = () => {
  //   setChecked(true)
  // }
  // const handleClick1 = () => {
  //   setChecked(false)
  // } 
  const dispatch = useDispatch();
  const userData = useSelector(state => state.accessTokenReducer.userData);
  const userid = userData?.user?.id

  const handleClick = (item) => {
    const obj = {};
    obj.userId = userid;
    obj.addressId = item?.id;
    dispatch(addressCountRequest(obj,userData?.jwt));
  }
  const { address } = props;
  return (
    <>
      {address?.map((item) => (
        <TouchableOpacity onPress={() => handleClick(item)} style={{ flex:1,borderWidth: 1, borderColor: "#909090", borderRadius: 10, margin: 20, alignSelf: "center", marginBottom: 10 }}>
          <View style={{ flexDirection: "row", padding: 10, justifyContent: "space-around" }}>
            <View style={{ flexDirection: "column" }}>
              <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 22, marginVertical: 5 }}>{item?.fullName}</Text>
                {item?.typeOfAddress ?
                  <View style={{ alignSelf: "center" }}>
                    <Icon1
                      name="check-circle"
                      size={20}
                    />
                  </View> : null}
              </View>
              <Text style={{ fontFamily: "Montserrat-Regular", fontSize: 16, marginVertical: 10 }}>{item?.houseNo},{item?.areaColony},{'\n'}{item?.city},{'\n'}Pincode: {item?.pincode}</Text>
              <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, marginVertical: 10 }}>Phone : {item?.mobileNumber}</Text>
            </View>
          </View>
          <View style={{ width: 318, borderWidth: 0.5, borderColor: "#909090", marginVertical: 0 }}></View>
          <TouchableOpacity onPress={() => navigation.navigate("UpdateAddressScreenLABL", { updateData: item })} style={{ marginVertical: 10, alignItems: "flex-end", justifyContent: "center", right: 20 }}>
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18, }}>Edit Address</Text>
          </TouchableOpacity>
        </TouchableOpacity>
      ))}
    </>
  )
};

