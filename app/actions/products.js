import {
  PRODUCTS_POST_CHECK_SUCCESS,
  PRODUCTS_SET_COUNT,
  PRODUCTS_COUNT_CHECK_SUCCESS,
  PRODUCTS_DELETE_CHECK_SUCCESS,
  PRODUCTS_FINDONE_CHECK_SUCCESS,
  PRODUCTS_SET_ONE_DATA,
  PRODUCTS_GET_CHECK_SUCCESS,
  PRODUCTVARIENT_GET_CHECK_SUCCESS,
  PRODUCTS_SET_DATA,
  LABLPROMOTED_SET_DATA,
  LABLEXCLUSIVE_SET_DATA,
  PRODUCTSIZE_SET_DATA,
  PRODUCTVARIENT_SET_DATA,
  PRODUCTS_UPDATE_CHECK_SUCCESS,
  PRODUCTS_GET_COLLECTIONID_SET_DATA
} from './actionTypes';


export function productsPostCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productsSetCount(object) {
  return {
    type: PRODUCTS_SET_COUNT,
    payload: object,
  };
}
export function productsCountCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productsDeleteCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productsFindOneCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productsSetOneData(object) {
  return {
    type: PRODUCTS_SET_ONE_DATA,
    payload: object,
  };
}
export function productsGetCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}
export function productVarientGetCheckSuccess(bool = false) {
  return {
    type: PRODUCTVARIENT_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function productsSetData(object) {
  return {
    type: PRODUCTS_SET_DATA,
    payload: object,
  };
}
export function lablPromotedSetData(object) {
  return {
    type: LABLPROMOTED_SET_DATA,
    payload: object,
  };
}
export function lablExclusiveSetData(object) {
  return {
    type: LABLEXCLUSIVE_SET_DATA,
    payload: object,
  };
}
export function productSizeSetData(object) {
  return {
    type: PRODUCTSIZE_SET_DATA,
    payload: object,
  };
}
export function productVarientSetData(object) {
  return {
    type: PRODUCTVARIENT_SET_DATA,
    payload: object,
  };
}

export function productsUpdateCheckSuccess(bool = false) {
  return {
    type: PRODUCTS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}
export function productsGetUsingCollectionIdSetData(object) {
  return {
    type: PRODUCTS_GET_COLLECTIONID_SET_DATA,
    payload: object,
  };
}
