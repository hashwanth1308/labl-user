import { StyleSheet } from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
  imageBackground: {
    height: 140,
    width: '100%',
    position: 'absolute',
  },
  searchForm: {
    padding: 10,
    borderRadius: 10,
    borderWidth: 0.5,
    width: '100%',
    shadowColor: 'black',
    shadowOffset: { width: 1.5, height: 1.5 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 1,
  },
  contentServiceIcon: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  contentCartPromotion: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  btnPromotion: {
    height: 35,
    borderRadius: 5,
    paddingHorizontal: 25,
    paddingVertical: 5,
  },
  btnPromotion1: {
    height: 35,
    borderRadius: 5,
    paddingHorizontal: 8,
    paddingVertical: 5,
  },
  contentHiking: {
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 10,
  },
  promotionBanner: {
    height: Utils.scaleWithPixel(150),
    width: '100%',
    marginTop: 10,
  },
  line: {
    height: 1,
    marginTop: 10,
    marginBottom: 15,
  },
  iconContent: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 120,
  },
  itemService: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    paddingTop: 10,
  },
  promotionItem: {
    width: Utils.scaleWithPixel(285),
    height: Utils.scaleWithPixel(270),
    justifyContent: 'center',
  },
  tourItem: {
    width: Utils.scaleWithPixel(120),
    height: Utils.scaleWithPixel(100),
    paddingTop: 5,
    right: Utils.scaleWithPixel(9),
  },

  titleView: {
    paddingHorizontal: 25,
    bottom: 30,
    fontSize: 20,
  },
  btnClearSearch: {
    position: 'absolute',
    right: Utils.scaleWithPixel(0),
    alignItems: 'center',
    justifyContent: 'center',
    width: Utils.scaleWithPixel(30),
    height: '100%',
    right: Utils.scaleWithPixel(30)
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    right: Utils.scaleWithPixel(23),
    top: 30,

  },
});
