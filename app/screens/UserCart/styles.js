import { StyleSheet } from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({
    imageBackground: {
        height: 140,
        width: '100%',
        position: 'absolute',
    },
    searchForm: {
        padding: 10,
        borderRadius: 10,
        borderWidth: 0.5,
        width: '100%',
        shadowColor: 'black',
        shadowOffset: { width: 1.5, height: 1.5 },
        shadowOpacity: 0.3,
        shadowRadius: 1,
        elevation: 1,
    },
    contentServiceIcon: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    contentCartPromotion: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    btnPromotion: {
        height: 25,
        borderRadius: 3,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    contentHiking: {
        marginTop: 20,
        marginLeft: 20,
        marginBottom: 10,
    },
    promotionBanner: {
        height: Utils.scaleWithPixel(150),
        width: '100%',
        marginTop: 10,
    },
    line: {
        height: 1,
        marginTop: 10,
        marginBottom: 15,
    },
    iconContent: {
        marginLeft: 40,
        // alignItems: 'center',
        height: 40,
    },

   
    lineRow: {
        justifyContent: 'space-between',
    },
    iconRight: {
        width: 100,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        left: 145,
        bottom: 50
    },
    couponSection: {
        height: 52,
        borderRadius: 50,
        borderColor: 'rgba(0,0,0,0.15)',
        borderStyle: 'solid',
        borderWidth: 1,
        paddingHorizontal: 29,
        marginTop: 32,
    },
    placeholder: {
        opacity: 0.5,
        color: '#707070',
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: '400',
        lineHeight: 32,
        flex: 1,
    },
    titleView: {
        paddingHorizontal: 25,
        // paddingTop: 5,
        // paddingBottom: 5,
        fontSize: 20,
    },
    checkbox: {
        alignSelf: "flex-start",
        bottom: 80,
        right: 5
    },
    content: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 30,
        alignItems: 'flex-end',
        

    },
    left: {
        flex: 7.5,
        alignItems: 'center',
        top:80,
        // left:80
    },
});
