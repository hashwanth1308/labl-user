import React, { useState, useEffect } from 'react';
import {
  View,
  Animated,
  RefreshControl,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Dimensions,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import {
  Text,
  HotelItem,
  Image,
  Header,
  ListThumbCircle,
  PostItem,
  Icon,
  ProfileAuthor,
} from '@components';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { BaseStyle, Images, useTheme, BaseColor } from '@config';
import * as Utils from '@utils';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { UserData } from '../../data/user';
import { HotelData } from '../../data/hotel';
import { modelsGetRequest } from '../../api/models';
import {
  userFollowingGetRequest,
  userFollowingDeleteRequest,
  userFollowingPostRequest,
} from '../../api/following';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

export default function FollowersScreen({ navigation }) {
  const { t } = useTranslation();
  const { colors } = useTheme();
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log(UserData);
  const obj = {};
  obj.user = UserData?.user?.id;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(modelsGetRequest());
  }, [dispatch]);
  useEffect(() => {
    dispatch(userFollowingGetRequest(obj));
  }, [dispatch]);
  const userFollowings = useSelector(
    state => state.followingReducer.userFollowingData,
  );
  const width = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const handleIndexChange = index => setIndex(index);
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: 'grid', title: `${userFollowings?.length}   Following`, icon: 'view-grid-outline' },
    { key: 'list', title: 'Follow More', icon: 'format-list-bulleted' },
  ]);
  // Customize UI tab bar
  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={[styles.indicator, { backgroundColor: colors.primary }]}
      style={[styles.tabbar, { backgroundColor: colors.background }]}
      tabStyle={styles.tab}
      inactiveColor={BaseColor.grayColor}
      activeColor={colors.text}
      renderLabel={({ route, focused, color }) => (
        <View style={{ flex: 1, alignItems: 'center', width: 150 }}>
          <Text
            headline
            semibold={focused}
            style={{
              color,
              fontFamily: 'Montserrat-Bold',
              fontSize: 16,
              margin: 10,
            }}>
            {route.title}
          </Text>
        </View>
      )}
    />
  );

  // Render correct screen container when tab is activated
  const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'grid':
        return <BookingTab jumpTo={jumpTo} navigation={navigation} />;
      case 'list':
        return <SettingTab jumpTo={jumpTo} navigation={navigation} />;
    }
  };
  const [loading1, setLoading1] = useState(true);
  useEffect(() => {
    setTimeout(() => (
      setLoading1((prev) => (!prev))
    ), 1000)
  }, []);
  return (
    <View style={{ flex: 1 }} >
      {/* <Header
        // title={t('Shopping Bag')}
        style={{ height: 80, backgroundColor: "#000" }}
        renderLeft={() => {
          return (
            <View style={{ flexDirection: "row", width: 200, justifyContent: "space-between", alignSelf: "flex-start", top: -10 }}>
              <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "#fff", alignItems: "center", justifyContent: "center" }}>
                <Icon
                  name="arrow-left"
                  size={20}
                  color={colors.primary}
                  enableRTL={true}
                />
              </View>
              <Text style={{ alignSelf: "center", fontFamily: "Montserrat-Bold", fontSize: 20, color: "#fff", right: 40 }}>Following</Text>
            
            </View>
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      /> */}
      <TabView
        // lazy
        navigationState={{ index, routes }}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={handleIndexChange}
        style={{ height: windowHeight * 5, marginTop: 20 }}
      />
      {loading1 &&
        <View style={{ width: width, height: windowHeight, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center", position: "absolute" }}>
          <View style={{ width: "60%", paddingVertical: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center" }}>
            <View>
              <ActivityIndicator size={"large"} color="#000" />
            </View>
            <Text style={{ marginLeft: 10, fontSize: 30, color: "#000", fontWeight: "bold" }}>Loading..</Text>
          </View>
        </View>
      }
    </View>
  )
}

/**
 * @description Show when tab Booking activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function BookingTab({ navigation }) {
  const [hotels] = useState(HotelData);
  const userFollowings = useSelector(
    state => state.followingReducer.userFollowingData,
  );

  return (
    <View>
      <FlatList
        contentContainerStyle={{
          paddingLeft: 5,
          paddingRight: 20,
          paddingTop: 20,
        }}
        // numColumns={3}
        data={userFollowings}
        keyExtractor={(item, index) => item.id}
        renderItem={({ item, index }) => (
          <TouchableOpacity onPress={() => navigation.navigate("Profile5", { object: item?.model })} style={{ flexDirection: "row", flexWrap: "wrap",alignItems:"center",marginHorizontal:20, flex: 1 }}>
            <Image source={{ uri: item?.model?.users?.image ? item?.model?.users?.image : "https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png" }} 
            style={{ height: 40, width: 40, borderRadius: 30, alignSelf: 'center' }} />
            <Text style={{
              fontFamily: 'Montserrat-Bold',
              fontSize: 20,
              textAlign: 'left',
              width: 100,
              marginHorizontal:10
            }}>{item?.model?.users?.username}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
}

/**
 * @description Show when tab Setting activated
 * @author Passion UI <passionui.com>
 * @date 2019-08-03
 * @class PreviewTab
 * @extends {Component}
 */
function SettingTab({ navigation }) {
  const models = useSelector(state => state.modelsReducer.modelsData);
  console.log(models);
  const windowHeight = Dimensions.get('window').height;
  return (
    <ScrollView style={{ height: '100%' }}>
      <View style={{ padding: 10 }}>
        <FlatList
          contentContainerStyle={{
            paddingLeft: 5,
            paddingRight: 20,
            paddingTop: 20,
          }}
          // numColumns={3}
          data={models}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) => (
            <FollowersComponent name={item?.users?.username} object={item} />
          )}
        />
      </View>
    </ScrollView>
  );
}

function FollowersComponent(props) {
  const { name, object } = props;
  console.log("object", object);
  const UserData = useSelector(state => state.accessTokenReducer.userData);
  console.log(UserData);
  const dispatch = useDispatch();
  const navigation = useNavigation()
  const [follow, setFollow] = useState(false);
  const userFollowings = useSelector(
    state => state.followingReducer.userFollowingData,
  );
  console.log('follow', userFollowings);
  const [followId, setFollowId] = useState('');
  useEffect(() => {
    const followed = _.find(userFollowings, function (o) {
      return o.model?.id === object.id;
    });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true);
    }
  }, []);

  useEffect(() => {
    const followed = _.find(userFollowings, function (o) {
      return o.model?.id === object.id;
    });
    if (followed) {
      setFollowId(followed.id);
      setFollow(true);
    }
  }, [userFollowings]);
  const handleClick = object => {
    if (follow) {
      setFollow(false);
      dispatch(userFollowingDeleteRequest(followId));
    } else {
      setFollow(true);
      let objectValue = {};
      (objectValue.user = UserData?.user?.id),
        (objectValue.model = object?.id),
        (objectValue.isFollowing = true);
      dispatch(userFollowingPostRequest(objectValue));
    }
  };
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 20,
      }}>
      {/* <View style={{ flex: 0.8, flexDirection: "row",justifyContent:"space-around" }}> */}
      <Image
        source={{ uri: object?.users?.image ? object?.users?.image : "https://www.pngkey.com/png/detail/52-523516_empty-profile-picture-circle.png" }}
        style={{ height: 40, width: 40, borderRadius: 30, alignSelf: 'center' }}
      />
      <TouchableOpacity
        onPress={() => navigation.navigate("Profile5", { object: object })}
        style={{
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: -50,
        }}>
        <Text
          style={{
            fontFamily: 'Montserrat-Bold',
            fontSize: 20,
            textAlign: 'left',
            width: 100,
          }}>
          {name}
        </Text>
        <Text
          style={{
            fontFamily: 'Montserrat-Medium',
            fontSize: 14,
            textAlign: 'left',
            width: 100,
          }}>
          Model
        </Text>
      </TouchableOpacity>
      {/* </View> */}
      {follow ? (
        <TouchableOpacity
          onPress={() => handleClick(object)}
          style={{
            width: 120,
            height: 30,
            borderRadius: 10,
            borderWidth: 1,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat-Bold',
              fontSize: 20,
              color: '#000',
            }}>
            Following
          </Text>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          onPress={() => handleClick(object)}
          style={{
            width: 120,
            height: 30,
            backgroundColor: '#000',
            borderRadius: 10,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontFamily: 'Montserrat-Bold',
              fontSize: 20,
              color: '#fff',
            }}>
            Follow
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

// FollowersComponent.propTypes = {
//     image: PropTypes.node.isRequired,
//     name: PropTypes.string,
// };

// FollowersComponent.defaultProps = {
//     image: '',
//     name: '',
// };
