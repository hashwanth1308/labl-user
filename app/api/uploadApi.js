import {
  uploadFilesGetCheckSuccess,
  uploadFilesSetData,
  uploadFilesPostCheckSuccess,
  uploadFilesUpdateCheckSuccess,
  uploadFilesDeleteCheckSuccess,
} from '../actions';
import axios from 'axios';
let a = [];

export function uploadFilesGetRequest(element, token) {
  return dispatch => {
    dispatch(uploadFilesGetCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'get',
      url: `upload/files`,
      headers: {
        //  Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"api data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(uploadFilesSetData(data));
        dispatch(uploadFilesGetCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin, 'furnishings');
        dispatch(uploadFilesGetCheckSuccess(false));
      });
  };
}

// export function uploadFilesPostRequest(element) {
//   console.log(element);

//   return dispatch => {
//     dispatch(uploadFilesPostCheckSuccess(false));
//     const axiosLogin = axios.create();
//     return axiosLogin({
//       method: 'post',
//       url: `uploads`,
//       headers: {
//         // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
//         'Content-Type': 'application/json',
//       },
//       files: element._parts,
//     })
//       .then(response => {
//         console.log('response', response);
//         if (response.status !== 200) {
//           throw Error(response.statusText);
//         }
//         return response.data;
//       })
//       .then(data => {
//         // const propertyValues = Object.values(data.data);
//         // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
//         // a.push(obj)
//         console.log(data)
//         dispatch(uploadFilesSetData(data));
//         dispatch(uploadFilesPostCheckSuccess(true));
//       })
//       .catch(error => {
//         console.log(error, axiosLogin);
//         dispatch(uploadFilesPostCheckSuccess(false));
//       });
//   };
// }

export function uploadFilesPostRequest(formData) {
  console.log("inside api",formData)
  return new Promise((resolve, reject) => {
    const axiosLogin = axios.create();
    axiosLogin({
      method: 'post',
      url: `upload/`,
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: formData,
    })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        console.log('response.data', response.data);
        return response.data;
      })
      .then(imgdata => {
        console.log("in api")

        resolve(imgdata)
      })
      .catch(error => {
        console.log(error);
      });
  });
}

export function uploadFilesUpdateRequest(element) {
  console.log(element.accessToken);
  const accessToken = element.accessToken;
  delete element.accessToken;
  return dispatch => {
    dispatch(uploadFilesSetData(element));
    dispatch(uploadFilesUpdateCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'put',
      url: `upload-limits/610ceb4c3be536107bcb5c73`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
      data: element,
    })
      .then(response => {
        // console.log(response)
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log("data",data)
        dispatch(uploadFilesSetData(a));
        dispatch(uploadFilesUpdateCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        dispatch(uploadFilesUpdateCheckSuccess(false));
      });
  };
}

export function uploadFilesDeleteRequest(element) {
  console.log("elemnt",element);
  return dispatch => {
    // dispatch(uploadFilesDeleteCheckSuccess(false));
    const axiosLogin = axios.create();
    return axiosLogin({
      method: 'delete',
      url: `upload/files/${element}`,
      headers: {
        // Authorization: `Basic ${btoa('aimApiapi:aimApiPassword')}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log("response",response);
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // const propertyValues = Object.values(data.data);
        // const obj = {
        //   categoryID: element.id,
        //   categoryName: element.name,
        //   categoryImage: element.icon,
        //   subCategory: propertyValues,
        // };
        // a.push(obj);
        dispatch(uploadFilesSetData(data));
        // dispatch(uploadFilesDeleteCheckSuccess(true));
      })
      .catch(error => {
        console.log(error, axiosLogin);
        // dispatch(uploadFilesDeleteCheckSuccess(false));
      });
  };
}
