import {
    FURNISHING_POST_CHECK_SUCCESS,
    FURNISHING_SET_COUNT,
    FURNISHING_COUNT_CHECK_SUCCESS,
    FURNISHING_DELETE_CHECK_SUCCESS,
    FURNISHING_FINDONE_CHECK_SUCCESS,
    FURNISHING_SET_ONE_DATA,
    FURNISHING_GET_CHECK_SUCCESS,
    FURNISHING_SET_DATA,
    FURNISHING_UPDATE_CHECK_SUCCESS,
  } from '../actions/actionTypes';
  
  const initialState = {

    furnishingsPostCheckSuccess: false,
    furnishingsCount: 0,
    furnishingsCountCheckSuccess : false,
    furnishingsDeleteCheckSuccess: false,
    furnishingsFindOneCheckSuccess: false,
    furnishingsOneData: [],
    furnishingsGetCheckSuccess: false,
    furnishingsData: [],
    furnishingsUpdateCheckSuccess: false,
    
   
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case FURNISHING_POST_CHECK_SUCCESS:
        return {
          ...state,
          furnishingsPostCheckSuccess: action.payload,
        };
        case FURNISHING_SET_COUNT:
        return {
          ...state,
          furnishingsCount: action.payload,
        };
        case FURNISHING_COUNT_CHECK_SUCCESS:
        return {
          ...state,
          furnishingsCountCheckSuccess: action.payload,
        };
        case FURNISHING_DELETE_CHECK_SUCCESS:
        return {
          ...state,
          furnishingsDeleteCheckSuccess: action.payload,
        };
        case FURNISHING_FINDONE_CHECK_SUCCESS:
        return {
          ...state,
          furnishingsFindOneCheckSuccess: action.payload,
            };
        case FURNISHING_SET_ONE_DATA:
        return {
          ...state,
          furnishingsOneData: action.payload,
            };
        case FURNISHING_GET_CHECK_SUCCESS:
        return {
          ...state,
          furnishingsGetCheckSuccess: action.payload,
            };
        case FURNISHING_SET_DATA:
        return {
          ...state,
          furnishingsData: action.payload,
            };
        case FURNISHING_UPDATE_CHECK_SUCCESS:
        return {
          ...state,
          furnishingsUpdateCheckSuccess: action.payload,
            };
  
      default:
        return state;
    }
  };
  
  export default reducer;
  