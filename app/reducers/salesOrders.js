import {
  SALESORDERS_POST_CHECK_SUCCESS,
  SALESORDERS_SET_COUNT,
  SALESORDERS_COUNT_CHECK_SUCCESS,
  SALESORDERS_DELETE_CHECK_SUCCESS,
  SALESORDERS_FINDONE_CHECK_SUCCESS,
  SALESORDERS_SET_ONE_DATA,
  SALESORDERS_GET_CHECK_SUCCESS,
  SALESORDERS_SET_DATA,
  SALESORDERS_UPDATE_CHECK_SUCCESS,
} from '../actions/actionTypes';

const initialState = {

  salesOrdersPostCheckSuccess: false,
  salesOrdersCount: 0,
  salesOrdersCountCheckSuccess : false,
  salesOrdersDeleteCheckSuccess: false,
  salesOrdersFindOneCheckSuccess: false,
  salesOrdersOneData: [],
  salesOrdersGetCheckSuccess: false,
  salesOrdersData: [],
  salesOrdersUpdateCheckSuccess: false,
  
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SALESORDERS_POST_CHECK_SUCCESS:
      return {
        ...state,
        salesOrdersPostCheckSuccess: action.payload,
      };
      case SALESORDERS_SET_COUNT:
      return {
        ...state,
        salesOrdersCount: action.payload,
      };
      case SALESORDERS_COUNT_CHECK_SUCCESS:
      return {
        ...state,
        salesOrdersCountCheckSuccess: action.payload,
      };
      case SALESORDERS_DELETE_CHECK_SUCCESS:
      return {
        ...state,
        salesOrdersDeleteCheckSuccess: action.payload,
      };
      case SALESORDERS_FINDONE_CHECK_SUCCESS:
      return {
        ...state,
        salesOrdersFindOneCheckSuccess: action.payload,
          };
      case SALESORDERS_SET_ONE_DATA:
      return {
        ...state,
        salesOrdersOneData: action.payload,
          };
      case SALESORDERS_GET_CHECK_SUCCESS:
      return {
        ...state,
        salesOrdersGetCheckSuccess: action.payload,
          };
      case SALESORDERS_SET_DATA:
      return {
        ...state,
        salesOrdersData: action.payload,
          };
      case SALESORDERS_UPDATE_CHECK_SUCCESS:
      return {
        ...state,
        salesOrdersUpdateCheckSuccess: action.payload,
          };

    default:
      return state;
  }
};

export default reducer;
