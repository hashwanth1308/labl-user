import{ 
    
  paymentsPostCheckSuccess,
  paymentsSetCount,
  paymentsCountCheckSuccess,
  paymentsDeleteCheckSuccess,
  paymentsFindOneCheckSuccess,
  paymentsSetOneData,
  paymentsGetCheckSuccess,
  paymentsSetData,
  paymentsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function paymentsPostRequest(element) {
      return dispatch => {
        dispatch(paymentsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `payments`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(paymentsSetData(a));
            dispatch(paymentsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(paymentsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function paymentsCountRequest(element) {
      return dispatch => {
          dispatch(paymentsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `payments/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(paymentsSetCount(data));
              dispatch(paymentsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(paymentsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function paymentsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(paymentsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `payments/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(paymentsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(paymentsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function paymentsFindOneRequest(id) {
      return dispatch => {
        dispatch(paymentsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `payments/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(paymentsOneData(data));
            dispatch(paymentsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(paymentsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function paymentsGetRequest(element) {
  return dispatch => {
    dispatch(paymentsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `payments`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(paymentsSetOneData(data));
        dispatch(paymentsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(paymentsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function paymentsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(paymentsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `payments/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(paymentsSetData(data));
              dispatch(paymentsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(paymentsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 