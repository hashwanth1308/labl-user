import { StyleSheet } from 'react-native';
import * as Utils from '@utils';

export default StyleSheet.create({

  iconContent: {

    // alignItems: 'center',
    width: 150,
    height: 190,
    // marginVertical:10

  },
  item: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 5,
  },
  bottomModal: {
    // justifyContent: 'center',
    flex: 1,
    // height:300,
    // backgroundColor:"#000",
    margin: 0,
  },
  contain: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-start',
    // height: 620,
    backgroundColor: "#CDCDCD",
    padding: 0,
    // flex: 1,
  },
  bottomModal1: {
    justifyContent: 'flex-end',
    // height:300,
    // backgroundColor:"#000",
    margin: 0,
  },
  contain1: {
    // alignItems: 'center',
    height: 400,
    backgroundColor: "#fff",
    padding: 0,
    // flex: 1,
  },
  thumb: { width: 55, height: 100, marginRight: 10 },
  content: {
    flex: 1,
    // flexDirection: 'row',
    margin: 10
  },
  left: {
    // flex: 7,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  right: {
    flex: 2.5,
    alignItems: 'flex-start',
    justifyContent: 'center',

  },
  content1: {
    flexDirection: 'row',
    flex: 1.3,
  },

  // menu Column - left
  menuColumn: {
    flex: .4,
    flexDirection: 'column',
    borderRightColor: '#F8F8FF',
    borderRightWidth: 1,
    backgroundColor:"#000"
  },
  menuItem: {
    // flex: 1,
    flex: 0,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    // alignItems: 'flex-start',
    // borderWidth:1,
  },
  selectedMenuItem: {
    backgroundColor: '#F8F8FF',
    borderLeftColor: "green",
    borderLeftWidth: 5,
    alignItems:"center",
    flexDirection:"row",
    justifyContent:"space-between",
    padding:10
  },

  menuItemText: {
    marginLeft: 10,
    alignSelf: 'flex-start',
    color: "#fff",
    fontSize: 16,
    fontWeight: 'bold',
  },


  // settings column -right
  settingsColumn: {
    flex: .6,
    padding: 15,
    backgroundColor: "#F8F8FF"
  },
  settingsView: {
    flex: 1,
    // padding: 15,
    // backgroundColor: "#F8F8FF"
  },

});
