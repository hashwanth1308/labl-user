import{ 
    
  modelEarningsPostCheckSuccess,
  modelEarningsSetCount,
  modelEarningsCountCheckSuccess,
  modelEarningsDeleteCheckSuccess,
  modelEarningsFindOneCheckSuccess,
  modelEarningsSetOneData,
  modelEarningsGetCheckSuccess,
  modelEarningsSetData,
  modelEarningsUpdateCheckSuccess,
  } from '../actions';
  import axios from 'axios';
  let a = [];

  //create
  export function modelEarningsPostRequest(element) {
      return dispatch => {
        dispatch(modelEarningsPostCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'post',
            url: `model-earnings`,
            headers:{
              // Authorization: `Basic ${btoa('aimApiUser:aimApiPassword')}`,
              "Content-Type" : "application/json"}
           
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(modelEarningsSetData(a));
            dispatch(modelEarningsPostCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin)
            dispatch(modelEarningsPostCheckSuccess(false));
           
          });
      };
  }

   //count
   export function modelEarningsCountRequest(element) {
      return dispatch => {
          dispatch(modelEarningsCountCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'get',
              url: `model-earnings/count`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              data:element,
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(modelEarningsSetCount(data));
              dispatch(modelEarningsCountCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(modelEarningsCountCheckSuccess(false));
              
          });
      };
  }

  //delete
  export function modelEarningsDeleteRequest(id) {
      
      return dispatch => {
          dispatch(modelEarningsDeleteCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'delete',
              url: `model-earnings/${id}`,
              headers:{
              Authorization: `Bearer ${accessToken}`,
              "Content-Type" : "application/json"},
              
              })
          .then(response => {
              // console.log(response)
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              dispatch(modelEarningsDeleteCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(modelEarningsDeleteCheckSuccess(false));
              
          });
      };
  }

  //findOne
  export function modelEarningsFindOneRequest(id) {
      return dispatch => {
        dispatch(modelEarningsFindOneCheckSuccess(false));
        const axiosLogin = axios.create();
          return axiosLogin({
            method: 'get',
            url: `model-earnings/${id}`,
            headers:{
              //  Authorization: `Bearer ${token}`,
              "Content-Type" : "application/json"
            }
            })
          .then(response => {
            if (response.status !== 200) {
              throw Error(response.statusText);
            }
            return response.data;
          })
          .then(data => {
            dispatch(modelEarningsOneData(data));
            dispatch(modelEarningsFindOneCheckSuccess(true));
          })
          .catch((error) => {
            console.log(error,axiosLogin,)
            dispatch(modelEarningsFindOneCheckSuccess(false));
          });
      };
  };

  //find - get
  export function modelEarningsGetRequest(element) {
  return dispatch => {
    dispatch(modelEarningsGetCheckSuccess(false));
    const axiosLogin = axios.create();
      return axiosLogin({
        method: 'get',
        url: `model-earnings`,
        headers:{
          //  Authorization: `Bearer ${token}`,
          "Content-Type" : "application/json",
          data:element
        }
        })
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        return response.data;
      })
      .then(data => {
        // console.log(data)
        // console.log(element,"user data",data.data[0])
        // const propertyValues = Object.values(data.data);
        // const obj = {categoryID:element.id,categoryName:element.name,categoryImage:element.icon,subCategory:propertyValues}
        // a.push(obj)
        dispatch(modelEarningsSetOneData(data));
        dispatch(modelEarningsGetCheckSuccess(true));
      })
      .catch((error) => {
        console.log(error,axiosLogin)
        dispatch(modelEarningsGetCheckSuccess(false));
      });
  };
  };
  
  //update
  export function modelEarningsUpdateRequest(id) {
     
      return dispatch => {
          dispatch(modelEarningsUpdateCheckSuccess(false));
          const axiosLogin = axios.create();
          return axiosLogin({
              method: 'put',
              url: `model-earnings/${id}`,
              headers:{
              "Content-Type" : "application/json"}
              
              })
          .then(response => {
              if (response.status !== 200) {
              throw Error(response.statusText);
              }
              return response.data;
          })
          .then(data => {
              // console.log("data",data)
              dispatch(modelEarningsSetData(data));
              dispatch(modelEarningsUpdateCheckSuccess(true));
          })
          .catch((error) => {
              console.log(error,axiosLogin)
              dispatch(modelEarningsUpdateCheckSuccess(false));
              
          });
      };
  }
  
 

 