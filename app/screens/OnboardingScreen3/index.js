import React, {useState} from 'react';
import {View, ScrollView, KeyboardAvoidingView, Platform} from 'react-native';
import {BaseStyle, useTheme} from '@config';
import {
  Image,
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  TextInput,
} from '@components';
import styles from './styles';
import {userda as UserData} from '@data';
import {useTranslation} from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';


export default function OnboardingScreen3({navigation}) {
  const {colors} = useTheme();
  const dispatch = useDispatch();
  const {t} = useTranslation();
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  // const { mobile, password } = route.params

  const UserData = useSelector(state => state.accessTokenReducer.userData)
  const [firstName, setFirstName] = useState(UserData?.user?.firstName);
  const [lastName, setlastName] = useState(UserData?.user?.lastName);
  const [email, setEmail] = useState(UserData?.user?.email);
  const [username, setusername] = useState(UserData?.user?.username);

 const userId = UserData?.user?.id;
 const signupSuccess = useSelector(state => state.signupReducer.signupSuccess)

  const userToken = useSelector(state => state.accessTokenReducer.accessToken)
  const [image] = useState("");
  const [loading, setLoading] = useState(false);

  const success = async () => {
    if (!firstName) {
      alert('please enter firstName')
    }
    else if (!lastName) {
      alert('please enter lastName')
    }

    else {
      setLoading(true);
        await dispatch(signupRequest(  firstName, lastName, userId));
     // navigation.navigate('onboardingUserSecond')
      setLoading(false);

    }

  };

  if (signupSuccess) {
    console.log("signupSuccesssignupSuccess 123----------------------", userdata)
    // navigation.navigate('Home')
    navigation.navigate('onboardingUserThird')
  }




  return (
    <View style={{flex: 1}}>
      <Header
        title={t('edit_profile')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
        onPressRight={() => {}}
      />
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        edges={['right', 'left', 'bottom']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'android' ? 'height' : 'padding'}
          keyboardVerticalOffset={offsetKeyboard}
          style={{flex: 1}}>
          <ScrollView contentContainerStyle={styles.contain}>
            <View>
              <Image source={image} style={styles.thumb} />
            </View>
            <View style={styles.contentTitle}>
              <Text headline semibold>
                {t('account')}
              </Text>
            </View>
            <TextInput
              onChangeText={text => setFirstName(text)}
              placeholder={t('f_name')}
              value={firstName}
            />
            <View style={styles.contentTitle}>
              <Text headline semibold>
                {t('name')}
              </Text>
            </View>
            <TextInput
               onChangeText={text => setlastName(text)}
               placeholder={t('l_name')}
               value={lastName}
             />
            <View style={styles.contentTitle}>
              <Text headline semibold>
                {t('email')}
              </Text>
            </View>
            <TextInput
              onChangeText={text => setEmail(text)}
              placeholder={t('email')}
              value={email}
            />
            <View style={styles.contentTitle}>
              <Text headline semibold>
                {t('address')}
              </Text>
            </View>
            <TextInput
              onChangeText={text => setusername(text)}
              placeholder={'username'}
              value={username}
            />
          </ScrollView>
          <View style={{paddingVertical: 15, paddingHorizontal: 20}}>
            <Button
              loading={loading}
              full
              onPress={() => {
                success()
               
              }}>
              {t('confirm')}
            </Button>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
}
