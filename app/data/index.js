import {BookingHistoryData} from './bookingHistory';
import {LanguageData} from './language';
import {CouponsData} from './/coupons';
import {CurrencyData} from './currency';
import {PromotionData} from './promotion';
import {TourData} from './tour';
import {TourData1} from './tour1';
import {HotelData} from './hotel';
import {UserData} from './user';
import {MessagesData} from './messages';
import {NotificationData} from './notification';
import {PostData} from './post';
import {PackageData} from './package';
import {WorkProgressData} from './workprogress';
import {HelpBlockData} from './helpblock';
import {ReviewData} from './review';
import {CarData} from './car';
import {FlightBrandData} from './flightBrand';
import {FlightData} from './flight';
import {CruiseData} from './cruise';
import {BusData} from './bus';
import {EventListData} from './event';
import {EarningsData} from './EarningsData';
import { BestData } from "./best";
import { ProductsData } from './products';
import { PromotionData1 } from './Promotion1';

// Sample data for display on template
export {
  BusData,
  CruiseData,
  FlightData,
  BookingHistoryData,
  LanguageData,
  CouponsData,
  CurrencyData,
  PromotionData,
  TourData,
  HotelData,
  UserData,
  MessagesData,
  NotificationData,
  PostData,
  PackageData,
  WorkProgressData,
  HelpBlockData,
  ReviewData,
  CarData,
  FlightBrandData,
  EventListData,
  EarningsData,
  BestData,
  ProductsData,
  TourData1,
  PromotionData1,

};
