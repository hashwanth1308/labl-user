import React from 'react';
import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';

export default StyleSheet.create({
  contentTitle: {
    alignItems: 'center',
    width: '100%',
    height: 32,
    top:-10,
    justifyContent: 'flex-start',
  },
  contain: {
    alignItems: 'center',
    padding: 0,
  },
  textInput: {
    height: 46,
    backgroundColor: BaseColor.fieldColor,
    borderRadius: 5,
    padding: 10,
    width: '100%',
    color: BaseColor.grayColor,
  },
  thumb: {
    width: 130,
    height: 160,
    borderRadius: 10,
    // marginBottom: 20,
  },
  gender:{
    flexDirection:"row",
    justifyContent:"space-evenly",
    marginTop:20,
    borderColor:"#909090",
    borderRadius:5,
    borderWidth:1.3,
    height:60,
    width:300
  }
});
