import {
  ORDERS_POST_CHECK_SUCCESS,
  ORDERS_SET_COUNT,
  ORDERS_COUNT_CHECK_SUCCESS,
  ORDERS_DELETE_CHECK_SUCCESS,
  ORDERS_FINDONE_CHECK_SUCCESS,
  ORDERS_SET_ONE_DATA,
  ORDERS_GET_CHECK_SUCCESS,
  ORDERS_SET_DATA,
  ORDERS_UPDATE_CHECK_SUCCESS,
} from './actionTypes';


export function ordersPostCheckSuccess(bool = false) {
  return {
    type: ORDERS_POST_CHECK_SUCCESS,
    payload: bool,
  };
}

export function ordersSetCount(object) {
  return {
    type: ORDERS_SET_COUNT,
    payload: object,
  };
}
export function ordersCountCheckSuccess(bool = false) {
  return {
    type: ORDERS_COUNT_CHECK_SUCCESS,
    payload: bool,
  };
}

export function ordersDeleteCheckSuccess(bool = false) {
  return {
    type: ORDERS_DELETE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function ordersFindOneCheckSuccess(bool = false) {
  return {
    type: ORDERS_FINDONE_CHECK_SUCCESS,
    payload: bool,
  };
}

export function ordersSetOneData(object) {
  return {
    type: ORDERS_SET_ONE_DATA,
    payload: object,
  };
}
export function ordersGetCheckSuccess(bool = false) {
  return {
    type: ORDERS_GET_CHECK_SUCCESS,
    payload: bool,
  };
}

export function ordersSetData(object) {
  return {
    type: ORDERS_SET_DATA,
    payload: object,
  };
}

export function ordersUpdateCheckSuccess(bool = false) {
  return {
    type: ORDERS_UPDATE_CHECK_SUCCESS,
    payload: bool,
  };
}

